package com.blood.bloodbook.DI;
import com.blood.bloodbook.UI.Account;
import com.blood.bloodbook.UI.AddLostAndFound;
import com.blood.bloodbook.UI.AddRequest;
import com.blood.bloodbook.UI.AfgListOFBlood;
import com.blood.bloodbook.UI.BloodBank;
import com.blood.bloodbook.UI.BloodDonationRestTime;
import com.blood.bloodbook.UI.BloodFilter;
import com.blood.bloodbook.UI.BloodRequest.AcceptedRequest;
import com.blood.bloodbook.UI.BloodRequest.MyRequest;
import com.blood.bloodbook.UI.BloodRequest.ReceiveRequest;
import com.blood.bloodbook.UI.BloodSearchResult;
import com.blood.bloodbook.UI.Chat;
import com.blood.bloodbook.UI.DetailsOFBloodRequest;
import com.blood.bloodbook.UI.DonorUserDetails;
import com.blood.bloodbook.UI.EditProfile;
import com.blood.bloodbook.UI.EmailSignIn;
import com.blood.bloodbook.UI.EmailSignUp;
import com.blood.bloodbook.UI.ForgotPassword;
import com.blood.bloodbook.UI.HistoryFragment.AllHistory;
import com.blood.bloodbook.UI.HistoryFragment.MyHistory;
import com.blood.bloodbook.UI.HomeFragment.Dashboard;
import com.blood.bloodbook.UI.HomeFragment.Profile;
import com.blood.bloodbook.UI.HomeFragment.World;
import com.blood.bloodbook.UI.ListOFBloodBank;
import com.blood.bloodbook.UI.LoginPhone;
import com.blood.bloodbook.UI.LostAndFound;
import com.blood.bloodbook.UI.MessageHistory;
import com.blood.bloodbook.UI.Notification;
import com.blood.bloodbook.UI.OTP;
import com.blood.bloodbook.UI.OnBoardingScreen;
import com.blood.bloodbook.UI.RegisterAsADonor;
import com.blood.bloodbook.UI.RegisterAsFriend;
import com.blood.bloodbook.UI.RegisterDonationDate;
import com.blood.bloodbook.UI.RegisterTab.BloodDonorFriendRegister;
import com.blood.bloodbook.UI.RegisterTab.RegisterDonation;
import com.blood.bloodbook.UI.SelectBloodGroup;
import com.blood.bloodbook.UI.SetUpProfile;
import com.blood.bloodbook.UI.SignInUp;
import com.blood.bloodbook.UI.TelephonyManager;
import com.blood.bloodbook.UI.UpdateFindDonorLocation;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component
public interface WeenaComponent {

    public void InjectMessageHistory(MessageHistory messageHistory);
    public void InjectSignIn(Account signIn);
    public void InjectEditProfile(EditProfile editProfile);
    public void InjecetProfile(Profile profile);
    public void InjectAddRequest(AddRequest addRequest);
    public void InjectRegisterAsADonor(RegisterAsADonor registerAsADonor);
    public void InjectSearchResult(BloodSearchResult bloodSearchResult);
    public void InjectDonorUserDetails(DonorUserDetails donorUserDetails);
    public void InjectAfgListOfBlood(AfgListOFBlood afgListOFBlood);
    public void InjectRegisterAsFriend(RegisterAsFriend registerAsFriend);
    public void InjectEmailSignIn(EmailSignIn emailSignIn);
    public void InjectEmailSignUp(EmailSignUp emailSignUp);
    public void InjectResetPassword(ForgotPassword forgotPassword);
    public void InjectLoginPhone(LoginPhone loginPhone);
    public void InjectOTP(OTP otp);
    public void InjectSetUpProfile(SetUpProfile setUpProfile);
    public void InjectRegisterDonationDate(RegisterDonationDate registerDonationDate);
    public void InjectBloodFilter(BloodFilter bloodFilter);
    public void InjectChat(Chat chat);
    public void InjectNotification(Notification notification);
    public void InjectTelephonyManager(TelephonyManager telephonyManager);
    public void InjectSelectBloodGroup(SelectBloodGroup selectBloodGroup);
    public void InjectLostAndFound(LostAndFound lostAndFound);
    public void InjectBloodBank(BloodBank bloodBank);
    public void InjectListOFBloodBank(ListOFBloodBank listOFBloodBank);
    public void InjectBloodDonationRestTime(BloodDonationRestTime bloodDonationRestTime);
    public void InjectDetailsOFBloodRequest(DetailsOFBloodRequest detailsOFBloodRequest);
    public void InjectReceiveRequest(ReceiveRequest receiveRequest);


    public void InjectProfile(Profile profile);
    public void InjectUpdateFindDonorLocation(UpdateFindDonorLocation location);
    public void InjectWorld(World world);
    public void InjectAcceptedRequest(AcceptedRequest acceptedRequest);
    public void InjectAccount(Account account);
    public void InjectAddLostAndFound(AddLostAndFound addLostAndFound);
    public void InjectAllHistory(AllHistory allHistory);
    public void InjectBloodDonorFriendRegister(BloodDonorFriendRegister bloodDonorFriendRegister);
    public void InjectMyHistory(MyHistory myHistory);
    public void InjectMyRequest(MyRequest myRequest);
    public void InjectOnBoardingScreen(OnBoardingScreen onBoardingScreen);
    public void InjectRegisterDonation(RegisterDonation registerDonation);
    public void InjectSignInUp(SignInUp signInUp);
    public void InjectDashboard(Dashboard dashboard);
}
