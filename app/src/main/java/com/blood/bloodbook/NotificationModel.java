package com.blood.bloodbook;

import lombok.Data;

@Data
public class NotificationModel {

    private String Message, MessageType, ReceiverUID, SenderUID, Type, MessageTopic;
    private long Timestamp, DocumentKey;
}
