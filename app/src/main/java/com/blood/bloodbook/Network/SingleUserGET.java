package com.blood.bloodbook.Network;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.blood.bloodbook.Data.DataManager;
import com.blood.bloodbook.ProfileModel;
import com.blood.bloodbook.Utils.Toast;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;

public class SingleUserGET {

    private Application application;
    private MutableLiveData<ProfileModel> data;
    private CollectionReference UserRef;
    private FirebaseAuth Mauth;

    public SingleUserGET(Application application){
        this.application = application;
        UserRef = FirebaseFirestore.getInstance().collection(DataManager.User);
        Mauth = FirebaseAuth.getInstance();
    }

    public LiveData<ProfileModel> GetSingleProfile(String UID){
        data = new MutableLiveData<>();
        var FirebaseUser = Mauth.getCurrentUser();
        if(FirebaseUser != null){
            UserRef.document(UID).get().addOnCompleteListener(task -> {
                if(task.isSuccessful()){
                    data.setValue(task.getResult().toObject(ProfileModel.class));
                }else {
                    data.setValue(null);
                    Toast.Message(application, task.getException().getMessage());
                }
            }).addOnFailureListener(e -> {
                data.setValue(null);
                Toast.Message(application, e.getMessage());
            });
        }
        return data;
    }
}
