package com.blood.bloodbook.Network;

import android.app.Application;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.blood.bloodbook.Data.DataManager;
import com.blood.bloodbook.Utils.Toast;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;

public class DeleteRegisterAsDonor {

    private Application application;
    private MutableLiveData<Boolean> data;
    private FirebaseAuth Mauth;
    private CollectionReference DonorRef;

    public DeleteRegisterAsDonor(Application application){
        this.application = application;
        Mauth = FirebaseAuth.getInstance();
        DonorRef = FirebaseFirestore.getInstance().collection(DataManager.DonorUser);
    }

    public LiveData<Boolean> DeleteRegisterDonor(){
        data = new MutableLiveData<>();
        var FirebaseUser = Mauth.getCurrentUser();
        if(FirebaseUser != null){
            DonorRef.document(FirebaseUser.getUid())
                    .delete().addOnCompleteListener(task -> {
                        if(task.isSuccessful()){
                            data.setValue(true);
                            Toast.Message(application, "Delete success");
                        }else {
                            data.setValue(false);
                            Toast.Message(application, task.getException().getMessage());
                        }
                    }).addOnFailureListener(e -> {
                        data.setValue(true);
                        Toast.Message(application, e.getMessage());
                    });
        }
        return data;
    }
}
