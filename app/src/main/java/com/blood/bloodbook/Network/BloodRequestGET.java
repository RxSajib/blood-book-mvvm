package com.blood.bloodbook.Network;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.blood.bloodbook.Utils.Toast;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.blood.bloodbook.BloodRequestModel;
import com.blood.bloodbook.Data.DataManager;

import java.util.List;

public class BloodRequestGET {

    private Application application;
    private FirebaseAuth Mauth;
    private CollectionReference RequestRef;
    private MutableLiveData<List<BloodRequestModel>> data;

    public BloodRequestGET(Application application) {
        this.application = application;
        Mauth = FirebaseAuth.getInstance();
        RequestRef = FirebaseFirestore.getInstance().collection(DataManager.BloodRequest);
    }

    public LiveData<List<BloodRequestModel>> GetBloodRequest(int Limit) {
        var FirebaseUser = Mauth.getCurrentUser();
        data = new MutableLiveData<>();
        var q = RequestRef.orderBy(DataManager.Timestamp, Query.Direction.DESCENDING).limit(Limit);
        if (FirebaseUser != null) {
            q.addSnapshotListener((value, error) -> {
                if (error != null) {
                    data.setValue(null);
                    return;
                }
                if (!value.isEmpty()) {
                    for (var ds : value.getDocumentChanges()) {
                        if (ds.getType() == DocumentChange.Type.ADDED || ds.getType() == DocumentChange.Type.MODIFIED || ds.getType() == DocumentChange.Type.REMOVED) {
                            data.setValue(value.toObjects(BloodRequestModel.class));
                        }
                    }
                } else {
                    data.setValue(null);
                }
            });
        }
        return data;
    }
}
