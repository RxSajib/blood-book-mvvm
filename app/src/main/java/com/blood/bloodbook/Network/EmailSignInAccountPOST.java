package com.blood.bloodbook.Network;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.auth.FirebaseAuth;
import com.blood.bloodbook.Utils.Toast;

public class EmailSignInAccountPOST {

    private Application application;
    private FirebaseAuth Mauth;
    private MutableLiveData<Boolean> data;

    public EmailSignInAccountPOST(Application application){
        this.application = application;
        Mauth = FirebaseAuth.getInstance();
    }

    public LiveData<Boolean> SigninWithEmail(String Email, String Password){
        data = new MutableLiveData<>();

        Mauth.signInWithEmailAndPassword(Email, Password).addOnCompleteListener(task -> {
            if(task.isSuccessful()){
                data.setValue(true);

            }else {
                data.setValue(false);
                Toast.Message(application, task.getException().getMessage());
            }
        }).addOnFailureListener(e -> {
            Toast.Message(application, e.getMessage());
            data.setValue(false);
        });
        return data;
    }
}
