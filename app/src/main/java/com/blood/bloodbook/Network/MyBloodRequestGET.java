package com.blood.bloodbook.Network;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.FirebaseFirestore;
import com.blood.bloodbook.BloodRequestModel;
import com.blood.bloodbook.Data.DataManager;
import com.google.firebase.firestore.Query;

import java.util.List;

public class MyBloodRequestGET {

    private android.app.Application application;
    private MutableLiveData<List<BloodRequestModel>> data;
    private FirebaseAuth Mauth;
    private CollectionReference BloodRequestRef;

    public MyBloodRequestGET(Application application){
        this.application = application;
        Mauth = FirebaseAuth.getInstance();
        BloodRequestRef = FirebaseFirestore.getInstance().collection(DataManager.BloodRequest);
    }

    public LiveData<List<BloodRequestModel>> MyBloodRequest(int Limit){
        data = new MutableLiveData<>();
        var FirebaseUser = Mauth.getCurrentUser();
        if(FirebaseUser != null){
            var q = BloodRequestRef.whereEqualTo(DataManager.SenderUID, FirebaseUser.getUid()).orderBy(DataManager.Timestamp, Query.Direction.DESCENDING).limit(Limit);
            q.addSnapshotListener((value, error) -> {
                if(error != null){
                    data.setValue(null);
                    return;
                }
                if(!value.isEmpty()){
                    for(var ds : value.getDocumentChanges()){
                        if(ds.getType() == DocumentChange.Type.ADDED || ds.getType() == DocumentChange.Type.MODIFIED || ds.getType() == DocumentChange.Type.REMOVED){
                            data.setValue(value.toObjects(BloodRequestModel.class));
                        }
                    }
                }else {
                    data.setValue(null);
                }
            });
        }
        return data;
    }
}
