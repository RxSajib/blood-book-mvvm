package com.blood.bloodbook.Network;

import android.app.Application;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.blood.bloodbook.UI.Adapter.MessageAdapter;
import com.blood.bloodbook.UI.Chat;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.blood.bloodbook.Data.DataManager;
import com.blood.bloodbook.MessageModel;

import java.util.ArrayList;
import java.util.List;

public class ChatMessageGET {

    private Application application;
    private MutableLiveData<List<MessageModel>> data;
    private DatabaseReference MessageRef;
    private FirebaseAuth Mauth;
    private List<MessageModel> list = new ArrayList<>();

    public ChatMessageGET(Application application){
        this.application = application;
        MessageRef = FirebaseDatabase.getInstance().getReference().child(DataManager.Message);
        Mauth = FirebaseAuth.getInstance();

    }

    public LiveData<List<MessageModel>> GetMessage(String SenderUID, int Limit){
            list.clear();
        data = new MutableLiveData<>();
        var FirebaseUser = Mauth.getCurrentUser();
        if(FirebaseUser.getUid() != null){

            var a =  MessageRef.child(FirebaseUser.getUid()).child(SenderUID).child(DataManager.Message).limitToLast(Limit);

                    a.addChildEventListener(new ChildEventListener() {
                        @Override
                        public void onChildAdded(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

                            if(snapshot.exists()){
                                var model = snapshot.getValue(MessageModel.class);
                                list.add(model);
                                data.setValue(list);
                            }else {
                                data.setValue(null);
                            }
                        }

                        @Override
                        public void onChildChanged(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

                        }

                        @Override
                        public void onChildRemoved(@NonNull DataSnapshot snapshot) {
                            var message = list.indexOf(snapshot.getKey());
                          /* data.getValue().remove(1);
                            list.remove(1);*/
                      //      Toast.makeText(application, String.valueOf(message), Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onChildMoved(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError error) {

                        }
                    });
        }
        return data;
    }
}
