package com.blood.bloodbook.Network;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.blood.bloodbook.AllHistoryModel;
import com.blood.bloodbook.Utils.Toast;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.FirebaseFirestore;
import com.blood.bloodbook.Data.DataManager;
import com.blood.bloodbook.HistoryModel;
import com.google.firebase.firestore.Query;

import java.util.List;

public class AllHistoryGET {

    private Application application;
    private MutableLiveData<List<AllHistoryModel>> data;
    private CollectionReference RegisterDonor;
    private FirebaseAuth Mauth;
    private static final String TAG = "AllHistoryGET";

    public AllHistoryGET(Application application){
        this.application = application;
        Mauth = FirebaseAuth.getInstance();
        RegisterDonor = FirebaseFirestore.getInstance().collection(DataManager.RegisterDonation);
    }

    public LiveData<List<AllHistoryModel>> AllMyRegisterDonor(int Limit){
        data = new MutableLiveData<>();

        var FirebaseUser = Mauth.getCurrentUser();
        if(FirebaseUser != null){
            var q = RegisterDonor.limit(Limit).orderBy(DataManager.Timestamp, Query.Direction.DESCENDING);
            q.addSnapshotListener((value, error) -> {
                if(error != null){
                    data.setValue(null);
                    return;
                }
                if(!value.isEmpty()){
                    for(var ds : value.getDocumentChanges()){
                        if(ds.getType() == DocumentChange.Type.ADDED || ds.getType() == DocumentChange.Type.MODIFIED || ds.getType() == DocumentChange.Type.REMOVED){
                            data.setValue(value.toObjects(AllHistoryModel.class));
                            Log.d(TAG, "AllMyRegisterDonor: ");
                        }
                    }
                }else {
                    data.setValue(null);
                }
            });
        }
        return data;
    }
}
