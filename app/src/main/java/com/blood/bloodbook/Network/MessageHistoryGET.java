package com.blood.bloodbook.Network;

import android.app.Application;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.blood.bloodbook.Data.DataManager;
import com.blood.bloodbook.MessageHistoryModel;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.List;

public class MessageHistoryGET {

    private Application application;
    private MutableLiveData<List<MessageHistoryModel>> data;
    private FirebaseAuth Mauth;
    private CollectionReference MessageHistoryRef;

    public MessageHistoryGET(Application application){
        this.application = application;
        Mauth = FirebaseAuth.getInstance();
        MessageHistoryRef = FirebaseFirestore.getInstance().collection(DataManager.MessageHistory);
    }

    public LiveData<List<MessageHistoryModel>> GetMessageHistory(int Size){
        data = new MutableLiveData<>();
        var FirebaseUser = Mauth.getCurrentUser();
        if(FirebaseUser != null){
            var q =  MessageHistoryRef.document(FirebaseUser.getUid())
                    .collection(DataManager.User).limit(Size).orderBy(DataManager.Timestamp, Query.Direction.DESCENDING);

                    q.addSnapshotListener(new EventListener<QuerySnapshot>() {
                        @Override
                        public void onEvent(@Nullable QuerySnapshot value, @Nullable FirebaseFirestoreException error) {
                            if(error != null){
                                data.setValue(null);
                                return;
                            }
                            if(!value.isEmpty()){
                                for(var ds : value.getDocumentChanges()){
                                    if(ds.getType() == DocumentChange.Type.ADDED || ds.getType() == DocumentChange.Type.MODIFIED || ds.getType() == DocumentChange.Type.REMOVED){
                                        data.setValue(value.toObjects(MessageHistoryModel.class));
                                    }
                                }
                            }else {
                                data.setValue(null);
                            }
                        }
                    });
        }
        return data;
    }
}
