package com.blood.bloodbook.Network;

import android.app.Application;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.blood.bloodbook.Data.FcmResponse;
import com.blood.bloodbook.Network.FCM.FcmApi;
import com.blood.bloodbook.Network.FCM.FcmClint;
import com.blood.bloodbook.Data.NotifactionResponse;
import com.blood.bloodbook.Utils.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationPOST {

    private Application application;
    private MutableLiveData<Boolean> data;
    private FcmApi fcmApi;

    public NotificationPOST(Application application){
        this.application = application;
        fcmApi = new FcmClint().getRetrofit().create(FcmApi.class);
    }

    public LiveData<Boolean> send_message(NotifactionResponse response){
        data = new MutableLiveData<>();
        fcmApi.sendNotification(response).enqueue(new Callback<>() {
            @Override
            public void onResponse(Call<FcmResponse> call, Response<FcmResponse> response) {
                if(response.isSuccessful()){
                    data.setValue(true);
                }else {
                    data.setValue(false);
                    Toast.Message(application, "Error sending message "+response.message());
                }
            }

            @Override
            public void onFailure(Call<FcmResponse> call, Throwable t) {
                data.setValue(false);
                Toast.Message(application, "Error sending message "+t.getMessage());
            }
        });


        return data;
    }
}
