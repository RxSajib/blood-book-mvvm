package com.blood.bloodbook.Network;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.blood.bloodbook.BloodRequestModel;
import com.blood.bloodbook.Data.DataManager;
import com.blood.bloodbook.Utils.Toast;

import java.util.HashMap;

public class MyBloodRequestPOST {

    private Application application;
    private MutableLiveData<Boolean> data;
    private CollectionReference MyBloodRequestRef;
    private FirebaseAuth Mauth;

    public MyBloodRequestPOST(Application application){
        this.application = application;
        Mauth = FirebaseAuth.getInstance();
        MyBloodRequestRef = FirebaseFirestore.getInstance().collection(DataManager.MyBloodRequest);
    }

    public LiveData<Boolean> MyBloodRequest(BloodRequestModel bloodRequestModel, String Quantity, long DocumentID, String BloodRequestID){
        data = new MutableLiveData<>();
        var FirebaseUser = Mauth.getCurrentUser();
        if(FirebaseUser != null){
            var TimesTamp = System.currentTimeMillis();
            var map  = new HashMap<String, Object>();
            map.put(DataManager.AcceptedQuantity, bloodRequestModel.getAcceptedQuantity());
            map.put(DataManager.Age, bloodRequestModel.getAge());
            map.put(DataManager.AttendantContactNumber, bloodRequestModel.getAttendantContactNumber());
            map.put(DataManager.BloodCondition, bloodRequestModel.getBloodCondition());
            map.put(DataManager.BloodGroup, bloodRequestModel.getBloodGroup());
            map.put(DataManager.BloodNeedFor, bloodRequestModel.getBloodNeedFor());
            map.put(DataManager.BloodRequestTimestamp, TimesTamp);
            map.put(DataManager.BloodRequiredDate, bloodRequestModel.getBloodRequiredDate());
            map.put(DataManager.BloodRequiredTime, bloodRequestModel.getBloodRequiredTime());
            map.put(DataManager.Country, bloodRequestModel.getCountry());
            map.put(DataManager.District, bloodRequestModel.getDistrict());
            map.put(DataManager.DocumentKey, TimesTamp);
            map.put(DataManager.DocumentID, DocumentID);
            map.put(DataManager.ExchangePossibility, bloodRequestModel.getExchangePossibility());
            map.put(DataManager.HospitalName, bloodRequestModel.getHospitalName());
            map.put(DataManager.NumberOFQuantityPints, Quantity);
            map.put(DataManager.PatientName, bloodRequestModel.getPatientName());
            map.put(DataManager.Province, bloodRequestModel.getProvince());
            map.put(DataManager.SenderUID, bloodRequestModel.getSenderUID());
            map.put(DataManager.Status, DataManager.Accept);
            map.put(DataManager.Timestamp, TimesTamp);
            map.put(DataManager.TransportAvailability, bloodRequestModel.getTransportAvailability());
            map.put(DataManager.TypeOFHBLevel, bloodRequestModel.getTypeOfHbLevel());
            map.put(DataManager.TypeOfPatientsCondition, bloodRequestModel.getTypeOfPatientsCondition());
            map.put(DataManager.BloodRequestID, BloodRequestID);

            MyBloodRequestRef.document(String.valueOf(TimesTamp)).set(map)
                    .addOnCompleteListener(task -> {
                        if(task.isSuccessful()){
                            data.setValue(true);
                        }else {
                            Toast.Message(application, task.getException().getMessage());
                            data.setValue(false);
                        }
                    }).addOnFailureListener(e -> {
                        data.setValue(false);
                        Toast.Message(application, e.getMessage());
                    });
        }
        return data;
    }

}
