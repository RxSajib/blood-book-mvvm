package com.blood.bloodbook.Network;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.blood.bloodbook.Data.DataManager;
import com.blood.bloodbook.Utils.Toast;

import java.util.HashMap;

public class ProfileUpdatePOST {

    private Application application;
    private MutableLiveData<Boolean> data;
    private CollectionReference UserRef;
    private FirebaseAuth Mauth;

    public ProfileUpdatePOST(Application application){
        this.application = application;
        UserRef = FirebaseFirestore.getInstance().collection(DataManager.User);
        Mauth = FirebaseAuth.getInstance();
    }

    public LiveData<Boolean> UpdateProfile(String FirstName, String MiddleName, String SureName, String FatherName){
        data = new MutableLiveData<>();
        var FirebaseUser = Mauth.getCurrentUser();
        if(FirebaseUser != null){
            var map = new HashMap<String, Object>();
            map.put(DataManager.FirstName, FirstName);
            map.put(DataManager.MiddleName, MiddleName);
            map.put(DataManager.Surname, SureName);
            map.put(DataManager.FatherName, FatherName);

            UserRef.document(FirebaseUser.getUid()).update(map).addOnCompleteListener(task -> {
                if(task.isSuccessful()){
                    data.setValue(true);
                    Toast.Message(application, "Update success");
                }else {
                    data.setValue(false);
                    Toast.Message(application, task.getException().getMessage());
                }
            }).addOnFailureListener(e -> {
                data.setValue(false);
                Toast.Message(application, e.getMessage());
            });
        }
        return data;
    }
}
