package com.blood.bloodbook.Network;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.auth.FirebaseAuth;
import com.blood.bloodbook.Utils.Toast;

public class ResetPasswordPOST {

    private Application application;
    private MutableLiveData<Boolean> data;
    private FirebaseAuth Mauth;

    public ResetPasswordPOST(Application application) {
        this.application = application;
        Mauth = FirebaseAuth.getInstance();
    }

    public LiveData<Boolean> ResetPassword(String EmailAddress) {
        data = new MutableLiveData<>();

        Mauth.sendPasswordResetEmail(EmailAddress).addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                data.setValue(true);
                Toast.Message(application, "A reset link will be sent to your registered email address");
            } else {
                data.setValue(false);
                Toast.Message(application, task.getException().getMessage());
            }
        }).addOnFailureListener(e -> {
            data.setValue(false);
            Toast.Message(application, e.getMessage());
        });
        return data;
    }
}
