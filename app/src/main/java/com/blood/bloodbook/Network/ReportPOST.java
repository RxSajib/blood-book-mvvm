package com.blood.bloodbook.Network;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.blood.bloodbook.Data.DataManager;
import com.blood.bloodbook.R;
import com.blood.bloodbook.Utils.Toast;
import java.util.HashMap;

public class ReportPOST {

    private Application application;
    private MutableLiveData<Boolean> data;
    private FirebaseAuth Mauth;
    private CollectionReference ReportRef;

    public ReportPOST(android.app.Application application) {
        this.application = application;
        Mauth = FirebaseAuth.getInstance();
        ReportRef = FirebaseFirestore.getInstance().collection(DataManager.Report);
    }

    public LiveData<Boolean> SendRepost(String ReceiverUID, String Message) {
        data = new MutableLiveData<>();
        var FirebaseUser = Mauth.getCurrentUser();
        if (FirebaseUser != null) {
            var map = new HashMap<String, Object>();

            var Timestamp = System.currentTimeMillis();
            map.put(DataManager.Message, Message);
            map.put(DataManager.SenderUID, FirebaseUser.getUid());
            map.put(DataManager.ReceiverUID, ReceiverUID);
            map.put(DataManager.Timestamp, Timestamp);
            map.put(DataManager.DocumentKey, Timestamp);

            ReportRef.document(String.valueOf(Timestamp)).set(map).addOnCompleteListener(task -> {
                if (task.isSuccessful()) {
                    data.setValue(true);
                    Toast.Message(application, String.valueOf(R.string.Success));
                } else {
                    Toast.Message(application, task.getException().getMessage());
                    data.setValue(false);
                }
            }).addOnFailureListener(e -> {
                data.setValue(false);
                Toast.Message(application, e.getMessage());
            });
        }
        return data;
    }
}
