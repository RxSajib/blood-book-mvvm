package com.blood.bloodbook.Network;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.blood.bloodbook.Data.DataManager;

public class ProfileExistsGET {

    private Application application;
    private MutableLiveData<Boolean> data;
    private CollectionReference UserRef;
    private FirebaseAuth Mauth;

    public ProfileExistsGET(Application application){
        this.application = application;
        UserRef = FirebaseFirestore.getInstance().collection(DataManager.User);
        Mauth = FirebaseAuth.getInstance();
    }

    public LiveData<Boolean> UserExists(){
        data = new MutableLiveData<>();
        var FirebaseUser = Mauth.getCurrentUser();
        if(FirebaseUser != null){
            UserRef.document(FirebaseUser.getUid()).addSnapshotListener((value, error) -> {
                if (error != null) {
                    data.setValue(false);
                    return;
                }
                if (value.exists()) {
                    data.setValue(true);
                } else {
                    data.setValue(false);
                }
            });
        }else {
            data.setValue(null);
        }
        return data;
    }
}
