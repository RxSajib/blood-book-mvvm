package com.blood.bloodbook.Network;

import android.app.Application;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.blood.bloodbook.Data.DataManager;
import com.blood.bloodbook.DonorModel;


public class DonorSingleUserGET {

    private Application application;
    private MutableLiveData<DonorModel> data;
    private FirebaseAuth Mauth;
    private CollectionReference DonorUserRef;

    public DonorSingleUserGET(Application application){
        this.application = application;
        Mauth = FirebaseAuth.getInstance();
        DonorUserRef = FirebaseFirestore.getInstance().collection(DataManager.DonorUser);
    }

    public LiveData<DonorModel> GetSingleDonorUser(String UID){
        data = new MutableLiveData<>();
        var FirebaseUser = Mauth.getCurrentUser();
        if(FirebaseUser != null){
            DonorUserRef.document(UID).addSnapshotListener((value, error) -> {
               if(error != null){
                   data.setValue(null);
                   return;
               }
               if(value.exists()){
                   data.setValue(value.toObject(DonorModel.class));
               }else {
                   data.setValue(null);
               }
            });
        }
        return data;
    }
}
