package com.blood.bloodbook.Network;
import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.FirebaseFirestore;
import com.blood.bloodbook.BloodBankModel;
import com.blood.bloodbook.Data.DataManager;

import java.util.List;

public class BloodStorageSearchByHospitalGET {

    private Application application;
    private FirebaseAuth Mauth;
    private CollectionReference BloodStoreRef;
    private MutableLiveData<List<BloodBankModel>> data;

    public BloodStorageSearchByHospitalGET(Application application){
        this.application = application;
        Mauth = FirebaseAuth.getInstance();
        BloodStoreRef = FirebaseFirestore.getInstance().collection(DataManager.BloodStorage);
    }

    public LiveData<List<BloodBankModel>> GetBloodStore(String UID, String HospitalName){
        data = new MutableLiveData<>();
        var  q = BloodStoreRef.whereEqualTo(DataManager.UID, UID).whereEqualTo(DataManager.HospitalName, HospitalName);
        q.addSnapshotListener((value, error) -> {
           if(error != null){
               data.setValue(null);
               return;
           }
           if(!value.isEmpty()){
               for(var ds : value.getDocumentChanges()){
                   if(ds.getType() == DocumentChange.Type.ADDED || ds.getType() == DocumentChange.Type.MODIFIED || ds.getType() == DocumentChange.Type.REMOVED){
                       data.setValue(value.toObjects(BloodBankModel.class));
                   }
               }
           }else {
               data.setValue(null);
           }
        });
        return data;
    }
    
}
