package com.blood.bloodbook.Network;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.blood.bloodbook.Data.DataManager;
import com.blood.bloodbook.Utils.BloodRequestDateToTimestamp;
import com.blood.bloodbook.Utils.Toast;

import java.util.HashMap;

public class BloodRequestPOST {

    private Application application;
    private MutableLiveData<Boolean> data;
    private CollectionReference BloodRequestRef;
    private FirebaseAuth Mauth;

    public BloodRequestPOST(Application application){
        this.application = application;
        BloodRequestRef = FirebaseFirestore.getInstance().collection(DataManager.BloodRequest);
        Mauth = FirebaseAuth.getInstance();
    }

    public LiveData<Boolean> BloodRequest(String PatientName, String Age, String BloodGroup, String BloodCondition, String CountryName, String Province, String District, String BloodNeededFor, String NoOfQuantityPints, String Time, String Date, String AttendantContactNumber, String HospitalName, String ExchangePossibility, String  TransportAvailability, String TyOfHBLevel, String TypeOFPatientsCondition){
        data = new MutableLiveData<>();
        var FirebaseUser = Mauth.getCurrentUser();
        if(FirebaseUser != null){

            var Timestamp = System.currentTimeMillis();


            var map = new HashMap<String, Object>();
            map.put(DataManager.PatientName, PatientName);
            map.put(DataManager.Age, Age);
            map.put(DataManager.BloodGroup, BloodGroup);
            map.put(DataManager.BloodCondition, BloodCondition);
            map.put(DataManager.Country, CountryName);
            map.put(DataManager.Province, Province);
            map.put(DataManager.District, District);
            map.put(DataManager.BloodNeedFor, BloodNeededFor);
            map.put(DataManager.NumberOFQuantityPints, NoOfQuantityPints);
            map.put(DataManager.BloodRequiredTime, Time);
            map.put(DataManager.BloodRequiredDate, Date);
            map.put(DataManager.AttendantContactNumber, AttendantContactNumber);
            map.put(DataManager.HospitalName, HospitalName);
            map.put(DataManager.BloodRequestTimestamp, BloodRequestDateToTimestamp.GetBloodRequestDateToTimestamp(Date, Time).getTime());
            map.put(DataManager.ExchangePossibility, ExchangePossibility);
            map.put(DataManager.TransportAvailability, TransportAvailability);
            map.put(DataManager.TypeOFHBLevel, TyOfHBLevel);
            map.put(DataManager.TypeOfPatientsCondition, TypeOFPatientsCondition);
            map.put(DataManager.SenderUID, FirebaseUser.getUid());
            map.put(DataManager.Status, DataManager.Request);
            map.put(DataManager.Timestamp, Timestamp);
            map.put(DataManager.DocumentKey, Timestamp);
            map.put(DataManager.AcceptedQuantity, 0);

            BloodRequestRef.document(String.valueOf(Timestamp)).set(map)
                    .addOnCompleteListener(task -> {
                        if(task.isSuccessful()){
                            Toast.Message(application, "Send success");
                            data.setValue(true);
                        }else {
                            data.setValue(false);
                            Toast.Message(application, task.getException().getMessage());
                        }
                    }).addOnFailureListener(e -> {
                        Toast.Message(application, e.getMessage());
                        data.setValue(false);
                    });
        }
        return data;
    }
}
