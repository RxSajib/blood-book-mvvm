package com.blood.bloodbook.Network.FCM;

import com.blood.bloodbook.Data.DataManager;
import com.blood.bloodbook.Data.FcmResponse;
import com.blood.bloodbook.Data.NotifactionResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface FcmApi {

    @Headers(
            {       DataManager.MSG_CONTENT_TYPE+": " + DataManager.MSG_CONTENT_TYPE_VAL,
                    DataManager.MSG_AUTHORIZATION+": " + DataManager.AuthorizationKey
            }
    )

    @POST("fcm/send")
    Call<FcmResponse> sendNotification(
            @Body NotifactionResponse response
    );

}
