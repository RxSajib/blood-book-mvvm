package com.blood.bloodbook.Network;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.blood.bloodbook.AcceptBloodRequestModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.blood.bloodbook.Data.DataManager;
import com.blood.bloodbook.R;
import com.blood.bloodbook.Utils.Toast;

import java.util.HashMap;

public class BloodRequestQuantityUpdate {

    private Application application;
    private MutableLiveData<Boolean> data;
    private FirebaseAuth MAuth;
    private CollectionReference BloodRequestRef;

    public BloodRequestQuantityUpdate(android.app.Application application){
        this.application = application;
        MAuth = FirebaseAuth.getInstance();
        BloodRequestRef = FirebaseFirestore.getInstance().collection(DataManager.BloodRequest);
    }

    public LiveData<Boolean> UpdateBloodRequestQuantity(String BloodQuantity, String DocumentID, AcceptBloodRequestModel acceptBloodRequestModel){
        data = new MutableLiveData<>();
        var FirebaseUser = MAuth.getCurrentUser();
        if(FirebaseUser != null){


            BloodRequestRef.document(String.valueOf(acceptBloodRequestModel.getDocumentID())).get().addOnCompleteListener(task -> {
                if(task.isSuccessful()){
                    if(task.getResult().exists()){
                        var map = new HashMap<String, Object>();
                        map.put(DataManager.NumberOFQuantityPints, String.valueOf(Integer.valueOf(BloodQuantity)+Integer.valueOf(acceptBloodRequestModel.getNumberOFQuantityPints())));
                        BloodRequestRef.document(String.valueOf(acceptBloodRequestModel.getDocumentID())).update(map)
                                .addOnCompleteListener(updatetask -> {
                                    if(updatetask.isSuccessful()){
                                        data.setValue(true);
                                        Toast.Message(application, application.getResources().getString(R.string.Success));
                                    }else {
                                        data.setValue(false);
                                        Toast.Message(application, updatetask.getException().getMessage());
                                    }
                                }).addOnFailureListener(e -> {
                                    data.setValue(false);
                                    Toast.Message(application, e.getMessage());
                                });
                    }else {

                        var map = new HashMap<String, Object>();
                        map.put(DataManager.AcceptedQuantity, 0);
                        map.put(DataManager.Age, acceptBloodRequestModel.getAge());
                        map.put(DataManager.AttendantContactNumber, acceptBloodRequestModel.getAttendantContactNumber());
                        map.put(DataManager.BloodCondition, acceptBloodRequestModel.getBloodCondition());
                        map.put(DataManager.BloodGroup, acceptBloodRequestModel.getBloodGroup());
                        map.put(DataManager.BloodNeedFor, acceptBloodRequestModel.getBloodNeedFor());
                        map.put(DataManager.BloodRequestTimestamp, acceptBloodRequestModel.getBloodRequestTimestamp());
                        map.put(DataManager.BloodRequiredDate, acceptBloodRequestModel.getBloodRequiredDate());
                        map.put(DataManager.BloodRequiredTime, acceptBloodRequestModel.getBloodRequiredTime());
                        map.put(DataManager.Country, acceptBloodRequestModel.getCountry());
                        map.put(DataManager.District, acceptBloodRequestModel.getDistrict());
                        map.put(DataManager.DocumentKey, acceptBloodRequestModel.getDocumentKey());
                        map.put(DataManager.DocumentID, acceptBloodRequestModel.getDocumentID());
                        map.put(DataManager.ExchangePossibility, acceptBloodRequestModel.getExchangePossibility());
                        map.put(DataManager.HospitalName, acceptBloodRequestModel.getHospitalName());
                        map.put(DataManager.NumberOFQuantityPints, acceptBloodRequestModel.getNumberOFQuantityPints());
                        map.put(DataManager.PatientName, acceptBloodRequestModel.getPatientName());
                        map.put(DataManager.Province, acceptBloodRequestModel.getProvince());
                        map.put(DataManager.SenderUID, acceptBloodRequestModel.getSenderUID());
                        map.put(DataManager.Status, acceptBloodRequestModel.getStatus());
                        map.put(DataManager.Timestamp, acceptBloodRequestModel.getTimestamp());
                        map.put(DataManager.TransportAvailability, acceptBloodRequestModel.getTransportAvailability());
                        map.put(DataManager.TypeOFHBLevel, acceptBloodRequestModel.getTypeOfHbLevel());
                        map.put(DataManager.TypeOfPatientsCondition, acceptBloodRequestModel.getTypeOfPatientsCondition());


                        BloodRequestRef.document(String.valueOf(acceptBloodRequestModel.getDocumentID())).set(map).addOnCompleteListener(task1 -> {
                            if(task1.isSuccessful()){
                                data.setValue(true);
                                Toast.Message(application, application.getResources().getString(R.string.Success));
                            }else {
                                data.setValue(false);
                                Toast.Message(application, task1.getException().getMessage());
                            }

                        }).addOnFailureListener(e -> {
                            data.setValue(false);
                            Toast.Message(application, e.getMessage());
                        });
                    }
                }else {
                    data.setValue(false);
                    Toast.Message(application, task.getException().getMessage());
                }
            }).addOnFailureListener(e -> {
                data.setValue(false);
                Toast.Message(application, e.getMessage());
            });


        }
        return data;
    }
}
