package com.blood.bloodbook.Network;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.FirebaseFirestore;
import com.blood.bloodbook.Data.DataManager;

public class RegisterDonorSizeGET {

    private Application application;
    private FirebaseAuth Mauth;
    private CollectionReference DonorRef;
    private MutableLiveData<Integer> data;

    public RegisterDonorSizeGET(Application application){
        this.application = application;
        Mauth = FirebaseAuth.getInstance();
        DonorRef = FirebaseFirestore.getInstance().collection(DataManager.DonorUser);
    }

    public LiveData<Integer>  RegisterAsDonorSize(){
        data = new MutableLiveData<>();
        var FirebaseUser = Mauth.getCurrentUser();
        if(FirebaseUser != null){
            var q = DonorRef.whereEqualTo(DataManager.Type, DataManager.RegisterAsDonor);
            q.addSnapshotListener((value, error) -> {
                if(error != null){
                    data.setValue(null);
                    return;
                }
                if(!value.isEmpty()){
                    for (var ds : value.getDocumentChanges()){
                        if(ds.getType() == DocumentChange.Type.ADDED || ds.getType() == DocumentChange.Type.MODIFIED || ds.getType() == DocumentChange.Type.REMOVED){
                            data.setValue(value.size());
                        }
                    }
                }else {
                    data.setValue(null);
                }
            });
        }
        return data;
    }
}
