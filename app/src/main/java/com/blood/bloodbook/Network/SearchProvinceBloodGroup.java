package com.blood.bloodbook.Network;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.FirebaseFirestore;
import com.blood.bloodbook.Data.DataManager;
import com.blood.bloodbook.DonorModel;

import java.util.List;

public class SearchProvinceBloodGroup {

    private Application application;
    private MutableLiveData<List<DonorModel>> data;
    private CollectionReference DonorUser;
    private FirebaseAuth Mauth;

    public SearchProvinceBloodGroup(Application application){
        this.application = application;
        DonorUser = FirebaseFirestore.getInstance().collection(DataManager.DonorUser);
        Mauth = FirebaseAuth.getInstance();
    }

    public LiveData<List<DonorModel>> SearchProvinceBlood(String BloodName, String Country, String Province){
        data = new MutableLiveData<>();
        var FirebaseUser = Mauth.getCurrentUser();
        if(FirebaseUser != null){
            var q = DonorUser.whereEqualTo(DataManager.BloodGroup, BloodName).whereEqualTo(DataManager.Country, Country).whereEqualTo(DataManager.ShowToOther, true)
                    .whereEqualTo(DataManager.Province, Province);
            q.addSnapshotListener((value, error) -> {
                if(error != null){
                    data.setValue(null);
                    return;
                }
                if(!value.isEmpty()){
                    for(var ds : value.getDocumentChanges()){
                        if(ds.getType() == DocumentChange.Type.ADDED || ds.getType() == DocumentChange.Type.MODIFIED || ds.getType() == DocumentChange.Type.REMOVED){
                            data.setValue(value.toObjects(DonorModel.class));
                        }
                    }
                }else {
                    data.setValue(null);
                }
            });
        }
        return data;
    }


}
