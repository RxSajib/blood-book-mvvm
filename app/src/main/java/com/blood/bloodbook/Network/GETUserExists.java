package com.blood.bloodbook.Network;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.auth.FirebaseAuth;

public class GETUserExists {

    private Application application;
    private MutableLiveData<Boolean> data;
    private FirebaseAuth Mauth;

    public GETUserExists(Application application){
        this.application = application;
        Mauth = FirebaseAuth.getInstance();
    }

    public LiveData<Boolean> CheckUserExists(){
        data = new MutableLiveData<>();
        var FirebaseUser = Mauth.getCurrentUser();

        if(FirebaseUser != null){
            data.setValue(true);
        }else {
            data.setValue(false);
        }
        return data;
    }

}
