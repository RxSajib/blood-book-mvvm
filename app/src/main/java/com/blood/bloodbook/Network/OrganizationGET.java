package com.blood.bloodbook.Network;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.FirebaseFirestore;
import com.blood.bloodbook.Data.DataManager;
import com.blood.bloodbook.OrganizationModel;
import java.util.List;

public class OrganizationGET {

    private Application application;
    private MutableLiveData<List<OrganizationModel>> data;
    private CollectionReference OrganizationRef;
    private FirebaseAuth Mauth;

    public OrganizationGET(Application application){
        this.application = application;
        OrganizationRef = FirebaseFirestore.getInstance().collection(DataManager.Organization);
        Mauth = FirebaseAuth.getInstance();
    }

    public LiveData<List<OrganizationModel>> GetOrganization(){
        data = new MutableLiveData<>();
        var FirebaseUser = Mauth.getCurrentUser();
        if(FirebaseUser != null){
            OrganizationRef.addSnapshotListener((value, error) -> {
                if(error != null){
                    data.setValue(null);
                    return;
                }
                if(!value.isEmpty()){
                    for(var ds : value.getDocumentChanges()){
                        if(ds.getType() == DocumentChange.Type.ADDED || ds.getType() == DocumentChange.Type.REMOVED || ds.getType() == DocumentChange.Type.MODIFIED){
                            data.setValue(value.toObjects(OrganizationModel.class));
                        }
                    }
                }else {
                    data.setValue(null);
                }
            });
        }
        return data;
    }
}
