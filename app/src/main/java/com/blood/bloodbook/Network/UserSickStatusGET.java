package com.blood.bloodbook.Network;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.blood.bloodbook.Data.DataManager;
import com.blood.bloodbook.ProfileModel;
import com.blood.bloodbook.Utils.Toast;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

public class UserSickStatusGET {

    private Application application;
    private MutableLiveData<ProfileModel> data;
    private FirebaseAuth Mauth;
    private CollectionReference UserRef;

    public UserSickStatusGET(Application application){
        this.application = application;
        Mauth = FirebaseAuth.getInstance();
        UserRef = FirebaseFirestore.getInstance().collection(DataManager.User);
    }

    public LiveData<ProfileModel> GetUserSickStatus(){
        data = new MutableLiveData<>();
        var FirebaseUser = Mauth.getCurrentUser();
        if(FirebaseUser != null){
            UserRef.document(FirebaseUser.getUid()).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                    if(task.isSuccessful()){
                        data.setValue(task.getResult().toObject(ProfileModel.class));
                    }else {
                        data.setValue(null);
                        Toast.Message(application, task.getException().getMessage());
                    }
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    data.setValue(null);
                    Toast.Message(application, e.getMessage());
                }
            });
        }
        return data;
    }
}
