package com.blood.bloodbook.Network;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.blood.bloodbook.Data.DataManager;
import com.blood.bloodbook.VideoAdsModel;

public class VideoAdsGET {

    private Application application;
    private FirebaseAuth Mauth;
    private CollectionReference VideoAdsRef;
    private MutableLiveData<VideoAdsModel> data;

    public VideoAdsGET(Application application){
        this.application = application;
        Mauth = FirebaseAuth.getInstance();
        VideoAdsRef = FirebaseFirestore.getInstance().collection(DataManager.VideoAds);
    }

    public LiveData<VideoAdsModel> GetVideoAds(){
        data = new MutableLiveData<>();
        var FirebaseUser = Mauth.getCurrentUser();
        if(FirebaseUser != null){
            VideoAdsRef.document(DataManager.VideoAds).addSnapshotListener((value, error) -> {
               if(error != null){
                   data.setValue(null);
                   return;
               }
               if(value.exists()){
                   data.setValue(value.toObject(VideoAdsModel.class));
               }else {
                   data.setValue(null);
               }
            });
        }
        return data;
    }
}
