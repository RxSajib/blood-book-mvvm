package com.blood.bloodbook.Network;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.FirebaseFirestore;
import com.blood.bloodbook.Data.DataManager;
import com.blood.bloodbook.LostAndFoundModel;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

public class LostAndFoundGET {

    private Application application;
    private MutableLiveData<List<LostAndFoundModel>> data;
    private FirebaseAuth Mauth;
    private CollectionReference LostAndFoundRef;
    private List<LostAndFoundModel> list = new ArrayList<>();

    public LostAndFoundGET(Application application){
        this.application = application;
        Mauth = FirebaseAuth.getInstance();
        LostAndFoundRef = FirebaseFirestore.getInstance().collection(DataManager.LostAndFound);
    }

    public LiveData<List<LostAndFoundModel>> GetLostAndFound(int Limit){
        data = new MutableLiveData<>();
        var FirebaseUser = Mauth.getCurrentUser();
        if(FirebaseUser != null){
            var q = LostAndFoundRef.orderBy(DataManager.Timestamp, Query.Direction.DESCENDING).limit(Limit);
            q.addSnapshotListener((value, error) -> {
                if(error != null){
                    data.setValue(null);
                    return;
                }
                if(!value.isEmpty()){
                    for(var ds : value.getDocumentChanges()){
                        if(ds.getType() == DocumentChange.Type.ADDED || ds.getType() == DocumentChange.Type.MODIFIED || ds.getType() == DocumentChange.Type.REMOVED){
                            data.setValue(value.toObjects(LostAndFoundModel.class));
                        }
                    }
                }else {
                    data.setValue(null);
                }
            });
        }
        return data;
    }
}
