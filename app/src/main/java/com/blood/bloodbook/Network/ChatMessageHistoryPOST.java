package com.blood.bloodbook.Network;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.blood.bloodbook.Data.DataManager;
import com.blood.bloodbook.Utils.Toast;

import java.util.HashMap;

public class ChatMessageHistoryPOST {

    private Application application;
    private MutableLiveData<Boolean> data;
    private CollectionReference NotificationRef;
    private FirebaseAuth Mauth;

    public ChatMessageHistoryPOST(Application application){
        this.application = application;
        NotificationRef = FirebaseFirestore.getInstance().collection(DataManager.Notification);
        Mauth = FirebaseAuth.getInstance();
    }

    public LiveData<Boolean> SendChatMessageNotification(String SenderUID, String Message, String Type, String MessageType){
        data = new MutableLiveData<>();

        var FirebaseUser = Mauth.getCurrentUser();
        if(FirebaseUser != null){
            var timestamp = System.currentTimeMillis();
            var map = new HashMap<String, Object>();
            map.put(DataManager.SenderUID, SenderUID);
            map.put(DataManager.Message, Message);
            map.put(DataManager.Type, Type);
            map.put(DataManager.MessageType, MessageType);
            map.put(DataManager.Timestamp, timestamp);
            map.put(DataManager.ReceiverUID, FirebaseUser.getUid());

            NotificationRef.document(String.valueOf(FirebaseUser.getUid()))
                    .set(map).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if(task.isSuccessful()){
                                data.setValue(true);
                            }else {
                                data.setValue(false);
                                Toast.Message(application, task.getException().getMessage());
                            }
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            data.setValue(false);
                            Toast.Message(application, e.getMessage());
                        }
                    });
        }
        return data;
    }
}
