package com.blood.bloodbook.Network;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.blood.bloodbook.Data.DataManager;
import com.blood.bloodbook.Utils.Toast;

import java.util.HashMap;

public class RegisterDonationDatePOST {

    private Application application;
    private MutableLiveData<Boolean> data;
    private CollectionReference RegisterDonationRef;
    private FirebaseAuth Mauth;


    public RegisterDonationDatePOST(Application application){
        this.application = application;
        RegisterDonationRef = FirebaseFirestore.getInstance().collection(DataManager.RegisterDonation);
        Mauth = FirebaseAuth.getInstance();
    }

    public LiveData<Boolean> UploadRegisterDonation(String DonationDate, String QuantityCC, String Country, String Province, String District, String HospitalName, String ImageUri){
        data = new MutableLiveData<>();

        var FirebaseUser = Mauth.getCurrentUser();
        if(FirebaseUser != null){

            long Timestamp = System.currentTimeMillis();
            var map = new HashMap<String, Object>();
            map.put(DataManager.BloodDonationDate, DonationDate);
            map.put(DataManager.QuantityOfBlood, QuantityCC);
            map.put(DataManager.Country, Country);
            map.put(DataManager.Province, Province);
            map.put(DataManager.District, District);
            map.put(DataManager.HospitalName, HospitalName);
            map.put(DataManager.RegisterDonationBloodImage, ImageUri);
            map.put(DataManager.SenderUID, FirebaseUser.getUid());
            map.put(DataManager.DocumentKey, Timestamp);
            map.put(DataManager.Timestamp, Timestamp);

            RegisterDonationRef.document(String.valueOf(Timestamp)).set(map)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if(task.isSuccessful()){
                                data.setValue(true);
                                Toast.Message(application, "Add success");
                            }else {
                                data.setValue(false);
                                Toast.Message(application, task.getException().getMessage());
                            }
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            data.setValue(false);
                            Toast.Message(application, e.getMessage());
                        }
                    });
        }
        return data;
    }
}
