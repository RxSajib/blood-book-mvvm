package com.blood.bloodbook.Network;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.blood.bloodbook.Data.DataManager;
import com.blood.bloodbook.Utils.Toast;

public class NotificationDelete {

    private Application application;
    private MutableLiveData<Boolean> data;
    private CollectionReference NotificationRef;
    private FirebaseAuth Mauth;

    public NotificationDelete(Application application){
        this.application = application;
        Mauth = FirebaseAuth.getInstance();
        NotificationRef = FirebaseFirestore.getInstance().collection(DataManager.Notification);
    }

    public LiveData<Boolean> NotificationDelete(String DocumentKey){
        data = new MutableLiveData<>();
        var FirebaseUser = Mauth.getCurrentUser();
        if(FirebaseUser != null){
            NotificationRef.document(DocumentKey).delete().addOnCompleteListener(task -> {
                if(task.isSuccessful()){
                    data.setValue(true);
                }else {
                    data.setValue(false);
                    Toast.Message(application, task.getException().getMessage());
                }
            }).addOnFailureListener(e -> {
               data.setValue(false);
                Toast.Message(application, e.getMessage());
            });
        }
        return data;
    }
}
