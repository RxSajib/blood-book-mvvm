package com.blood.bloodbook.Network;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.blood.bloodbook.Data.DataManager;
import com.blood.bloodbook.R;
import com.blood.bloodbook.Utils.Toast;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;

public class UpdateQuantity {

    private Application application;
    private MutableLiveData<Boolean> data;
    private FirebaseAuth Mauth;
    private CollectionReference BloodRequestRef;

    public UpdateQuantity(Application application){
        this.application = application;
        Mauth = FirebaseAuth.getInstance();
        BloodRequestRef = FirebaseFirestore.getInstance().collection(DataManager.BloodRequest);
    }

    public LiveData<Boolean> UpdateQuantity(String BloodQuantity, String DocumentID){
        data = new MutableLiveData<>();
        var FirebaseUser = Mauth.getCurrentUser();
        if(FirebaseUser != null){
            var map = new HashMap<String, Object>();
            map.put(DataManager.NumberOFQuantityPints, BloodQuantity);
            BloodRequestRef.document(DocumentID).update(map)
                    .addOnCompleteListener(updatetask -> {
                        if(updatetask.isSuccessful()){
                            data.setValue(true);
                            Toast.Message(application, application.getResources().getString(R.string.Success));
                        }else {
                            data.setValue(false);
                            Toast.Message(application, updatetask.getException().getMessage());
                        }
                    }).addOnFailureListener(e -> {
                        data.setValue(false);
                        Toast.Message(application, e.getMessage());
                    });
        }
        return data;
    }
}
