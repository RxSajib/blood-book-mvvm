package com.blood.bloodbook.Network;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.blood.bloodbook.Data.DataManager;
import com.blood.bloodbook.Utils.Toast;

public class MyAcceptBloodRequestDelete {

    private Application application;
    private MutableLiveData<Boolean> data;
    private FirebaseAuth Mauth;
    private CollectionReference MyBloodRequestRef;

    public MyAcceptBloodRequestDelete(Application application) {
        this.application = application;
        Mauth = FirebaseAuth.getInstance();
        MyBloodRequestRef = FirebaseFirestore.getInstance().collection(DataManager.MyBloodRequest);
    }

    public LiveData<Boolean> MyAcceptBloodRequestDelete(String DocumentID) {
        data = new MutableLiveData<>();
        var FirebaseUser = Mauth.getCurrentUser();
        if (FirebaseUser != null) {
            MyBloodRequestRef.document(DocumentID)
                    .delete().addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            data.setValue(true);
                        } else {
                            data.setValue(false);
                            Toast.Message(application, task.getException().getMessage());
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.Message(application, e.getMessage());
                            data.setValue(false);
                        }
                    });
        }
        return data;
    }
}
