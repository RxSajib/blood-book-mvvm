package com.blood.bloodbook.Network;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.blood.bloodbook.Data.DataManager;
import com.blood.bloodbook.UI.MessageHistory;
import com.blood.bloodbook.Utils.Toast;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;

public class MessageHistoryPOST {

    private Application application;
    private MutableLiveData<Boolean> data;
    private FirebaseAuth Mauth;
    private CollectionReference MessageRef;

    public MessageHistoryPOST(Application application){
        this.application = application;
        Mauth = FirebaseAuth.getInstance();
        MessageRef = FirebaseFirestore.getInstance().collection(DataManager.MessageHistory);
    }

    public LiveData<Boolean> SendMessageHistory(String ReceiverUID, String Type, String Message){
        data = new MutableLiveData<>();
        var FirebaseUser = Mauth.getCurrentUser();
        if(FirebaseUser != null){
            var timestamp = System.currentTimeMillis();
            var map = new HashMap<String, Object>();
            map.put(DataManager.Message, Message);
            map.put(DataManager.SenderUID, FirebaseUser.getUid());
            map.put(DataManager.ReceiverUID, ReceiverUID);
            map.put(DataManager.Type, Type);
            map.put(DataManager.Timestamp, timestamp);

            MessageRef.document(ReceiverUID)
                    .collection(DataManager.User)
                    .document(FirebaseUser.getUid())
                    .set(map).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if(task.isSuccessful()){
                                data.setValue(true);
                            }else {
                                data.setValue(false);
                                Toast.Message(application, task.getException().getMessage());
                            }
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            data.setValue(false);
                            Toast.Message(application, e.getMessage());
                        }
                    });
        }
        return data;
    }
}
