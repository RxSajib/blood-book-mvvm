package com.blood.bloodbook.Network;

import android.app.Application;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.blood.bloodbook.Data.DataManager;
import com.blood.bloodbook.ProfileModel;

public class ProfileGET {

    private Application application;
    private MutableLiveData<ProfileModel> data;
    private CollectionReference UserRef;
    private FirebaseAuth Mauth;

    public ProfileGET(Application application){
        this.application = application;
        UserRef = FirebaseFirestore.getInstance().collection(DataManager.User);
        Mauth = FirebaseAuth.getInstance();
    }

    public LiveData<ProfileModel> GetProfile(){
        data = new MutableLiveData<>();
        var FirebaseUser = Mauth.getCurrentUser();
        if(FirebaseUser != null){
            UserRef.document(FirebaseUser.getUid())
                    .addSnapshotListener(new EventListener<DocumentSnapshot>() {
                        @Override
                        public void onEvent(@Nullable DocumentSnapshot value, @Nullable FirebaseFirestoreException error) {
                            if(error != null){
                                data.setValue(null);
                                return;
                            }
                            if(value.exists()){
                                data.setValue(value.toObject(ProfileModel.class));
                            }else {
                                data.setValue(null);
                            }
                        }
                    });
        }
        return data;
    }
}
