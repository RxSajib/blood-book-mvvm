package com.blood.bloodbook.Network;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.blood.bloodbook.Data.DataManager;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

public class RegisterDonorNumberExistsGET {

    private Application application;
    private MutableLiveData<Boolean> data;
    private CollectionReference DonorUserRef;
    private FirebaseAuth Mauth;

    public RegisterDonorNumberExistsGET(Application application){
        this.application = application;
        DonorUserRef = FirebaseFirestore.getInstance().collection(DataManager.DonorUser);
        Mauth = FirebaseAuth.getInstance();
    }

    public LiveData<Boolean> SearchDonorNumber(String MainPhoneNumber){
        data = new MutableLiveData<>();

        var FirebaseUser = Mauth.getCurrentUser();
        if(FirebaseUser != null){
            var q = DonorUserRef.whereEqualTo(DataManager.MainPhoneNumber, MainPhoneNumber);
            q.get().addOnCompleteListener(task -> {
                if(task.isSuccessful()){
                    for (var document : task.getResult()) {
                        if (document.exists()) {
                           data.setValue(true);
                        }else {
                            data.setValue(false);
                        }
                    }
                }else {
                    data.setValue(false);
                }
            }).addOnFailureListener(e -> {
                data.setValue(false);
            });
        }
        return data;
    }
}
