package com.blood.bloodbook.Network;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.blood.bloodbook.Data.DataManager;
import com.blood.bloodbook.Utils.Toast;

import java.util.HashMap;

public class DonorUserShowToUserUpdate {

    private Application application;
    private MutableLiveData<Boolean> data;
    private FirebaseAuth Mauth;
    private CollectionReference DonorUserRef;
    private CollectionReference UserRef;

    public DonorUserShowToUserUpdate(Application application){
        this.application = application;
        Mauth = FirebaseAuth.getInstance();
        DonorUserRef = FirebaseFirestore.getInstance().collection(DataManager.DonorUser);
        UserRef = FirebaseFirestore.getInstance().collection(DataManager.User);
    }

    public LiveData<Boolean> UpdateDonorUserShowToUser(boolean IsShow){
        data = new MutableLiveData<>();
        var FirebaseUser = Mauth.getCurrentUser();
        if(FirebaseUser != null){
            var map  = new HashMap<String, Object>();
            map.put(DataManager.ShowToOther, IsShow);

            DonorUserRef.document(FirebaseUser.getUid())
                    .update(map).addOnCompleteListener(task -> {
                        if(task.isSuccessful()){
                            var usermap = new HashMap<String, Object>();
                            usermap.put(DataManager.IsRequestEnable, IsShow);
                            UserRef.document(FirebaseUser.getUid())
                                            .update(usermap)
                                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                        @Override
                                                        public void onComplete(@NonNull Task<Void> task) {
                                                            if(task.isSuccessful()){
                                                                data.setValue(true);
                                                            }else {
                                                                data.setValue(false);
                                                                Toast.Message(application, task.getException().getMessage());
                                                            }
                                                        }
                                                    }).addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                            data.setValue(false);
                                            Toast.Message(application, e.getMessage());
                                        }
                                    });


                        }else {
                            data.setValue(false);
                            Toast.Message(application, task.getException().getMessage());
                        }
                    }).addOnFailureListener(e -> {
                        data.setValue(false);
                        Toast.Message(application, e.getMessage());
                    });
        }
        return data;
    }
}
