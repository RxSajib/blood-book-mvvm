package com.blood.bloodbook.Network;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.FirebaseFirestore;
import com.blood.bloodbook.Data.DataManager;
import com.blood.bloodbook.HospitalModel;

import java.util.List;

public class HospitalInfoBloodBankGET {

    private Application application;
    private FirebaseAuth Mauth;
    private CollectionReference HospitalRef;
    private MutableLiveData<List<HospitalModel>> data;

    public HospitalInfoBloodBankGET(Application application){
        this.application = application;
        Mauth = FirebaseAuth.getInstance();
        HospitalRef = FirebaseFirestore.getInstance().collection(DataManager.Hospital);
    }

    public LiveData<List<HospitalModel>> GetHospitalData(){
        data = new MutableLiveData<>();
        var FirebaseUser = Mauth.getCurrentUser();
        if(FirebaseUser != null){
            HospitalRef.addSnapshotListener((value, error) -> {
                if(error != null){
                    data.setValue(null);
                    return;
                }
                if(!value.isEmpty()){
                    for(var ds : value.getDocumentChanges()){
                        if(ds.getType() == DocumentChange.Type.ADDED || ds.getType() == DocumentChange.Type.MODIFIED || ds.getType() == DocumentChange.Type.REMOVED){
                            data.setValue(value.toObjects(HospitalModel.class));
                        }
                    }
                }else {
                    data.setValue(null);
                }
            });
        }
        return data;
    }
}
