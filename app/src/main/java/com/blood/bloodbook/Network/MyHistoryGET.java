package com.blood.bloodbook.Network;

import android.app.Application;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.blood.bloodbook.MyHistoryModel;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.blood.bloodbook.Data.DataManager;
import com.blood.bloodbook.HistoryModel;

import java.util.List;

public class MyHistoryGET {

    private Application application;
    private MutableLiveData<List<MyHistoryModel>> data;
    private CollectionReference MyRegisterDonationRef;
    private FirebaseAuth Mauth;

    public MyHistoryGET(Application application){
        this.application = application;
        MyRegisterDonationRef = FirebaseFirestore.getInstance().collection(DataManager.RegisterDonation);
        Mauth = FirebaseAuth.getInstance();
    }

    public LiveData<List<MyHistoryModel>> GetMyRegisterDonation(int Limit){
        data = new MutableLiveData<>();

        var FirebaseUser = Mauth.getCurrentUser();
        if(FirebaseUser != null){
            var q = MyRegisterDonationRef.whereEqualTo(DataManager.SenderUID, FirebaseUser.getUid()).orderBy(DataManager.Timestamp, Query.Direction.DESCENDING).limit(Limit);
            q.addSnapshotListener(new EventListener<QuerySnapshot>() {
                @Override
                public void onEvent(@Nullable QuerySnapshot value, @Nullable FirebaseFirestoreException error) {
                    if(error != null){
                        data.setValue(null);
                        return;
                    }

                    if(!value.isEmpty()){
                        for(var ds : value.getDocumentChanges()){
                            if(ds.getType() == DocumentChange.Type.ADDED || ds.getType() == DocumentChange.Type.MODIFIED || ds.getType() == DocumentChange.Type.REMOVED){
                                data.setValue(value.toObjects(MyHistoryModel.class));
                            }
                        }
                    }else {
                        data.setValue(null);
                    }
                }
            });
        }
        return data;
    }
}
