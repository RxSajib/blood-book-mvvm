package com.blood.bloodbook.Network.ViewModel;

import android.app.Application;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.blood.bloodbook.AcceptBloodRequestModel;
import com.blood.bloodbook.AdModel;
import com.blood.bloodbook.AllHistoryModel;
import com.blood.bloodbook.BloodBankModel;
import com.blood.bloodbook.BloodRequestModel;
import com.blood.bloodbook.Data.CountryNameModel;
import com.blood.bloodbook.Data.DistrictNameModel;
import com.blood.bloodbook.Data.Model.AdminModel;
import com.blood.bloodbook.Data.ProvinceNameModel;
import com.blood.bloodbook.DonorModel;
import com.blood.bloodbook.HistoryModel;
import com.blood.bloodbook.HospitalModel;
import com.blood.bloodbook.LostAndFoundModel;
import com.blood.bloodbook.LostAndFoundTypeModel;
import com.blood.bloodbook.MessageHistoryModel;
import com.blood.bloodbook.MessageModel;
import com.blood.bloodbook.MyHistoryModel;
import com.blood.bloodbook.Network.AcceptBloodRequestGET;
import com.blood.bloodbook.Network.AdminUserGET;
import com.blood.bloodbook.Network.AllHistoryGET;
import com.blood.bloodbook.Network.BloodRequestGET;
import com.blood.bloodbook.Network.BloodRequestNotification;
import com.blood.bloodbook.Network.BloodRequestNotificationPOST;
import com.blood.bloodbook.Network.BloodRequestPOST;
import com.blood.bloodbook.Network.BloodRequestQuantityUpdate;
import com.blood.bloodbook.Network.BloodRequestSizeGET;
import com.blood.bloodbook.Network.BloodStorageSearchByHospitalGET;
import com.blood.bloodbook.Network.ChatFileUploadPOST;
import com.blood.bloodbook.Network.ChatMessageDelete;
import com.blood.bloodbook.Network.ChatMessageGET;
import com.blood.bloodbook.Network.ChatMessageHistoryPOST;
import com.blood.bloodbook.Network.CountryNameGET;
import com.blood.bloodbook.Network.CreateProfilePOST;
import com.blood.bloodbook.Network.CurrentUserDonorRegisterGET;
import com.blood.bloodbook.Network.DashBoardAdDataGET;
import com.blood.bloodbook.Network.DeleteLostAndFoundType;
import com.blood.bloodbook.Network.DeleteRegisterAsDonor;
import com.blood.bloodbook.Network.DistrictNameGET;
import com.blood.bloodbook.Network.DonorPhoneNumberExistsGET;
import com.blood.bloodbook.Network.DonorSingleUserGET;
import com.blood.bloodbook.Network.DonorUserByUserIDGET;
import com.blood.bloodbook.Network.DonorUserExistsGET;
import com.blood.bloodbook.Network.DonorUserShowToUserUpdate;
import com.blood.bloodbook.Network.DonorUserUpdatePUT;
import com.blood.bloodbook.Network.EmailSignInAccountPOST;
import com.blood.bloodbook.Network.EmailSignUpAccount;
import com.blood.bloodbook.Network.EmailSignUpAccountPOST;
import com.blood.bloodbook.Network.FilterLostAndFoundGET;
import com.blood.bloodbook.Network.FilterOrganizationGET;
import com.blood.bloodbook.Network.GETUserExists;
import com.blood.bloodbook.Network.HospitalInfoBloodBankGET;
import com.blood.bloodbook.Network.LastRegisterDonationDateGET;
import com.blood.bloodbook.Network.LogoutPOST;
import com.blood.bloodbook.Network.LostAndFoundDataPOST;
import com.blood.bloodbook.Network.LostAndFoundGET;
import com.blood.bloodbook.Network.LostAndFoundRemove;
import com.blood.bloodbook.Network.LostAndFoundTypeGET;
import com.blood.bloodbook.Network.LostAndFoundTypePOST;
import com.blood.bloodbook.Network.MessageHistoryGET;
import com.blood.bloodbook.Network.MessageHistoryPOST;
import com.blood.bloodbook.Network.MessageSizeGET;
import com.blood.bloodbook.Network.MyAcceptBloodRequestDelete;
import com.blood.bloodbook.Network.MyBloodRequestDelete;
import com.blood.bloodbook.Network.MyBloodRequestGET;
import com.blood.bloodbook.Network.MyBloodRequestPOST;
import com.blood.bloodbook.Network.MyHistoryGET;
import com.blood.bloodbook.Network.NotificationDelete;
import com.blood.bloodbook.Network.NotificationHistoryGET;
import com.blood.bloodbook.Network.NotificationPOST;
import com.blood.bloodbook.Network.OrganizationTypeDataGET;
import com.blood.bloodbook.Network.OrganizationGET;
import com.blood.bloodbook.Network.ProfileExistsGET;
import com.blood.bloodbook.Network.ProfileGET;
import com.blood.bloodbook.Network.ProfileImageUpdatePOST;
import com.blood.bloodbook.Network.ProfileUpdatePOST;
import com.blood.bloodbook.Network.ProvinceNameGET;
import com.blood.bloodbook.Network.RegisterAsADonorPOST;
import com.blood.bloodbook.Network.RegisterDonationDatePOST;
import com.blood.bloodbook.Network.RegisterDonationGET;
import com.blood.bloodbook.Network.RegisterDonationImageUploadPOST;
import com.blood.bloodbook.Network.RegisterDonorAsFriendPOST;
import com.blood.bloodbook.Network.RegisterDonorNumberExistsGET;
import com.blood.bloodbook.Network.RegisterDonorSizeGET;
import com.blood.bloodbook.Network.RegisterFriendDonorSizeGET;
import com.blood.bloodbook.Network.ReportGET;
import com.blood.bloodbook.Network.ReportPOST;
import com.blood.bloodbook.Network.ResetPasswordPOST;
import com.blood.bloodbook.Network.SearchLostAndFoundGET;
import com.blood.bloodbook.Network.SearchTelephonyManager;
import com.blood.bloodbook.Network.SingleBloodRequestGET;
import com.blood.bloodbook.Network.SingleUserGET;
import com.blood.bloodbook.Network.UpdateDonateMonthPOST;
import com.blood.bloodbook.Network.UpdateQuantity;
import com.blood.bloodbook.Network.UploadLostAndFoundImagePOST;
import com.blood.bloodbook.Network.UploadProfileImagePOST;
import com.blood.bloodbook.Network.UserGET;
import com.blood.bloodbook.Data.NotifactionResponse;
import com.blood.bloodbook.Network.UserSickStatusGET;
import com.blood.bloodbook.Network.VideoAdsGET;
import com.blood.bloodbook.NotificationModel;
import com.blood.bloodbook.OrganizationModel;
import com.blood.bloodbook.OrganizationTypeModel;
import com.blood.bloodbook.ProfileModel;
import com.blood.bloodbook.RegisterDonationModel;
import com.blood.bloodbook.ReportModel;
import com.blood.bloodbook.UI.ChatMessagePOST;
import com.blood.bloodbook.VideoAdsModel;

import org.checkerframework.checker.units.qual.Area;

import java.util.List;

public class ViewModel extends AndroidViewModel {

    //todo masuma madam
    private MessageSizeGET messageSizeGET;
    private MessageHistoryGET messageHistoryGET;
    private MessageHistoryPOST messageHistoryPOST;
    private DonorUserByUserIDGET donorUserByUserIDGET;
    private UserSickStatusGET userSickStatusGET;
    private DonorPhoneNumberExistsGET donorPhoneNumberExistsGET;
    private ChatMessageDelete chatMessageDelete;
    private EmailSignUpAccount emailSignUpAccount;
    private AdminUserGET adminUserGET;
    private UpdateQuantity updateQuantity;
    private SingleUserGET singleUserGET;
    private DeleteRegisterAsDonor deleteRegisterAsDonor;
    private DonorUserExistsGET donorUserExistsGET;
    private UploadLostAndFoundImagePOST uploadLostAndFoundImagePOST;
    private LostAndFoundRemove lostAndFoundRemove;
    private LostAndFoundTypePOST lostAndFoundTypePOST;
    private DeleteLostAndFoundType deleteLostAndFoundType;
    private LostAndFoundDataPOST lostAndFoundDataPOST;
    private BloodRequestSizeGET bloodRequestSizeGET;
    private RegisterFriendDonorSizeGET registerFriendDonorSizeGET;
    private RegisterDonorSizeGET registerDonorSizeGET;
    private ReportGET reportGET;
    private ReportPOST reportPOST;
    private BloodRequestNotification bloodRequestNotification;
    private VideoAdsGET videoAdsGET;
    private DonorUserUpdatePUT donorUserUpdatePUT;
    private CurrentUserDonorRegisterGET currentUserDonorRegisterGET;
    private NotificationDelete notificationDelete;
    private DonorSingleUserGET donorSingleUserGET;
    private UpdateDonateMonthPOST updateDonateMonthPOST;
    private RegisterDonationGET registerDonationGET;
    private DonorUserShowToUserUpdate donorUserShowToUserUpdate;
    private BloodRequestNotificationPOST bloodRequestNotificationPOST;
    private SingleBloodRequestGET singleBloodRequestGET;
    private MyAcceptBloodRequestDelete myAcceptBloodRequestDelete;
    private AcceptBloodRequestGET acceptBloodRequestGET;
    private MyBloodRequestPOST myBloodRequestPOST;
    private BloodRequestQuantityUpdate bloodRequestQuantityUpdate;
    private LastRegisterDonationDateGET lastRegisterDonationDateGET;
    private MyBloodRequestDelete myBloodRequestDelete;
    private MyBloodRequestGET myBloodRequestGET;
    private DashBoardAdDataGET dashBoardAdDataGET;
    private BloodStorageSearchByHospitalGET bloodStorageSearchByHospitalGET;
    private HospitalInfoBloodBankGET hospitalInfoBloodBankGET;
    private FilterLostAndFoundGET filterLostAndFoundGET;
    private LostAndFoundTypeGET lostAndFoundTypeGET;
    private SearchLostAndFoundGET searchLostAndFoundGET;
    private LostAndFoundGET lostAndFoundGET;
    private SearchTelephonyManager searchTelephonyManager;
    private OrganizationTypeDataGET organizationTypeDataGET;
    private FilterOrganizationGET filterOrganizationGET;
    private OrganizationGET organizationGET;
    private NotificationHistoryGET notificationHistoryGET;

    private ChatMessageHistoryPOST chatMessageHistoryPOST;
    private ChatFileUploadPOST chatFileUploadPOST;
    private NotificationPOST notificationPOST;

    private ChatMessageGET chatMessageGET;
    private ChatMessagePOST chatMessagePOST;
    private UserGET userGET;

    private AllHistoryGET allHistoryGET;
    private MyHistoryGET myHistoryGET;

    private RegisterDonationDatePOST registerDonationDatePOST;
    private RegisterDonationImageUploadPOST registerDonationImageUploadPOST;

    private UploadProfileImagePOST uploadProfileImagePOST;
    private GETUserExists getUserExists;

    private ProfileExistsGET profileExistsGET;
    private ResetPasswordPOST resetPasswordPOST;
    private EmailSignInAccountPOST emailSignInAccountPOST;
    private EmailSignUpAccountPOST emailSignUpAccountPOST;

    private CreateProfilePOST createProfilePOST;
    private ProfileGET profileGET;
    private ProfileUpdatePOST profileUpdatePOST;
    private LogoutPOST logoutPOST;
    private BloodRequestPOST bloodRequestPOST;

    private CountryNameGET countryNameGET;
    private ProvinceNameGET provinceNameGET;
    private DistrictNameGET districtNameGET;
    private BloodRequestGET bloodRequestGET;
    private RegisterAsADonorPOST registerAsADonorPOST;
    private ProfileImageUpdatePOST profileImageUpdatePOST;
    private RegisterDonorAsFriendPOST registerDonorAsFriendPOST;
    private RegisterDonorNumberExistsGET registerDonorNumberExistsGET;

    public ViewModel(@NonNull Application application) {
        super(application);

        messageSizeGET = new MessageSizeGET(application);
        messageHistoryGET = new MessageHistoryGET(application);
        messageHistoryPOST = new MessageHistoryPOST(application);
        donorUserByUserIDGET = new DonorUserByUserIDGET(application);
        userSickStatusGET = new UserSickStatusGET(application);
        donorPhoneNumberExistsGET = new DonorPhoneNumberExistsGET(application);
        chatMessageDelete = new ChatMessageDelete(application);
        emailSignUpAccount = new EmailSignUpAccount(application);
        adminUserGET = new AdminUserGET(application);
        updateQuantity = new UpdateQuantity(application);
        singleUserGET = new SingleUserGET(application);
        deleteRegisterAsDonor = new DeleteRegisterAsDonor(application);
        donorUserExistsGET = new DonorUserExistsGET(application);
        uploadLostAndFoundImagePOST = new UploadLostAndFoundImagePOST(application);
        lostAndFoundRemove = new LostAndFoundRemove(application);
        lostAndFoundTypePOST = new LostAndFoundTypePOST(application);
        deleteLostAndFoundType = new DeleteLostAndFoundType(application);
        lostAndFoundDataPOST = new LostAndFoundDataPOST(application);
        bloodRequestSizeGET = new BloodRequestSizeGET(application);
        registerFriendDonorSizeGET = new RegisterFriendDonorSizeGET(application);
        registerDonorSizeGET = new RegisterDonorSizeGET(application);
        reportGET = new ReportGET(application);
        reportPOST = new ReportPOST(application);
        bloodRequestNotification = new BloodRequestNotification(application);
        videoAdsGET = new VideoAdsGET(application);
        donorUserUpdatePUT = new DonorUserUpdatePUT(application);
        currentUserDonorRegisterGET = new CurrentUserDonorRegisterGET(application);
        notificationDelete = new NotificationDelete(application);
        donorSingleUserGET = new DonorSingleUserGET(application);
        updateDonateMonthPOST = new UpdateDonateMonthPOST(application);
        registerDonationGET = new RegisterDonationGET(application);
        donorUserShowToUserUpdate = new DonorUserShowToUserUpdate(application);
        bloodRequestNotificationPOST = new BloodRequestNotificationPOST(application);
        singleBloodRequestGET = new SingleBloodRequestGET(application);
        myAcceptBloodRequestDelete = new MyAcceptBloodRequestDelete(application);
        acceptBloodRequestGET = new AcceptBloodRequestGET(application);
        myBloodRequestPOST = new MyBloodRequestPOST(application);
        bloodRequestQuantityUpdate = new BloodRequestQuantityUpdate(application);
        lastRegisterDonationDateGET = new LastRegisterDonationDateGET(application);
        myBloodRequestDelete = new MyBloodRequestDelete(application);
        myBloodRequestGET = new MyBloodRequestGET(application);
        dashBoardAdDataGET = new DashBoardAdDataGET(application);
        bloodStorageSearchByHospitalGET = new BloodStorageSearchByHospitalGET(application);
        hospitalInfoBloodBankGET = new HospitalInfoBloodBankGET(application);
        filterLostAndFoundGET = new FilterLostAndFoundGET(application);
        lostAndFoundTypeGET = new LostAndFoundTypeGET(application);
        searchLostAndFoundGET = new SearchLostAndFoundGET(application);
        lostAndFoundGET = new LostAndFoundGET(application);
        searchTelephonyManager = new SearchTelephonyManager(application);
        organizationTypeDataGET = new OrganizationTypeDataGET(application);
        filterOrganizationGET = new FilterOrganizationGET(application);
        organizationGET = new OrganizationGET(application);
        notificationHistoryGET = new NotificationHistoryGET(application);
        chatMessageHistoryPOST = new ChatMessageHistoryPOST(application);
        chatFileUploadPOST = new ChatFileUploadPOST(application);
        notificationPOST = new NotificationPOST(application);

        chatMessageGET = new ChatMessageGET(application);
        chatMessagePOST = new ChatMessagePOST(application);
        userGET = new UserGET(application);

        allHistoryGET = new AllHistoryGET(application);
        myHistoryGET = new MyHistoryGET(application);

        registerDonationDatePOST = new RegisterDonationDatePOST(application);
        registerDonationImageUploadPOST = new RegisterDonationImageUploadPOST(application);

        uploadProfileImagePOST = new UploadProfileImagePOST(application);
        getUserExists = new GETUserExists(application);

        profileExistsGET = new ProfileExistsGET(application);
        resetPasswordPOST = new ResetPasswordPOST(application);
        emailSignInAccountPOST = new EmailSignInAccountPOST(application);
        emailSignUpAccountPOST = new EmailSignUpAccountPOST(application);

        registerDonorNumberExistsGET = new RegisterDonorNumberExistsGET(application);
        registerDonorAsFriendPOST = new RegisterDonorAsFriendPOST(application);
        profileImageUpdatePOST = new ProfileImageUpdatePOST(application);
        registerAsADonorPOST = new RegisterAsADonorPOST(application);
        bloodRequestGET = new BloodRequestGET(application);
        logoutPOST = new LogoutPOST(application);
        createProfilePOST = new CreateProfilePOST(application);
        profileGET = new ProfileGET(application);
        profileUpdatePOST = new ProfileUpdatePOST(application);
        bloodRequestPOST = new BloodRequestPOST(application);
        countryNameGET = new CountryNameGET(application);
        provinceNameGET = new ProvinceNameGET(application);
        districtNameGET = new DistrictNameGET(application);
    }

    public LiveData<Integer> MessageSize(){
        return messageSizeGET.MessageSize();
    }
    public LiveData<List<MessageHistoryModel>> MessageHistoryGET(int Limit){
        return messageHistoryGET.GetMessageHistory(Limit);
    }
    public LiveData<Boolean> SendMessageHistory(String ReceiverUID, String Type, String Message){
        return messageHistoryPOST.SendMessageHistory(ReceiverUID, Type, Message);
    }
    public LiveData<DonorModel> GetUserByUserID(){
        return donorUserByUserIDGET.DonorUsrByUserID();
    }
    public LiveData<ProfileModel> GetUserSickStatus(){
        return userSickStatusGET.GetUserSickStatus();
    }

    public LiveData<Boolean> DonorPhoneNumberIsExists(String PhoneNumber){
        return donorPhoneNumberExistsGET.DonorPhoneNumberIsExists(PhoneNumber);
    }
    public LiveData<Boolean> DeleteMessage(String SenderUID, String ReceiverUID, long DocumentKey){
        return chatMessageDelete.DeleteMessage(SenderUID, ReceiverUID, DocumentKey);
    }
    public LiveData<Boolean> SignUpAccount(String Email, String Password){
        return emailSignUpAccount.SignUpAccount(Email, Password);
    }
    public LiveData<AdminModel> GetAdminUser(){
        return adminUserGET.AdminUser();
    }
    public LiveData<Boolean> UpdateQuantity(String BloodQuantity, String DocumentID){
        return updateQuantity.UpdateQuantity(BloodQuantity, DocumentID);
    }
    public LiveData<ProfileModel> GetSingleUser(String UID){
        return singleUserGET.GetSingleProfile(UID);
    }
    public LiveData<Boolean> DeleteRegisterDonor(){
        return deleteRegisterAsDonor.DeleteRegisterDonor();
    }
    public LiveData<Boolean> IsDonorUserExists(){
        return donorUserExistsGET.DonorUserExists();
    }
    public LiveData<String> UploadLostAndFoundImage(Uri ImageUri){
        return uploadLostAndFoundImagePOST.UploadLostAndFoundImage(ImageUri);
    }
    public LiveData<Boolean> RemoveLostAndFound(long DocumentKey){
        return lostAndFoundRemove.RemoveLostAndFound(DocumentKey);
    }
    public LiveData<Boolean> UploadLostAndFoundCategory(String Category) {
        return lostAndFoundTypePOST.UploadLostAndFoundType(Category);
    }
    public LiveData<Boolean> DeleteLostAndFound(long DocumentID) {
        return deleteLostAndFoundType.DeleteLostAndFoundType(DocumentID);
    }
    public LiveData<Boolean> LostAndFoundDataPost(String PostTitle, String LostAndFound, String TelNumber, String Country, String Province, String District, String FullDetails, String Image){
        return lostAndFoundDataPOST.UploadLostAndFound(PostTitle, LostAndFound, TelNumber, Country, Province, District, FullDetails, Image);
    }
    public LiveData<Integer> BloodRequestSize(){
        return bloodRequestSizeGET.BloodRequestSize();
    }
    public LiveData<Integer> RegisterFriendDonorSize(){
        return registerFriendDonorSizeGET.RegisterFriendDonorSize();
    }
    public LiveData<Integer> SizeOFRegisterDonor(){
        return registerDonorSizeGET.RegisterAsDonorSize();
    }
    public LiveData<List<ReportModel>> GetReport(){
        return reportGET.ReportGET();
    }
    public LiveData<Boolean> SendRepost(String ReceiverUID, String Message){
        return reportPOST.SendRepost(ReceiverUID, Message);
    }
    public LiveData<Boolean> SendBloodRequestNotification(long DocumentKey, String Message, String MessageTopic, String MessageType, String ReceiverUID, String SenderUID){
        return bloodRequestNotification.SendBloodRequestNotification(DocumentKey, Message, MessageTopic, MessageType, ReceiverUID, SenderUID);
    }
    public LiveData<VideoAdsModel> GetVideoAds(){
        return videoAdsGET.GetVideoAds();
    }
    public LiveData<Boolean> DonorUserUpdate(String Country, String Province, String District, String AreaName){
        return donorUserUpdatePUT.DonorUserUpdate(Country, Province, District, AreaName);
    }
    public LiveData<DonorModel> GetCurrentDonorUser(){
        return currentUserDonorRegisterGET.GetCurrentDonorUser();
    }
    public LiveData<Boolean> NotificationDelete(String DocumentKey){
        return notificationDelete.NotificationDelete(DocumentKey);
    }
    public LiveData<DonorModel> GetSingleDonorUser(String UID){
        return donorSingleUserGET.GetSingleDonorUser(UID);
    }
    public LiveData<Boolean> UpdateDonateMonthPOST(String MonthDay){
        return updateDonateMonthPOST.UpdateDonateMonth(MonthDay);
    }
    public LiveData<List<RegisterDonationModel>> RegisterDonationGET(String UID){
        return registerDonationGET.GetRegisterDonation(UID);
    }
    public LiveData<Boolean> UpdateDonorUserShowToUser(Boolean IsShow){
        return donorUserShowToUserUpdate.UpdateDonorUserShowToUser(IsShow);
    }
    public LiveData<Boolean> BloodNotificationPOST(String ReceiverUID, String BloodQuantity, String BloodRequestID){
        return bloodRequestNotificationPOST.BloodNotificationPOST(ReceiverUID, BloodQuantity, BloodRequestID);
    }
    public LiveData<BloodRequestModel> GetSingleBloodRequest(String DocumentID){
        return singleBloodRequestGET.GetSingleBloodRequest(DocumentID);
    }
    public LiveData<Boolean> MyAcceptedBloodRequestDelete(String DocumentID){
        return myAcceptBloodRequestDelete.MyAcceptBloodRequestDelete(DocumentID);
    }
    public LiveData<List<AcceptBloodRequestModel>> GetMyAcceptBloodRequest(int Limit){
        return acceptBloodRequestGET.GetMyBloodAcceptRequest(Limit);
    }
    public LiveData<Boolean> MyBloodRequest(BloodRequestModel bloodRequestModel, String Quantity, long DocumentID, String BloodRequestID){
        return myBloodRequestPOST.MyBloodRequest(bloodRequestModel, Quantity, DocumentID, BloodRequestID);
    }
    public LiveData<Boolean> UpdateBloodRequestQuantity(String BloodQuantity, String DocumentID, AcceptBloodRequestModel acceptBloodRequestModel){
        return bloodRequestQuantityUpdate.UpdateBloodRequestQuantity(BloodQuantity, DocumentID, acceptBloodRequestModel);
    }
    public LiveData<List<HistoryModel>> LastRegisterDonationDate(){
        return lastRegisterDonationDateGET.LastRegisterDonationDate();
    }
    public LiveData<Boolean> DeleteMyBloodRequest(long DocumentID){
        return myBloodRequestDelete.BloodRequestDelete(DocumentID);
    }
    public LiveData<List<BloodRequestModel>> GetMyBloodRequest(int Limit){
        return myBloodRequestGET.MyBloodRequest(Limit);
    }
    public LiveData<AdModel> GetDashBoardAdData(){
        return dashBoardAdDataGET.GetDashBoardAd();
    }
    public LiveData<List<BloodBankModel>> GetBloodStore(String UID, String HospitalName){
        return bloodStorageSearchByHospitalGET.GetBloodStore(UID, HospitalName);
    }
    public LiveData<List<HospitalModel>> GetHospitalInfo(){
        return hospitalInfoBloodBankGET.GetHospitalData();
    }
    public LiveData<List<LostAndFoundModel>> FilterLostAndFound(String country, String province, String district, String orgType){
        return filterLostAndFoundGET.FilterLostAndFound(country, province, district, orgType);
    }
    public LiveData<List<LostAndFoundTypeModel>> GetLostAndFoundType() {
        return lostAndFoundTypeGET.GetLostAndFoundType();
    }
    public LiveData<List<LostAndFoundModel>> SearchLostAndFound(String LostAndFound){
        return searchLostAndFoundGET.SearchLostAndFound(LostAndFound);
    }
    public LiveData<List<LostAndFoundModel>> GetLostAndFound(int Limit){
        return lostAndFoundGET.GetLostAndFound(Limit);
    }
    public LiveData<List<OrganizationModel>> SearchOrganization(String OrgName){
        return searchTelephonyManager.SearchTelephonyManager(OrgName);
    }

    public LiveData<List<OrganizationTypeModel>> GetOrganizationType(){
        return organizationTypeDataGET.GetOrganizationType();
    }

    public LiveData<List<OrganizationModel>> FilterOrganizationData(String Country, String Province, String District, String OrganizationType){
        return filterOrganizationGET.FilterOrganization(Country, Province, District, OrganizationType);
    }

    public LiveData<List<OrganizationModel>> Getorganization(){
        return organizationGET.GetOrganization();
    }
    public LiveData<List<NotificationModel>> GetNotification(int Limit){
        return notificationHistoryGET.GetNotification(Limit);
    }

    public LiveData<Boolean> ChatMessageHistory(String SenderUID, String Message, String Type, String MessageType){
        return chatMessageHistoryPOST.SendChatMessageNotification(SenderUID, Message, Type, MessageType);
    }

    public LiveData<String> ChatFileUpload(Uri FileUri, String FileType){
        return chatFileUploadPOST.UploadFile(FileUri, FileType);
    }

    public LiveData<Boolean> SendNotification(NotifactionResponse notifactionResponse){
        return notificationPOST.send_message(notifactionResponse);
    }

    public LiveData<List<MessageModel>> GetMessage(String SenderUID, int LastLimit){
        return chatMessageGET.GetMessage(SenderUID, LastLimit);
    }

    public LiveData<Boolean> SendChatMessage(String Message, String Type, String SenderUID, String FileName, String FileExtension, String FileSize){
        return chatMessagePOST.SendMessage(Message, Type, SenderUID, FileName, FileExtension, FileSize);
    }

    public LiveData<ProfileModel> GetUserInfo(String UID){
        return userGET.GetUserInfo(UID);
    }

    public LiveData<List<AllHistoryModel>> GetAllHistory(int Limit){
        return allHistoryGET.AllMyRegisterDonor(Limit);
    }

    public LiveData<List<MyHistoryModel>> MyMyRegisterDonation(int Limit){
        return myHistoryGET.GetMyRegisterDonation(Limit);
    }

    public LiveData<Boolean> RegisterDonationDate(String DonationDate, String QuantityCC, String Country, String Province, String District, String HospitalName, String ImageUri){
        return registerDonationDatePOST.UploadRegisterDonation(DonationDate, QuantityCC, Country, Province, District, HospitalName, ImageUri);
    }


    public LiveData<String> RegisterDonationImageUpload(Uri ImageUri){
        return registerDonationImageUploadPOST.UploadRegisterDonationImage(ImageUri);
    }

    public LiveData<String> UploadProfileImage(Uri ImageUri){
        return uploadProfileImagePOST.UploadProfileImage(ImageUri);
    }

    public LiveData<Boolean> CheckUserExists(){
        return getUserExists.CheckUserExists();
    }

    public LiveData<Boolean> ProfileExists(){
       return profileExistsGET.UserExists();
    }

    public LiveData<Boolean> ResetPassword(String EmailAddress){
        return resetPasswordPOST.ResetPassword(EmailAddress);
    }

    public LiveData<Boolean> EmailSignUpAccount(String Email, String Password){
        return emailSignUpAccountPOST.SignUpWithEmail(Email, Password);
    }
    public LiveData<Boolean> EmailSignInAccount(String Email, String Password){
        return emailSignInAccountPOST.SigninWithEmail(Email, Password);
    }

    public LiveData<Boolean> RegisterDonorNumberExists(String MainPhoneNumber){
        return registerDonorNumberExistsGET.SearchDonorNumber(MainPhoneNumber);
    }

    public LiveData<Boolean> RegisterBloodDonorFriend(String Type, String GivenName, String SureName, String FatherName, String BloodGroup, String Age, String Width,
                                                      String Country, String Province, String District, String AreaName, String MainPhoneNumber,
                                                      String RoshanNumber, String EtisalatNumber, String AWCCNumber, String MTNNumber, String WhatsappNumber,
                                                      String EmailAddress, String DonateMonth, String LastDonationDate, boolean EnableMyNumber, String LastDonationDayCount){
        return registerDonorAsFriendPOST.RegisterBloodDonorFriend(Type, GivenName, SureName, FatherName, BloodGroup, Age, Width, Country, Province, District, AreaName, MainPhoneNumber, RoshanNumber, EtisalatNumber, AWCCNumber, MTNNumber, WhatsappNumber, EmailAddress, DonateMonth, LastDonationDate, EnableMyNumber, LastDonationDayCount);
    }

    public LiveData<Boolean> ProfileImageUpdate(Uri ImageUri){
        return profileImageUpdatePOST.UpdateProfileImage(ImageUri);
    }

    public LiveData<Boolean> RegisterAsADonor(String Type, String GivenName, String SureName, String FatherName, String BloodGroup, String Age, String Width,
                                              String Country, String Province, String District, String AreaName, String MainPhoneNumber,
                                              String RoshanNumber, String EtisalatNumber, String AWCCNumber, String MTNNumber, String WhatsappNumber,
                                              String EmailAddress, String DonateMonth, String LastDonationDate, boolean EnableMyNumber, String LastDonationDayCount){


        return registerAsADonorPOST.RegisterAsDonor(Type, GivenName, SureName, FatherName, BloodGroup, Age, Width, Country, Province, District, AreaName, MainPhoneNumber, RoshanNumber,
                EtisalatNumber, AWCCNumber, MTNNumber, WhatsappNumber, EmailAddress, DonateMonth, LastDonationDate, EnableMyNumber, LastDonationDayCount);
    }
    public LiveData<List<BloodRequestModel>> GetBloodRequest(int Limit){
        return bloodRequestGET.GetBloodRequest(Limit);
    }

    public LiveData<List<DistrictNameModel>> GetDistrictName(String CountryName, String Province){
        return districtNameGET.GetDistrictName(CountryName, Province);
    }

    public LiveData<List<ProvinceNameModel>> GetProvinceName(String CountryName){
        return provinceNameGET.GetProvinceData(CountryName);
    }
    public LiveData<List<CountryNameModel>> GetCountryName(){
        return countryNameGET.GetCountryNameData();
    }

    public LiveData<Boolean> BloodRequest(String PatientName, String Age, String BloodGroup, String BloodCondition, String CountryName, String Province, String District, String BloodNeededFor, String NoOfQuantityPints, String Time, String Date, String AttendantContactNumber, String HospitalName, String ExchangePossibility, String TransportAvailability, String TyOfHBLevel, String TypeOFPatientsCondition){
        return bloodRequestPOST.BloodRequest(PatientName, Age, BloodGroup, BloodCondition, CountryName, Province, District, BloodNeededFor, NoOfQuantityPints, Time, Date, AttendantContactNumber, HospitalName, ExchangePossibility, TransportAvailability, TyOfHBLevel, TypeOFPatientsCondition);
    }

    public LiveData<Boolean> LogOutAccount(){
        return logoutPOST.LogOutAccount();
    }
    public LiveData<Boolean> ProfileUpdate(String FirstName, String MiddleName, String SureName, String FatherName){
        return profileUpdatePOST.UpdateProfile(FirstName, MiddleName, SureName, FatherName);
    }

    public LiveData<ProfileModel> ProfileGET(){
        return profileGET.GetProfile();
    }

    public LiveData<Boolean> CreateProfile(String FirstName, String MiddleName, String SureName, String FatherName, String ProfileImage, String Email, String Account){
        return createProfilePOST.ProfilePOST(FirstName, MiddleName, SureName, FatherName, ProfileImage, Email, Account);
    }
}
