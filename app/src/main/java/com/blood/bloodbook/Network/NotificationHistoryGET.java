package com.blood.bloodbook.Network;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.FirebaseFirestore;
import com.blood.bloodbook.Data.DataManager;
import com.blood.bloodbook.NotificationModel;
import com.google.firebase.firestore.Query;

import java.util.List;

public class NotificationHistoryGET {

    private Application application;
    private MutableLiveData<List<NotificationModel>> data;
    private CollectionReference NotificationRef;
    private FirebaseAuth Mauth;


    public NotificationHistoryGET(Application application){
        this.application = application;
        Mauth = FirebaseAuth.getInstance();
        NotificationRef = FirebaseFirestore.getInstance().collection(DataManager.Notification);
    }

    public LiveData<List<NotificationModel>> GetNotification(int Limit){
        data = new MutableLiveData<>();
        var FirebaseUser = Mauth.getCurrentUser();
        if(FirebaseUser != null){
            var order = NotificationRef.orderBy(DataManager.Timestamp, Query.Direction.DESCENDING).limit(Limit);
            order.addSnapshotListener((value, error) -> {
                if(error != null){
                    data.setValue(null);
                    return;
                }
                if(!value.isEmpty()){
                    for(var ds : value.getDocumentChanges()){
                        if(ds.getType() == DocumentChange.Type.ADDED || ds.getType() == DocumentChange.Type.MODIFIED || ds.getType() == DocumentChange.Type.REMOVED){
                            data.setValue(value.toObjects(NotificationModel.class));
                        }
                    }
                }else {
                    data.setValue(null);
                }
            });
        }
        return data;
    }
}
