package com.blood.bloodbook.Network;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.blood.bloodbook.Data.DataManager;
import com.blood.bloodbook.Utils.Toast;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class ChatMessageDelete {

    private Application application;
    private MutableLiveData<Boolean> data;
    private DatabaseReference MessageRef;
    private FirebaseAuth Mauth;

    public ChatMessageDelete(Application application){
        this.application = application;
        MessageRef = FirebaseDatabase.getInstance().getReference().child(DataManager.Message);
        Mauth = FirebaseAuth.getInstance();
    }

    public LiveData<Boolean> DeleteMessage(String SenderUID, String ReceiverUID, long DocumentKey){
        data = new MutableLiveData<>();
        MessageRef.child(SenderUID).child(ReceiverUID).child(DataManager.Message)
                .child(String.valueOf(DocumentKey))
                .removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if(task.isSuccessful()){
                            MessageRef.child(ReceiverUID).child(SenderUID).child(DataManager.Message)
                                    .child(String.valueOf(DocumentKey))
                                    .removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if(task.isSuccessful()){
                                                data.setValue(true);
                                            }else {
                                                data.setValue(false);
                                                Toast.Message(application, task.getException().getMessage());
                                            }
                                        }
                                    }).addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                            data.setValue(false);
                                            Toast.Message(application, e.getMessage());
                                        }
                                    });
                        }else {
                            data.setValue(false);
                            Toast.Message(application, task.getException().getMessage());
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.Message(application, e.getMessage());
                        data.setValue(false);
                    }
                });
        return data;
    }
}
