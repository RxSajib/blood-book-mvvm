package com.blood.bloodbook.Network;

import android.app.Application;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.blood.bloodbook.Data.DataManager;
import com.blood.bloodbook.Utils.Toast;

import java.util.HashMap;

public class ProfileImageUpdatePOST {

    private Application application;
    private FirebaseAuth Mauth;
    private CollectionReference UserRef;
    private StorageReference PorfileRef;
    private MutableLiveData<Boolean> data;

    public ProfileImageUpdatePOST(Application application){
        this.application = application;
        Mauth = FirebaseAuth.getInstance();
        UserRef = FirebaseFirestore.getInstance().collection(DataManager.User);
        PorfileRef = FirebaseStorage.getInstance().getReference().child(DataManager.User);
    }

    public LiveData<Boolean> UpdateProfileImage(Uri ImageUri){
        data = new MutableLiveData<>();

        var FirebaseUser = Mauth.getCurrentUser();
        if(FirebaseUser != null){
            var storage = PorfileRef.child(ImageUri.getLastPathSegment());
            storage.putFile(ImageUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    if(taskSnapshot.getMetadata() != null){
                        if(taskSnapshot.getMetadata().getReference() != null){
                            var task = taskSnapshot.getStorage().getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    var map = new HashMap<String, Object>();
                                    map.put(DataManager.ProfileImage, uri.toString());
                                    UserRef.document(FirebaseUser.getUid()).update(map).addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if(task.isSuccessful()){
                                                data.setValue(true);
                                                Toast.Message(application, "success");
                                            }else {
                                                data.setValue(false);
                                                Toast.Message(application, task.getException().getMessage());
                                            }
                                        }
                                    }).addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                            data.setValue(false);
                                            Toast.Message(application, e.getMessage());
                                        }
                                    });
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Toast.Message(application, e.getMessage());
                                    data.setValue(false);
                                }
                            });
                        }
                    }
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    data.setValue(null);
                    Toast.Message(application, e.getMessage());
                }
            });
        }
        return data;
    }
}
