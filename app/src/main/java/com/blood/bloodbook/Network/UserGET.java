package com.blood.bloodbook.Network;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.blood.bloodbook.Data.DataManager;
import com.blood.bloodbook.ProfileModel;

public class UserGET {

    private Application application;
    private CollectionReference User;
    private MutableLiveData<ProfileModel> data;
    private FirebaseAuth Mauth;

    public UserGET(Application application){
        this.application = application;
        Mauth = FirebaseAuth.getInstance();
        User = FirebaseFirestore.getInstance().collection(DataManager.User);
    }

    public LiveData<ProfileModel> GetUserInfo(String UID){
        data = new MutableLiveData<>();
        var FirebaseUser = Mauth.getCurrentUser();
        if(FirebaseUser != null){
            User.document(UID).addSnapshotListener((value, error) -> {
                if(error != null){
                    data.setValue(null);
                    return;
                }
                if(value.exists()){
                    data.setValue(value.toObject(ProfileModel.class));
                }else {
                    data.setValue(null);
                }
            });
        }
        return data;
    }
}
