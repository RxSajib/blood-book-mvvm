package com.blood.bloodbook.Network;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.blood.bloodbook.Data.DataManager;
import com.blood.bloodbook.Data.Model.AdminModel;
import com.blood.bloodbook.Utils.Toast;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

public class AdminUserGET {

    private Application application;
    private MutableLiveData<AdminModel> data;
    private CollectionReference AdminRef;

    public AdminUserGET(Application application){
        this.application = application;
        AdminRef = FirebaseFirestore.getInstance().collection(DataManager.Admin);
    }

    public LiveData<AdminModel> AdminUser(){
        data = new MutableLiveData<>();
        AdminRef.document(DataManager.User)
                .get().addOnCompleteListener(task -> {
                    if(task.isSuccessful()){
                        data.setValue(task.getResult().toObject(AdminModel.class));
                    }else {
                        Toast.Message(application, task.getException().getMessage());
                        data.setValue(null);
                    }
                }).addOnFailureListener(e -> {
                    data.setValue(null);
                    Toast.Message(application, e.getMessage());
                });
        return data;
    }

}
