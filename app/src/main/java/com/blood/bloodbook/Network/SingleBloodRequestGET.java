package com.blood.bloodbook.Network;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.blood.bloodbook.BloodRequestModel;
import com.blood.bloodbook.Data.DataManager;
import com.blood.bloodbook.Utils.Toast;

public class SingleBloodRequestGET {

    private Application application;
    private MutableLiveData<BloodRequestModel> data;
    private CollectionReference BloodRequestRef;
    private FirebaseAuth Mauth;

    public SingleBloodRequestGET(Application application){
        this.application = application;
        Mauth = FirebaseAuth.getInstance();
        BloodRequestRef = FirebaseFirestore.getInstance().collection(DataManager.BloodRequest);
    }

    public LiveData<BloodRequestModel> GetSingleBloodRequest(String DocumentID){
        data = new MutableLiveData<>();
        var FirebaseUser = Mauth.getCurrentUser();
        if(FirebaseUser != null){
            BloodRequestRef.document(DocumentID).get().addOnCompleteListener(task -> {
                if(task.isSuccessful()){
                    data.setValue(task.getResult().toObject(BloodRequestModel.class));
                }else {
                    data.setValue(null);
                    Toast.Message(application, task.getException().getMessage());
                }
            }).addOnFailureListener(e -> {
                data.setValue(null);
                Toast.Message(application, e.getMessage());
            });
        }
        return data;
    }
}
