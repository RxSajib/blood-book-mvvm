package com.blood.bloodbook.Network;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.blood.bloodbook.Data.DataManager;
import com.blood.bloodbook.HistoryModel;

import java.util.List;

public class LastRegisterDonationDateGET {

    private Application application;
    private MutableLiveData<List<HistoryModel>> data;
    private CollectionReference RegisterDonor;
    private FirebaseAuth Mauth;

    public LastRegisterDonationDateGET(Application application){
        this.application = application;
        Mauth = FirebaseAuth.getInstance();
        RegisterDonor = FirebaseFirestore.getInstance().collection(DataManager.RegisterDonation);
    }

    public LiveData<List<HistoryModel>> LastRegisterDonationDate(){
        data = new MutableLiveData<>();
        var FirebaseUser = Mauth.getCurrentUser();
        if(FirebaseUser != null){
           var q = RegisterDonor.whereEqualTo(DataManager.SenderUID, FirebaseUser.getUid()).orderBy(DataManager.Timestamp, Query.Direction.ASCENDING)
                   .limit(1);
           q.addSnapshotListener((value, error) -> {
                if(error != null){
                    data.setValue(null);
                    return;
                }
                if(!value.isEmpty()){
                    for(var ds : value.getDocumentChanges()){
                        if(ds.getType() == DocumentChange.Type.ADDED ||ds.getType() == DocumentChange.Type.MODIFIED || ds.getType() == DocumentChange.Type.REMOVED){
                            data.setValue(value.toObjects(HistoryModel.class));
                        }
                    }
                }else {
                    data.setValue(null);
                }
           });
        }
        return data;
    }
}
