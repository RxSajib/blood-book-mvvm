package com.blood.bloodbook.Network;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.blood.bloodbook.Data.DataManager;
import com.blood.bloodbook.Utils.Toast;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

public class DonorPhoneNumberExistsGET {

    private Application application;
    private MutableLiveData<Boolean> data;
    private FirebaseAuth Mauth;
    private CollectionReference DonorUserRef;

    public DonorPhoneNumberExistsGET(Application application) {
        this.application = application;
        Mauth = FirebaseAuth.getInstance();
        DonorUserRef = FirebaseFirestore.getInstance().collection(DataManager.DonorUser);
    }

    public LiveData<Boolean> DonorPhoneNumberIsExists(String PhoneNumber) {
        data = new MutableLiveData<>();
        var FirebaseUser = Mauth.getCurrentUser();
        if (FirebaseUser != null) {
            var q = DonorUserRef.whereEqualTo(DataManager.MainPhoneNumber, PhoneNumber);
            q.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                @Override
                public void onComplete(@NonNull Task<QuerySnapshot> task) {
                    if (task.isSuccessful()) {
                        var exists = task.getResult();
                        if(exists.isEmpty()){
                            data.setValue(false);
                        }else {
                            data.setValue(true);
                        }

                    } else {
                        data.setValue(false);
                        Toast.Message(application, task.getException().getMessage());
                    }
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    data.setValue(false);
                    Toast.Message(application, e.getMessage());
                }
            });
        }
        return data;
    }

}
