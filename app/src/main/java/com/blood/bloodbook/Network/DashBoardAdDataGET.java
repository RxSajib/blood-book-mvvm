package com.blood.bloodbook.Network;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.blood.bloodbook.AdModel;
import com.blood.bloodbook.Data.DataManager;

public class DashBoardAdDataGET {

    private android.app.Application application;
    private MutableLiveData<AdModel> data;
    private CollectionReference AdManagerRef;
    private FirebaseAuth Mauth;

    public DashBoardAdDataGET(Application application){
        this.application = application;
        AdManagerRef = FirebaseFirestore.getInstance().collection(DataManager.AdManager);
        Mauth = FirebaseAuth.getInstance();
    }

    public LiveData<AdModel> GetDashBoardAd(){
        data = new MutableLiveData<>();
        var FirebaseUser = Mauth.getCurrentUser();
        if(FirebaseUser != null){
            AdManagerRef.document(DataManager.DashBoard).addSnapshotListener((value, error) -> {
                if(error != null){
                    data.setValue(null);
                    return;
                }
                if(value.exists()){
                    data.setValue(value.toObject(AdModel.class));
                }else {
                    data.setValue(null);
                }
            });
        }
        return data;
    }
}
