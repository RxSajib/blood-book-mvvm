package com.blood.bloodbook.Network;

import android.app.Application;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.blood.bloodbook.Data.DataManager;
import com.blood.bloodbook.Utils.Toast;

public class ChatFileUploadPOST {

    private Application application;
    private MutableLiveData<String> data;
    private FirebaseAuth Mauth;
    private StorageReference FileRef;

    public ChatFileUploadPOST(Application application) {
        this.application = application;
        Mauth = FirebaseAuth.getInstance();
        FileRef = FirebaseStorage.getInstance().getReference().child(DataManager.MessageFile);
    }

    public LiveData<String> UploadFile(Uri FileUri, String FileType) {
        data = new MutableLiveData<>();

        var FirebaseUser = Mauth.getCurrentUser();
        if (FirebaseUser != null) {
            var StoreRef = FileRef.child(FileType).child(FileUri.getLastPathSegment()+String.valueOf(System.currentTimeMillis()));

            StoreRef.putFile(FileUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    if(taskSnapshot.getMetadata() != null){
                        if(taskSnapshot.getMetadata().getReference() != null){
                            var task = taskSnapshot.getStorage().getDownloadUrl();
                            task.addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    data.setValue(uri.toString());
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Toast.Message(application, e.getMessage());
                                    data.setValue(null);
                                }
                            });
                        }
                    }
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.Message(application, e.getMessage());
                    data.setValue(null);
                }
            });
        }
        return data;
    }
}
