package com.blood.bloodbook.Network;

import android.app.Application;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.blood.bloodbook.Data.DataManager;
import com.blood.bloodbook.OrganizationTypeModel;

import java.util.List;

public class OrganizationTypeDataGET {

    private Application application;
    private MutableLiveData<List<OrganizationTypeModel>> data;
    private CollectionReference OrganizationTypeRef;
    private FirebaseAuth Mauth;

    public OrganizationTypeDataGET(Application application){
        this.application = application;
        Mauth = FirebaseAuth.getInstance();
        OrganizationTypeRef = FirebaseFirestore.getInstance().collection(DataManager.OrganizationType);
    }

    public LiveData<List<OrganizationTypeModel>> GetOrganizationType(){
        data = new MutableLiveData<>();
        var FirebaseUser = Mauth.getCurrentUser();
        if(FirebaseUser != null){
            OrganizationTypeRef.addSnapshotListener(new EventListener<QuerySnapshot>() {
                @Override
                public void onEvent(@Nullable QuerySnapshot value, @Nullable FirebaseFirestoreException error) {
                    if(error != null){
                        data.setValue(null);
                        return;
                    }
                    if(!value.isEmpty()){
                        for(var ds : value.getDocumentChanges()){
                            if(ds.getType() == DocumentChange.Type.ADDED || ds.getType() == DocumentChange.Type.MODIFIED || ds.getType() == DocumentChange.Type.REMOVED){
                                data.setValue(value.toObjects(OrganizationTypeModel.class));
                            }
                        }
                    }else {
                        data.setValue(null);
                    }
                }
            });
        }
        return data;
    }
}
