package com.blood.bloodbook.Network;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.auth.FirebaseAuth;
import com.blood.bloodbook.Utils.Toast;

public class EmailSignUpAccountPOST {

    private Application application;
    private MutableLiveData<Boolean> data;
    private FirebaseAuth Mauth;

    public EmailSignUpAccountPOST(Application application){
        this.application = application;
        Mauth = FirebaseAuth.getInstance();
    }

    public LiveData<Boolean> SignUpWithEmail(String Email, String Password){
        data = new MutableLiveData<>();

        Mauth.createUserWithEmailAndPassword(Email, Password).addOnCompleteListener(task -> {
            if(task.isSuccessful()){
                data.setValue(true);
                Toast.Message(application, "SignUp success");
            }else {
                data.setValue(false);
                Toast.Message(application, task.getException().getMessage());
            }
        }).addOnFailureListener(e -> {
            data.setValue(false);
            Toast.Message(application, e.getMessage());
        });
        return data;
    }
}
