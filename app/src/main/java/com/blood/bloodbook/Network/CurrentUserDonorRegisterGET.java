package com.blood.bloodbook.Network;

import android.app.Application;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.blood.bloodbook.Data.DataManager;
import com.blood.bloodbook.DonorModel;

public class CurrentUserDonorRegisterGET {

    private Application application;
    private MutableLiveData<DonorModel> data;
    private CollectionReference DonorUerRef;
    private FirebaseAuth Mauth;

    public CurrentUserDonorRegisterGET(Application application){
        this.application = application;
        DonorUerRef = FirebaseFirestore.getInstance().collection(DataManager.DonorUser);
        Mauth = FirebaseAuth.getInstance();
    }

    public LiveData<DonorModel> GetCurrentDonorUser(){
        data = new MutableLiveData<>();
        var FirebaseUser = Mauth.getCurrentUser();
        if(FirebaseUser != null){
            DonorUerRef.document(FirebaseUser.getUid())
                    .addSnapshotListener((value, error) -> {
                        if(error != null){
                            data.setValue(null);
                            return;
                        }

                        if(value.exists()){
                            data.setValue(value.toObject(DonorModel.class));
                        }else {
                            data.setValue(null);
                        }
                    });
        }
        return data;
    }
}
