package com.blood.bloodbook.Network;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.blood.bloodbook.Utils.Toast;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class EmailSignUpAccount {

    private Application application;
    private MutableLiveData<Boolean> data;
    private FirebaseAuth Mauth;


    public EmailSignUpAccount(Application application){
        this.application = application;
        Mauth = FirebaseAuth.getInstance();
    }

    public LiveData<Boolean> SignUpAccount(String Email, String Password){
        data = new MutableLiveData<>();
        Mauth.createUserWithEmailAndPassword(Email, Password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    data.setValue(true);
                }else {
                    data.setValue(false);
                    Toast.Message(application, task.getException().getMessage());
                }
            }
        }).addOnFailureListener(e -> {
            data.setValue(false);
            Toast.Message(application, e.getMessage());
        });
        return data;
    }
}
