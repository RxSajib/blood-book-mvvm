package com.blood.bloodbook.Network;

import android.app.Application;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.blood.bloodbook.Data.DataManager;
import com.blood.bloodbook.Utils.Toast;

import java.util.HashMap;

public class DonorUserUpdatePUT {

    private Application application;
    private MutableLiveData<Boolean> data;
    private CollectionReference DonorUserRef;
    private FirebaseAuth Mauth;

    public DonorUserUpdatePUT(Application application){
        this.application = application;
        DonorUserRef = FirebaseFirestore.getInstance().collection(DataManager.DonorUser);
        Mauth = FirebaseAuth.getInstance();
    }

    public LiveData<Boolean> DonorUserUpdate(String Country, String Province, String District, String AreaName){
        data = new MutableLiveData<>();
        var FirebaseUser = Mauth.getCurrentUser();

        if(FirebaseUser != null){

            var map = new HashMap<String, Object>();
            map.put(DataManager.Country, Country);
            map.put(DataManager.Province, Province);
            map.put(DataManager.District, District);
            map.put(DataManager.AreaName, AreaName);

            DonorUserRef.document(FirebaseUser.getUid()).update(map).addOnCompleteListener(task -> {
                if(task.isSuccessful()){
                    data.setValue(true);
                }else {
                    data.setValue(false);
                    Toast.Message(application, task.getException().getMessage());
                }
            }).addOnFailureListener(e -> {
               data.setValue(false);
                Toast.Message(application, e.getMessage());
            });
        }
        return data;
    }
}
