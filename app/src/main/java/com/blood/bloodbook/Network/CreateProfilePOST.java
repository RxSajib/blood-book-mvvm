package com.blood.bloodbook.Network;

import android.app.Application;
import android.content.Context;
import android.net.wifi.WifiManager;
import android.text.format.Formatter;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.messaging.FirebaseMessaging;
import com.blood.bloodbook.Data.DataManager;
import com.blood.bloodbook.Data.SharePref;
import com.blood.bloodbook.Utils.Toast;

import java.util.HashMap;
import java.util.Locale;

public class CreateProfilePOST {

    private Application application;
    private MutableLiveData<Boolean> data;
    private CollectionReference ProfileRef;
    private FirebaseAuth Mauth;
    private SharePref sharePref;

    public CreateProfilePOST(Application application) {
        this.application = application;
        ProfileRef = FirebaseFirestore.getInstance().collection(DataManager.User);
        Mauth = FirebaseAuth.getInstance();
        sharePref = new SharePref(application);
    }

    public LiveData<Boolean> ProfilePOST(String FirstName, String MiddleName, String SureName, String FatherName, String ProfileImage, String Email, String LogInWith) {
        data = new MutableLiveData<>();
        var FirebaseUser = Mauth.getCurrentUser();
        if (FirebaseUser != null) {
            var wm = (WifiManager) application.getSystemService(Context.WIFI_SERVICE);
            var ip = Formatter.formatIpAddress(wm.getConnectionInfo().getIpAddress());


            ProfileRef.document(FirebaseUser.getUid()).get().addOnCompleteListener(task -> {
                if(task.isSuccessful()){
                    if(task.getResult().exists()){
                        FirebaseMessaging.getInstance().deleteToken().addOnCompleteListener(task1my -> {
                            if(task1my.isSuccessful()){
                                var newmap = new HashMap<String, Object>();
                                FirebaseMessaging.getInstance().getToken().addOnSuccessListener(s1 -> {
                                    newmap.put(DataManager.Token, s1);
                                    newmap.put(DataManager.IpAddress, ip);
                                    ProfileRef.document(FirebaseUser.getUid()).update(newmap).addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if(task.isSuccessful()){
                                                data.setValue(true);
                                                Toast.Message(application, "SignIn Success");
                                            }else {
                                                data.setValue(false);
                                                Toast.Message(application, task.getException().getMessage());
                                            }
                                        }
                                    }).addOnFailureListener(e -> {
                                        Toast.Message(application, e.getMessage());
                                        data.setValue(false);
                                    });
                                }).addOnFailureListener(e -> {
                                    Toast.Message(application, e.getMessage());
                                    data.setValue(false);
                                });

                            }else {
                                data.setValue(false);
                                Toast.Message(application, task1my.getException().getMessage());
                            }
                        }).addOnFailureListener(e -> {
                            Toast.Message(application, e.getMessage());
                            data.setValue(false);
                        });
                    }else {

                        var Timestamp = System.currentTimeMillis();
                        FirebaseMessaging.getInstance().getToken().addOnSuccessListener(s -> {
                            var map = new HashMap<String, Object>();
                            map.put(DataManager.FirstName, FirstName);
                            map.put(DataManager.MiddleName, MiddleName);
                            map.put(DataManager.Surname, SureName);
                            map.put(DataManager.FatherName, FatherName);
                            map.put(DataManager.ProfileImage, ProfileImage);
                            map.put(DataManager.Token, s);
                            map.put(DataManager.Email, Email);
                            map.put(DataManager.LoginWith, LogInWith);
                            map.put(DataManager.IpAddress, ip);
                            map.put(DataManager.Search, FirstName.toLowerCase(Locale.ROOT));
                            map.put(DataManager.LoginAddress, sharePref.GetData(DataManager.LoginAddress));
                            map.put(DataManager.LoginType, sharePref.GetData(DataManager.LoginType));
                            map.put(DataManager.DocumentKey, FirebaseUser.getUid());
                            map.put(DataManager.Timestamp, Timestamp);
                            map.put(DataManager.IsActive, true);
                            map.put(DataManager.IsRequestEnable, true);


                            ProfileRef.document(FirebaseUser.getUid()).get().addOnCompleteListener(task2 -> {
                                if (task2.isSuccessful()) {
                                    if (!task2.getResult().exists()) {
                                        ProfileRef.document(FirebaseUser.getUid())
                                                .set(map).addOnCompleteListener(task1 -> {
                                                    if (task1.isSuccessful()) {
                                                        data.setValue(true);
                                                        Toast.Message(application, "Success");
                                                    } else {
                                                        data.setValue(false);
                                                        Toast.Message(application, task1.getException().getMessage());
                                                    }
                                                }).addOnFailureListener(e -> {
                                                    data.setValue(false);
                                                    Toast.Message(application, e.getMessage());
                                                });
                                    }else {
                                        FirebaseMessaging.getInstance().deleteToken().addOnCompleteListener(task1my -> {
                                            if(task1my.isSuccessful()){
                                                var newmap = new HashMap<String, Object>();
                                                FirebaseMessaging.getInstance().getToken().addOnSuccessListener(s1 -> {
                                                    newmap.put(DataManager.Token, s1);
                                                    ProfileRef.document(FirebaseUser.getUid()).update(map).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                        @Override
                                                        public void onComplete(@NonNull Task<Void> task) {
                                                            if(task.isSuccessful()){
                                                                data.setValue(true);
                                                                Toast.Message(application, "SignIn Success");
                                                            }else {
                                                                data.setValue(false);
                                                                Toast.Message(application, task.getException().getMessage());
                                                            }
                                                        }
                                                    }).addOnFailureListener(e -> {
                                                        Toast.Message(application, e.getMessage());
                                                        data.setValue(false);
                                                    });
                                                }).addOnFailureListener(e -> {
                                                    Toast.Message(application, e.getMessage());
                                                    data.setValue(false);
                                                });

                                            }else {
                                                data.setValue(false);
                                                Toast.Message(application, task1my.getException().getMessage());
                                            }
                                        }).addOnFailureListener(e -> {
                                            Toast.Message(application, e.getMessage());
                                            data.setValue(false);
                                        });

                                    }
                                } else {
                                    Toast.Message(application, task.getException().getMessage());
                                    data.setValue(false);
                                }
                            }).addOnFailureListener(e -> {
                                Toast.Message(application, e.getMessage());
                                data.setValue(false);
                            });
                        });

                    }
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    data.setValue(false);
                    Toast.Message(application, e.getMessage());
                }
            });

        }
        return data;
    }
}
