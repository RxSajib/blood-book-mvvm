package com.blood.bloodbook.Network;

import android.app.Application;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.blood.bloodbook.Data.DataManager;
import com.blood.bloodbook.Utils.Toast;

public class MyBloodRequestDelete {

    private Application application;
    private MutableLiveData<Boolean> data;
    private FirebaseAuth Mauth;
    private CollectionReference BloodRequestRef;

    public MyBloodRequestDelete(Application application){
        this.application = application;
        Mauth = FirebaseAuth.getInstance();
        BloodRequestRef = FirebaseFirestore.getInstance().collection(DataManager.BloodRequest);
    }

    public LiveData<Boolean> BloodRequestDelete(long DocumentID){
        data = new MutableLiveData<>();
        var FirebaseUser = Mauth.getCurrentUser();
        if(FirebaseUser != null){
            BloodRequestRef.document(String.valueOf(DocumentID))
                    .delete().addOnCompleteListener(task -> {
                        if(task.isSuccessful()){
                            data.setValue(true);
                        }else {
                            data.setValue(false);
                            Toast.Message(application, task.getException().getMessage());
                        }
                    }).addOnFailureListener(e -> {
                       data.setValue(false);
                        Toast.Message(application, e.getMessage());
                    });
        }
        return data;
    }
}
