package com.blood.bloodbook.Network;

import android.app.Application;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.blood.bloodbook.Data.DataManager;
import com.blood.bloodbook.Utils.Toast;
import java.util.HashMap;

public class BloodRequestNotification{

    private Application application;
    private MutableLiveData<Boolean> data;
    private CollectionReference NotificationRef;
    private FirebaseAuth Mauth;

    public BloodRequestNotification(Application application){
        this.application = application;
        Mauth = FirebaseAuth.getInstance();
        NotificationRef = FirebaseFirestore.getInstance().collection(DataManager.Notification);
    }

    public LiveData<Boolean> SendBloodRequestNotification(long DocumentKey, String Message, String MessageTopic, String MessageType, String ReceiverUID, String SenderUID){
        data = new MutableLiveData<>();
        var FirebaseUser = Mauth.getCurrentUser();
        if(FirebaseUser != null){



            NotificationRef.document(String.valueOf(DocumentKey)).get().addOnCompleteListener(task -> {
                if(task.isSuccessful()){
                    if(task.getResult().exists()){
                        var Timestamp = System.currentTimeMillis();
                        var map = new HashMap<String, Object>();
                        map.put(DataManager.DocumentKey, DocumentKey);
                        map.put(DataManager.Message, String.valueOf(Integer.valueOf(task.getResult().getString(DataManager.Message)) + Integer.valueOf(Message)));
                        map.put(DataManager.MessageTopic, MessageTopic);
                        map.put(DataManager.MessageType, MessageType);
                        map.put(DataManager.ReceiverUID, ReceiverUID);
                        map.put(DataManager.SenderUID, SenderUID);
                        map.put(DataManager.Timestamp, Timestamp);

                        NotificationRef.document(String.valueOf(DocumentKey))
                                .set(map).addOnCompleteListener(task3 -> {
                                    if(task3.isSuccessful()){
                                        data.setValue(true);
                                    }else {
                                        data.setValue(false);
                                        Toast.Message(application, task3.getException().getMessage());
                                    }
                                }).addOnFailureListener(e -> {
                                    data.setValue(false);
                                    Toast.Message(application, e.getMessage());
                                });
                    }else {
                        var Timestamp = System.currentTimeMillis();
                        var map = new HashMap<String, Object>();
                        map.put(DataManager.DocumentKey, DocumentKey);
                        map.put(DataManager.Message, Message);
                        map.put(DataManager.MessageTopic, MessageTopic);
                        map.put(DataManager.MessageType, MessageType);
                        map.put(DataManager.ReceiverUID, ReceiverUID);
                        map.put(DataManager.SenderUID, SenderUID);
                        map.put(DataManager.Timestamp, Timestamp);

                        NotificationRef.document(String.valueOf(DocumentKey))
                                .set(map).addOnCompleteListener(task4 -> {
                                    if(task4.isSuccessful()){
                                        data.setValue(true);
                                    }else {
                                        data.setValue(false);
                                        Toast.Message(application, task4.getException().getMessage());
                                    }
                                }).addOnFailureListener(e -> {
                                    data.setValue(false);
                                    Toast.Message(application, e.getMessage());
                                });
                    }
                }else {
                    Toast.Message(application, task.getException().getMessage());
                }
            }).addOnFailureListener(e -> {
                Toast.Message(application, e.getMessage());
            });

        }
        return data;
    }

}
