package com.blood.bloodbook.Network;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.blood.bloodbook.Data.DataManager;
import com.blood.bloodbook.Utils.Toast;

import java.util.HashMap;

public class BloodRequestNotificationPOST {

    private Application application;
    private MutableLiveData<Boolean> data;
    private CollectionReference NotifiactionRef;
    private FirebaseAuth Mauth;

    public BloodRequestNotificationPOST(Application application){
        this.application = application;
        NotifiactionRef = FirebaseFirestore.getInstance().collection(DataManager.Notification);
        Mauth = FirebaseAuth.getInstance();
    }

    public LiveData<Boolean> BloodNotificationPOST(String ReceiverUID, String BloodQuantity, String BloodRequestID){
        data = new MutableLiveData<>();
        var FirebaseUser = Mauth.getCurrentUser();
        if(FirebaseUser != null){
            var Timestamp = System.currentTimeMillis();
            var map = new HashMap<String, Object>();
            map.put(DataManager.Message, "");
            map.put(DataManager.QuantityOfBlood, BloodQuantity);
            map.put(DataManager.SenderUID, FirebaseUser.getUid());
            map.put(DataManager.ReceiverUID, ReceiverUID);
            map.put(DataManager.MessageType, DataManager.BloodRequest);
            map.put(DataManager.BloodRequestID, BloodRequestID);

            NotifiactionRef.document(String.valueOf(Timestamp))
                    .set(map).addOnCompleteListener(task -> {
                        if(task.isSuccessful()){
                            data.setValue(true);
                        }else {
                            data.setValue(false);
                            Toast.Message(application, task.getException().getMessage());
                        }
                    }).addOnFailureListener(e -> {
                       data.setValue(false);
                        Toast.Message(application, e.getMessage());
                    });
        }
        return data;
    }
}
