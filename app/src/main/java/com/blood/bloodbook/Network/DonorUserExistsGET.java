package com.blood.bloodbook.Network;

import android.app.Application;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.blood.bloodbook.Data.DataManager;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;

public class DonorUserExistsGET {

    private Application application;
    private MutableLiveData<Boolean> data;
    private FirebaseAuth Mauth;
    private CollectionReference DonorUserRef;

    public DonorUserExistsGET(Application application){
        this.application = application;
        Mauth = FirebaseAuth.getInstance();
        DonorUserRef = FirebaseFirestore.getInstance().collection(DataManager.DonorUser);
    }

    public LiveData<Boolean> DonorUserExists(){
        data = new MutableLiveData<>();
        var FirebaseUser = Mauth.getCurrentUser();
        if(FirebaseUser != null){
            DonorUserRef.document(FirebaseUser.getUid())
                    .get().addOnSuccessListener(documentSnapshot -> {
                        if(documentSnapshot.exists()){
                            data.setValue(true);
                        }else {
                            data.setValue(false);
                        }
                    }).addOnFailureListener(e -> {
                        data.setValue(false);
                    });
        }
        return data;
    }
}
