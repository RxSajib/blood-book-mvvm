package com.blood.bloodbook;

import lombok.Data;

@Data
public class Bloodbank {
    private String BloodCategory, BloodGroup, DonationDate, DonorName, DonorPhoneNumber, ExpiryDate, HospitalLogo, HospitalName, Location, Quantity, Status;
    private String TagNumber, UID;
    private long Timestamp, DocumentID;
}
