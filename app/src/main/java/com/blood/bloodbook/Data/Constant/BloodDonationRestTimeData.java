package com.blood.bloodbook.Data.Constant;

import com.blood.bloodbook.BloodDonationRestTimeModel;
import com.blood.bloodbook.Data.DataManager;

import java.util.ArrayList;
import java.util.List;

public class BloodDonationRestTimeData {

    public static List<BloodDonationRestTimeModel> list = new ArrayList<>();

    public static List<BloodDonationRestTimeModel> GetBloodDonationRestTime() {

        list.clear();
        var ThreeMonth = new BloodDonationRestTimeModel(DataManager.ThreeMonth);
        var FourMonth = new BloodDonationRestTimeModel(DataManager.FourMonth);
        var FiveMonth = new BloodDonationRestTimeModel(DataManager.FiveMonth);
        var SixMonth = new BloodDonationRestTimeModel(DataManager.SixMonth);
        var SevenMonth = new BloodDonationRestTimeModel(DataManager.SevenMonth);
        var EightMonth = new BloodDonationRestTimeModel(DataManager.EightMonth);
        var NineMonth = new BloodDonationRestTimeModel(DataManager.NineMonth);
        var TenMonth = new BloodDonationRestTimeModel(DataManager.TenMonth);
        var ElevenMonth = new BloodDonationRestTimeModel(DataManager.ElevenMonth);
        var TwelveMonth = new BloodDonationRestTimeModel(DataManager.TwelveMonth);

        list.add(ThreeMonth);
        list.add(FourMonth);
        list.add(FiveMonth);
        list.add(SixMonth);
        list.add(SevenMonth);
        list.add(EightMonth);
        list.add(NineMonth);
        list.add(TenMonth);
        list.add(ElevenMonth);
        list.add(TwelveMonth);

        return list;
    }
}
