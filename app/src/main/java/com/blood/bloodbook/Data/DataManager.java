package com.blood.bloodbook.Data;

import javax.xml.transform.sax.SAXResult;

public class DataManager {

    public static final String RegisterAsFriend = "RegisterAsFriend";
    public static final String IsRequestEnable = "IsRequestEnable";
    public static final String RegistrationType = "RegistrationType";
    public static final String MainPhoneNumber = "MainPhoneNumber";
    public static final String MessageHistory = "MessageHistory";
    public static final String Admin = "Admin";
    public static final String Message = "Message";
    public static final String FirstTitleOneBoardingScreen = "WELCOME TO BLOOD BOOK";
    public static final String FirstDetailsOneBoardingScreen = "Your one place for finding blood quickly. Aim: Saving Lives, Saving Humanity This application is owned by: Mohammad Naeem Ahady";

    public static final String SecondTitleOneBoardingScreen = "Founder (بنسټګر):";
    public static final String SecondDetailsOneBoardingScreen = "Mohammad Naeem Ahady Bio: Afghan living in United Kingdom MBA (Finance) UK , 2015";

    public static final String ThirdTitleOneBoardingScreen = "Founder’s message:";
    public static final String ThirdDetailsOneBoardingScreen = "1- Donate blood and save lives. 2- Blood donation is free and nobody can demand anything whatsoever from you. 3- If anybody from the application asks you for money or anything whatsoever, please report him/her through their profile in find donors’ list and we will take strict action. Your cooperation is highly appreciable. Thanks";

    public static final String FourthTitleOneBoardingScreen = "Blood Group Types:";
    public static final String FourthDetailsOneBoardingScreen = "There are eight (8) types of general blood groups which are as follows: 1. A+ (ARH+) 2. A- (ARH-) 3. B+ (BRH+) 4. B- (BRH-) 5. AB+ (ABRH+) 6. AB- (ABRH-) 7. O+ (ORH+) 8. O- (ORH-) Notes: a- One person can donate up to 500 cc in the time duration of minimum three months. b- Minimum age for donation is 18 years and maximum age is 65 years. c- Weight must be minimum 55 Kilo grams (Kgs)";

    public static final String FiveTitleOneBoardingScreen = "Find Donors";
    public static final String FiveDetailsOneBoardingScreen = "This is where you can find list of all donors. This will help you in finding blood based on the blood group types and location quickly.";

    public static final String SixthTitleOneBoardingScreen = "Blood Banks";
    public static final String SixDetailsOneBoardingScreen = "You don’t need to physically go to each hospital to check for blood availability. With this application, now you can easily see each hospital’s blood stock from your phone. This will also open a new era of blood delivery from hospital to hospital in Afghanistan and around the world to save lives and time.";

    public static final String SevenTitleOneBoardingScreen = "Blood Requests";
    public static final String SevenDetailsOneBoardingScreen = "Here you can request blood from donors near you or accept blood requests from patients near you. If you accept blood request and still couldn’t go to patient, you can send request back so that other people can accept it. No pressure at all.";

    public static final String EightTitleOneBoardingScreen = "Lost and Found";
    public static final String EightDetailsOneBoardingScreen = "If you lose anything right from your dear ones such as relatives or mobile phones, cars, etc, or you find something, you can easily put ads here. This will help greatly in reducing life deaths because of mobile snatching and car thefts. Our main aim is to help saving as many lives as we can.";

    public static final String TypeRegisterAsFriend = "RegisterAsFriend";
    public static final String TypeRegisterAsDonor = "RegisterAsDonor";
    public static final String ShowToOther = "ShowToOther";
    public static final String Category = "Category";
    public static final String High = "high";
    public static final String LoginWith = "LoginWith";
    public static final String BloodRequestID = "BloodRequestID";
    public static final String BloodRequestTimestamp = "BloodRequestTimestamp";
    public static final String PhoneNumber = "PhoneNumber";
    public static final String BloodRequiredDate = "BloodRequiredDate";
    public static final String BloodRequiredTime = "BloodRequiredTime";
    public static final String RegisterAsDonor = "RegisterAsDonor";
    public static final String Type = "Type";
    public static final String BloodRequestAccept = "BloodRequestAccept";
    public static final String Report = "Report";
    public static final String User = "User";
    public static final String FirstName = "FirstName";
    public static final String MiddleName = "MiddleName";
    public static final String Surname = "Surname";
    public static final String FatherName = "FatherName";
    public static final String ProfileImage = "ProfileImage";
    public static final String Email = "Email";
    public static final String Token = "Token";
    public static final String Data = "Data";
    public static final String Text = "Text";
    public static final String ChatTextMessage = "ChatTextMessage";
    public static final String Broadcast = "Broadcast";
    public static final String UserBroadcastMessage = "UserBroadcastMessage";
    public static final String VideoAds = "VideoAds";

    public static final String TimeFormat = "HH:mm a";
    public static final String DateFormat = "dd MMM yyyy";

    public static final String ProfileSetting = "Profile setting";
    public static final String RegisterAsADonor = "Register as a donor";
    public static final String Registerdonationdate = "Register donation date";
    public static final String History = "History";
    public static final String Feelingsick = "Feeling sick";
    public static final String UpdateLocation = "Update Location";
    public static final String NotificationSetting = "Notification Setting";
    public static final String Blooddonationmonthlygap = "Blood Donation Rest Time";
    public static final String AddBlooddonorfriend = "Add Blood donor friend";
    public static final String AboutUS = "About us";
    public static final String Optoutofblooddonation = "Opt out of blood donation";
    public static final String LogOut = "Logout";
    public static final String BloodRequest = "BloodRequest";
    public static final String Timestamp = "Timestamp";
    public static final String DocumentKey = "DocumentKey";
    public static final String FullDetails = "FullDetails";

    public static final String MessageTopic = "MessageTopic";
    public static final String AcceptedQuantity = "AcceptedQuantity";
    public static final String Send = "Send";
    public static final String Receive = "Receive";
    public static final String DeliveryStatus = "DeliveryStatus";
    public static final String Search = "Search";

    public static final String PatientName = "PatientName";
    public static final String Age = "Age";
    public static final String BloodGroup = "BloodGroup";
    public static final String BloodCondition = "BloodCondition";
    public static final String Country = "Country";
    public static final String Province = "Province";
    public static final String Provance = "Provance";
    public static final String District = "District";
    public static final String BloodNeedFor = "BloodNeedFor";
    public static final String NumberOFQuantityPints = "NumberOFQuantityPints";
    public static final String AttendantContactNumber = "AttendantContactNumber";
    public static final String HospitalName = "HospitalName";
    public static final String ExchangePossibility = "ExchangePossibility";
    public static final String TransportAvailability = "TransportAvailability";
    public static final String TypeOFHBLevel = "TypeOfHbLevel";
    public static final String TypeOfPatientsCondition = "TypeOfPatientsCondition";
    public static final String SenderUID = "SenderUID";
    public static final String ReceiverUID = "ReceiverUID";
    public static final String Status = "Status";
    public static final String Request = "Request";
    public static final String Location = "Location";
    public static final String CountryName = "CountryName";
    public static final String TimetoAM_PMpattern = "hh:mm a";
    public static final String DatePattern = "yyyy-MM-dd";
    public static final String DonorUser = "DonorUser";

    public static final String GivenName = "GivenName";
    public static final String Width = "Width";
    public static final String AreaName = "AreaName";
    public static final String RoshanNumber = "RoshanNumber";
    public static final String EtisalatNumber = "EtisalatNumber";
    public static final String AWCCNumber = "AWCCNumber";
    public static final String MTNNumber = "MTNNumber";
    public static final String WhatsappNumber = "WhatsappNumber";
    public static final String EmailAddress = "EmailAddress";
    public static final String DonateMonth = "DonateMonth";
    public static final String LastDonationDate = "LastDonationDate";
    public static final String EnableMyNumber = "EnableMyNumber";
    public static final String DonationMonthlyGap = "DonationMonthlyGap";
    public static final String LastDonationDayCount = "LastDonationDayCount";
    public static final String Call = "Call";
    public static final String Code = "Code";
    public static final String LoginType = "LoginType";
    public static final String LoginAddress = "LoginAddress";
    public static final String RegisterDonation = "RegisterDonation";

    public static final String BloodDonationDate = "BloodDonationDate";
    public static final String QuantityOfBlood = "QuantityOfBlood";
    public static final String RegisterDonationBloodImage = "RegisterDonationBloodImage";

    public static final String MSG_CONTENT_TYPE = "Content-Type";
    public static final String MSG_CONTENT_TYPE_VAL = "application/json";
    public static final String MSG_AUTHORIZATION = "Authorization";
    public static final String AuthorizationKey = "key=AAAATcRmlZs:APA91bHK4tfNRdP_JvVnhoJGp0GAq2Mbmoiw3Ol8FANWun5UOPUTOwIcAmjApO4bK0cTX8YU0xZw5zegcTJzZq5_7MJHNZrLJaVx7u2T5LCTFpEogHv3QcS8tKpeR49m8LrXXxbcV4ae";
    public static final String MessagingBASEURL = "https://fcm.googleapis.com/";
    public static final String IsActive = "IsActive";
    public static final String ContactWhatsaAppNumber = "00447990303248";


    public static final String MessageFile = "MessageFile";
    public static final String Image = "Image";
    public static final String FileName = "FileName";
    public static final String FileExtension = "FileExtension";
    public static final String FileSize = "FileSize";
    public static final String JPG = ".jpg";
    public static final String PDF = ".pdf";
    public static final String DOC = ".doc";
    public static final String DOCX = ".docx";
    public static final String XLS = ".xls";
    public static final String XLSX = ".xlsx";

    public static final String PDFFILE = "Pdf";
    public static final String DOCFILE = "Doc";
    public static final String DOCXFILE = "Docx";
    public static final String XLSFILE = "Xls";
    public static final String XLSXFILE = "Xlsx";
    public static final String  Notification = "Notification";
    public static final String MessageType = "MessageType";
    public static final String Organization = "Organization";
    public static final String OrganizationType = "OrganizationType";
    public static final String NameOfOrganization = "NameOfOrganization";
    public static final String LostAndFound = "LostAndFound";
    public static final String Title = "Title";
    public static final String LostAndFoundType = "LostAndFoundType";

    public static final String APlus = "A+";
    public static final String AMinus = "A-";
    public static final String BPlus = "B+";
    public static final String BMinus = "B-";
    public static final String OPlus = "O+";
    public static final String OMinus = "O-";
    public static final String ABPlus = "AB+";
    public static final String ABMinus = "AB-";


    public static final String ARHPlus = "ARH+";
    public static final String ARHMinus = "ARH-";
    public static final String BRHPlus = "BRH+";
    public static final String BRHMinus = "BRH-";
    public static final String ORHPlus = "ORH+";
    public static final String ORHMinus = "ORH-";
    public static final String ABRHPlus = "ABRH+";
    public static final String ABRHMinus = "ABRH-";
    public static final String Hospital = "Hospital";
    public static final String BloodStorage = "BloodStorage";
    public static final String UID = "UID";
    public static final String AdManager = "AdManager";
    public static final String DashBoard = "DashBoard";
    public static final String IpAddress = "IpAddress";
    public static final String RegisterDonor = "RegisterDonor";
    public static final String MyBloodRequest = "MyBloodRequest";
    public static final String Accept = "Accept";
    public static final String DocumentID = "DocumentID";

    public static final String ThreeMonth = "3 Month";
    public static final String FourMonth = "4 Month";
    public static final String FiveMonth = "5 Month";
    public static final String SixMonth = "6 Month";
    public static final String SevenMonth = "7 Month";
    public static final String EightMonth = "8 Month";
    public static final String NineMonth = "9 Month";
    public static final String TenMonth = "10 Month";
    public static final String ElevenMonth = "11 Month";
    public static final String TwelveMonth = "12 Month";


    public static final String AppOpen  = "ca-app-pub-3940256099942544/3419835294";
}