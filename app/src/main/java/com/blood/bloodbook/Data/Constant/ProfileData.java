package com.blood.bloodbook.Data.Constant;

import com.blood.bloodbook.Data.DataManager;
import com.blood.bloodbook.Data.ProfileDataModel;
import com.blood.bloodbook.R;

import java.util.ArrayList;
import java.util.List;

public class ProfileData {

    private static List<ProfileDataModel> list;

    public static List<ProfileDataModel> GetProfileData() {

        if (list == null) {
            list = new ArrayList<>();

            var Profile = new ProfileDataModel(DataManager.ProfileSetting, R.drawable.ic_profile);
            var RegisterAsADonor = new ProfileDataModel(DataManager.RegisterAsADonor, R.drawable.ic_donar);
            var RegisterDonctionDate = new ProfileDataModel(DataManager.Registerdonationdate, R.drawable.ic_calender);
            var History = new ProfileDataModel(DataManager.History, R.drawable.ic_history);
            var FeelingSick = new ProfileDataModel(DataManager.Feelingsick, R.drawable.ic_sick);
            var TrackLocation = new ProfileDataModel(DataManager.UpdateLocation, R.drawable.ic_location);
            var BloodDonactionMonthlyGap = new ProfileDataModel(DataManager.Blooddonationmonthlygap, R.drawable.ic_gap);
            var Optoutofblooddonation = new ProfileDataModel(DataManager.Optoutofblooddonation, R.drawable.ic_optout);
            var AddBlooddonerfriend = new ProfileDataModel(DataManager.AddBlooddonorfriend, R.drawable.ic_addfriend);
            var AboutUS = new ProfileDataModel(DataManager.AboutUS, R.drawable.ic_aboutus);
            var LogOut = new ProfileDataModel(DataManager.LogOut, R.drawable.ic_logout);

            list.add(Profile);
            list.add(RegisterAsADonor);
            list.add(RegisterDonctionDate);
            list.add(History);
            list.add(FeelingSick);
            list.add(TrackLocation);
            list.add(BloodDonactionMonthlyGap);
            list.add(Optoutofblooddonation);
            list.add(AddBlooddonerfriend);
            list.add(AboutUS);
            list.add(LogOut);

        }

        return list;
    }
}
