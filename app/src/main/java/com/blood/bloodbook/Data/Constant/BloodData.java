package com.blood.bloodbook.Data.Constant;

import com.blood.bloodbook.BloodModel;
import com.blood.bloodbook.Data.DataManager;

import java.util.ArrayList;
import java.util.List;

public class BloodData {

    private static  List<BloodModel> list;
    public static List<BloodModel> GetBlood(String Province){


        //todo 550 USD

        if(list == null) {
            list = new ArrayList<>();
            list.clear();
            var aplus = new BloodModel(DataManager.APlus, Province, DataManager.ARHPlus);
            var aminus = new BloodModel(DataManager.AMinus, Province, DataManager.ARHMinus);
            var bplus = new BloodModel(DataManager.BPlus, Province, DataManager.BRHPlus);
            var bminus = new BloodModel(DataManager.BMinus, Province, DataManager.BRHMinus);
            var oplus = new BloodModel(DataManager.OPlus, Province, DataManager.ORHPlus);
            var ominus = new BloodModel(DataManager.OMinus, Province, DataManager.ORHMinus);
            var abplus = new BloodModel(DataManager.ABPlus, Province, DataManager.ABRHPlus);
            var abminus = new BloodModel(DataManager.ABMinus, Province, DataManager.ABRHMinus);

            list.add(aplus);
            list.add(aminus);
            list.add(bplus);
            list.add(bminus);
            list.add(oplus);
            list.add(ominus);
            list.add(abplus);
            list.add(abminus);

        }

        return list;
    }
}
