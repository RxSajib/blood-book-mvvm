package com.blood.bloodbook.Data;

import com.google.gson.annotations.SerializedName;

import lombok.Data;

@Data
public class Notification {

    @SerializedName("body")
    private String Body;

    @SerializedName("title")
    private String Title;


}
