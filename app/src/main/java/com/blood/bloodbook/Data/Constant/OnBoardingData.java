package com.blood.bloodbook.Data.Constant;

import com.blood.bloodbook.Data.DataManager;
import com.blood.bloodbook.Data.OnBoardingModel;
import com.blood.bloodbook.R;
import java.util.ArrayList;
import java.util.List;

public class OnBoardingData {

    public static List<OnBoardingModel> AddOnBoardingData(){
        List<OnBoardingModel> list = new ArrayList<>();


        var one = new OnBoardingModel();
        one.setTitle(DataManager.FirstTitleOneBoardingScreen);
        one.setDetails(DataManager.FirstDetailsOneBoardingScreen);
        one.setImage(R.drawable.bloodbook);

        var two = new OnBoardingModel();
        two.setTitle(DataManager.SecondTitleOneBoardingScreen);
        two.setDetails(DataManager.SecondDetailsOneBoardingScreen);
        two.setImage(R.drawable.foundertwo);

        var three = new OnBoardingModel();
        three.setTitle(DataManager.ThirdTitleOneBoardingScreen);
        three.setDetails(DataManager.ThirdDetailsOneBoardingScreen);
        three.setImage(R.drawable.thiredonboardingimage);

        var four = new OnBoardingModel();
        four.setTitle(DataManager.FourthTitleOneBoardingScreen);
        four.setDetails(DataManager.FourthDetailsOneBoardingScreen);
        four.setImage(R.drawable.fourthonboardingimage);

        var five = new OnBoardingModel();
        five.setTitle(DataManager.FiveTitleOneBoardingScreen);
        five.setDetails(DataManager.FiveDetailsOneBoardingScreen);
        five.setImage(R.drawable.splashscreenfinddonor);

        var six = new OnBoardingModel();
        six.setTitle(DataManager.SixthTitleOneBoardingScreen);
        six.setDetails(DataManager.SixDetailsOneBoardingScreen);
        six.setImage(R.drawable.splashscreenbloodbank);

        var seven = new OnBoardingModel();
        seven.setTitle(DataManager.SevenTitleOneBoardingScreen);
        seven.setDetails(DataManager.SevenDetailsOneBoardingScreen);
        seven.setImage(R.drawable.splashscreenbloodrequest);

        var eight = new OnBoardingModel();
        eight.setTitle(DataManager.EightTitleOneBoardingScreen);
        eight.setDetails(DataManager.EightDetailsOneBoardingScreen);
        eight.setImage(R.drawable.splashscreenlostandfound);


        list.add(one);
        list.add(two);
        list.add(three);
        list.add(four);
        list.add(five);
        list.add(six);
        list.add(seven);
        list.add(eight);


        return list;
    }
}
