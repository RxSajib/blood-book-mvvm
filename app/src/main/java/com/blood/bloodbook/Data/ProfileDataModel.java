package com.blood.bloodbook.Data;

import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class ProfileDataModel {

    private @NonNull String Title;
    private @NonNull int Icon;
}
