package com.blood.bloodbook.Data;

import com.google.gson.annotations.SerializedName;

public class Data {
    @SerializedName("body")
    private String body;

    @SerializedName("title")
    private String title;

    @SerializedName("image")
    private String image;

    @SerializedName("icon")
    private String icon;


    public Data(String body, String title, String image, String icon) {
        this.body = body;
        this.title = title;
        this.image = image;
        this.icon = icon;
    }

    public Data(String body, String title) {
        this.body = body;
        this.title = title;
    }
}
