package com.blood.bloodbook.Data;

import lombok.Data;

@Data
public class OnBoardingModel {

    private String Title;
    private String Details;
    private int Image;
    private int introheading_image;
}
