package com.blood.bloodbook.Data;

import com.google.gson.annotations.SerializedName;

public class NotifactionResponse {

    @SerializedName("notification")
    public  Data data;

    @SerializedName("to")
    public String to;

    @SerializedName("content_available")
    private boolean content_available;

    @SerializedName("priority")
    private String priority;

    public NotifactionResponse(Data data, String to) {
        this.data = data;
        this.to = to;
    }

    public NotifactionResponse(Data data, String to, boolean content_available, String priority) {
        this.data = data;
        this.to = to;
        this.content_available = content_available;
        this.priority = priority;
    }
}
