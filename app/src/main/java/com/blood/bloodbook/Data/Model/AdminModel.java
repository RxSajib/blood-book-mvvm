package com.blood.bloodbook.Data.Model;

import lombok.Data;

@Data
public class AdminModel {

    private String Admin;
    private String Email;
    private String Name;
    private String Password;
    private String PhotoUri;
    private String Token;
    private String UID;
}
