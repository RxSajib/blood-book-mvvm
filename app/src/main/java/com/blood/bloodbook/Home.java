package com.blood.bloodbook;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.blood.bloodbook.Network.ViewModel.ViewModel;
import com.blood.bloodbook.UI.HomeFragment.Dashboard;
import com.blood.bloodbook.UI.HomeFragment.World;
import com.blood.bloodbook.UI.HomeFragment.Profile;
import com.blood.bloodbook.UI.HomeFragment.Request;
import com.blood.bloodbook.Utils.HandleActivity;
import com.blood.bloodbook.databinding.HomeBinding;

public class Home extends AppCompatActivity {

    private HomeBinding binding;
    private com.blood.bloodbook.Network.ViewModel.ViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.home);
        viewModel = new ViewModelProvider(this).get(ViewModel.class);


        TransactionFragment(new Dashboard());
        SendMessageNotification();
    }

    private void SendMessageNotification(){

        TransactionFragment(new Dashboard());

        binding.NiceBottomBar.setOnItemSelected(integer -> {
            if(integer == 0){
                TransactionFragment(new Dashboard());
            }
            if(integer == 1){
                TransactionFragment(new Request());
            }
            if(integer == 2){
                TransactionFragment(new World());
            }
            if(integer == 3){
                TransactionFragment(new Profile());
            }
            return null;
        });
    }

    private void TransactionFragment(Fragment fragment) {
        var transctation = getSupportFragmentManager().beginTransaction();
        transctation.replace(R.id.FrameLayout, fragment);
        transctation.commit();
    }

    @Override
    protected void onStart() {
        super.onStart();
        viewModel.CheckUserExists().observe(Home.this, aBoolean -> {
            if (!aBoolean) {
                HandleActivity.GotoSignIn(Home.this);
                Animatoo.animateSlideLeft(Home.this);
                finish();
            }
        });
    }
}