package com.blood.bloodbook.UI.Adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.blood.bloodbook.HospitalModel;
import com.blood.bloodbook.UI.ViewHolder.HospitalViewHolder;
import com.blood.bloodbook.databinding.HospitalitemBinding;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import lombok.Getter;
import lombok.Setter;
@Singleton
public class HospitalAdapter extends RecyclerView.Adapter<HospitalViewHolder> {
    @Setter @Getter
    private List<HospitalModel> list;
    private OnClick OnClick;

    @Inject
    public HospitalAdapter(){

    }

    @NonNull
    @Override
    public HospitalViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        var l = LayoutInflater.from(parent.getContext());
        var v = HospitalitemBinding.inflate(l, parent, false);
        return new HospitalViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull HospitalViewHolder holder, int position) {
        holder.binding.setHospitalData(list.get(position));

        holder.itemView.setOnClickListener(view -> {
            OnClick.Click(list.get(position));
        });
    }

    @Override
    public int getItemCount() {
        if(list == null) {
            return 0;
        }return list.size();
    }


    public interface OnClick{
        void Click(HospitalModel hospitalModel);
    }
    public void OnClickEvent(OnClick OnClick){
        this.OnClick = OnClick;
    }
}
