package com.blood.bloodbook.UI.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.blood.bloodbook.Data.ProvinceNameModel;
import com.blood.bloodbook.UI.ViewHolder.ProvinceOfAFGViewHolder;
import com.blood.bloodbook.databinding.ProvinceoffghanistanitemBinding;

import java.util.List;

import javax.inject.Inject;

import lombok.Getter;
import lombok.Setter;

public class ProvinceOfAfgAdapter extends RecyclerView.Adapter<ProvinceOfAFGViewHolder> {

    @Inject
    public ProvinceOfAfgAdapter(){
    }

    @Setter
    @Getter
    private List<ProvinceNameModel> list;
    private OnClick OnClick;

    @NonNull
    @Override
    public ProvinceOfAFGViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        var l = LayoutInflater.from(parent.getContext());
        var v = ProvinceoffghanistanitemBinding.inflate(l, parent, false);
        return new ProvinceOfAFGViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ProvinceOfAFGViewHolder holder, int position) {
        holder.binding.ProvinceName.setText(list.get(position).getProvance());
        holder.binding.LocationName.setVisibility(View.GONE);

        holder.itemView.setOnClickListener(view -> {
            OnClick.Click(list.get(position).getProvance());
        });
    }

    @Override
    public int getItemCount() {
        if (list == null) {
            return 0;
        }return list.size();
    }

    public interface OnClick{
        void Click(String Province);
    }
    public void OnClickState(OnClick OnClick){
        this.OnClick = OnClick;
    }
}
