package com.blood.bloodbook.UI.Adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.blood.bloodbook.OrganizationModel;
import com.blood.bloodbook.UI.ViewHolder.TelephonyManagerViewHolder;
import com.blood.bloodbook.databinding.TelephonymanageritemBinding;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import lombok.Getter;
import lombok.Setter;
@Singleton
public class TelephonyManagerAdapter extends RecyclerView.Adapter<TelephonyManagerViewHolder> {

    @Inject
    public TelephonyManagerAdapter() {
    }

    @Setter
    @Getter
    private List<OrganizationModel> list;
    private OnClick OnClick;

    @NonNull
    @Override
    public TelephonyManagerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        var l = LayoutInflater.from(parent.getContext());
        var v = TelephonymanageritemBinding.inflate(l, parent, false);
        return new TelephonyManagerViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull TelephonyManagerViewHolder holder, int position) {
        holder.binding.setOrganization(list.get(position));

        holder.binding.CallNowBtn.setOnClickListener(view -> {
            OnClick.Call(list.get(position));
        });
    }

    @Override
    public int getItemCount() {
        if (list == null) {
            return 0;
        }
        return list.size();
    }

    public interface OnClick{
        void Call(OrganizationModel organizationModel);
    }
    public void OnCallLisiner(OnClick OnClick){
        this.OnClick = OnClick;
    }
}
