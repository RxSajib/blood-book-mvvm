package com.blood.bloodbook.UI.ViewHolder;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.blood.bloodbook.databinding.HospitalitemBinding;

public class HospitalViewHolder extends RecyclerView.ViewHolder {

    public HospitalitemBinding binding;

    public HospitalViewHolder(@NonNull HospitalitemBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }
}
