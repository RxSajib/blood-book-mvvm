package com.blood.bloodbook.UI.BloodRequest;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.blood.bloodbook.Application;
import com.blood.bloodbook.DI.DaggerWeenaComponent;
import com.blood.bloodbook.Network.ViewModel.ViewModel;
import com.blood.bloodbook.R;
import com.blood.bloodbook.UI.Adapter.BloodRequestAdapter;
import com.blood.bloodbook.Widget.ProgressDialog;
import com.blood.bloodbook.databinding.MyrequestBinding;

import javax.inject.Inject;

public class MyRequest extends Fragment {

    private MyrequestBinding binding;
    private ViewModel viewModel;
    @Inject BloodRequestAdapter bloodRequestAdapter;
    @Inject
    ProgressDialog progressDialog;
    private int Limit = 25;

    public MyRequest() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.myrequest, container, false);
        viewModel = new ViewModelProvider(this).get(ViewModel.class);
        var component = Application.weenaComponent;
        component.InjectMyRequest(this);

        binding.SwipeRefreshLayout.setOnRefreshListener(() -> {
            GetData(Limit);
        });
        binding.RecyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if(!recyclerView.canScrollVertically(1)){
                    Limit = Limit+25;
                    GetData(Limit);
                }
            }
        });

        InitView();
        GetData(Limit);
        return binding.getRoot();
    }

    private void GetData(int Limit) {
        viewModel.GetMyBloodRequest(Limit).observe(getActivity(), bloodRequestModels -> {
            bloodRequestAdapter.setList(bloodRequestModels);
            bloodRequestAdapter.notifyDataSetChanged();
            binding.SwipeRefreshLayout.setRefreshing(false);
            if (bloodRequestModels == null) {
                binding.Icon.setVisibility(View.VISIBLE);
                binding.message.setVisibility(View.VISIBLE);
            } else {
                binding.Icon.setVisibility(View.GONE);
                binding.message.setVisibility(View.GONE);
            }
        });
    }

    private void InitView() {
        binding.RecyclerView.setHasFixedSize(true);
        binding.RecyclerView.setAdapter(bloodRequestAdapter);

        bloodRequestAdapter.OnClickLisiner(model -> {
            var dialog = new AlertDialog.Builder(getActivity());
            dialog.setTitle(getResources().getString(R.string.Delete));
            dialog.setMessage(getResources().getString(R.string.DoYouWantToRemove));
            dialog.setPositiveButton(getResources().getString(R.string.Delete), (dialogInterface, i) -> {
                progressDialog.ProgressDialog(getActivity());
                viewModel.DeleteMyBloodRequest(model.getDocumentKey()).observe(getActivity(), aBoolean -> {
                    if (aBoolean) {
                        progressDialog.CancelProgressDialog();
                    } else {
                        progressDialog.CancelProgressDialog();
                    }
                });
            });
            dialog.setNegativeButton(getResources().getString(R.string.Cancel), (dialogInterface, i) -> {

            });

            var d = dialog.create();
            d.show();
        });
    }
}