package com.blood.bloodbook.UI;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.os.Bundle;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.blood.bloodbook.Application;
import com.blood.bloodbook.Widget.TermsAndConditionDialog;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.GoogleAuthProvider;
import com.blood.bloodbook.DI.DaggerWeenaComponent;
import com.blood.bloodbook.Data.DataManager;
import com.blood.bloodbook.Data.SharePref;
import com.blood.bloodbook.Network.ViewModel.ViewModel;
import com.blood.bloodbook.R;
import com.blood.bloodbook.Utils.HandleActivity;
import com.blood.bloodbook.Utils.Toast;
import com.blood.bloodbook.Widget.ProgressDialog;
import com.blood.bloodbook.databinding.AccountBinding;

import javax.inject.Inject;

public class Account extends AppCompatActivity {

    private AccountBinding binding;
    @Inject
    ProgressDialog progressDialog;
    private FirebaseAuth Mauth;
    private GoogleSignInClient mGoogleSignInClient;
    private static final int RC_SIGN_IN = 100;
    private com.blood.bloodbook.Network.ViewModel.ViewModel viewModel;
    private SharePref sharePref;
    @Inject
    TermsAndConditionDialog termsAndConditionDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.account);
        viewModel = new ViewModelProvider(this).get(ViewModel.class);

        var component = Application.weenaComponent;
        component.InjectAccount(this);
        Mauth = FirebaseAuth.getInstance();
        sharePref = new SharePref(getApplicationContext());

        creating_request();
        InitView();
    }


    private void signIn() {
        var signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void creating_request() {
        var gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(Account.this, gso);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account.getIdToken());
            } catch (ApiException e) {
                Toast.Message(Account.this, e.getMessage());
            }
        }
    }

    private void firebaseAuthWithGoogle(String idToken) {
        progressDialog.ProgressDialog(Account.this);
        var credential = GoogleAuthProvider.getCredential(idToken, null);
        Mauth.signInWithCredential(credential).addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                sharePref.SetData(DataManager.LoginType, DataManager.Email);
                sharePref.SetData(DataManager.LoginAddress, task.getResult().getUser().getEmail());
                var Email = task.getResult().getUser().getEmail();
                var Name = task.getResult().getUser().getDisplayName();
                var PhotoUri = task.getResult().getUser().getPhotoUrl().toString();
                SetUpProfileData(Email, Name, PhotoUri);
            } else {
                ProgressDialog.CancelProgressDialog();

            }
        });
    }

    private void SetUpProfileData(String Email, String Name, String PhotoUri){
      viewModel.CreateProfile(Name, null, null, null, PhotoUri, Email, Email).observe(this, aBoolean -> {
          if(aBoolean){
              progressDialog.CancelProgressDialog();
              HandleActivity.GotoHome(Account.this);
              Animatoo.animateSlideLeft(Account.this);
              finish();
          }else {
              progressDialog.CancelProgressDialog();
              HandleActivity.GotoHome(Account.this);
              Animatoo.animateSlideLeft(Account.this);
              finish();
          }
      });
    }

    private void InitView(){

        binding.TermsCondition.setOnClickListener(view -> {
            termsAndConditionDialog.Show(Account.this);
        });

        binding.PhoneBtn.setOnClickListener(view -> {
            HandleActivity.GotoPhoneLogin(Account.this);
            Animatoo.animateSlideLeft(Account.this);
        });

        binding.FacebookBtn.setOnClickListener(view -> {
            progressDialog.ProgressDialog(Account.this);
        });

        binding.EmailBtn.setOnClickListener(view -> {
            HandleActivity.GotoEmailSIgnIn(Account.this);
            Animatoo.animateSlideLeft(Account.this);
        });

        binding.SignUpBtn.setOnClickListener(view -> {
            HandleActivity.GotoEmailSIgnUp(Account.this);
            Animatoo.animateSlideLeft(Account.this);
        });
        binding.GoogleBtn.setOnClickListener(view -> {
            signIn();
        });
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}