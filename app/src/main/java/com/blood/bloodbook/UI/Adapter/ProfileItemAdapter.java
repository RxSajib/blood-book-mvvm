package com.blood.bloodbook.UI.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.blood.bloodbook.Data.DataManager;
import com.blood.bloodbook.Data.ProfileDataModel;
import com.blood.bloodbook.UI.ViewHolder.ProfileViewHolder;
import com.blood.bloodbook.databinding.ProfileitemBinding;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import lombok.Getter;
import lombok.Setter;

@Singleton
public class ProfileItemAdapter extends RecyclerView.Adapter<ProfileViewHolder> {

    @Setter @Getter
    private List<ProfileDataModel> list;
    private OnClick OnClick;
    private CollectionReference DonorUserRef;
    private FirebaseAuth Mauth;
    private OnClickFeelingSickSwitch OnClickFeelingSickSwitch;

    @Inject
    public ProfileItemAdapter(){

    }

    @NonNull
    @Override
    public ProfileViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Mauth = FirebaseAuth.getInstance();
        DonorUserRef = FirebaseFirestore.getInstance().collection(DataManager.DonorUser);
        var l = LayoutInflater.from(parent.getContext());
        var v = ProfileitemBinding.inflate(l, parent, false);
        return new ProfileViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ProfileViewHolder holder, int position) {
        holder.binding.Title.setText(list.get(position).getTitle());
        holder.binding.Icon.setImageResource(list.get(position).getIcon());

        if(list.get(position).getTitle().equals(DataManager.Feelingsick)){
            holder.binding.SwitchButton.setVisibility(View.VISIBLE);
            DonorUserRef.document(Mauth.getCurrentUser().getUid()).addSnapshotListener((value, error) -> {
                if(error != null){
                    return;
                }
                if(value.exists()){
                    var ShowToOther = value.getBoolean(DataManager.ShowToOther);
                    if(ShowToOther){
                        holder.binding.SwitchButton.setChecked(false);
                    }else {
                        holder.binding.SwitchButton.setChecked(true);
                    }
                }
            });

            holder.binding.SwitchButton.setOnCheckedChangeListener((compoundButton, b) -> {
                 OnClickFeelingSickSwitch.Click(b);
            });

        }


        holder.itemView.setOnClickListener(view -> {
            OnClick.Click(list.get(position).getTitle());
        });
    }

    @Override
    public int getItemCount() {
        if(list == null) {
            return 0;
        }return list.size();
    }
    public interface OnClick{
        void Click(String Title);
    }
    public void OnClickLisiner(OnClick OnClick){
        this.OnClick = OnClick;
    }


    public interface OnClickFeelingSickSwitch{
        void Click(Boolean IsEnable);
    }
    public void OnClickFeelingSickSwitchState(OnClickFeelingSickSwitch OnClickFeelingSickSwitch){
        this.OnClickFeelingSickSwitch = OnClickFeelingSickSwitch;
    }
}
