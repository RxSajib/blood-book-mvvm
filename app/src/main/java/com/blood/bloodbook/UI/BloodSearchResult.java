package com.blood.bloodbook.UI;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.blood.bloodbook.Application;
import com.blood.bloodbook.DI.DaggerWeenaComponent;
import com.blood.bloodbook.Data.DataManager;
import com.blood.bloodbook.R;
import com.blood.bloodbook.UI.Adapter.DonorUserAdapter;
import com.blood.bloodbook.Utils.HandleActivity;
import com.blood.bloodbook.Utils.Toast;
import com.blood.bloodbook.ViewModel.ViewModel;
import com.blood.bloodbook.databinding.BloodsearchresultBinding;

import javax.inject.Inject;

public class BloodSearchResult extends AppCompatActivity {

    private BloodsearchresultBinding binding;
    private ViewModel viewModel;
    @Inject
    DonorUserAdapter adapter;
    private String BloodGroup, CountryName, Province, District;
    private static final String TAG = "BloodSearchResult";
    private int Limit = 25;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.bloodsearchresult);
        viewModel = new ViewModelProvider(this).get(ViewModel.class);

        BloodGroup = getIntent().getStringExtra(DataManager.BloodGroup);
        CountryName = getIntent().getStringExtra(DataManager.CountryName);
        Province = getIntent().getStringExtra(DataManager.Province);
        District = getIntent().getStringExtra(DataManager.District);

        var component = Application.weenaComponent;
        component.InjectSearchResult(this);


        InitView();
        binding.RecyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if(!recyclerView.canScrollVertically(1)){
                    Limit = Limit+25;
                    GetData(Limit);
                }
            }
        });
        binding.SwipeRefreshLayout.setOnRefreshListener(() -> {
            GetData(Limit);
        });
        GetData(Limit);
    }

    private void InitView(){
        binding.Toolbar.Title.setText(BloodGroup+" Search Result");
        binding.Toolbar.BackButton.setOnClickListener(view -> {
            finish();
            Animatoo.animateSlideRight(BloodSearchResult.this);
        });
        binding.RecyclerView.setHasFixedSize(true);
        binding.RecyclerView.setAdapter(adapter);

        adapter.OnClickLisiner(model -> {
            HandleActivity.GotoBloodResult(BloodSearchResult.this, model);
            Animatoo.animateSlideLeft(BloodSearchResult.this);
        });

    }


    private void GetData(int Limit){
        viewModel.SearchBloodGroup(BloodGroup, CountryName, Province, District, Limit).observe(this, bloodRequestModels -> {
            adapter.setList(bloodRequestModels);
            adapter.notifyDataSetChanged();
            binding.SwipeRefreshLayout.setRefreshing(false);
            if(bloodRequestModels != null){
                binding.Icon.setVisibility(View.GONE);
                binding.Message.setVisibility(View.GONE);
            }else {
                binding.Icon.setVisibility(View.VISIBLE);
                binding.Message.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
        Animatoo.animateSlideRight(BloodSearchResult.this);
    }
}