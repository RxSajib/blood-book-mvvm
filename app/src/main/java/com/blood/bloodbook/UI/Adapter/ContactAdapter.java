package com.blood.bloodbook.UI.Adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.blood.bloodbook.ContactModel;
import com.blood.bloodbook.Data.DataManager;
import com.blood.bloodbook.UI.ViewHolder.ContactViewHolder;
import com.blood.bloodbook.databinding.ContactitemBinding;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import lombok.Getter;
import lombok.Setter;
@Singleton
public class ContactAdapter extends RecyclerView.Adapter<ContactViewHolder> {

    @Inject
    public ContactAdapter(){}

    @Setter @Getter
    private List<ContactModel> list;
    private OnClick OnClick;

    @NonNull
    @Override
    public ContactViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        var l = LayoutInflater.from(parent.getContext());
        var v = ContactitemBinding.inflate(l, parent, false);
        return new ContactViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ContactViewHolder holder, int position) {
      holder.binding.PhoneNumber.setText(list.get(position).getNumber());
      holder.binding.NumberOfCompany.setText(list.get(position).getProvider());

      holder.binding.IconCall.setOnClickListener(view -> {
          OnClick.Click(list.get(position).getNumber(), DataManager.Call);
      });
      holder.binding.IconMessage.setOnClickListener(view -> {
          OnClick.Click(list.get(position).getNumber(), DataManager.Message);
      });
    }

    @Override
    public int getItemCount() {
        if(list == null) {
            return 0;
        }return list.size();
    }

    public interface OnClick{
        void Click(String Number, String Type);
    }
    public void OnCLickLisiner(OnClick OnClick){
        this.OnClick = OnClick;
    }
}
