package com.blood.bloodbook.UI.Adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.blood.bloodbook.BloodRequestModel;
import com.blood.bloodbook.UI.ViewHolder.BloodRequestViewHolder;
import com.blood.bloodbook.databinding.BloodrequestitemBinding;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import lombok.Getter;
import lombok.Setter;
@Singleton
public class BloodRequestAdapter extends RecyclerView.Adapter<BloodRequestViewHolder> {
    @Setter @Getter
    private List<BloodRequestModel> list;
    private OnClick OnClick;

    @Inject
    public BloodRequestAdapter(){
    }

    @NonNull
    @Override
    public BloodRequestViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        var l = LayoutInflater.from(parent.getContext());
        var v = BloodrequestitemBinding.inflate(l, parent, false);
        return new BloodRequestViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull BloodRequestViewHolder holder, int position) {
        holder.binding.BloodCondition.setText(list.get(position).getTypeOfPatientsCondition());
        holder.binding.BloodGroup.setText(list.get(position).getBloodGroup());
        holder.binding.PatientName.setText(list.get(position).getHospitalName());
        holder.binding.Date.setText(list.get(position).getBloodRequiredDate());
        holder.binding.Time.setText(list.get(position).getBloodRequiredTime());
        holder.binding.Location.setText(list.get(position).getProvince()+" "+list.get(position).getDistrict());
        holder.binding.QuantityPints.setText(list.get(position).getNumberOFQuantityPints()+"CC");

        holder.itemView.setOnClickListener(view -> {
            OnClick.Click(list.get(position));
        });
    }


    @Override
    public int getItemCount() {
        if(list == null) {
            return 0;
        }return list.size();
    }

    public interface OnClick{
        void Click(BloodRequestModel model);
    }
    public void OnClickLisiner(OnClick OnClick){
        this.OnClick = OnClick;
    }
}
