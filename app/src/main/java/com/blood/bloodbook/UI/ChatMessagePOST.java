package com.blood.bloodbook.UI;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.blood.bloodbook.Data.DataManager;
import com.blood.bloodbook.Utils.Toast;

import java.util.HashMap;

public class ChatMessagePOST {

    private Application application;
    private MutableLiveData<Boolean> data;
    private DatabaseReference MessageRef;
    private FirebaseAuth Mauth;

    public ChatMessagePOST(Application application){
        this.application = application;
        MessageRef = FirebaseDatabase.getInstance().getReference().child(DataManager.Message);
        Mauth = FirebaseAuth.getInstance();
    }

    public LiveData<Boolean> SendMessage(String Message, String Type, String SenderUID, String FileName, String FileExtension, String FileSize){
        data = new MutableLiveData<>();

        var FirebaseUser = Mauth.getCurrentUser();
        if(FirebaseUser != null){
            var timestamp = System.currentTimeMillis();

            Toast.Message(application, "cccc");

            var map = new HashMap<String, Object>();
            map.put(DataManager.Message, Message);
            map.put(DataManager.SenderUID, SenderUID);
            map.put(DataManager.ReceiverUID, FirebaseUser.getUid());
            map.put(DataManager.Type, Type);
            map.put(DataManager.DocumentKey, timestamp);
            map.put(DataManager.FileName, FileName);
            map.put(DataManager.FileExtension, FileExtension);
            map.put(DataManager.Timestamp, timestamp);
            map.put(DataManager.FileSize, FileSize);

            MessageRef.child(FirebaseUser.getUid())
                    .child(SenderUID).child(DataManager.Message)
                    .child(String.valueOf(timestamp)).updateChildren(map).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if(task.isSuccessful()){
                                MessageRef.child(SenderUID).child(FirebaseUser.getUid())
                                        .child(DataManager.Message).child(String.valueOf(timestamp))
                                        .updateChildren(map).addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                if(task.isSuccessful()){
                                                    data.setValue(true);
                                                }else {
                                                    data.setValue(false);
                                                    Toast.Message(application, task.getException().getMessage());
                                                }
                                            }
                                        }).addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull Exception e) {
                                                data.setValue(false);
                                                Toast.Message(application, e.getMessage());
                                            }
                                        });
                            }else {
                                data.setValue(false);
                                Toast.Message(application, task.getException().getMessage());
                            }
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            data.setValue(false);
                        }
                    });
        }
        return data;
    }
}
