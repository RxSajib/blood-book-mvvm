package com.blood.bloodbook.UI;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.blood.bloodbook.R;
import com.blood.bloodbook.databinding.AboutusBinding;

public class AboutUS extends AppCompatActivity {

    private AboutusBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.aboutus);

        InitView();
    }

    private void InitView(){
        binding.Toolbar.Title.setText("About us");
        binding.Toolbar.BackButton.setOnClickListener(view -> {
            finish();
            Animatoo.animateSlideRight(AboutUS.this);
        });
    }

    @Override
    public void onBackPressed() {
        finish();
        Animatoo.animateSlideRight(AboutUS.this);
    }
}