package com.blood.bloodbook.UI.NotificationViewHolder;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.blood.bloodbook.R;

public class NotificationUserBroadcastMessageVH extends RecyclerView.ViewHolder {

    public TextView Time, Topic, Message;

    public NotificationUserBroadcastMessageVH(@NonNull View itemView) {
        super(itemView);

        Topic = itemView.findViewById(R.id.Topic);
        Time = itemView.findViewById(R.id.TimeDate);
        Message = itemView.findViewById(R.id.Message);
    }
}
