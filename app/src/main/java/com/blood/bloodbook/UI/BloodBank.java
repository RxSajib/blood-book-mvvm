package com.blood.bloodbook.UI;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;

import android.os.Bundle;
import android.view.View;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.blood.bloodbook.Application;
import com.blood.bloodbook.DI.DaggerWeenaComponent;
import com.blood.bloodbook.Network.ViewModel.ViewModel;
import com.blood.bloodbook.R;
import com.blood.bloodbook.UI.Adapter.HospitalAdapter;
import com.blood.bloodbook.Utils.HandleActivity;
import com.blood.bloodbook.databinding.BloodbankBinding;

import javax.inject.Inject;

public class BloodBank extends AppCompatActivity {

    private BloodbankBinding binding;
    @Inject
    HospitalAdapter hospitalAdapter;
    private ViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.bloodbank);

        viewModel = new ViewModelProvider(this).get(ViewModel.class);
        var component = Application.weenaComponent;
        component.InjectBloodBank(this);

        InitView();
        GetData();
    }

    private void GetData(){
        binding.RecyclerView.setHasFixedSize(true);
        binding.RecyclerView.setLayoutManager(new GridLayoutManager(getApplicationContext(), 2));
        binding.RecyclerView.setAdapter(hospitalAdapter);

        viewModel.GetHospitalInfo().observe(this, hospitalModels -> {
            hospitalAdapter.setList(hospitalModels);
            hospitalAdapter.notifyDataSetChanged();

            if(hospitalModels != null){
                binding.Icon.setVisibility(View.GONE);
                binding.Message.setVisibility(View.GONE);
            }else {
                binding.Message.setVisibility(View.VISIBLE);
                binding.Icon.setVisibility(View.VISIBLE);
            }
        });

        hospitalAdapter.OnClickEvent(hospitalModel -> {
            HandleActivity.GotoListOFBloodBank(BloodBank.this, hospitalModel);
            Animatoo.animateSlideLeft(BloodBank.this);
        });
    }

    private void InitView(){
        binding.Toolbar.Title.setText(getResources().getString(R.string.BloodBank));
        binding.Toolbar.BackButton.setOnClickListener(view -> {
            finish();
            Animatoo.animateSlideRight(BloodBank.this);
        });
    }

    @Override
    public void onBackPressed() {
        finish();
        Animatoo.animateSlideRight(BloodBank.this);
    }
}