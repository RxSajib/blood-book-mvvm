package com.blood.bloodbook.UI;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Bundle;
import android.view.View;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.blood.bloodbook.Application;
import com.blood.bloodbook.Network.ViewModel.ViewModel;
import com.blood.bloodbook.R;
import com.blood.bloodbook.Singleton.Singleton;
import com.blood.bloodbook.UI.Adapter.NotificationAdapter;
import com.blood.bloodbook.Utils.HandleActivity;
import com.blood.bloodbook.Widget.ProgressDialog;
import com.blood.bloodbook.databinding.NotificationBinding;
import javax.inject.Inject;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class Notification extends AppCompatActivity {

    private NotificationBinding binding;
    private ViewModel viewModel;
    @Inject
    NotificationAdapter notificationAdapter;
    @Inject
    ProgressDialog progressDialog;
    private int Limit = 25;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.notification);
        viewModel = Singleton.getNotification(this);

        var component = Application.weenaComponent;
        component.InjectNotification(this);

        binding.SwipeRefreshLayout.setOnRefreshListener(() -> {
            GetDataFromServer(Limit);
        });

        InitView();
        binding.RecyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if(!recyclerView.canScrollVertically(1)){
                    Limit = Limit+25;
                    GetDataFromServer(Limit);
                }
            }
        });
        GetDataFromServer(Limit);

    }



    private void GetDataFromServer(int Limit){

        viewModel.GetNotification(Limit).observe(this, notificationModels -> {
            notificationAdapter.setList(notificationModels);
            notificationAdapter.notifyDataSetChanged();
            binding.SwipeRefreshLayout.setRefreshing(false);

            if(notificationModels != null){
                binding.Icon.setVisibility(View.GONE);
                binding.Message.setVisibility(View.GONE);
            }else {
                binding.Icon.setVisibility(View.VISIBLE);
                binding.Message.setVisibility(View.VISIBLE);
            }
        });
    }

    private void InitView(){
        binding.RecyclerView.setHasFixedSize(true);
        binding.RecyclerView.setAdapter(notificationAdapter);
        binding.Toolbar.BackButton.setOnClickListener(view -> {
            finish();
            Animatoo.animateSlideRight(Notification.this);
        });

        binding.Toolbar.Title.setText(getResources().getString(R.string.Notification));

        notificationAdapter.OnChatLisner((UID, GivenName, SureName) -> {
            HandleActivity.GotoChat(Notification.this, UID, GivenName, SureName);
            Animatoo.animateSlideLeft(Notification.this);
        });

        notificationAdapter.OnClickState(DocumentKey -> {
            new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                    .setTitleText("Are you sure?")
                    .setContentText("Won't be able to recover this file!")
                    .setConfirmText("Yes,delete it!")
                    .setConfirmClickListener(sDialog -> viewModel.NotificationDelete(String.valueOf(DocumentKey)).observe(Notification.this, aBoolean -> {
                        if(aBoolean){
                            sDialog
                                    .setTitleText("Deleted!")
                                    .setContentText("Your imaginary file has been deleted!")
                                    .setConfirmText("OK")
                                    .setConfirmClickListener(null)
                                    .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
                        }else {
                        }
                    }))
                    .show();
        });
    }

    @Override
    public void onBackPressed() {
        finish();
        Animatoo.animateSlideRight(Notification.this);
    }
}