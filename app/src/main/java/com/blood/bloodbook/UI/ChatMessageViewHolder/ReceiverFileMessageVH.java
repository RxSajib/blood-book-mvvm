package com.blood.bloodbook.UI.ChatMessageViewHolder;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.blood.bloodbook.R;

import org.w3c.dom.Text;

public class ReceiverFileMessageVH extends RecyclerView.ViewHolder {

    public TextView FileName, FileSize, Time, Download, FileType;

    public ReceiverFileMessageVH(@NonNull View itemView) {
        super(itemView);

        FileName = itemView.findViewById(R.id.FileName);
        FileSize = itemView.findViewById(R.id.FileSize);
        Time = itemView.findViewById(R.id.ReceiverFileTime);
        Download = itemView.findViewById(R.id.DownloadBtn);
        FileType = itemView.findViewById(R.id.FileType);
    }
}
