package com.blood.bloodbook.UI.ViewHolder;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.blood.bloodbook.databinding.ContactitemBinding;

public class ContactViewHolder extends RecyclerView.ViewHolder {

    public ContactitemBinding binding;

    public ContactViewHolder(@NonNull ContactitemBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }
}
