package com.blood.bloodbook.UI.ViewHolder;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.blood.bloodbook.databinding.TelephonymanageritemBinding;

public class TelephonyManagerViewHolder extends RecyclerView.ViewHolder {

    public TelephonymanageritemBinding binding;

    public TelephonyManagerViewHolder(@NonNull TelephonymanageritemBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }
}
