package com.blood.bloodbook.UI;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.DatePicker;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.blood.bloodbook.Application;
import com.google.android.gms.ads.AdRequest;
import com.blood.bloodbook.DI.DaggerWeenaComponent;
import com.blood.bloodbook.Network.ViewModel.LocationViewModel;
import com.blood.bloodbook.Network.ViewModel.ViewModel;
import com.blood.bloodbook.R;
import com.blood.bloodbook.UI.Adapter.DistrictNameDataAdapter;
import com.blood.bloodbook.UI.Adapter.LocationDataAdapter;
import com.blood.bloodbook.UI.Adapter.ProvinceNameDataAdapter;
import com.blood.bloodbook.Utils.DatePickerDialogFragment;
import com.blood.bloodbook.Utils.Permission;
import com.blood.bloodbook.Utils.Toast;
import com.blood.bloodbook.Widget.ProgressDialog;
import com.blood.bloodbook.databinding.LocationdialogBinding;
import com.blood.bloodbook.databinding.RegisterdonationdateBinding;

import java.text.DecimalFormat;

import javax.inject.Inject;

public class RegisterDonationDate extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {

    private RegisterdonationdateBinding binding;
    @Inject LocationDataAdapter locationDataAdapter;
    @Inject ProvinceNameDataAdapter provinceNameDataAdapter;
    private LocationViewModel locationViewModel;
    @Inject DistrictNameDataAdapter districtNameDataAdapter;
    private ViewModel viewModel;
    private String ImageDownloadUri = null;
    @Inject
    ProgressDialog progressDialog;
    private ActivityResultLauncher<Intent> launcher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.registerdonationdate);

        locationViewModel = new ViewModelProvider(this).get(LocationViewModel.class);
        viewModel = new ViewModelProvider(this).get(ViewModel.class);
        var component = Application.weenaComponent;
        component.InjectRegisterDonationDate(this);


        InitView();
        UpdateData();
        GetCountryName();
        GetProvincesName();
        GetDistrictName();
        GetImage();
        LoadBannerAd();
    }

    private void LoadBannerAd(){
        var adRequest = new AdRequest.Builder().build();
        binding.AdView.loadAd(adRequest);
    }

    private void GetImage(){
        launcher = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), result -> {
           if(result.getData() != null){
               binding.PickImage.setImageURI(result.getData().getData());
               progressDialog.ProgressDialog(RegisterDonationDate.this);
               viewModel.RegisterDonationImageUpload(result.getData().getData()).observe(this, s -> {
                   ImageDownloadUri = s;
                   progressDialog.CancelProgressDialog();
               });
           }else {
               progressDialog.CancelProgressDialog();
               ImageDownloadUri = null;
           }
        });
    }

    private void UpdateData() {
        binding.UploadBtn.setOnClickListener(view -> {
            var LastDonationDate = binding.LastDonationDateInput.getText().toString().trim();
            var QuantityOfBloodDonated = binding.QuantityofblooddonatedInput.getText().toString().trim();
            var Country = binding.Country.getText().toString().trim();
            var Province = binding.Province.getText().toString().trim();
            var District = binding.District.getText().toString().trim();
            var HospitalName = binding.HospitalNameInput.getText().toString().trim();

            if (LastDonationDate.isEmpty()) {
                Toast.Message(RegisterDonationDate.this, getResources().getString(R.string.SelectLastDonationDate));
            } else if (QuantityOfBloodDonated.isEmpty()) {
                Toast.Message(RegisterDonationDate.this, getResources().getString(R.string.SelectQuantityBloodDonated));
            } else if (Country == "") {
                Toast.Message(RegisterDonationDate.this, getResources().getString(R.string.CountryEmpty));
            } else if (Province == "") {
                Toast.Message(RegisterDonationDate.this, getResources().getString(R.string.ProvinceEmpty));
            } else if (District == "") {
                Toast.Message(RegisterDonationDate.this, getResources().getString(R.string.DistrictEmpty));
            } else if(HospitalName.isEmpty()){
                Toast.Message(RegisterDonationDate.this, getResources().getString(R.string.HospitalNameEmpty));
            }
            else {
                progressDialog.ProgressDialog(RegisterDonationDate.this);
                viewModel.RegisterDonationDate(LastDonationDate, QuantityOfBloodDonated, Country, Province, District, HospitalName, ImageDownloadUri).observe(this, aBoolean -> {
                    if(aBoolean){
                        ProgressDialog.CancelProgressDialog();
                        finish();
                        Animatoo.animateSlideRight(RegisterDonationDate.this);
                    }else {
                        ProgressDialog.CancelProgressDialog();
                    }
                });
            }
        });
    }

    private void InitView() {

        binding.PickImage.setOnClickListener(view -> {
            if (Permission.PermissionExternalStorage(RegisterDonationDate.this, 511)) {
                var intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("image/*");
                launcher.launch(intent);
            }
        });


        binding.Toolbar.Title.setText(getResources().getString(R.string.RegisterDonationDate));
        binding.Toolbar.BackButton.setOnClickListener(view -> {
            finish();
            Animatoo.animateSlideRight(RegisterDonationDate.this);
        });

        binding.LastDonationDateInput.setKeyListener(null);
        binding.LastDonationDateInput.setOnClickListener(view -> {
            var dialog = new DatePickerDialogFragment();
            dialog.show(getSupportFragmentManager(), "sdsaasd");
        });


        binding.CountryInput.setOnClickListener(view -> {
            locationViewModel.DeleteAllCountryNameData();
            var dialog = new AlertDialog.Builder(RegisterDonationDate.this);
            LocationdialogBinding locationitemBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.locationdialog, null, false);
            dialog.setView(locationitemBinding.getRoot());

            var alertdialog = dialog.create();
            alertdialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            locationitemBinding.LocationRecyclerView.setHasFixedSize(true);
            locationitemBinding.LocationRecyclerView.setAdapter(locationDataAdapter);

            alertdialog.show();
            viewModel.GetCountryName().observe(RegisterDonationDate.this, countryNameModels -> {
                if(countryNameModels != null){
                    locationViewModel.InsertCounyryNameData(countryNameModels);
                }
            });
            locationitemBinding.SearchInput.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    var CountryName = editable.toString();
                    if (CountryName.isEmpty()) {
                        GetCountryName();
                    } else {
                        GetCountryNameSearch(CountryName);
                    }
                }
            });

            locationDataAdapter.OnCountryLisiner(CountryName -> {
                binding.Country.setText(CountryName);
                binding.District.setText("");
                binding.Province.setText("");
                alertdialog.dismiss();
            });
        });


        binding.ProvinceNameInput.setOnClickListener(view -> {
            var CountryName = binding.Country.getText().toString().trim();
            if (CountryName == "") {
                Toast.Message(RegisterDonationDate.this, getResources().getString(R.string.CountryEmpty));
            } else {
                locationViewModel.DeleteAllProvinceNameData();
                var dialog = new AlertDialog.Builder(RegisterDonationDate.this);
                LocationdialogBinding locationitemBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.locationdialog, null, false);
                dialog.setView(locationitemBinding.getRoot());

                var alertdialog = dialog.create();
                locationitemBinding.LocationRecyclerView.setHasFixedSize(true);
                locationitemBinding.LocationRecyclerView.setAdapter(provinceNameDataAdapter);
                alertdialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                alertdialog.show();

                viewModel.GetProvinceName(CountryName).observe(RegisterDonationDate.this, provinceNameModels -> {
                    if(provinceNameModels != null){
                        locationViewModel.InsertProvincesNameData(provinceNameModels);
                    }
                });

                provinceNameDataAdapter.OnClickLisiner(ProvinceName -> {
                    alertdialog.dismiss();
                    binding.Province.setText(ProvinceName);
                    binding.District.setText("");
                });
                locationitemBinding.SearchInput.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        var Data = editable.toString();
                        if (Data.isEmpty()) {
                            GetProvincesName();
                        } else {
                            SearchProvinceName(Data);
                        }
                    }
                });
            }
        });

        binding.DistrictNameInput.setOnClickListener(view -> {
            var CountryName = binding.Country.getText().toString().trim();
            var ProvincesName = binding.Province.getText().toString().trim();
            if (CountryName.isEmpty()) {
                Toast.Message(RegisterDonationDate.this, getResources().getString(R.string.CountryEmpty));
            } else if (ProvincesName == "") {
                Toast.Message(RegisterDonationDate.this, getResources().getString(R.string.ProvinceEmpty));
            } else {
                locationViewModel.DeleteDistrct();
                var dialog = new AlertDialog.Builder(RegisterDonationDate.this);
                LocationdialogBinding locationitemBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.locationdialog, null, false);
                dialog.setView(locationitemBinding.getRoot());

                var alertdialog = dialog.create();
                locationitemBinding.LocationRecyclerView.setHasFixedSize(true);
                locationitemBinding.LocationRecyclerView.setAdapter(districtNameDataAdapter);
                alertdialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                alertdialog.show();


                viewModel.GetDistrictName(CountryName, ProvincesName).observe(RegisterDonationDate.this, districtNameModelList -> {
                    if(districtNameDataAdapter != null){
                        locationViewModel.InsertDistrict(districtNameModelList);
                    }
                });

                districtNameDataAdapter.OnClickLisiner(DictrictName -> {
                    alertdialog.dismiss();
                    binding.District.setText(DictrictName);
                });

                locationitemBinding.SearchInput.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        var Data = editable.toString();
                        if (Data.isEmpty()) {
                            GetDistrictName();
                        } else {
                            SearchDustrictName(Data);
                        }
                    }
                });
            }
        });

    }


    //todo get all location name
    private void GetCountryNameSearch(String CountryName) {
        locationViewModel.SearchByCountryName(CountryName).observe(RegisterDonationDate.this, countryNameModels -> {
            locationDataAdapter.setList(countryNameModels);
            locationDataAdapter.notifyDataSetChanged();
        });
    }

    private void GetCountryName() {
        locationViewModel.GetCountryName().observe(RegisterDonationDate.this, countryNameModels -> {
            locationDataAdapter.setList(countryNameModels);
            locationDataAdapter.notifyDataSetChanged();
        });
    }

    private void GetProvincesName() {
        locationViewModel.GetProvincesNameData().observe(RegisterDonationDate.this, provinceNameModelList -> {
            provinceNameDataAdapter.setList(provinceNameModelList);
            provinceNameDataAdapter.notifyDataSetChanged();
        });
    }

    private void SearchProvinceName(String Name) {
        locationViewModel.SearchProvincesName(Name).observe(RegisterDonationDate.this, provinceNameModelList -> {
            provinceNameDataAdapter.setList(provinceNameModelList);
            provinceNameDataAdapter.notifyDataSetChanged();
        });
    }

    private void GetDistrictName() {
        locationViewModel.GetDistrictName().observe(RegisterDonationDate.this, districtNameModelList -> {
            districtNameDataAdapter.setList(districtNameModelList);
            districtNameDataAdapter.notifyDataSetChanged();
        });
    }

    private void SearchDustrictName(String Name) {
        locationViewModel.SearchDistrictName(Name).observe(RegisterDonationDate.this, districtNameModelList -> {
            districtNameDataAdapter.setList(districtNameModelList);
            districtNameDataAdapter.notifyDataSetChanged();
        });
    }

    //todo get all location name


    @Override
    public void onBackPressed() {
        finish();
        Animatoo.animateSlideRight(RegisterDonationDate.this);
    }

    @Override
    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {

        var format = new DecimalFormat("00");
        var day = format.format(Double.valueOf(i2));
        var month = format.format(Double.valueOf(i1+1));
        binding.LastDonationDateInput.setText(i + "-" + month + "-" + day);
    }
}