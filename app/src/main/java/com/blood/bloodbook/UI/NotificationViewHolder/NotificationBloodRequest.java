package com.blood.bloodbook.UI.NotificationViewHolder;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.blood.bloodbook.R;

public class NotificationBloodRequest extends RecyclerView.ViewHolder {

    public TextView Time, UserName, Message;

    public NotificationBloodRequest(@NonNull View itemView) {
        super(itemView);

        Time = itemView.findViewById(R.id.TimeDate);
        UserName = itemView.findViewById(R.id.DonorName);
        Message = itemView.findViewById(R.id.Message);
    }
}
