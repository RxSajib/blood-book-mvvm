package com.blood.bloodbook.UI.ViewHolder;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.blood.bloodbook.databinding.OnboardingitemBinding;

public class OnBoardingViewHolder extends RecyclerView.ViewHolder{

    public OnboardingitemBinding binding;

    public OnBoardingViewHolder(@NonNull OnboardingitemBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }
}
