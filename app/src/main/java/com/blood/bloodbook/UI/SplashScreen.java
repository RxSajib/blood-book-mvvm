package com.blood.bloodbook.UI;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;
import android.os.Handler;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.blood.bloodbook.Network.ViewModel.ViewModel;
import com.blood.bloodbook.R;
import com.blood.bloodbook.Utils.HandleActivity;
import com.blood.bloodbook.databinding.SplashscreenBinding;

public class SplashScreen extends AppCompatActivity {

    private SplashscreenBinding binding;
    private ViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.splashscreen);
        viewModel = new ViewModelProvider(this).get(ViewModel.class);


        new Handler().postDelayed(() ->
                viewModel.CheckUserExists().observe(this, aBoolean -> {
                    if (aBoolean) {
                        viewModel.ProfileExists().observe(SplashScreen.this, a -> {

                            if (a) {
                                viewModel.ProfileGET().observe(this, profileModel -> {
                                    if (profileModel != null) {
                                        if (profileModel.isIsActive()) {
                                            HandleActivity.GotoHome(SplashScreen.this);
                                            Animatoo.animateSlideLeft(SplashScreen.this);
                                            finish();
                                        } else {
                                            HandleActivity.GotoRestrictionAccount(SplashScreen.this);
                                            Animatoo.animateSlideLeft(SplashScreen.this);
                                            finish();
                                        }
                                    }
                                });

                            } else {
                                HandleActivity.GotoSetUpProfile(SplashScreen.this);
                                Animatoo.animateSlideLeft(SplashScreen.this);
                                finish();
                            }
                        });
                    } else {
                        HandleActivity.GotoOnBoardingScreen(SplashScreen.this);
                        Animatoo.animateSlideLeft(SplashScreen.this);
                        finish();
                    }
                }), 3000);
    }


}