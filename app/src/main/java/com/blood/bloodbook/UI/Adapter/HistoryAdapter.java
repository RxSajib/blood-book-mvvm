package com.blood.bloodbook.UI.Adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.blood.bloodbook.AllHistoryModel;
import com.blood.bloodbook.Data.DataManager;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.blood.bloodbook.HistoryModel;
import com.blood.bloodbook.R;
import com.blood.bloodbook.UI.ViewHolder.HistoryViewHolder;
import com.blood.bloodbook.databinding.HistoryitemBinding;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.squareup.picasso.Picasso;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import lombok.Getter;
import lombok.Setter;
@Singleton
public class HistoryAdapter extends RecyclerView.Adapter<HistoryViewHolder> {
    @Setter
    @Getter
    private List<AllHistoryModel> list;
    private CollectionReference MUserRef;

    @Inject
    public HistoryAdapter(){}

    @NonNull
    @Override
    public HistoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        var l = LayoutInflater.from(parent.getContext());
        var v = HistoryitemBinding.inflate(l, parent, false);
        MUserRef = FirebaseFirestore.getInstance().collection(DataManager.User);
        return new HistoryViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull HistoryViewHolder holder, int position) {
        holder.binding.HospitalName.setText(list.get(position).getHospitalName());
        holder.binding.Location.setText(list.get(position).getProvince()+" "+list.get(position).getDistrict());
        holder.binding.BloodDonationDate.setText(list.get(position).getBloodDonationDate());
        holder.binding.QuantityOfBlood.setText(list.get(position).getQuantityOfBlood());
        Picasso.get().load(list.get(position).getRegisterDonationBloodImage()).into(holder.binding.Image);

        MUserRef.document(list.get(position).getSenderUID()).get().addOnCompleteListener(task -> {
            if(task.isSuccessful()){
                var image = task.getResult().getString(DataManager.ProfileImage).toString();
                Picasso.get().load(image).into(holder.binding.UserImage);
                holder.binding.Name.setText(task.getResult().getString(DataManager.FirstName));
            }else {

            }
        }).addOnFailureListener(e -> {

        });

    }

    @Override
    public int getItemCount() {
        if (list == null) {
            return 0;
        } else {
            return list.size();
        }
    }
}
