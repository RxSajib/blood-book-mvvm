package com.blood.bloodbook.UI;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.os.Bundle;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.blood.bloodbook.Application;
import com.blood.bloodbook.DI.DaggerWeenaComponent;
import com.blood.bloodbook.Data.DataManager;
import com.blood.bloodbook.Data.SharePref;
import com.blood.bloodbook.Network.ViewModel.ViewModel;
import com.blood.bloodbook.R;
import com.blood.bloodbook.Utils.HandleActivity;
import com.blood.bloodbook.Utils.Permission;
import com.blood.bloodbook.Utils.Toast;
import com.blood.bloodbook.Widget.ProgressDialog;
import com.blood.bloodbook.databinding.SetupprofileBinding;

import javax.inject.Inject;

public class SetUpProfile extends AppCompatActivity {

    private SetupprofileBinding binding;
    @Inject
    ProgressDialog progressDialog;
    private ViewModel viewModel;
    private ActivityResultLauncher<Intent> launcher;
    private String ProfileImageUri;
    private SharePref sharePref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.setupprofile);
        viewModel = new ViewModelProvider(this).get(ViewModel.class);
        sharePref = new SharePref(SetUpProfile.this);

        var component = Application.weenaComponent;
        component.InjectSetUpProfile(this);

        LoadData();
        InitView();
        SetUpProfile();
        GetImageFromGallery();
    }

    private void LoadData(){
        viewModel.ProfileGET().observe(this, profileModel -> {
            if(profileModel != null){
                binding.setProfile(profileModel);
            }
        });
    }

    private void GetImageFromGallery(){
        launcher = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), result -> {
            if(result.getResultCode() == RESULT_OK){
                progressDialog.ProgressDialog(SetUpProfile.this);
                binding.Image.setImageURI(result.getData().getData());
                if(result.getData() != null){
                    viewModel.UploadProfileImage(result.getData().getData()).observe(this, imageuri -> {
                        if(imageuri != null){
                            progressDialog.CancelProgressDialog();
                            ProfileImageUri = imageuri;
                        }else {
                            progressDialog.CancelProgressDialog();
                        }
                    });
                }else {
                    progressDialog.CancelProgressDialog();
                }
            }
        });
    }


    private void InitView(){
        binding.Toolbar.Title.setText("Setup your profile");
        binding.Toolbar.BackButton.setOnClickListener(view -> {
            finish();
        });

        binding.Image.setOnClickListener(view -> {
            if(Permission.PermissionExternalStorage(SetUpProfile.this, 52)){
                var intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("image/*");
                launcher.launch(intent);
            }

        });
    }


    private void SetUpProfile(){
        binding.UpdateBtn.setOnClickListener(view -> {
            var FirstName = binding.FirstNameInput.getText().toString().trim();
            var MiddleName = binding.MiddleNameInput.getText().toString().trim();
            var SureName = binding.SureNameInput.getText().toString().trim();
            var FatherName = binding.FatherNameInput.getText().toString().trim();

            if(FirstName.isEmpty()){
                Toast.Message(SetUpProfile.this, "First name empty");
            }else if(MiddleName.isEmpty()){
                Toast.Message(SetUpProfile.this, "Middle name empty");
            }else if(SureName.isEmpty()){
                Toast.Message(SetUpProfile.this, "Surename empty");
            }else {
                progressDialog.ProgressDialog(SetUpProfile.this);
                viewModel.CreateProfile(FirstName, MiddleName, SureName, FatherName, String.valueOf(ProfileImageUri), sharePref.GetData(DataManager.LoginAddress), sharePref.GetData(DataManager.LoginAddress)).observe(this, new Observer<Boolean>() {
                    @Override
                    public void onChanged(Boolean aBoolean) {
                        if(aBoolean){

                            HandleActivity.GotoHome(SetUpProfile.this);
                            Animatoo.animateSlideLeft(SetUpProfile.this);
                            finish();
                        }
                    }
                });
            }

        });

    }
}