package com.blood.bloodbook.UI.Adapter;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.blood.bloodbook.BloodDonationRestTimeModel;
import com.blood.bloodbook.Data.DataManager;
import com.blood.bloodbook.R;
import com.blood.bloodbook.UI.ViewHolder.BloodDonationRestItemViewHolder;
import com.blood.bloodbook.databinding.BlooddonationresttimeitemBinding;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import lombok.Getter;
import lombok.Setter;
@Singleton
public class BloodDonationRestTimeAdapter extends RecyclerView.Adapter<BloodDonationRestItemViewHolder> {

    private int row_index = -1;
    private CollectionReference DonorUser;
    private FirebaseAuth Mauth;

    @Inject
    public BloodDonationRestTimeAdapter() {
    }

    @Setter
    @Getter
    private List<BloodDonationRestTimeModel> list;
    private OnClick OnClick;

    @NonNull
    @Override
    public BloodDonationRestItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Mauth = FirebaseAuth.getInstance();
        DonorUser = FirebaseFirestore.getInstance().collection(DataManager.DonorUser);
        var l = LayoutInflater.from(parent.getContext());
        var v = BlooddonationresttimeitemBinding.inflate(l, parent, false);
        return new BloodDonationRestItemViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull BloodDonationRestItemViewHolder holder, int position) {
        holder.binding.setBloodDonationRestTime(list.get(position));
        holder.itemView.setOnClickListener(view -> {
            OnClick.Click(list.get(position).getDay());
            row_index = position;
           /// notifyDataSetChanged();
        });

        if (row_index == position) {
            holder.binding.Background.setBackgroundResource(R.color.carbon_red_400);
            holder.binding.Duration.setTextColor(Color.WHITE);
        } else {
            holder.binding.Background.setBackgroundResource(R.color.carbon_black_6);
            holder.binding.Duration.setTextColor(R.color.carbon_black_54);
        }

        DonorUser.document(FirebaseAuth.getInstance().getUid()).addSnapshotListener((value, error) -> {
            if(error != null){
                return;
            }
            if(value.exists()){

               var month = value.getString(DataManager.DonateMonth);
               if(month.equals("3")){
                   if(position == 0){
                       holder.binding.Background.setBackgroundResource(R.color.carbon_red_400);
                       holder.binding.Duration.setTextColor(Color.WHITE);
                     //  OnClick.Click(DataManager.ThreeMonth);
                   }
               }
               else if(month.equals("4")){
                   if(position == 1){
                       holder.binding.Background.setBackgroundResource(R.color.carbon_red_400);
                       holder.binding.Duration.setTextColor(Color.WHITE);
                     //  OnClick.Click(DataManager.FourMonth);
                   }
               }

               else if(month.equals("5")){
                   if(position == 2){
                       holder.binding.Background.setBackgroundResource(R.color.carbon_red_400);
                       holder.binding.Duration.setTextColor(Color.WHITE);
                   //    OnClick.Click(DataManager.FiveMonth);
                   }
               }

               else if(month.equals("6")){
                   if(position == 3){
                       holder.binding.Background.setBackgroundResource(R.color.carbon_red_400);
                       holder.binding.Duration.setTextColor(Color.WHITE);
                     //  OnClick.Click(DataManager.SixMonth);
                   }
               }

               else if(month.equals("7")){
                   if(position == 4){
                       holder.binding.Background.setBackgroundResource(R.color.carbon_red_400);
                       holder.binding.Duration.setTextColor(Color.WHITE);
                    //   OnClick.Click(DataManager.SevenMonth);
                   }
               }
               else if(month.equals("8")){
                   if(position == 5){
                       holder.binding.Background.setBackgroundResource(R.color.carbon_red_400);
                       holder.binding.Duration.setTextColor(Color.WHITE);
                     //  OnClick.Click(DataManager.EightMonth);
                   }
               }
               else if(month.equals("9")){
                   if(position == 6){
                       holder.binding.Background.setBackgroundResource(R.color.carbon_red_400);
                       holder.binding.Duration.setTextColor(Color.WHITE);
                       OnClick.Click(DataManager.NineMonth);
                   }
               }
               else if(month.equals("10")){
                   if(position == 7){
                       holder.binding.Background.setBackgroundResource(R.color.carbon_red_400);
                       holder.binding.Duration.setTextColor(Color.WHITE);
                       OnClick.Click(DataManager.TenMonth);
                   }
               }
            }
        });
    }

    @Override
    public int getItemCount() {
        if (list == null) {
            return 0;
        }
        return list.size();
    }

    public interface OnClick {
        void Click(String DateOfMonth);
    }

    public void OnClickEvent(OnClick OnClick) {
        this.OnClick = OnClick;
    }
}
