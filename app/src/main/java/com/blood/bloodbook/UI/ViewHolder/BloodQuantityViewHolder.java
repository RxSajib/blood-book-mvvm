package com.blood.bloodbook.UI.ViewHolder;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.blood.bloodbook.databinding.BloodquantityitemBinding;

public class BloodQuantityViewHolder extends RecyclerView.ViewHolder {

    public BloodquantityitemBinding binding;

    public BloodQuantityViewHolder(@NonNull BloodquantityitemBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }
}
