package com.blood.bloodbook.UI.ViewHolder;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.blood.bloodbook.databinding.ProfileitemBinding;

public class ProfileViewHolder extends RecyclerView.ViewHolder {

    public ProfileitemBinding binding;

    public ProfileViewHolder(@NonNull ProfileitemBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }
}
