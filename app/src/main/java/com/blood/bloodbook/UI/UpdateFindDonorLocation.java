package com.blood.bloodbook.UI;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import android.app.AlertDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.blood.bloodbook.Application;
import com.blood.bloodbook.Network.ViewModel.LocationViewModel;
import com.blood.bloodbook.Network.ViewModel.ViewModel;
import com.blood.bloodbook.R;
import com.blood.bloodbook.UI.Adapter.DistrictNameDataAdapter;
import com.blood.bloodbook.UI.Adapter.LocationDataAdapter;
import com.blood.bloodbook.UI.Adapter.ProvinceNameDataAdapter;
import com.blood.bloodbook.Utils.Toast;
import com.blood.bloodbook.Widget.ProgressDialog;
import com.blood.bloodbook.databinding.LocationdialogBinding;
import com.blood.bloodbook.databinding.UpdatefinddonorlocationBinding;

import javax.inject.Inject;

public class UpdateFindDonorLocation extends AppCompatActivity {

    private UpdatefinddonorlocationBinding binding;
    private ViewModel viewModel;
    @Inject LocationDataAdapter locationDataAdapter;
    @Inject ProvinceNameDataAdapter provinceNameDataAdapter;
    private LocationViewModel locationViewModel;
    @Inject DistrictNameDataAdapter districtNameDataAdapter;
    @Inject
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.updatefinddonorlocation);
        viewModel = new ViewModelProvider(this).get(ViewModel.class);

        var component = Application.weenaComponent;
        component.InjectUpdateFindDonorLocation(this);
        locationViewModel = new ViewModelProvider(this).get(LocationViewModel.class);
        GetCountryName();
        GetProvincesName();
        GetDistrictName();
        IntiView();
        GetDataFromServer();
        UpdateLocation();
    }


    private void UpdateLocation(){
        binding.UpdateBtn.setOnClickListener(view -> {
            var CountryName = binding.Country.getText().toString().trim();
            var Province = binding.Province.getText().toString().trim();
            var District = binding.District.getText().toString().trim();
            var AreName = binding.AreaNameInput.getText().toString().trim();

            if(CountryName == ""){
                Toast.Message(UpdateFindDonorLocation.this, getResources().getString(R.string.CountryEmpty));
            }else if(Province == ""){
                Toast.Message(UpdateFindDonorLocation.this, getResources().getString(R.string.ProvinceEmpty));
            }else if(District == ""){
                Toast.Message(UpdateFindDonorLocation.this, getResources().getString(R.string.DistrictEmpty));
            }else if(AreName.isEmpty()){
                Toast.Message(UpdateFindDonorLocation.this, getResources().getString(R.string.AreaNameEmpty));
            }else {
                progressDialog.ProgressDialog(UpdateFindDonorLocation.this);
                viewModel.DonorUserUpdate(CountryName, Province, District, AreName).observe(this, aBoolean -> {
                    if(aBoolean){
                        progressDialog.CancelProgressDialog();
                        Toast.Message(UpdateFindDonorLocation.this, getResources().getString(R.string.Success));
                    }else {
                        progressDialog.CancelProgressDialog();
                    }
                });
            }
        });
    }

    private void GetDataFromServer(){
        viewModel.GetCurrentDonorUser().observe(this, donorModel -> {
            if(donorModel != null){
                binding.RegisterMessage.setVisibility(View.GONE);
                binding.UpdateBtn.setVisibility(View.VISIBLE);
                binding.UpdateLocationText.setVisibility(View.VISIBLE);
                if(donorModel.getCountry() != null){
                    binding.Country.setText(donorModel.getCountry());
                }
                if(donorModel.getProvince() != null){
                    binding.Province.setText(donorModel.getProvince());
                }
                if(donorModel.getDistrict() != null){
                    binding.District.setText(donorModel.getDistrict());
                }
                if(donorModel.getAreaName() != null){
                    binding.AreaNameInput.setText(donorModel.getAreaName());
                }
            }else {
                binding.UpdateBtn.setVisibility(View.GONE);
                binding.UpdateLocationText.setVisibility(View.GONE);
                binding.RegisterMessage.setVisibility(View.VISIBLE);
            }
        });
    }


    private void IntiView() {

        binding.Toolbar.BackButton.setOnClickListener(view -> {
            finish();
            Animatoo.animateSlideRight(UpdateFindDonorLocation.this);
        });
        binding.Toolbar.Title.setText(getResources().getString(R.string.UpdateLocation));

        binding.CountryName.setOnClickListener(view -> {
            locationViewModel.DeleteAllCountryNameData();
            var dialog = new AlertDialog.Builder(UpdateFindDonorLocation.this);
            LocationdialogBinding locationitemBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.locationdialog, null, false);
            dialog.setView(locationitemBinding.getRoot());

            var alertdialog = dialog.create();
            alertdialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            locationitemBinding.LocationRecyclerView.setHasFixedSize(true);
            locationitemBinding.LocationRecyclerView.setAdapter(locationDataAdapter);

            alertdialog.show();
            viewModel.GetCountryName().observe(UpdateFindDonorLocation.this, countryNameModels -> {
                locationViewModel.InsertCounyryNameData(countryNameModels);
                GetCountryName();

            });
            locationitemBinding.SearchInput.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    var CountryName = editable.toString();
                    if (CountryName.isEmpty()) {
                        GetCountryName();
                    } else {
                        GetCountryNameSearch(CountryName);
                    }
                }
            });

            locationDataAdapter.OnCountryLisiner(CountryName -> {
                binding.Country.setText(CountryName);
                binding.District.setText(null);
                binding.Province.setText(null);
                alertdialog.dismiss();
            });
        });

        binding.ProvinceStateInput.setOnClickListener(view -> {
            var CountryName = binding.Country.getText().toString().trim();
            if (CountryName == "") {
                Toast.Message(UpdateFindDonorLocation.this, getResources().getString(R.string.PleaseSelectCountryNameFirst));
            } else {
                locationViewModel.DeleteAllProvinceNameData();
                var dialog = new AlertDialog.Builder(UpdateFindDonorLocation.this);
                LocationdialogBinding locationitemBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.locationdialog, null, false);
                dialog.setView(locationitemBinding.getRoot());

                var alertdialog = dialog.create();
                locationitemBinding.LocationRecyclerView.setHasFixedSize(true);
                locationitemBinding.LocationRecyclerView.setAdapter(provinceNameDataAdapter);
                alertdialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                alertdialog.show();

                viewModel.GetProvinceName(CountryName).observe(UpdateFindDonorLocation.this, provinceNameModels -> {
                    locationViewModel.InsertProvincesNameData(provinceNameModels);
                });

                provinceNameDataAdapter.OnClickLisiner(ProvinceName -> {
                    alertdialog.dismiss();
                    binding.Province.setText(ProvinceName);
                    binding.District.setText(null);
                });
                locationitemBinding.SearchInput.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        var Data = editable.toString();
                        if(Data.isEmpty()){
                            GetProvincesName();
                        }else {
                            SearchProvinceName(Data);
                        }
                    }
                });
            }
        });

        binding.SelectDistrictInput.setOnClickListener(view -> {
            var CountryName = binding.Country.getText().toString().trim();
            var ProvincesName = binding.Province.getText().toString().trim();

            if(CountryName == ""){
                Toast.Message(UpdateFindDonorLocation.this, getResources().getString(R.string.CountryEmpty));
            }else if(ProvincesName == ""){
                Toast.Message(UpdateFindDonorLocation.this, getResources().getString(R.string.ProvinceEmpty));
            }else {
                locationViewModel.DeleteDistrct();
                var dialog = new AlertDialog.Builder(UpdateFindDonorLocation.this);
                LocationdialogBinding locationitemBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.locationdialog, null, false);
                dialog.setView(locationitemBinding.getRoot());

                var alertdialog = dialog.create();
                locationitemBinding.LocationRecyclerView.setHasFixedSize(true);
                locationitemBinding.LocationRecyclerView.setAdapter(districtNameDataAdapter);
                alertdialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                alertdialog.show();


                viewModel.GetDistrictName(CountryName, ProvincesName).observe(UpdateFindDonorLocation.this, districtNameModelList -> {
                    locationViewModel.InsertDistrict(districtNameModelList);
                });

                districtNameDataAdapter.OnClickLisiner(DictrictName -> {
                    alertdialog.dismiss();
                    binding.District.setText(DictrictName);
                });

                locationitemBinding.SearchInput.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        var Data = editable.toString();
                        if(Data.isEmpty()){
                            GetDistrictName();
                        }else {
                            SearchDustrictName(Data);
                        }
                    }
                });
            }
        });
    }


    private void GetCountryNameSearch(String CountryName) {
        locationViewModel.SearchByCountryName(CountryName).observe(UpdateFindDonorLocation.this, countryNameModels -> {
            locationDataAdapter.setList(countryNameModels);
            locationDataAdapter.notifyDataSetChanged();
        });
    }

    private void GetCountryName() {
        locationViewModel.GetCountryName().observe(UpdateFindDonorLocation.this, countryNameModels -> {
            locationDataAdapter.setList(countryNameModels);
            locationDataAdapter.notifyDataSetChanged();
        });
    }

    private void GetProvincesName(){
        locationViewModel.GetProvincesNameData().observe(UpdateFindDonorLocation.this, provinceNameModelList -> {
            provinceNameDataAdapter.setList(provinceNameModelList);
            provinceNameDataAdapter.notifyDataSetChanged();
        });
    }
    private void SearchProvinceName(String Name){
        locationViewModel.SearchProvincesName(Name).observe(UpdateFindDonorLocation.this, provinceNameModelList -> {
            provinceNameDataAdapter.setList(provinceNameModelList);
            provinceNameDataAdapter.notifyDataSetChanged();
        });
    }

    private void GetDistrictName(){
        locationViewModel.GetDistrictName().observe(UpdateFindDonorLocation.this, districtNameModelList -> {
            districtNameDataAdapter.setList(districtNameModelList);
            districtNameDataAdapter.notifyDataSetChanged();
        });
    }
    private void SearchDustrictName(String Name){
        locationViewModel.SearchDistrictName(Name).observe(UpdateFindDonorLocation.this, districtNameModelList -> {
            districtNameDataAdapter.setList(districtNameModelList);
            districtNameDataAdapter.notifyDataSetChanged();
        });
    }

    @Override
    public void onBackPressed() {
        finish();
        Animatoo.animateSlideRight(UpdateFindDonorLocation.this);
    }
}