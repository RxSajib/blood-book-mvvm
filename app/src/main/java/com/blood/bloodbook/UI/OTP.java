package com.blood.bloodbook.UI;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthProvider;
import com.blood.bloodbook.DI.DaggerWeenaComponent;
import com.blood.bloodbook.Data.DataManager;
import com.blood.bloodbook.Data.SharePref;
import com.blood.bloodbook.Network.ViewModel.ViewModel;
import com.blood.bloodbook.R;
import com.blood.bloodbook.Utils.HandleActivity;
import com.blood.bloodbook.Utils.Toast;
import com.blood.bloodbook.Widget.ProgressDialog;
import com.blood.bloodbook.databinding.OtpBinding;

import javax.inject.Inject;

import in.aabhasjindal.otptextview.OTPListener;

public class OTP extends AppCompatActivity {

    private OtpBinding binding;
    private String FullNumberWithCode;
    private String Token;
    @Inject
    ProgressDialog progressDialog;
    private SharePref sharePref;
    private ViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.otp);

        FullNumberWithCode = getIntent().getStringExtra(DataManager.PhoneNumber);
        Token = getIntent().getStringExtra(DataManager.Code);
        binding.setPhoneNumber(FullNumberWithCode);
        sharePref = new SharePref(OTP.this);

        viewModel = new ViewModelProvider(this).get(ViewModel.class);
        var component = DaggerWeenaComponent.create();
        component.InjectOTP(this);

        InitView();
    }

    private void InitView() {
        binding.Toolbar.Title.setText(getResources().getString(R.string.EnterYourOTP));
        binding.Toolbar.BackButton.setOnClickListener(view -> {
            finish();
            Animatoo.animateSlideRight(OTP.this);
        });

        binding.otpView.setOtpListener(new OTPListener() {
            @Override
            public void onInteractionListener() {
            }

            @Override
            public void onOTPComplete(@NonNull String s) {
                Verification(FullNumberWithCode, Token);
            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
        Animatoo.animateSlideRight(OTP.this);
    }

    private void Verification(String FullNumber, String Code) {
        progressDialog.ProgressDialog(OTP.this);
        var PhoneAuth = PhoneAuthProvider.getCredential(
                Token,
                Code
        );
        FirebaseAuth.getInstance().signInWithCredential(PhoneAuth).addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                viewModel.ProfileExists().observe(this, aBoolean -> {
                    if (aBoolean) {
                        progressDialog.CancelProgressDialog();
                        HandleActivity.GotoHome(OTP.this);
                        Animatoo.animateSlideLeft(OTP.this);
                        finish();
                    } else {
                        sharePref.SetData(DataManager.LoginType, DataManager.PhoneNumber);
                        sharePref.SetData(DataManager.PhoneNumber, FullNumber);
                        HandleActivity.GotoSetUpProfile(OTP.this);
                        Animatoo.animateSlideLeft(OTP.this);
                        progressDialog.CancelProgressDialog();
                        finish();
                    }
                });

            } else {
                progressDialog.CancelProgressDialog();
                Toast.Message(getApplicationContext(), task.getException().getMessage());
            }
        }).addOnFailureListener(e -> {
            progressDialog.CancelProgressDialog();
            Toast.Message(getApplicationContext(), e.getMessage());
        });
    }
}