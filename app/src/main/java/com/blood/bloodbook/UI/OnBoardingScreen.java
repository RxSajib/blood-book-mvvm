package com.blood.bloodbook.UI;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.viewpager2.widget.ViewPager2;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.blood.bloodbook.Application;
import com.blood.bloodbook.Data.Constant.OnBoardingData;
import com.blood.bloodbook.R;
import com.blood.bloodbook.UI.Adapter.OnBoardingAdapter;
import com.blood.bloodbook.Utils.HandleActivity;
import com.blood.bloodbook.databinding.OnboardingscreenBinding;

import javax.inject.Inject;

public class OnBoardingScreen extends AppCompatActivity {

    private OnboardingscreenBinding binding;
    @Inject
    OnBoardingAdapter onBoardingAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.onboardingscreen);

        var component = Application.weenaComponent;
        component.InjectOnBoardingScreen(this);

        Init_view();

        setup_onboarding_indicator();
        setcurrentonboarding_indicator(0);


        binding.ViewPagerID.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                super.onPageScrolled(position, positionOffset, positionOffsetPixels);
                setcurrentonboarding_indicator(position);
            }
        });
        binding.NextButton.setOnClickListener(v -> {
            if (binding.ViewPagerID.getCurrentItem() + 1 < onBoardingAdapter.getItemCount()) {
                binding.ViewPagerID.setCurrentItem(binding.ViewPagerID.getCurrentItem() + 1);
            } else {
                HandleActivity.GotoHome(OnBoardingScreen.this);
                finish();
            }
        });

    }


    private void Init_view() {
        onBoardingAdapter.setOnBoardingModelList(OnBoardingData.AddOnBoardingData());
        binding.ViewPagerID.setAdapter(onBoardingAdapter);
    }


    private void setup_onboarding_indicator() {
        ImageView[] indicator = new ImageView[onBoardingAdapter.getItemCount()];
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT
        );

        layoutParams.setMargins(8, 8, 8, 8);
        for (int i = 0; i < indicator.length; i++) {
            indicator[i] = new ImageView(getApplicationContext());
            indicator[i].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.onboarding_inactive)

            );

            indicator[i].setLayoutParams(layoutParams);
            binding.Indicator.addView(indicator[i]);

        }

    }

    private void setcurrentonboarding_indicator(int index) {
        int childcount = binding.Indicator.getChildCount();
        for (int i = 0; i < childcount; i++) {
            ImageView imageView = (ImageView) binding.Indicator.getChildAt(i);
            if (i == index) {
                imageView.setImageDrawable(
                        ContextCompat.getDrawable(getApplicationContext(), R.drawable.onboarding_active)
                );
            } else {
                imageView.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.onboarding_inactive));
            }
        }

        if (index == onBoardingAdapter.getItemCount() - 1) {
            //NextButton.setText("Start");
        } else {
            // NextButton.setText("Next");
        }
    }
}