package com.blood.bloodbook.UI;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.blood.bloodbook.Application;
import com.blood.bloodbook.DI.DaggerWeenaComponent;
import com.blood.bloodbook.Data.DataManager;
import com.blood.bloodbook.Data.SharePref;
import com.blood.bloodbook.Network.ViewModel.ViewModel;
import com.blood.bloodbook.R;
import com.blood.bloodbook.Utils.HandleActivity;
import com.blood.bloodbook.Utils.Toast;
import com.blood.bloodbook.Widget.ProgressDialog;
import com.blood.bloodbook.databinding.EmailsignupBinding;

import javax.inject.Inject;

public class EmailSignUp extends AppCompatActivity {

    private EmailsignupBinding binding;
    @Inject
    ProgressDialog progressDialog;
    private ViewModel viewModel;
    private SharePref sharePref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.emailsignup);

        sharePref = new SharePref(EmailSignUp.this);
        viewModel = new ViewModelProvider(this).get(ViewModel.class);
        var component = Application.weenaComponent;
        component.InjectEmailSignUp(this);

        InitView();
        CreateNewAccount();
    }

    private void InitView() {
        binding.Toolbar.Title.setText("Sign Up Account");
        binding.Toolbar.BackButton.setOnClickListener(view -> {
            finish();
            Animatoo.animateSlideRight(EmailSignUp.this);
        });

        binding.SignInText.setOnClickListener(view -> {
            HandleActivity.GotoEmailSIgnIn(EmailSignUp.this);
            Animatoo.animateSlideRight(EmailSignUp.this);
        });
    }

    private void CreateNewAccount() {
        binding.SignUpBtn.setOnClickListener(view -> {
            var Email = binding.EmailInput.getText().toString().trim();
            var Password = binding.PasswordInput.getText().toString().trim();
            var ConfirmPassword = binding.CPasswordInput.getText().toString().trim();

            if (Email.isEmpty()) {
                Toast.Message(EmailSignUp.this, "Email empty");
            } else if (Password.isEmpty()) {
                com.blood.bloodbook.Utils.Toast.Message(EmailSignUp.this, "Password empty");
            } else if (ConfirmPassword.isEmpty()) {
                com.blood.bloodbook.Utils.Toast.Message(EmailSignUp.this, "Confirm password empty");
            } else if (!Password.equals(ConfirmPassword)) {
                Toast.Message(EmailSignUp.this, "Password not match");
            } else if (Password.length() < 7) {
                Toast.Message(EmailSignUp.this, "Password need minimum 8 char");
            } else {
                progressDialog.ProgressDialog(EmailSignUp.this);
                viewModel.EmailSignUpAccount(Email, ConfirmPassword).observe(this, aBoolean -> {
                    if (aBoolean) {
                        progressDialog.CancelProgressDialog();
                        viewModel.ProfileExists().observe(this, aBoolean1 -> {
                            if (aBoolean1) {
                                HandleActivity.GotoHome(EmailSignUp.this);
                                finish();
                                Animatoo.animateSlideLeft(EmailSignUp.this);
                            } else {
                                HandleActivity.GotoSetUpProfile(EmailSignUp.this);
                                finish();
                                Animatoo.animateSlideLeft(EmailSignUp.this);
                                sharePref.SetData(DataManager.LoginType, DataManager.Email);
                                sharePref.SetData(DataManager.LoginAddress, Email);
                            }
                        });

                    } else {
                        progressDialog.CancelProgressDialog();
                    }
                });
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        Animatoo.animateSlideLeft(EmailSignUp.this);
    }
}