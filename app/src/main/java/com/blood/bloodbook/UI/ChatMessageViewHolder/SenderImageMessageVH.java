package com.blood.bloodbook.UI.ChatMessageViewHolder;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.imageview.ShapeableImageView;
import com.blood.bloodbook.R;

public class SenderImageMessageVH extends RecyclerView.ViewHolder {

    public ShapeableImageView senderimage;
    public TextView senderimagetime;

    public SenderImageMessageVH(@NonNull View itemView) {
        super(itemView);

        senderimage = itemView.findViewById(R.id.SenderImage);
        senderimagetime = itemView.findViewById(R.id.SenderImageTime);
    }
}
