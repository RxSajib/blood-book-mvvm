package com.blood.bloodbook.UI.Adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.blood.bloodbook.AcceptBloodRequestModel;
import com.blood.bloodbook.UI.ViewHolder.AcceptBloodRequestViewHolder;
import com.blood.bloodbook.databinding.AcceptbloodrequestitemBinding;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import lombok.Getter;
import lombok.Setter;
@Singleton
public class AcceptBloodRequestAdapter extends RecyclerView.Adapter<AcceptBloodRequestViewHolder> {
    @Setter @Getter
    private List<AcceptBloodRequestModel> list;
    private OnClick OnClick;

    @Inject
    public AcceptBloodRequestAdapter(){

    }

    @NonNull
    @Override
    public AcceptBloodRequestViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        var l = LayoutInflater.from(parent.getContext());
        var v = AcceptbloodrequestitemBinding.inflate(l, parent, false);
        return new AcceptBloodRequestViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull AcceptBloodRequestViewHolder holder, int position) {
        holder.binding.BloodCondition.setText(list.get(position).getTypeOfPatientsCondition());
        holder.binding.BloodGroup.setText(list.get(position).getBloodGroup());
        holder.binding.PatientName.setText(list.get(position).getHospitalName());
        holder.binding.Date.setText(list.get(position).getBloodRequiredDate());
        holder.binding.Time.setText(list.get(position).getBloodRequiredTime());
        holder.binding.Location.setText(list.get(position).getProvince()+" "+list.get(position).getDistrict());
        holder.binding.Quantity.setText(list.get(position).getNumberOFQuantityPints()+"CC");

        holder.itemView.setOnClickListener(view -> {
            OnClick.Click(list.get(position));
        });
    }

    @Override
    public int getItemCount() {
        if(list == null) {
            return 0;
        }return list.size();
    }

    public interface OnClick{
        void Click(AcceptBloodRequestModel bloodRequestModel);
    }
    public void OnClickState(OnClick OnClick){
        this.OnClick = OnClick;
    }
}
