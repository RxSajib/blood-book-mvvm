package com.blood.bloodbook.UI.ViewHolder;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.blood.bloodbook.databinding.AcceptbloodrequestitemBinding;

public class AcceptBloodRequestViewHolder extends RecyclerView.ViewHolder {

    public AcceptbloodrequestitemBinding binding;

    public AcceptBloodRequestViewHolder(@NonNull AcceptbloodrequestitemBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }
}
