package com.blood.bloodbook.UI;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.blood.bloodbook.Application;
import com.blood.bloodbook.DI.DaggerWeenaComponent;
import com.blood.bloodbook.Network.ViewModel.ViewModel;
import com.blood.bloodbook.R;
import com.blood.bloodbook.Utils.HandleActivity;
import com.blood.bloodbook.Utils.Toast;
import com.blood.bloodbook.Widget.ProgressDialog;
import com.blood.bloodbook.databinding.EmailsigninBinding;

import javax.inject.Inject;

public class EmailSignIn extends AppCompatActivity {

    private EmailsigninBinding binding;
    private ViewModel viewModel;
    @Inject
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.emailsignin);
        viewModel = new ViewModelProvider(this).get(ViewModel.class);

        var component = Application.weenaComponent;
        component.InjectEmailSignIn(this);


        InitView();
        SignInAccount();
    }

    private void InitView() {
        binding.Toolbar.BackButton.setOnClickListener(view -> {
            finish();
            Animatoo.animateSlideRight(EmailSignIn.this);
        });

        binding.Toolbar.Title.setText("Sign In Account");

        binding.SignUpText.setOnClickListener(view -> {
            HandleActivity.GotoEmailSIgnUp(EmailSignIn.this);
            Animatoo.animateSlideLeft(EmailSignIn.this);
        });

        binding.ForGotPassword.setOnClickListener(view -> {
            HandleActivity.GotoResetPassword(EmailSignIn.this);
            Animatoo.animateSlideLeft(EmailSignIn.this);
        });
    }

    private void SignInAccount() {
        binding.SignInBtn.setOnClickListener(view -> {
            var EmailAddress = binding.EmailInput.getText().toString().trim();
            var Password = binding.PasswordInput.getText().toString().trim();
            if (EmailAddress.isEmpty()) {
                Toast.Message(EmailSignIn.this, "Email empty");
            } else if (Password.isEmpty()) {
                Toast.Message(EmailSignIn.this, "Password empty");
            } else {
                progressDialog.ProgressDialog(EmailSignIn.this);
                viewModel.EmailSignInAccount(EmailAddress, Password).observe(this, aBoolean -> {
                    if (aBoolean) {
                        progressDialog.CancelProgressDialog();
                        viewModel.ProfileExists().observe(this, aBoolean1 -> {
                            if (!aBoolean1) {
                                HandleActivity.GotoSetUpProfile(EmailSignIn.this);
                                finish();
                                Animatoo.animateSlideLeft(EmailSignIn.this);
                            } else {
                                viewModel.CreateProfile(null, null, null, null, null, EmailAddress, EmailAddress).observe(this, new Observer<Boolean>() {
                                    @Override
                                    public void onChanged(Boolean aBoolean) {
                                        if (aBoolean) {
                                            if (aBoolean) {
                                                Toast.Message(EmailSignIn.this, "Login success");
                                                HandleActivity.GotoHome(EmailSignIn.this);
                                                finish();
                                                Animatoo.animateSlideLeft(EmailSignIn.this);
                                            } else {

                                            }
                                        }
                                    }
                                });

                            }
                        });

                    } else {
                        progressDialog.CancelProgressDialog();
                    }
                });
            }
        });
    }


    @Override
    public void onBackPressed() {
        finish();
        Animatoo.animateSlideRight(EmailSignIn.this);
    }
}