package com.blood.bloodbook.UI.Adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.blood.bloodbook.Data.DataManager;
import com.blood.bloodbook.MessageHistoryModel;
import com.blood.bloodbook.UI.ViewHolder.MessageHistoryViewHolder;
import com.blood.bloodbook.Utils.TimestampDayConverter;
import com.blood.bloodbook.databinding.TextmessagenotificationitemBinding;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.squareup.picasso.Picasso;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import lombok.Getter;
import lombok.Setter;
@Singleton
public class MessageHistoryAdapter extends RecyclerView.Adapter<MessageHistoryViewHolder> {
    @Setter @Getter
    private List<MessageHistoryModel> list;
    private CollectionReference UserRef;
    private OnClick OnClick;

    @Inject
    public MessageHistoryAdapter(){}

    @NonNull
    @Override
    public MessageHistoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        var l = LayoutInflater.from(parent.getContext());
        var v = TextmessagenotificationitemBinding.inflate(l, parent, false);
        UserRef = FirebaseFirestore.getInstance().collection(DataManager.User);
        return new MessageHistoryViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MessageHistoryViewHolder holder, int position) {
        holder.binding.MessageText.setText(list.get(position).getMessage());
        holder.binding.TimeDate.setText(TimestampDayConverter.getMyPrettyDate(list.get(position).getTimestamp()));
        UserRef.document(list.get(position).getSenderUID())
                .addSnapshotListener((value, error) -> {
                    if(error != null){
                        return;
                    }
                    if(value.exists()){
                        var imageuri = value.getString(DataManager.ProfileImage).toString();
                        Picasso.get().load(imageuri).into(holder.binding.ProfileImage);
                        var FirstName = value.getString(DataManager.FirstName).toString();
                        holder.binding.NameOfSenderName.setText(FirstName);
                    }
                });
        holder.itemView.setOnClickListener(view -> {
            OnClick.Click(list.get(position).getSenderUID(), list.get(position).getReceiverUID());
        });
    }

    @Override
    public int getItemCount() {
        if(list == null) {
            return 0;
        }return list.size();
    }

    public interface OnClick{
        void Click(String SenderUID, String ReceiverUID);
    }
    public void OnClickState(OnClick OnClick){
        this.OnClick = OnClick;
    }

}
