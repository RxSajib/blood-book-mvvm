package com.blood.bloodbook.UI.Adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.blood.bloodbook.BloodBankModel;
import com.blood.bloodbook.databinding.StorageitemBinding;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import lombok.Getter;
import lombok.Setter;
@Singleton
public class BloodStorageAdapter extends RecyclerView.Adapter<com.blood.bloodbook.UI.ViewHolder.StorageViewHolder> {

    @Setter @Getter
    private List<BloodBankModel> list;

    @Inject
    public BloodStorageAdapter(){

    }

    @NonNull
    @Override
    public com.blood.bloodbook.UI.ViewHolder.StorageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        var l = LayoutInflater.from(parent.getContext());
        var v = StorageitemBinding.inflate(l, parent, false);
        return new com.blood.bloodbook.UI.ViewHolder.StorageViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull com.blood.bloodbook.UI.ViewHolder.StorageViewHolder holder, int position) {
        holder.binding.setStorage(list.get(position));
    }

    @Override
    public int getItemCount() {
        if(list == null) {
            return 0;
        }return list.size();
    }
}
