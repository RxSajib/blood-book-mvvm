package com.blood.bloodbook.UI;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentPagerAdapter;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.blood.bloodbook.R;
import com.blood.bloodbook.UI.Adapter.RequestViewPagerAdapter;
import com.blood.bloodbook.UI.BloodRequest.AcceptedRequest;
import com.blood.bloodbook.UI.BloodRequest.MyRequest;
import com.blood.bloodbook.UI.BloodRequest.ReceiveRequest;
import com.blood.bloodbook.Utils.HandleActivity;
import com.blood.bloodbook.databinding.RequestofbloodhomeBinding;

import android.os.Bundle;

public class RequestOFBloodHome extends AppCompatActivity {

    private RequestofbloodhomeBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.requestofbloodhome);


        SetFragment();
        InitView();
    }

    private void InitView() {
        binding.Toolbar.BackButton.setOnClickListener(view -> {
            finish();
            Animatoo.animateSlideRight(RequestOFBloodHome.this);
        });
        binding.Toolbar.Title.setText(getResources().getString(R.string.BloodRequest));
        binding.RequestBtn.setOnClickListener(view -> {
            HandleActivity.GotoAddRequest(RequestOFBloodHome.this);
            Animatoo.animateSlideLeft(RequestOFBloodHome.this);
        });
    }

    private void SetFragment() {
        binding.TabLayout.setupWithViewPager(binding.ViewPager);
        var fadapter = new RequestViewPagerAdapter(getSupportFragmentManager(), FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        fadapter.AddFragement(new ReceiveRequest(), getResources().getString(R.string.ReceiveRequest));
        fadapter.AddFragement(new MyRequest(), getResources().getString(R.string.MyRequest));
        fadapter.AddFragement(new AcceptedRequest(), getResources().getString(R.string.AcceptedRequests));

        binding.ViewPager.setAdapter(fadapter);
    }

    @Override
    public void onBackPressed() {
        finish();
        Animatoo.animateSlideRight(RequestOFBloodHome.this);
    }
}