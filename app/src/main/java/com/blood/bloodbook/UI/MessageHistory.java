package com.blood.bloodbook.UI;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Bundle;
import android.view.View;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.blood.bloodbook.Application;
import com.blood.bloodbook.MessageHistoryModel;
import com.blood.bloodbook.Network.ViewModel.ViewModel;
import com.blood.bloodbook.R;
import com.blood.bloodbook.UI.Adapter.MessageHistoryAdapter;
import com.blood.bloodbook.Utils.HandleActivity;
import com.blood.bloodbook.Utils.Toast;
import com.blood.bloodbook.databinding.MessagehistoryBinding;

import java.util.List;

import javax.inject.Inject;

public class MessageHistory extends AppCompatActivity {

    private MessagehistoryBinding binding;
    private ViewModel viewModel;
    @Inject
    MessageHistoryAdapter messageHistoryAdapter;
    private int Limit = 25;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.messagehistory);
        viewModel = new ViewModelProvider(this).get(ViewModel.class);
        var component = Application.weenaComponent;
        component.InjectMessageHistory(this);

        InitView();
        binding.SwipeRefreshLayout.setOnRefreshListener(() -> {
            GetData(Limit);
        });

        binding.RecyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                Limit = Limit+25;
                GetData(Limit);
            }
        });
        GetData(Limit);
    }

    private void GetData(int Limit){
        viewModel.MessageHistoryGET(Limit).observe(this, messageHistoryModels -> {
            messageHistoryAdapter.setList(messageHistoryModels);
            messageHistoryAdapter.notifyDataSetChanged();
            binding.SwipeRefreshLayout.setRefreshing(false);
            if(messageHistoryModels != null){
                binding.UserEmptyFeedbackIcon.setVisibility(View.GONE);
                binding.UserEmptyFeedbackMessage.setVisibility(View.GONE);
            }else{
                binding.UserEmptyFeedbackIcon.setVisibility(View.VISIBLE);
                binding.UserEmptyFeedbackMessage.setVisibility(View.VISIBLE);
            }
        });
    }

    private void InitView(){
        binding.RecyclerView.setHasFixedSize(true);
        binding.RecyclerView.setAdapter(messageHistoryAdapter);
        binding.Toolbar.BackButton.setOnClickListener(view -> {
            finish();
            Animatoo.animateSlideRight(MessageHistory.this);
        });
        binding.Toolbar.Title.setText(R.string.MessageHistory);

        messageHistoryAdapter.OnClickState((SenderUID, ReceiverUID) -> {
            HandleActivity.GotoChat(MessageHistory.this, SenderUID, null, null);
            Animatoo.animateSlideLeft(MessageHistory.this);
        });
    }

    @Override
    public void onBackPressed() {
        finish();
        Animatoo.animateSlideRight(MessageHistory.this);
    }
}
