package com.blood.bloodbook.UI;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.View;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.blood.bloodbook.Application;
import com.blood.bloodbook.Data.Data;
import com.blood.bloodbook.Data.Model.AdminModel;
import com.blood.bloodbook.Data.NotifactionResponse;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.firebase.auth.FirebaseAuth;
import com.squareup.picasso.Picasso;
import com.blood.bloodbook.ContactModel;
import com.blood.bloodbook.DI.DaggerWeenaComponent;
import com.blood.bloodbook.Data.DataManager;
import com.blood.bloodbook.DonorModel;
import com.blood.bloodbook.Network.ViewModel.ViewModel;
import com.blood.bloodbook.R;
import com.blood.bloodbook.UI.Adapter.ContactAdapter;
import com.blood.bloodbook.Utils.HandleActivity;
import com.blood.bloodbook.Utils.Permission;
import com.blood.bloodbook.Utils.Toast;
import com.blood.bloodbook.Widget.ProgressDialog;
import com.blood.bloodbook.databinding.ContactlistdialogBinding;
import com.blood.bloodbook.databinding.DonoruserdetailsBinding;
import com.blood.bloodbook.databinding.ReportdialogBinding;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class DonorUserDetails extends AppCompatActivity {

    private DonoruserdetailsBinding binding;
    private DonorModel donorModel;
    @Inject
    ContactAdapter contactAdapter;
    private List<ContactModel> contactModelList = new ArrayList<>();
    private ViewModel viewModel;
    private FirebaseAuth Mauth;
    private ProgressDialog progressDialog = new ProgressDialog();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.donoruserdetails);
        donorModel = (DonorModel)getIntent().getSerializableExtra(DataManager.Data);
        viewModel = new ViewModelProvider(this).get(ViewModel.class);
        Mauth = FirebaseAuth.getInstance();

        var component = Application.weenaComponent;
        component.InjectDonorUserDetails(this);

        binding.setDonor(donorModel);
        InitView();
        SetContact();
        GetProfileImage();

        var FirebaseUser = Mauth.getCurrentUser();
        if(!FirebaseUser.getUid().equals(donorModel.getSenderUID())){
            binding.ChatNowText.setVisibility(View.VISIBLE);
            binding.ChatIcon.setVisibility(View.VISIBLE);
            binding.ChatBtn.setVisibility(View.VISIBLE);

            binding.DonorStatus.setVisibility(View.VISIBLE);
            binding.Icon.setVisibility(View.VISIBLE);
            binding.AVAILABLEText.setVisibility(View.VISIBLE);
        }


        viewModel.GetVideoAds().observe(this, videoAdsModel -> {
            if(videoAdsModel != null){
             //   Toast.Message(getApplicationContext(), videoAdsModel.getVideoAdsUri());
            }
        });
        SetVideo();
    }

    private void SetVideo(){
    //    var uri = Uri.parse("android.resource://"+getPackageName()+"/"+R.raw.sample);
     //   binding.VideoView.setVideoURI(uri);
     //   binding.VideoView.start();
    }

    private void GetProfileImage(){
        viewModel.GetUserInfo(donorModel.getSenderUID()).observe(this, profileModel -> {
            if(profileModel != null){
                Picasso.get().load(profileModel.getProfileImage()).into(binding.ProfileImage);
            }
        });
    }

    private void SetContact(){
        if(donorModel.getMainPhoneNumber() != null){
            var MainNumber = new ContactModel("Main Number", donorModel.getMainPhoneNumber());
            contactModelList.add(MainNumber);
        }
        if(donorModel.getRoshanNumber() != null){
            var RoshanNumber = new ContactModel(DataManager.RoshanNumber, donorModel.getRoshanNumber());
            contactModelList.add(RoshanNumber);
        }
        if(donorModel.getEtisalatNumber() != null){
            var EtisalatNumber = new ContactModel(DataManager.EtisalatNumber, donorModel.getEtisalatNumber());
            contactModelList.add(EtisalatNumber);
        }
        if(donorModel.getAWCCNumber() != null){
            var AWCC = new ContactModel(DataManager.AWCCNumber, donorModel.getAWCCNumber());
            contactModelList.add(AWCC);
        }
        if(donorModel.getMTNNumber() != null){
            var MTNNumber = new ContactModel(DataManager.MTNNumber, donorModel.getMTNNumber());
            contactModelList.add(MTNNumber);
        }
        if(donorModel.getWhatsappNumber() != null){
            var whatsappnumber = new ContactModel(DataManager.WhatsappNumber, donorModel.getWhatsappNumber());
            contactModelList.add(whatsappnumber);
        }

    }

    private void InitView(){
        binding.Toolbar.BackButton.setOnClickListener(view -> {
            finish();
            Animatoo.animateSlideRight(DonorUserDetails.this);
        });
        binding.Toolbar.Title.setText(donorModel.getGivenName());

        binding.CallBtn.setOnClickListener(view -> {
            var dialog = new AlertDialog.Builder(DonorUserDetails.this);
            ContactlistdialogBinding v = DataBindingUtil.inflate(getLayoutInflater(), R.layout.contactlistdialog, null, false);
            dialog.setView(v.getRoot());


            AlertDialog alertDialog = dialog.create();
            alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            alertDialog.show();

            v.ContactRecyclerView.setHasFixedSize(true);
            contactAdapter.setList(contactModelList);
            v.ContactRecyclerView.setAdapter(contactAdapter);

            v.ContinueBtn.setOnClickListener(view1 -> {
                alertDialog.dismiss();
            });
        });

        contactAdapter.OnCLickLisiner((Number, Type) -> {
            var messagetext = "";
            if(Type.equals(DataManager.Call)){
                if(Permission.PermissionCall(DonorUserDetails.this, 544)){
                    var intent = new Intent(Intent.ACTION_CALL);
                    intent.setData(Uri.parse("tel:"+Number));
                    startActivity(intent);
                }

                if(Type.equals(DataManager.Message)){
                    if(Permission.PermissionSendMessage(DonorUserDetails.this, 200)){
                        var smsmanager = SmsManager.getDefault();
                       // smsmanager.sendTextMessage(Number, );
                    }
                }

            }
        });

        binding.ChatBtn.setOnClickListener(view -> {
            HandleActivity.GotoChat(DonorUserDetails.this, donorModel.getSenderUID(), donorModel.getGivenName(), donorModel.getSurname());
            Animatoo.animateSlideLeft(DonorUserDetails.this);
        });

        binding.DonorStatus.setOnClickListener(view -> {
            var alertdialog = new MaterialAlertDialogBuilder(DonorUserDetails.this);
            ReportdialogBinding v = DataBindingUtil.inflate(getLayoutInflater(), R.layout.reportdialog, null, false);
            alertdialog.setView(v.getRoot());

            var d = alertdialog.create();
            d.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            d.show();

            v.ReportBtn.setOnClickListener(view1 -> {
                var FeedbackMessage = v.ReportInput.getText().toString().trim();
                if(FeedbackMessage.isEmpty()){
                    Toast.Message(this, "Input your feedback");
                }else {

                    d.dismiss();
                    progressDialog.ProgressDialog(this);
                    viewModel.SendRepost(donorModel.getSenderUID(), FeedbackMessage).observe(this, aBoolean -> {
                        if(aBoolean){
                            viewModel.GetAdminUser().observe(DonorUserDetails.this, adminModel -> {
                                if(adminModel != null){
                                    var data = new Data(FeedbackMessage, "Report");
                                    var notificationresponse = new NotifactionResponse(data, adminModel.getToken());
                                    viewModel.SendNotification(notificationresponse).observe(DonorUserDetails.this, aBoolean1 -> {
                                        if(aBoolean1){
                                            progressDialog.CancelProgressDialog();
                                        }else {
                                            progressDialog.CancelProgressDialog();
                                        }
                                    });

                                }else {
                                    progressDialog.CancelProgressDialog();
                                }
                            });

                        }else {
                            progressDialog.CancelProgressDialog();
                        }
                    });
                }
            });
        });
    }

    @Override
    public void onBackPressed() {
        finish();
        Animatoo.animateSlideRight(DonorUserDetails.this);
    }
}