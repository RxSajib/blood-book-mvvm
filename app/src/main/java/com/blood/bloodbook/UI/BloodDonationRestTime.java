package com.blood.bloodbook.UI;

import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.blood.bloodbook.Application;
import com.google.firebase.auth.FirebaseAuth;
import com.blood.bloodbook.DI.DaggerWeenaComponent;
import com.blood.bloodbook.Data.Constant.BloodDonationRestTimeData;
import com.blood.bloodbook.Data.DataManager;
import com.blood.bloodbook.Network.ViewModel.ViewModel;
import com.blood.bloodbook.R;
import com.blood.bloodbook.UI.Adapter.BloodDonationRestTimeAdapter;
import com.blood.bloodbook.Utils.Toast;
import com.blood.bloodbook.Widget.ProgressDialog;
import com.blood.bloodbook.databinding.BlooddonationresttimeBinding;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.inject.Inject;

public class BloodDonationRestTime extends AppCompatActivity {

    private BlooddonationresttimeBinding binding;
    private ViewModel viewModel;
    @Inject
    BloodDonationRestTimeAdapter bloodDonationRestTimeAdapter;
    private String Date;
    @Inject
    ProgressDialog progressDialog;
    private FirebaseAuth Mauth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.blooddonationresttime);
        viewModel = new ViewModelProvider(this).get(ViewModel.class);
        Mauth = FirebaseAuth.getInstance();


        var component = Application.weenaComponent;
        component.InjectBloodDonationRestTime(this);

        InitView();
        GetData();
        SetData();
    }

    private void SetData() {
        binding.RecyclerView.setHasFixedSize(true);
        binding.RecyclerView.setAdapter(bloodDonationRestTimeAdapter);
        bloodDonationRestTimeAdapter.setList(BloodDonationRestTimeData.GetBloodDonationRestTime());
    }

    private void GetData() {
        viewModel.GetSingleDonorUser(Mauth.getCurrentUser().getUid()).observe(this, donorModel -> {
            if(donorModel != null){
                binding.Date.setText(donorModel.getLastDonationDate());
                Date = donorModel.getLastDonationDate();
            }
        });
    }

    private void InitView() {
        binding.Toolbar.Title.setText(getResources().getString(R.string.BloodDonationRestTime));
        binding.Toolbar.BackButton.setOnClickListener(view -> {
            finish();
            Animatoo.animateSlideRight(BloodDonationRestTime.this);
        });

        bloodDonationRestTimeAdapter.OnClickEvent(DateOfMonth -> {
            binding.UpdateDonationRestTime.setVisibility(View.VISIBLE);
            binding.UpdateDonationRestTimeText.setVisibility(View.VISIBLE);
            binding.setUpdateBtnText(DateOfMonth);

            if (DateOfMonth.equals(DataManager.ThreeMonth)) {
                binding.NextDonationDate.setText(Date(90));
            }
            if (DateOfMonth.equals(DataManager.FourMonth)) {
                binding.NextDonationDate.setText(Date(120));
            }
            if (DateOfMonth.equals(DataManager.FiveMonth)) {
                binding.NextDonationDate.setText(Date(150));
            }
            if (DateOfMonth.equals(DataManager.SixMonth)) {
                binding.NextDonationDate.setText(Date(180));
            }
            if (DateOfMonth.equals(DataManager.SevenMonth)) {
                binding.NextDonationDate.setText(Date(210));
            }
            if (DateOfMonth.equals(DataManager.EightMonth)) {
                binding.NextDonationDate.setText(Date(240));
            }
            if (DateOfMonth.equals(DataManager.NineMonth)) {
                binding.NextDonationDate.setText(Date(270));
            }
            if (DateOfMonth.equals(DataManager.TenMonth)) {
                binding.NextDonationDate.setText(Date(300));
            }
            if(DateOfMonth.equals(DataManager.ElevenMonth)){
                binding.NextDonationDate.setText(Date(330));
            }
            if(DateOfMonth.equals(DataManager.TwelveMonth)){
                binding.NextDonationDate.setText(Date(360));
            }

            binding.UpdateDonationRestTime.setOnClickListener(view -> {
                if(DateOfMonth == null){
                    Toast.Message(getApplicationContext(), "Please select date");
                }else {
                    progressDialog.ProgressDialog(BloodDonationRestTime.this);
                    var month= DateOfMonth.replaceAll("[^0-9]", "");
                    viewModel.UpdateDonateMonthPOST(month).observe(this, aBoolean -> {
                        if(aBoolean){
                            progressDialog.CancelProgressDialog();
                        }else {
                            progressDialog.CancelProgressDialog();
                        }
                    });
                }
            });

        });
    }

    @Override
    public void onBackPressed() {
        finish();
        Animatoo.animateSlideRight(BloodDonationRestTime.this);
    }

    private String Date(int Days) {
        var dt = Date;
        var sdf = new SimpleDateFormat(DataManager.DatePattern);
        var c = Calendar.getInstance();
        try {
            c.setTime(sdf.parse(dt));
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
        c.add(Calendar.DATE, Days);
        dt = sdf.format(c.getTime());
        return dt;
    }
}