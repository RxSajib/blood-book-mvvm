package com.blood.bloodbook.UI.Adapter;

import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.squareup.picasso.Picasso;
import com.blood.bloodbook.Data.DataManager;
import com.blood.bloodbook.MessageModel;
import com.blood.bloodbook.R;
import com.blood.bloodbook.UI.ChatMessageViewHolder.ReceiverFileMessageVH;
import com.blood.bloodbook.UI.ChatMessageViewHolder.ReceiverImageMessageVH;
import com.blood.bloodbook.UI.ChatMessageViewHolder.ReceiverTextMessageVH;
import com.blood.bloodbook.UI.ChatMessageViewHolder.SendFileMessageVH;
import com.blood.bloodbook.UI.ChatMessageViewHolder.SenderImageMessageVH;
import com.blood.bloodbook.UI.ChatMessageViewHolder.SenderTextMessageVH;

import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;
import javax.inject.Singleton;

import lombok.Getter;
import lombok.Setter;
@Singleton
public class MessageAdapter extends RecyclerView.Adapter {

    @Inject
    public MessageAdapter(){}

    @Getter
    @Setter
    private List<MessageModel> list;

    private int TextMessageSend = 1;
    private int TextMessageReceive = 2;
    private int ImageMessageSend = 3;
    private int ImageMessageReceive = 4;
    private int DocumentMessageSend = 5;
    private int DocumentMessageReceive = 6;
    private OnDate OnDate;
    private OnImage OnImage;
    private OnClick OnClick;
    private OnDocumentDownload OnDocumentDownload;


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if(viewType == TextMessageSend){
            return new SenderTextMessageVH(LayoutInflater.from(parent.getContext()).inflate(R.layout.sendermessage, parent, false));
        }
        if(viewType == TextMessageReceive) {
            return new ReceiverTextMessageVH(LayoutInflater.from(parent.getContext()).inflate(R.layout.receivertextmessage, parent, false));
        }
        if(viewType == ImageMessageSend){
            return new SenderImageMessageVH(LayoutInflater.from(parent.getContext()).inflate(R.layout.senderimage, parent, false));
        }
        if(viewType == ImageMessageReceive){
            return new ReceiverImageMessageVH(LayoutInflater.from(parent.getContext()).inflate(R.layout.receverimage, parent, false));
        }
        if(viewType == DocumentMessageSend){
            return new SendFileMessageVH(LayoutInflater.from(parent.getContext()).inflate(R.layout.sendfile, parent, false));
        }
        if(viewType == DocumentMessageReceive){
            return new ReceiverFileMessageVH(LayoutInflater.from(parent.getContext()).inflate(R.layout.receivefile, parent, false));
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        if(holder.getClass() == SenderTextMessageVH.class){
            var Type = list.get(position).getType();
            if(Type.equals(DataManager.Text)){
                var senderTextMessageVH = (SenderTextMessageVH) holder;
                senderTextMessageVH.Message.setText(list.get(position).getMessage());

                var calender = Calendar.getInstance(Locale.ENGLISH);
                calender.setTimeInMillis(list.get(position).getTimestamp());
                var time = DateFormat.format(DataManager.TimeFormat, calender).toString();
                senderTextMessageVH.TimeDate.setText(time);
            }
        }
        if(holder.getClass() == ReceiverTextMessageVH.class){
            var Type = list.get(position).getType();
            if(Type.equals(DataManager.Text)){
                var receiverTextMessageVH = (ReceiverTextMessageVH) holder;
                receiverTextMessageVH.Message.setText(list.get(position).getMessage());
                var calender = Calendar.getInstance(Locale.ENGLISH);
                calender.setTimeInMillis(list.get(position).getTimestamp());
                var time = DateFormat.format(DataManager.TimeFormat, calender).toString();
                receiverTextMessageVH.TimeDate.setText(time);
            }
        }

        if(holder.getClass() == SenderImageMessageVH.class){
            var Type = list.get(position).getFileExtension();
            if(Type.equals(DataManager.JPG)){
                var senderImageMessageVH = (SenderImageMessageVH) holder;
                Picasso.get().load(list.get(position).getMessage()).placeholder(R.drawable.imagemessageplaceholder).into(senderImageMessageVH.senderimage);
                var calender = Calendar.getInstance(Locale.ENGLISH);
                calender.setTimeInMillis(list.get(position).getTimestamp());
                var time = DateFormat.format(DataManager.TimeFormat, calender).toString();
                senderImageMessageVH.senderimagetime.setText(time);

                senderImageMessageVH.senderimage.setOnClickListener(view -> {
                    OnImage.Image(list.get(position).getMessage(), list.get(position).getFileName(), list.get(position).getTimestamp());
                });
            }
        }

        if(holder.getClass() == ReceiverImageMessageVH.class){
            var Type = list.get(position).getFileExtension();

            if(Type.equals(DataManager.JPG)){
                var receiverImageMessageVH = (ReceiverImageMessageVH) holder;
                Picasso.get().load(list.get(position).getMessage()).placeholder(R.drawable.imagemessageplaceholder).into(receiverImageMessageVH.receiverimage);
                var calender = Calendar.getInstance(Locale.ENGLISH);
                calender.setTimeInMillis(list.get(position).getTimestamp());
                var time = DateFormat.format(DataManager.TimeFormat, calender).toString();
                receiverImageMessageVH.receiverimagetime.setText(time);

                receiverImageMessageVH.receiverimage.setOnClickListener(view -> {
                    OnImage.Image(list.get(position).getMessage(), list.get(position).getFileName(), list.get(position).getTimestamp());
                });
            }
        }

        if(holder.getClass() == SendFileMessageVH.class){
            var Type = list.get(position).getFileExtension();

            if( Type.equals(DataManager.PDF) || Type.equals(DataManager.DOC) || Type.equals(DataManager.DOCX) || Type.equals(DataManager.XLS) || Type.equals(DataManager.XLSX)){
                var sendFileMessageVH = (SendFileMessageVH) holder;
                sendFileMessageVH.FileName.setText(list.get(position).getFileName());
                sendFileMessageVH.FileSize.setText(list.get(position).getFileSize()+" "+holder.itemView.getContext().getResources().getString(R.string.UploadFileSize));

                if(Type.equals(DataManager.PDF)){
                    sendFileMessageVH.FileType.setText(DataManager.PDFFILE);
                }
                if(Type.equals(DataManager.DOC)){
                    sendFileMessageVH.FileType.setText(DataManager.DOCFILE);
                }
                if(Type.equals(DataManager.DOCX)){
                    sendFileMessageVH.FileType.setText(DataManager.DOCXFILE);
                }
                if(Type.equals(DataManager.XLS)){
                    sendFileMessageVH.FileType.setText(DataManager.XLSFILE);
                }
                if(Type.equals(DataManager.XLSX)){
                    sendFileMessageVH.FileType.setText(DataManager.XLSXFILE);
                }

                var calender = Calendar.getInstance(Locale.ENGLISH);
                calender.setTimeInMillis(list.get(position).getTimestamp());
                var time = DateFormat.format(DataManager.TimeFormat, calender).toString();
                sendFileMessageVH.Time.setText(time);

                sendFileMessageVH.Download.setOnClickListener(view -> {
                    OnDocumentDownload.OnDownload(list.get(position).getMessage(), list.get(position).getFileName(), list.get(position).getFileExtension());
                });
            }
        }
        if(holder.getClass() == ReceiverFileMessageVH.class){
            var Type = list.get(position).getFileExtension();

            if( Type.equals(DataManager.PDF) || Type.equals(DataManager.DOC) || Type.equals(DataManager.DOCX) || Type.equals(DataManager.XLS) || Type.equals(DataManager.XLSX)){
                var sendFileMessageVH = (ReceiverFileMessageVH) holder;
                sendFileMessageVH.FileName.setText(list.get(position).getFileName());
                sendFileMessageVH.FileSize.setText(list.get(position).getFileSize()+" "+holder.itemView.getContext().getResources().getString(R.string.UploadFileSize));

                if(Type.equals(DataManager.PDF)){
                    sendFileMessageVH.FileType.setText(DataManager.PDFFILE);
                }
                if(Type.equals(DataManager.DOC)){
                    sendFileMessageVH.FileType.setText(DataManager.DOCFILE);
                }
                if(Type.equals(DataManager.DOCX)){
                    sendFileMessageVH.FileType.setText(DataManager.DOCXFILE);
                }
                if(Type.equals(DataManager.XLS)){
                    sendFileMessageVH.FileType.setText(DataManager.XLSFILE);
                }
                if(Type.equals(DataManager.XLSX)){
                    sendFileMessageVH.FileType.setText(DataManager.XLSXFILE);
                }

                var calender = Calendar.getInstance(Locale.ENGLISH);
                calender.setTimeInMillis(list.get(position).getTimestamp());
                var time = DateFormat.format(DataManager.TimeFormat, calender).toString();
                sendFileMessageVH.Time.setText(time);

                sendFileMessageVH.Download.setOnClickListener(view -> {
                    OnDocumentDownload.OnDownload(list.get(position).getMessage(), list.get(position).getFileName(), list.get(position).getFileExtension());
                });
            }
        }

        OnDate.Date(list.get(position).getTimestamp());

        holder.itemView.setOnLongClickListener(view -> {
            OnClick.Click(list.get(position).getSenderUID(), list.get(position).getReceiverUID(), list.get(position).getDocumentKey(), list, position);
            return true;
        });
    }

    @Override
    public int getItemCount() {
        if (list == null) {
            return 0;
        }
        return list.size();
    }

    @Override
    public int getItemViewType(int position) {
        var model = list.get(position);
        if(model.getType().equals( DataManager.Text)){
            if (FirebaseAuth.getInstance().getCurrentUser().getUid().equals(model.getReceiverUID())) {
                return TextMessageSend;
            }else {
                return TextMessageReceive;
            }
        }

        if(model.getFileExtension().equals(DataManager.JPG)){
            if (FirebaseAuth.getInstance().getCurrentUser().getUid().equals(model.getReceiverUID())) {
                return ImageMessageSend;
            }else {
                return ImageMessageReceive;
            }
        }

        if(model.getFileExtension().equals(DataManager.PDF) || model.getFileExtension().equals(DataManager.DOC) || model.getFileExtension().equals(DataManager.DOCX) || model.getFileExtension().equals(DataManager.XLS) || model.getFileExtension().equals(DataManager.XLSX)){
            if(FirebaseAuth.getInstance().getCurrentUser().getUid().equals(model.getReceiverUID())){
                return DocumentMessageSend;
            }else {
                return DocumentMessageReceive;
            }
        }
        return 486;

    }

    public interface OnDate{
        void Date(long Timestamp);
    }
    public void OnDateLisiner(OnDate OnDate){
        this.OnDate = OnDate;
    }

    public interface OnImage{
        void Image(String ImageUri, String FileName, long Timestamp);
    }
    public void OnImageClick(OnImage OnImage){
        this.OnImage = OnImage;
    }


    //todo document download
    public interface OnDocumentDownload {
        void OnDownload(String DocumentDownloadUri, String FileName, String Extension);
    }
    public void OnDocumentDownloadState(OnDocumentDownload OnDocumentDownload){
        this.OnDocumentDownload = OnDocumentDownload;
    }



    public interface OnClick{
        void Click(String SenderUID, String ReceiverUID, long DocumentKey, List<MessageModel> list, int position);
    }
    public void OnRemoveState(OnClick OnClick){
        this.OnClick = OnClick;
    }
}
