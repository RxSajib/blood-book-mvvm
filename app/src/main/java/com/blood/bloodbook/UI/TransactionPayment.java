package com.blood.bloodbook.UI;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.blood.bloodbook.R;
import com.blood.bloodbook.databinding.TransactionpaymentBinding;

public class TransactionPayment extends AppCompatActivity {

    private TransactionpaymentBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.transactionpayment);


        InitView();
    }

    private void InitView(){
        binding.Toolbar.BackButton.setOnClickListener(view -> {
            finish();
            Animatoo.animateSlideRight(TransactionPayment.this);
        });
        binding.Toolbar.Title.setText(getResources().getString(R.string.TransactionMoney));
    }

    @Override
    public void onBackPressed() {
        finish();
        Animatoo.animateSlideRight(TransactionPayment.this);
    }
}