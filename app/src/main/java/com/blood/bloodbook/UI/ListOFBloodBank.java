package com.blood.bloodbook.UI;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;
import android.view.View;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.blood.bloodbook.Application;
import com.blood.bloodbook.DI.DaggerWeenaComponent;
import com.blood.bloodbook.Data.DataManager;
import com.blood.bloodbook.HospitalModel;
import com.blood.bloodbook.Network.ViewModel.ViewModel;
import com.blood.bloodbook.R;
import com.blood.bloodbook.UI.Adapter.BloodStorageAdapter;
import com.blood.bloodbook.databinding.ListofbloodbankBinding;

import javax.inject.Inject;

public class ListOFBloodBank extends AppCompatActivity {

    private ListofbloodbankBinding binding;
    private HospitalModel hospitalModel;
    private ViewModel viewModel;
    @Inject BloodStorageAdapter bloodStorageAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.listofbloodbank);
        hospitalModel = (HospitalModel) getIntent().getSerializableExtra(DataManager.Data);
        viewModel = new ViewModelProvider(this).get(ViewModel.class);

        var component = Application.weenaComponent;
        component.InjectListOFBloodBank(this);

        InitView();
        GetData();
    }


    private void GetData(){
        binding.RecyclerView.setHasFixedSize(true);
        binding.RecyclerView.setAdapter(bloodStorageAdapter);

        viewModel.GetBloodStore(hospitalModel.getUID(), hospitalModel.getHospitalName()).observe(this, bloodBankModels -> {
            bloodStorageAdapter.setList(bloodBankModels);
            bloodStorageAdapter.notifyDataSetChanged();

            if(bloodBankModels == null){
                binding.Message.setVisibility(View.VISIBLE);
                binding.Icon.setVisibility(View.VISIBLE);
            }else {
                binding.Message.setVisibility(View.GONE);
                binding.Icon.setVisibility(View.GONE);
            }
        });
    }
    private void InitView(){
        binding.Toolbar.BackButton.setOnClickListener(view -> {
            finish();
            Animatoo.animateSlideRight(ListOFBloodBank.this);
        });

        binding.Toolbar.Title.setText(getResources().getString(R.string.BloodList));
    }

    @Override
    public void onBackPressed() {
        finish();
        Animatoo.animateSlideRight(ListOFBloodBank.this);
    }
}