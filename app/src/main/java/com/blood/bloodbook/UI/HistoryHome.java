package com.blood.bloodbook.UI;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentPagerAdapter;

import android.os.Bundle;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.blood.bloodbook.R;
import com.blood.bloodbook.UI.Adapter.HistoryViewPagerAdapter;
import com.blood.bloodbook.UI.HistoryFragment.AllHistory;
import com.blood.bloodbook.UI.HistoryFragment.MyHistory;
import com.blood.bloodbook.databinding.HistoryhomeBinding;

public class HistoryHome extends AppCompatActivity {

    private HistoryhomeBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.historyhome);


        SetFragment();
        BackButton();
    }

    private void BackButton(){
        binding.Toolbar.Title.setText(getResources().getString(R.string.History));
        binding.Toolbar.BackButton.setOnClickListener(view -> {
            finish();
            Animatoo.animateSlideRight(HistoryHome.this);
        });
    }

    private void SetFragment(){
        binding.TabLayout.setupWithViewPager(binding.ViewPager);
        var fadapter = new HistoryViewPagerAdapter(getSupportFragmentManager(), FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        fadapter.AddFragement(new AllHistory(), getResources().getString(R.string.AllHistory));
        fadapter.AddFragement(new MyHistory(), getResources().getString(R.string.MyHistory));
        binding.ViewPager.setAdapter(fadapter);
    }


    @Override
    public void onBackPressed() {
        finish();
        Animatoo.animateSlideRight(HistoryHome.this);
    }
}