package com.blood.bloodbook.UI;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import com.blood.bloodbook.Data.DataManager;
import com.blood.bloodbook.R;
import com.blood.bloodbook.databinding.AccountrestrictionBinding;

public class AccountRestriction extends AppCompatActivity {

    private AccountrestrictionBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.accountrestriction);


        InitView();
    }

    private void InitView() {
        binding.CallWhatsApp.setOnClickListener(view -> {
            try {
                var uri = Uri.parse("smsto:" + DataManager.ContactWhatsaAppNumber);
                var i = new Intent(Intent.ACTION_SENDTO, uri);
                i.setPackage("com.whatsapp");
                startActivity(Intent.createChooser(i, ""));
            } catch (Exception e) {
                e.printStackTrace();
            }

        });
    }
}