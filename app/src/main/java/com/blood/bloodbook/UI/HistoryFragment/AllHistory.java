package com.blood.bloodbook.UI.HistoryFragment;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.blood.bloodbook.Application;
import com.blood.bloodbook.Network.ViewModel.ViewModel;
import com.blood.bloodbook.R;
import com.blood.bloodbook.UI.Adapter.HistoryAdapter;
import com.blood.bloodbook.Utils.Toast;
import com.blood.bloodbook.databinding.AllhistoryBinding;

import javax.inject.Inject;

public class AllHistory extends Fragment {

    private AllhistoryBinding binding;
    private ViewModel viewModel;
    @Inject
    HistoryAdapter historyAdapter;
    private int Limit = 25;
    private static final String TAG = "AllHistory";

    public AllHistory() {
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.allhistory, container, false);
        viewModel = new ViewModelProvider(getActivity()).get(ViewModel.class);
        var component = Application.weenaComponent;
        component.InjectAllHistory(this);

        binding.SwipeRefreshLayout.setOnRefreshListener(() -> {
            GetData(Limit);
        });
        InitView();

        binding.RecyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if(!recyclerView.canScrollVertically(1)){
                    Limit = Limit+25;
                    GetData(Limit);
                }
            }
        });
        GetData(Limit);

        return binding.getRoot();
    }

    private void InitView(){
        binding.RecyclerView.setHasFixedSize(true);
        binding.RecyclerView.setAdapter(historyAdapter);
    }
    private void GetData(int Limit){
        viewModel.GetAllHistory(Limit).observe(getActivity(), allHistoryModels ->  {
            historyAdapter.setList(allHistoryModels);
            historyAdapter.notifyDataSetChanged();
            binding.SwipeRefreshLayout.setRefreshing(false);
            if(allHistoryModels != null){
                binding.Icon.setVisibility(View.GONE);
                binding.Message.setVisibility(View.GONE);
            }else {
                binding.Icon.setVisibility(View.VISIBLE);
                binding.Message.setVisibility(View.VISIBLE);
            }
        });
    }
}