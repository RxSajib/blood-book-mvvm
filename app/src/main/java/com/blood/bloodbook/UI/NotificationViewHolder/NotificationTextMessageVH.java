package com.blood.bloodbook.UI.NotificationViewHolder;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.imageview.ShapeableImageView;
import com.blood.bloodbook.R;

public class NotificationTextMessageVH extends RecyclerView.ViewHolder {

    public ShapeableImageView ProfileImage;
    public TextView SenderName, Message, TimeDate;

    public NotificationTextMessageVH(@NonNull View itemView) {
        super(itemView);

        ProfileImage = itemView.findViewById(R.id.ProfileImage);
        SenderName = itemView.findViewById(R.id.NameOfSenderName);
        Message = itemView.findViewById(R.id.MessageText);
        TimeDate = itemView.findViewById(R.id.TimeDate);
    }
}
