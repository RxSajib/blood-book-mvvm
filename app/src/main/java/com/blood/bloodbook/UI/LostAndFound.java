package com.blood.bloodbook.UI;

import androidx.activity.result.ActivityResultLauncher;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SimpleItemAnimator;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.blood.bloodbook.Application;
import com.google.firebase.auth.FirebaseAuth;
import com.blood.bloodbook.DI.DaggerWeenaComponent;
import com.blood.bloodbook.Network.ViewModel.LocationViewModel;
import com.blood.bloodbook.Network.ViewModel.ViewModel;
import com.blood.bloodbook.R;
import com.blood.bloodbook.UI.Adapter.DistrictNameDataAdapter;
import com.blood.bloodbook.UI.Adapter.FilterLostAndFoundTypeAdapter;
import com.blood.bloodbook.UI.Adapter.LocationDataAdapter;
import com.blood.bloodbook.UI.Adapter.LostAndFoundAdapter;
import com.blood.bloodbook.UI.Adapter.ProvinceNameDataAdapter;
import com.blood.bloodbook.Utils.FilterOptionDialog;
import com.blood.bloodbook.Utils.HandleActivity;
import com.blood.bloodbook.Utils.Permission;
import com.blood.bloodbook.Utils.Toast;
import com.blood.bloodbook.Widget.ProgressDialog;
import com.blood.bloodbook.databinding.FilterdialogBinding;
import com.blood.bloodbook.databinding.LocationdialogBinding;
import com.blood.bloodbook.databinding.LostandfoundBinding;

import java.util.List;

import javax.inject.Inject;

public class LostAndFound extends AppCompatActivity {

    private LostandfoundBinding binding;

    @Inject LostAndFoundAdapter andFoundAdapter;
    private ViewModel viewModel;

    @Inject FilterOptionDialog filterOptionDialog;
    private LocationViewModel locationViewModel;

    @Inject ProvinceNameDataAdapter provinceNameDataAdapter;
    @Inject LocationDataAdapter locationDataAdapter;

    @Inject DistrictNameDataAdapter districtNameDataAdapter;
    @Inject FilterLostAndFoundTypeAdapter filterLostAndFoundTypeAdapter;
    @Inject ProgressDialog progressDialog;
    private ActivityResultLauncher<Intent> launcher;
    private FirebaseAuth Mauth;
    private int Limit = 25;
    private static final String TAG = "LostAndFound";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.lostandfound);
        viewModel = new ViewModelProvider(this).get(ViewModel.class);
        var component = Application.weenaComponent;
        component.InjectLostAndFound(this);
        locationViewModel = new ViewModelProvider(this).get(LocationViewModel.class);
        Mauth = FirebaseAuth.getInstance();

        binding.RecyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if(!recyclerView.canScrollVertically(1)){
                    Limit = Limit+25;
                    GetData(Limit);

                }
            }
        });
        InitView();
        GetData(Limit);
        GetCountryName();
        GetProvincesName();
        GetDistrictName();
        GetLostAndFoundType();
        RemoveItem();
    }

    private void RemoveItem(){
        andFoundAdapter.OnLongClickState(lostAndFoundModel -> {
            if(lostAndFoundModel.getUID().equals(Mauth.getCurrentUser().getUid())){
                var dialog = new android.app.AlertDialog.Builder(LostAndFound.this);
                dialog.setTitle(getResources().getString(R.string.Delete));
                dialog.setMessage(getResources().getString(R.string.DoYouWantToRemove));
                dialog.setPositiveButton(getResources().getString(R.string.Delete), (dialogInterface, i) -> {
                    dialogInterface.dismiss();
                    progressDialog.ProgressDialog(LostAndFound.this);
                    viewModel.RemoveLostAndFound(lostAndFoundModel.getDocumentKey()).observe(this, aBoolean -> {
                        if(aBoolean){
                            progressDialog.CancelProgressDialog();
                        }else {
                            progressDialog.CancelProgressDialog();
                        }
                    });
                });
                dialog.setNegativeButton(getResources().getString(R.string.No), (dialogInterface, i) -> {
                    dialogInterface.dismiss();
                });

                var d = dialog.create();
                d.show();
            }else {
                Toast.Message(LostAndFound.this, getResources().getString(R.string.Not_your_data));
            }

        });
    }
    private void InitView(){

        binding.AddBtn.setOnClickListener(view -> {
            HandleActivity.GotoAddLostAndFound(LostAndFound.this);
            Animatoo.animateSlideLeft(LostAndFound.this);
        });

        binding.RecyclerView.setHasFixedSize(true);
        binding.RecyclerView.setAdapter(andFoundAdapter);
        RecyclerView.ItemAnimator animator = binding.RecyclerView.getItemAnimator();
        if (animator instanceof SimpleItemAnimator) {
            ((SimpleItemAnimator) animator).setSupportsChangeAnimations(false);
        }

        binding.SearchInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                var searchdata = editable.toString().trim();
                if(searchdata.isEmpty()){
                    Limit = 25;
                    GetData(Limit);
                }
            }
        });

        binding.Toolbar.BackButton.setOnClickListener(view -> {
            finish();
            Animatoo.animateSlideRight(LostAndFound.this);
        });
        binding.Toolbar.Title.setText(getResources().getString(R.string.LostAndFound));

        andFoundAdapter.OnCallLisiner(organizationModel -> {
            if (Permission.PermissionCall(LostAndFound.this, 150)) {
                var intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse("tel:" + organizationModel.getPhoneNumber()));
                startActivity(intent);
            }
        });

        binding.SearchInput.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                SearchingLostAndFound();
                return true;
            }
            return false;
        });

        filterOptionDialog.OnclickLisiner(Item -> {
            if (Item == 0) {
                Limit = 25;
                GetData(Limit);
            }
            if (Item == 1) {
                var alertdialog = new AlertDialog.Builder(LostAndFound.this);
                FilterdialogBinding v = DataBindingUtil.inflate(getLayoutInflater(), R.layout.filterdialog, null, false);
                alertdialog.setView(v.getRoot());

                var dialog = alertdialog.create();
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();

                DialogView(dialog, v);
                v.ClearBtn.setOnClickListener(view -> {
                    dialog.dismiss();
                });
            }
        });

        binding.SearchInput.setOnTouchListener((view, motionEvent) -> {
            final int DRAWABLE_LEFT = 0;
            final int DRAWABLE_TOP = 1;
            final int DRAWABLE_RIGHT = 2;
            final int DRAWABLE_BOTTOM = 3;

            if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                if (motionEvent.getRawX() >= (binding.SearchInput.getRight() - binding.SearchInput.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                    filterOptionDialog.show(getSupportFragmentManager(), "filter");
                    return true;
                }
            }
            return false;
        });
    }

    private void SearchingLostAndFound(){
        var SearchInputData = binding.SearchInput.getText().toString().trim();
        if(SearchInputData.isEmpty()){
            Toast.Message(getApplicationContext(), "Empty");
        }else {
            viewModel.SearchLostAndFound(SearchInputData).observe(this, lostAndFoundModels -> {
                andFoundAdapter.setList(lostAndFoundModels);
                andFoundAdapter.notifyDataSetChanged();

                if(lostAndFoundModels != null){
                    binding.Icon.setVisibility(View.GONE);
                    binding.Message.setVisibility(View.GONE);
                }else {
                    binding.Icon.setVisibility(View.VISIBLE);
                    binding.Message.setVisibility(View.VISIBLE);
                }
            });
        }
    }

    private void DialogView(AlertDialog dialog, FilterdialogBinding v) {

        v.CountryInput.setOnClickListener(view -> {
            locationViewModel.DeleteAllCountryNameData();
            var mydialog = new android.app.AlertDialog.Builder(LostAndFound.this);
            LocationdialogBinding locationitemBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.locationdialog, null, false);
            mydialog.setView(locationitemBinding.getRoot());

            var alertdialog = mydialog.create();
            alertdialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            locationitemBinding.LocationRecyclerView.setHasFixedSize(true);
            locationitemBinding.LocationRecyclerView.setAdapter(locationDataAdapter);

            alertdialog.show();

            viewModel.GetCountryName().observe(LostAndFound.this, countryNameModels -> {
                locationViewModel.InsertCounyryNameData(countryNameModels);
                GetCountryName();

            });
            locationitemBinding.SearchInput.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    var CountryName = editable.toString();
                    if (CountryName.isEmpty()) {
                        GetCountryName();
                    } else {
                        GetCountryNameSearch(CountryName);
                    }
                }
            });

            locationDataAdapter.OnCountryLisiner(CountryName -> {
                v.Country.setText(CountryName);
                v.District.setText(null);
                v.Province.setText(null);
                alertdialog.dismiss();
            });
        });

        v.SelectProvince.setOnClickListener(view -> {
            var CountryName = v.Country.getText().toString().trim();
            if (CountryName == "") {
                Toast.Message(LostAndFound.this, getResources().getString(R.string.CountryEmpty));
            } else {
                locationViewModel.DeleteAllProvinceNameData();
                var provincedialog = new android.app.AlertDialog.Builder(LostAndFound.this);
                LocationdialogBinding locationitemBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.locationdialog, null, false);
                provincedialog.setView(locationitemBinding.getRoot());

                var alertdialog = provincedialog.create();
                locationitemBinding.LocationRecyclerView.setHasFixedSize(true);
                locationitemBinding.LocationRecyclerView.setAdapter(provinceNameDataAdapter);
                alertdialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                alertdialog.show();

                viewModel.GetProvinceName(CountryName).observe(LostAndFound.this, provinceNameModels -> {
                    locationViewModel.InsertProvincesNameData(provinceNameModels);
                });

                provinceNameDataAdapter.OnClickLisiner(ProvinceName -> {
                    alertdialog.dismiss();
                    v.Province.setText(ProvinceName);
                    v.District.setText(null);
                });

                locationitemBinding.SearchInput.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        var Data = editable.toString();
                        if (Data.isEmpty()) {
                            GetProvincesName();
                            Toast.Message(getApplicationContext(), "No data");
                        } else {
                            SearchProvinceName(Data);
                        }
                    }
                });
            }
        });

        v.DistrictInput.setOnClickListener(view -> {
            var CountryName = v.Country.getText().toString().trim();
            var ProvincesName = v.Province.getText().toString().trim();

            if (CountryName == "") {
                Toast.Message(LostAndFound.this, getResources().getString(R.string.CountryEmpty));
            } else if (ProvincesName == "") {
                Toast.Message(LostAndFound.this, getResources().getString(R.string.ProvinceEmpty));
            } else {
                locationViewModel.DeleteDistrct();
                var districtdialog = new android.app.AlertDialog.Builder(LostAndFound.this);
                LocationdialogBinding locationitemBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.locationdialog, null, false);
                districtdialog.setView(locationitemBinding.getRoot());

                var alertdialog = districtdialog.create();
                locationitemBinding.LocationRecyclerView.setHasFixedSize(true);
                locationitemBinding.LocationRecyclerView.setAdapter(districtNameDataAdapter);
                alertdialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                alertdialog.show();

                viewModel.GetDistrictName(CountryName, ProvincesName).observe(LostAndFound.this, districtNameModelList -> {
                    locationViewModel.InsertDistrict(districtNameModelList);
                });

                districtNameDataAdapter.OnClickLisiner(DictrictName -> {
                    alertdialog.dismiss();
                    v.District.setText(DictrictName);
                });

                locationitemBinding.SearchInput.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        var Data = editable.toString();
                        if (Data.isEmpty()) {
                            GetDistrictName();
                        } else {
                            SearchDistrictName(Data);
                        }
                    }
                });
            }
        });

        v.SelectOrgTypeInput.setOnClickListener(view -> {
            var districtdialog = new android.app.AlertDialog.Builder(LostAndFound.this);
            LocationdialogBinding locationitemBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.locationdialog, null, false);
            districtdialog.setView(locationitemBinding.getRoot());

            var alertdialog = districtdialog.create();
            locationitemBinding.LocationRecyclerView.setHasFixedSize(true);
            locationitemBinding.LocationRecyclerView.setAdapter(filterLostAndFoundTypeAdapter);
            alertdialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            alertdialog.show();

            viewModel.GetLostAndFoundType().observe(this, lostAndFoundTypeModels -> {
                locationViewModel.InsertLostAndFound(lostAndFoundTypeModels);
            });

            filterLostAndFoundTypeAdapter.OnClickState(CategoryName -> {
                v.OrgType.setText(CategoryName);
                alertdialog.dismiss();
            });

            locationitemBinding.SearchInput.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    var s = editable.toString();
                    if (s.isEmpty()) {
                        GetLostAndFoundType();
                    } else {
                        SearchLostAndFoundType(s);
                    }
                }
            });
        });


        v.ApplyBtn.setOnClickListener(view -> {
            var Country = v.Country.getText().toString().trim();
            var Province = v.Province.getText().toString().trim();
            var District = v.District.getText().toString().trim();
            var OrgType = v.OrgType.getText().toString().trim();

            if (Country == "") {
                Toast.Message(getApplicationContext(), getResources().getString(R.string.CountryEmpty));
            } else if (Province == "") {
                Toast.Message(getApplicationContext(), getResources().getString(R.string.ProvinceEmpty));
            } else if (District == "") {
                Toast.Message(getApplicationContext(), getResources().getString(R.string.DistrictEmpty));
            } else if (OrgType == "") {
                Toast.Message(getApplicationContext(), getResources().getString(R.string.OrganizationTypeEmpty));
            } else {
                FilterLostAndFound(Country, Province, District, OrgType);
                dialog.dismiss();
            }
        });
    }


    @Override
    public void onBackPressed() {
        finish();
        Animatoo.animateSlideRight(LostAndFound.this);
    }


    private void GetData(int Limit){
        viewModel.GetLostAndFound(Limit).observe(this, lostAndFoundModels -> {
            andFoundAdapter.setList(lostAndFoundModels);
            andFoundAdapter.notifyDataSetChanged();
            if(lostAndFoundModels != null){
                binding.Icon.setVisibility(View.GONE);
                binding.Message.setVisibility(View.GONE);

            }else {
                binding.Icon.setVisibility(View.VISIBLE);
                binding.Message.setVisibility(View.VISIBLE);
            }
        });
    }


    private void FilterLostAndFound(String country, String province, String district, String orgType) {
        viewModel.FilterLostAndFound(country, province, district, orgType).observe(this, lostAndFoundModels -> {
            andFoundAdapter.setList(lostAndFoundModels);
            andFoundAdapter.notifyDataSetChanged();

            if(lostAndFoundModels != null){
                binding.Icon.setVisibility(View.GONE);
                binding.Message.setVisibility(View.GONE);
            }else {
                binding.Icon.setVisibility(View.VISIBLE);
                binding.Message.setVisibility(View.VISIBLE);
            }
        });
    }

    private void GetCountryNameSearch(String CountryName) {
        locationViewModel.SearchByCountryName(CountryName).observe(LostAndFound.this, countryNameModels -> {
            locationDataAdapter.setList(countryNameModels);
            locationDataAdapter.notifyDataSetChanged();
        });
    }
    private void GetCountryName() {
        locationViewModel.GetCountryName().observe(LostAndFound.this, countryNameModels -> {
                locationDataAdapter.setList(countryNameModels);
                locationDataAdapter.notifyDataSetChanged();


        });
    }

    private void GetProvincesName() {
        locationViewModel.GetProvincesNameData().observe(LostAndFound.this, provinceNameModelList -> {
            provinceNameDataAdapter.setList(provinceNameModelList);
            provinceNameDataAdapter.notifyDataSetChanged();
        });
    }

    private void SearchProvinceName(String Name) {
        locationViewModel.SearchProvincesName(Name).observe(LostAndFound.this, provinceNameModelList -> {
            provinceNameDataAdapter.setList(provinceNameModelList);
            provinceNameDataAdapter.notifyDataSetChanged();
        });
    }

    private void GetDistrictName() {
        locationViewModel.GetDistrictName().observe(LostAndFound.this, districtNameModelList -> {
            districtNameDataAdapter.setList(districtNameModelList);
            districtNameDataAdapter.notifyDataSetChanged();
        });
    }

    private void SearchDistrictName(String Name) {
        locationViewModel.SearchDistrictName(Name).observe(LostAndFound.this, districtNameModelList -> {
            districtNameDataAdapter.setList(districtNameModelList);
            districtNameDataAdapter.notifyDataSetChanged();
        });
    }


    private void GetLostAndFoundType(){
        locationViewModel.GetLostAndFoundType().observe(this, list -> {
            filterLostAndFoundTypeAdapter.setList(list);
            filterLostAndFoundTypeAdapter.notifyDataSetChanged();
        });
    }
    private void SearchLostAndFoundType(String NameOFLostAndFound){
        locationViewModel.SearchLostAndFound(NameOFLostAndFound);
    }

}