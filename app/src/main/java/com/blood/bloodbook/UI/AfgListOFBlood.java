package com.blood.bloodbook.UI;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import android.app.AlertDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.blood.bloodbook.Application;
import com.blood.bloodbook.DI.DaggerWeenaComponent;
import com.blood.bloodbook.Data.DataManager;
import com.blood.bloodbook.Network.ViewModel.LocationViewModel;
import com.blood.bloodbook.R;

import com.blood.bloodbook.UI.Adapter.DistrictNameDataAdapter;
import com.blood.bloodbook.UI.Adapter.DonorUserAdapter;
import com.blood.bloodbook.Utils.HandleActivity;
import com.blood.bloodbook.Widget.BloodFilterDialog;
import com.blood.bloodbook.databinding.AgflistofbloodBinding;
import com.blood.bloodbook.databinding.LocationdialogBinding;

import javax.inject.Inject;

public class AfgListOFBlood extends AppCompatActivity {

    private AgflistofbloodBinding binding;
    private com.blood.bloodbook.ViewModel.ViewModel viewModel;
    private com.blood.bloodbook.Network.ViewModel.ViewModel viewModeldistrict;

    @Inject
    DonorUserAdapter adapter;
    @Inject
    DistrictNameDataAdapter districtNameDataAdapter;
    private LocationViewModel locationViewModel;
    private String BloodGroup, Province;
    @Inject
    BloodFilterDialog bloodFilterDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.agflistofblood);
        viewModel = new ViewModelProvider(this).get(com.blood.bloodbook.ViewModel.ViewModel.class);
        viewModeldistrict = new ViewModelProvider(this).get(com.blood.bloodbook.Network.ViewModel.ViewModel.class);

        BloodGroup = getIntent().getStringExtra(DataManager.BloodGroup);
        Province = getIntent().getStringExtra(DataManager.Province);
        locationViewModel = new ViewModelProvider(this).get(LocationViewModel.class);

        var component = Application.weenaComponent;
        component.InjectAfgListOfBlood(this);
        InitView();
        SearchBlood();
        GetDistrict();
        GetDistrictName();
        BloodFilterDialogClick();
    }

    private void BloodFilterDialogClick(){
        bloodFilterDialog.OnclickLisiner(Item -> {
            if(Item == 0){
                SearchBlood();
                binding.District.setText("");
            } if(Item == 1){
                OpenFilterDialog();
            }
        });
    }

    private void OpenFilterDialog(){

        locationViewModel.DeleteDistrct();
        var dialog = new AlertDialog.Builder(AfgListOFBlood.this);
        LocationdialogBinding locationitemBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.locationdialog, null, false);
        dialog.setView(locationitemBinding.getRoot());

        var alertdialog = dialog.create();
        locationitemBinding.LocationRecyclerView.setHasFixedSize(true);
        locationitemBinding.LocationRecyclerView.setAdapter(districtNameDataAdapter);
        alertdialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertdialog.show();


        viewModeldistrict.GetDistrictName("Afghanistan", Province).observe(this, districtNameModelList -> {
            if(districtNameModelList != null){
                locationViewModel.InsertDistrict(districtNameModelList);
            }
        });

        districtNameDataAdapter.OnClickLisiner(DictrictName -> {
            alertdialog.dismiss();
            binding.District.setText(DictrictName);
            GetDistrictFilter(DictrictName);
        });

        locationitemBinding.SearchInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                var Data = editable.toString();
                if(Data.isEmpty()){
                    GetDistrictName();
                }else {
                    SearchDustrictName(Data);
                }
            }
        });
    }

    private void GetDistrictFilter(String District){
        viewModel.SearchBloodGroup(BloodGroup, "Afghanistan", Province, District, 100).observe(this, donorModels -> {
            adapter.setList(donorModels);
            adapter.notifyDataSetChanged();
            if (donorModels != null) {
                binding.Icon.setVisibility(View.GONE);
                binding.Message.setVisibility(View.GONE);
            } else {
                binding.Icon.setVisibility(View.VISIBLE);
                binding.Message.setVisibility(View.VISIBLE);
            }
        });
    }

    private void GetDistrict(){
        binding.SearchBox.setOnClickListener(view -> {

            bloodFilterDialog.show(getSupportFragmentManager(), "show");
        });
    }

    private void SearchBlood() {
        viewModel.SearchProvinceBlood(BloodGroup, "Afghanistan", Province).observe(this, bloodRequestModels -> {
            adapter.setList(bloodRequestModels);
            adapter.notifyDataSetChanged();
            if (bloodRequestModels != null) {
                binding.Icon.setVisibility(View.GONE);
                binding.Message.setVisibility(View.GONE);
            } else {
                binding.Icon.setVisibility(View.VISIBLE);
                binding.Message.setVisibility(View.VISIBLE);
            }
        });

        adapter.OnClickLisiner(model -> {
            HandleActivity.GotoBloodResult(AfgListOFBlood.this, model);
            Animatoo.animateSlideLeft(AfgListOFBlood.this);
        });
    }

    private void InitView() {
        binding.Toolbar.Title.setText(BloodGroup + " Results");
        binding.RecyclerView.setHasFixedSize(true);
        binding.RecyclerView.setAdapter(adapter);
        binding.Toolbar.BackButton.setOnClickListener(view -> {
            finish();
            Animatoo.animateSlideRight(AfgListOFBlood.this);
        });
    }


    @Override
    public void onBackPressed() {
        finish();
        Animatoo.animateSlideRight(AfgListOFBlood.this);
    }


    private void GetDistrictName(){
        locationViewModel.GetDistrictName().observe(this, districtNameModelList -> {
            districtNameDataAdapter.setList(districtNameModelList);
            districtNameDataAdapter.notifyDataSetChanged();
        });
    }
    private void SearchDustrictName(String Name){
        locationViewModel.SearchDistrictName(Name).observe(this, districtNameModelList -> {
            districtNameDataAdapter.setList(districtNameModelList);
            districtNameDataAdapter.notifyDataSetChanged();
        });
    }
}