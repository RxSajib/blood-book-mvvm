package com.blood.bloodbook.UI.Adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.blood.bloodbook.Data.CountryNameModel;
import com.blood.bloodbook.UI.ViewHolder.LocationViewHolder;
import com.blood.bloodbook.databinding.LocationitemBinding;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import lombok.Getter;
import lombok.Setter;

@Singleton
public class LocationDataAdapter extends RecyclerView.Adapter<LocationViewHolder> {

    @Inject
    public LocationDataAdapter(){

    }

    @Setter @Getter
    private List<CountryNameModel> list;
    private OnClick OnClick;

    @NonNull
    @Override
    public LocationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        var l = LayoutInflater.from(parent.getContext());
        var v = LocationitemBinding.inflate(l, parent, false);
        return new LocationViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull LocationViewHolder holder, int position) {
        holder.binding.LocationName.setText(list.get(position).getCountryName());

        holder.itemView.setOnClickListener(view -> {
            OnClick.Click(list.get(position).getCountryName());
        });
    }

    @Override
    public int getItemCount() {
        if(list == null) {
            return 0;
        }return list.size();
    }

    public interface OnClick{
        void Click(String CountryName);
    }
    public void OnCountryLisiner(OnClick OnClick){
        this.OnClick = OnClick;
    }
}
