package com.blood.bloodbook.UI;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.documentfile.provider.DocumentFile;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.app.DownloadManager;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.view.View;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.blood.bloodbook.Application;
import com.blood.bloodbook.DI.DaggerWeenaComponent;
import com.blood.bloodbook.Data.Data;
import com.blood.bloodbook.Data.NotifactionResponse;
import com.blood.bloodbook.MessageModel;
import com.squareup.picasso.Picasso;
import com.blood.bloodbook.Data.DataManager;
import com.blood.bloodbook.Network.ViewModel.ViewModel;
import com.blood.bloodbook.R;
import com.blood.bloodbook.UI.Adapter.MessageAdapter;
import com.blood.bloodbook.Utils.Permission;
import com.blood.bloodbook.Utils.Toast;
import com.blood.bloodbook.Widget.ChatOptionDialog;
import com.blood.bloodbook.Widget.ProgressDialog;
import com.blood.bloodbook.databinding.ChatBinding;
import com.blood.bloodbook.databinding.ImagefullscreendialogBinding;

import java.io.File;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import dev.shreyaspatil.MaterialDialog.BottomSheetMaterialDialog;
import dev.shreyaspatil.MaterialDialog.interfaces.DialogInterface;

public class Chat extends AppCompatActivity {

    private ChatBinding binding;
    private boolean EnableSendButton = false;
    private String SenderUID;
    private String GivenName;
    private String SureName;
    private ViewModel viewModel;
    @Inject
    MessageAdapter messageAdapter;
    @Inject
    ChatOptionDialog chatOptionDialog;
    private ActivityResultLauncher<Intent> launchercamera;
    private ActivityResultLauncher<Intent> launcherimage;
    private ActivityResultLauncher<Intent> launcherattachement;
    private Uri cam_uri;
    @Inject
    ProgressDialog progressDialog;
    private static final DecimalFormat df = new DecimalFormat("0.00");
    private int datasize = 0;
    private boolean IslastPosition = false;
    private int MessageLimit = 20;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.chat);
        viewModel = new ViewModelProvider(this).get(ViewModel.class);
        SenderUID = getIntent().getStringExtra(DataManager.SenderUID);
        GivenName = getIntent().getStringExtra(DataManager.GivenName);
        SureName = getIntent().getStringExtra(DataManager.Surname);

        var component = Application.weenaComponent;
        component.InjectChat(this);
        InitView();
        binding.SwipeRefreshLayout.setRefreshing(true);

        binding.SwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                MessageLimit = MessageLimit + 20;
                GetMessage(MessageLimit);
            }
        });

        GetMessage(MessageLimit);

        GetCameraImage();
        GetGalleryImage();
        GetAttachment();
        CheckLastPosition();
        messageAdapter.OnDateLisiner(Timestamp -> {
            binding.Date.setVisibility(View.VISIBLE);
            var calender = Calendar.getInstance(Locale.ENGLISH);
            calender.setTimeInMillis(Timestamp);
            var time = DateFormat.format(DataManager.DateFormat, calender).toString();
            binding.Date.setText(time);
        });


        GetProfileInfo();
        RemoveMessage();
        DownloadFile();
    }

    private void DownloadFile(){
        messageAdapter.OnDocumentDownloadState(new MessageAdapter.OnDocumentDownload() {
            @Override
            public void OnDownload(String DocumentDownloadUri, String FileName, String FileExtension) {

                Toast.Message(Chat.this, "Downloading ...");
                var downloadmanager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
                var uri = Uri.parse(DocumentDownloadUri);

                var request = new DownloadManager.Request(uri);
                request.setTitle(FileName);
                request.setDescription("Downloading");
                request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS,FileName);
                downloadmanager.enqueue(request);
            }
        });
    }

    private void RemoveMessage() {
        messageAdapter.OnRemoveState(new MessageAdapter.OnClick() {
            @Override
            public void Click(String SenderUID, String ReceiverUID, long DocumentKey, List<MessageModel> list, int position) {
                progressDialog.ProgressDialog(Chat.this);
                viewModel.DeleteMessage(SenderUID, ReceiverUID, DocumentKey).observe(Chat.this, new Observer<Boolean>() {
                    @Override
                    public void onChanged(Boolean aBoolean) {
                        if (aBoolean) {
                            progressDialog.CancelProgressDialog();
                        } else {
                            progressDialog.CancelProgressDialog();
                        }
                    }
                });
            }
        });
    }

    private void GetProfileInfo() {
        viewModel.GetUserInfo(SenderUID).observe(this, profileModel -> {
            if (profileModel != null) {
                Picasso.get().load(profileModel.getProfileImage()).placeholder(R.drawable.profileplaceholder).into(binding.ImageProfile);
                binding.Name.setText(profileModel.getFirstName());
            }
        });
    }


    private void CheckLastPosition() {
        binding.RecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (!recyclerView.canScrollVertically(1)) {
                    IslastPosition = true;
                } else {
                    IslastPosition = false;
                }
            }
        });
    }

    private void InitView() {

        messageAdapter.OnImageClick((ImageUri, FileName, Timestamp) -> {
            var dialog = new AlertDialog.Builder(Chat.this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
            ImagefullscreendialogBinding v = DataBindingUtil.inflate(getLayoutInflater(), R.layout.imagefullscreendialog, null, false);
            dialog.setView(v.getRoot());
            Picasso.get().load(ImageUri).into(v.PhotoView);
            v.FileName.setText(FileName);
            var calender = Calendar.getInstance(Locale.ENGLISH);
            calender.setTimeInMillis(Timestamp);
            var time = DateFormat.format(DataManager.TimeFormat, calender).toString();
            var date = DateFormat.format(DataManager.DatePattern, calender).toString();
            v.Time.setText(date + " " + time);
            var alertdialog = dialog.create();
            alertdialog.show();
            v.BackButton.setOnClickListener(view -> {
                alertdialog.dismiss();
            });

            v.DownloadButton.setOnClickListener(view -> {
                Toast.Message(Chat.this, "Downloading ...");
                var downloadmanager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
                var uri = Uri.parse(ImageUri);

                var request = new DownloadManager.Request(uri);
                request.setTitle(FileName);
                request.setDescription("Downloading");
                request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS,FileName);
                downloadmanager.enqueue(request);
            });

        });



        var layoutmanager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        binding.RecyclerView.setHasFixedSize(true);
        binding.RecyclerView.setLayoutManager(layoutmanager);
        layoutmanager.setStackFromEnd(true);
        binding.RecyclerView.setAdapter(messageAdapter);
        binding.BackButton.setOnClickListener(view -> {
            finish();
            Animatoo.animateSlideRight(Chat.this);
        });

        binding.InputMessage.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                var Message = editable.toString();
                if (Message.isEmpty()) {
                    binding.SendIcon.setImageResource(R.drawable.ic_camera);
                    binding.AttachmentIcon.setImageResource(R.drawable.ic_attachment);
                    binding.AttachmentIcon.setVisibility(View.VISIBLE);
                    EnableSendButton = false;
                } else {
                    EnableSendButton = true;
                    binding.SendIcon.setImageResource(R.drawable.ic_send);
                    binding.AttachmentIcon.setVisibility(View.GONE);
                }
            }
        });

        binding.SendIcon.setOnClickListener(view -> {
            if (EnableSendButton) {
                var Message = binding.InputMessage.getText().toString().trim();
                binding.InputMessage.setText(null);
                viewModel.SendChatMessage(Message, DataManager.Text, SenderUID, null, null, null).observe(this, aBoolean -> {
                    SendMessageHistory(Message, DataManager.ChatTextMessage);
                    SendNotificationToUser(Message, DataManager.Text, SenderUID);
                });
            } else {
                OpenCamera();
            }
        });


        binding.AttachmentIcon.setOnClickListener(view -> {
            chatOptionDialog.show(getSupportFragmentManager(), "show");
        });

        chatOptionDialog.OnclickLisiner(Item -> {
            if (Item == 0) {
                var intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                launcherimage.launch(intent);
            }
            if (Item == 1) {
                var intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("*/*");
                launcherattachement.launch(intent);
            }
        });
    }

    private void SendNotificationToUser(String Message, String Type, String SenderUID) {
        viewModel.GetSingleUser(SenderUID).observe(this, profileModel -> {
            if (profileModel != null) {

                if (Type.equals(DataManager.Text)) {
                    var data = new Data(Message, profileModel.getFirstName(), null, "chatnotificationicon");
                    var notificationresponse = new NotifactionResponse(data, profileModel.getToken(), true, DataManager.High);
                    viewModel.SendNotification(notificationresponse).observe(this, aBoolean -> {
                    });
                }
                if (Type.equals(DataManager.Image)) {
                    var data = new Data("Send a image", profileModel.getFirstName(), Message, "chatnotificationicon");
                    var notificationresponse = new NotifactionResponse(data, profileModel.getToken(), true, DataManager.High);
                    viewModel.SendNotification(notificationresponse).observe(this, aBoolean -> {
                    });
                }
                if (Type.equals(DataManager.PDF)) {
                    var data = new Data(Message, profileModel.getFirstName(), null, "chatnotificationicon");
                    var notificationresponse = new NotifactionResponse(data, profileModel.getToken(), true, DataManager.High);
                    viewModel.SendNotification(notificationresponse).observe(this, aBoolean -> {
                    });
                }
                if (Type.equals(DataManager.DOC)) {
                    var data = new Data(Message, profileModel.getFirstName(), null, "chatnotificationicon");
                    var notificationresponse = new NotifactionResponse(data, profileModel.getToken(), true, DataManager.High);
                    viewModel.SendNotification(notificationresponse).observe(this, aBoolean -> {
                    });
                }
                if (Type.equals(DataManager.DOCX)) {
                    var data = new Data(Message, profileModel.getFirstName(), null, "chatnotificationicon");
                    var notificationresponse = new NotifactionResponse(data, profileModel.getToken(), true, DataManager.High);
                    viewModel.SendNotification(notificationresponse).observe(this, aBoolean -> {
                    });
                }
                if (Type.equals(DataManager.XLS)) {
                    var data = new Data(Message, profileModel.getFirstName(), null, "chatnotificationicon");
                    var notificationresponse = new NotifactionResponse(data, profileModel.getToken(), true, DataManager.High);
                    viewModel.SendNotification(notificationresponse).observe(this, aBoolean -> {
                    });
                }
                if (Type.equals(DataManager.XLSX)) {
                    var data = new Data(Message, profileModel.getFirstName(), null, "chatnotificationicon");
                    var notificationresponse = new NotifactionResponse(data, profileModel.getToken(), true, DataManager.High);
                    viewModel.SendNotification(notificationresponse).observe(this, aBoolean -> {
                    });
                }

            }
        });
    }


    private void GetMessage(int Limit) {
        viewModel.GetMessage(SenderUID, Limit).observe(this, messageModels -> {
            messageAdapter.setList(messageModels);
            messageAdapter.notifyDataSetChanged();

            binding.SwipeRefreshLayout.setRefreshing(false);
            if (messageModels != null) {

            } else {

            }

            if (IslastPosition) {
                binding.RecyclerView.smoothScrollToPosition(messageAdapter.getItemCount() - 1);
            }
        });
    }

    private void OpenCamera() {
        if (Permission.PermissionCamera(Chat.this, 123)) {
            var values = new ContentValues();
            values.put(MediaStore.Images.Media.TITLE, "New Picture");
            values.put(MediaStore.Images.Media.DESCRIPTION, "From Camera");
            cam_uri = getApplicationContext().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, cam_uri);
            launchercamera.launch(cameraIntent);
        }
    }

    private void GetCameraImage() {
        launchercamera = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), result -> {
            //  progressDialog.ProgressDialog(Chat.this);
            if (result.getResultCode() == RESULT_OK) {
                progressDialog.ProgressDialog(Chat.this);
                if (result.getResultCode() == RESULT_OK) {
                    var uri = cam_uri;
                    var scheme = uri.getScheme();
                    if (scheme.equals(ContentResolver.SCHEME_CONTENT)) {
                        try {
                            var fileInputStream = getApplicationContext().getContentResolver().openInputStream(uri);
                            datasize = fileInputStream.available();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        double kb = datasize / 1000;
                        double mb = kb / 1000;
                        if (mb <= 15) {
                            //todo uploading file
                            var documentFile = DocumentFile.fromSingleUri(Chat.this, uri);
                            var fileName = documentFile.getName();
                            var extension = fileName.substring(fileName.lastIndexOf("."));
                            // Toast.Message(getApplicationContext(), extension);
                            if (extension.equals(DataManager.JPG)) {
                                viewModel.ChatFileUpload(uri, extension).observe(this, new Observer<String>() {
                                    @Override
                                    public void onChanged(String s) {
                                        viewModel.SendChatMessage(s, extension, SenderUID, fileName, extension, String.valueOf(df.format(mb))).observe(Chat.this, aBoolean -> {
                                            if (aBoolean) {
                                                progressDialog.CancelProgressDialog();
                                                SendMessageHistory(s, extension);
                                                SendNotificationToUser(s, DataManager.Image, SenderUID);
                                            } else {
                                                progressDialog.CancelProgressDialog();

                                            }
                                        });

                                    }
                                });
                            } else {
                                progressDialog.CancelProgressDialog();
                                Toast.Message(Chat.this, "File not support");
                            }

                        } else {
                            progressDialog.CancelProgressDialog();
                            Toast.Message(Chat.this, "File size is bigger minimum file size of less than 15 MB");
                        }

                    } else if (scheme.equals(ContentResolver.SCHEME_FILE)) {
                        String path = uri.getPath();
                        try {
                            var f = new File(path);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        // System.out.println("File size in bytes"+f.length());
                    }
                } else {
                    progressDialog.CancelProgressDialog();
                }
            } else {

            }
        });
    }

    private void GetAttachment() {
        launcherattachement = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), result -> {
            progressDialog.ProgressDialog(Chat.this);
            if (result.getResultCode() == RESULT_OK) {
                var uri = result.getData().getData();
                var scheme = uri.getScheme();
                if (scheme.equals(ContentResolver.SCHEME_CONTENT)) {
                    try {
                        var fileInputStream = getApplicationContext().getContentResolver().openInputStream(uri);
                        datasize = fileInputStream.available();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    double kb = datasize / 1000;
                    double mb = kb / 1000;
                    if (mb <= 5) {
                        //todo uploading file
                        var documentFile = DocumentFile.fromSingleUri(Chat.this, result.getData().getData());
                        var fileName = documentFile.getName();
                        var extension = fileName.substring(fileName.lastIndexOf("."));
                        // Toast.Message(getApplicationContext(), extension);
                        if (extension.equals(DataManager.JPG) || extension.equals(DataManager.PDF) || extension.equals(DataManager.DOC) || extension.equals(DataManager.DOCX) || extension.equals(DataManager.XLS) || extension.equals(DataManager.XLSX)) {
                            viewModel.ChatFileUpload(result.getData().getData(), extension).observe(this, new Observer<String>() {
                                @Override
                                public void onChanged(String s) {
                                    viewModel.SendChatMessage(s, extension, SenderUID, fileName, extension, String.valueOf(df.format(mb))).observe(Chat.this, aBoolean -> {
                                        if (aBoolean) {
                                            progressDialog.CancelProgressDialog();
                                            SendMessageHistory(s, extension);
                                            if (extension.equals(DataManager.JPG)) {
                                                SendNotificationToUser(s, DataManager.Image, SenderUID);
                                            }
                                            if (extension.equals(DataManager.PDF)) {
                                                SendNotificationToUser(fileName, DataManager.PDF, SenderUID);
                                            }
                                            if (extension.equals(DataManager.DOC)) {
                                                SendNotificationToUser(fileName, DataManager.DOC, SenderUID);
                                            }
                                            if (extension.equals(DataManager.DOCX)) {
                                                SendNotificationToUser(fileName, DataManager.DOCX, SenderUID);
                                            }
                                            if (extension.equals(DataManager.XLS)) {
                                                SendNotificationToUser(fileName, DataManager.XLS, SenderUID);
                                            }
                                            if (extension.equals(DataManager.XLSX)) {
                                                SendNotificationToUser(fileName, DataManager.XLSX, SenderUID);
                                            }
                                        } else {
                                            progressDialog.CancelProgressDialog();

                                        }
                                    });

                                }
                            });
                        } else {
                            progressDialog.CancelProgressDialog();
                            Toast.Message(Chat.this, "File not support");
                        }

                    } else {
                        progressDialog.CancelProgressDialog();
                        Toast.Message(Chat.this, "File size is bigger minimum file size of less than 5 MB");
                    }

                } else if (scheme.equals(ContentResolver.SCHEME_FILE)) {
                    String path = uri.getPath();
                    try {
                        var f = new File(path);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else {
                progressDialog.CancelProgressDialog();
            }
        });
    }

    private void GetGalleryImage() {
        launcherimage = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), result -> {
            progressDialog.ProgressDialog(Chat.this);
            if (result.getResultCode() == RESULT_OK) {

                var uri = result.getData().getData();
                var scheme = uri.getScheme();
                if (scheme.equals(ContentResolver.SCHEME_CONTENT)) {
                    try {
                        var fileInputStream = getApplicationContext().getContentResolver().openInputStream(uri);
                        datasize = fileInputStream.available();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    double kb = datasize / 1000;
                    double mb = kb / 1000;
                    if (mb <= 15) {
                        //todo uploading file
                        var documentFile = DocumentFile.fromSingleUri(Chat.this, result.getData().getData());
                        var fileName = documentFile.getName();
                        var extension = fileName.substring(fileName.lastIndexOf("."));
                        viewModel.ChatFileUpload(result.getData().getData(), DataManager.Image).observe(this, new Observer<String>() {
                            @Override
                            public void onChanged(String s) {
                                viewModel.SendChatMessage(s, DataManager.Image, SenderUID, fileName, extension, String.valueOf(df.format(mb))).observe(Chat.this, aBoolean -> {
                                    if (aBoolean) {
                                        progressDialog.CancelProgressDialog();
                                        SendNotificationToUser(s, DataManager.Image, SenderUID);
                                    } else {
                                        progressDialog.CancelProgressDialog();

                                    }
                                });

                            }
                        });
                    } else {
                        progressDialog.CancelProgressDialog();
                        Toast.Message(Chat.this, "File size is bigger minimum file size of less than 15 MB");
                    }

                } else if (scheme.equals(ContentResolver.SCHEME_FILE)) {
                    String path = uri.getPath();
                    try {
                        var f = new File(path);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            } else {
                progressDialog.CancelProgressDialog();
            }
        });
    }


    private void SendMessageHistory(String Message, String ChatType) {
        viewModel.SendMessageHistory(SenderUID, ChatType, Message).observe(this, aBoolean -> {

        });

    }

    @Override
    public void onBackPressed() {
        finish();
        Animatoo.animateSlideRight(Chat.this);
    }
}