package com.blood.bloodbook.UI.Adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.blood.bloodbook.LostAndFoundModel;
import com.blood.bloodbook.UI.ViewHolder.LostAndFoundVH;
import com.blood.bloodbook.databinding.LostandfounditemBinding;
import com.squareup.picasso.Picasso;

import java.util.List;
import javax.inject.Inject;
import javax.inject.Singleton;

import lombok.Getter;
import lombok.Setter;
@Singleton
public class LostAndFoundAdapter extends RecyclerView.Adapter<LostAndFoundVH> {
    @Setter
    @Getter
    private List<LostAndFoundModel> list;
    private OnClick OnClick;
    private OnLongClick OnLongClick;

    @Inject
    public LostAndFoundAdapter(){

    }

    @NonNull
    @Override
    public LostAndFoundVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        var l = LayoutInflater.from(parent.getContext());
        var v = LostandfounditemBinding.inflate(l, parent, false);
        return new LostAndFoundVH(v);
    }

    @Override
    public void onBindViewHolder(@NonNull LostAndFoundVH holder, int position) {

        holder.binding.LostAndFount.setText(list.get(position).getLostAndFound());
        holder.binding.PostTitle.setText(list.get(position).getTitle());
        Picasso.get().load(list.get(position).getImage()).into(holder.binding.Image);
        holder.binding.CountryName.setText(list.get(position).getCountry());
        holder.binding.ProvinceStateName.setText(list.get(position).getProvince());
        holder.binding.DistrictName.setText(list.get(position).getDistrict());
        holder.binding.Details.setText(list.get(position).getFullDetails());
        holder.binding.ContactNumberID.setText(list.get(position).getPhoneNumber());

        holder.binding.CallIcon.setOnClickListener(view -> {
            OnClick.Call(list.get(position));
        });

        holder.itemView.setOnLongClickListener(view -> {
            OnLongClick.Click(list.get(position));
            return true;
        });

    }

    @Override
    public int getItemCount() {
        if(list == null) {
            return 0;
        }return list.size();
    }

    public interface OnClick{
        void Call(LostAndFoundModel organizationModel);
    }
    public void OnCallLisiner(OnClick OnClick){
        this.OnClick = OnClick;
    }




    public interface OnLongClick{
        void Click(LostAndFoundModel lostAndFoundModel);
    }
    public void OnLongClickState(OnLongClick OnLongClick){
        this.OnLongClick = OnLongClick;
    }
}
