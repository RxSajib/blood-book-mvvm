package com.blood.bloodbook.UI.Adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.blood.bloodbook.Utils.Toast;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.squareup.picasso.Picasso;
import com.blood.bloodbook.Data.DataManager;
import com.blood.bloodbook.NotificationModel;
import com.blood.bloodbook.R;
import com.blood.bloodbook.UI.NotificationViewHolder.NotificationBloodRequest;
import com.blood.bloodbook.UI.NotificationViewHolder.NotificationBroadcastMessageVH;
import com.blood.bloodbook.UI.NotificationViewHolder.NotificationTextMessageVH;
import com.blood.bloodbook.UI.NotificationViewHolder.NotificationUserBroadcastMessageVH;
import com.blood.bloodbook.Utils.TimestampDayConverter;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import lombok.Getter;
import lombok.Setter;

@Singleton
public class NotificationAdapter extends RecyclerView.Adapter {

    @Setter
    @Getter
    private List<NotificationModel> list;
    private final int TextChatMessage = 1;
    private final int BroadcastMessage = 2;
    private final int UserBroadcastMessage = 3;
    private final int BloodRequestMessage = 4;
    private CollectionReference UserRef;
    private OnClickChat OnClickChat;
    private static final String TAG = "NotificationAdapter";
    private OnCLick OnCLick;

    @Inject
    public NotificationAdapter() {
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        UserRef = FirebaseFirestore.getInstance().collection(DataManager.User);
        if (viewType == TextChatMessage) {
            return new NotificationTextMessageVH(LayoutInflater.from(parent.getContext()).inflate(R.layout.textmessagenotificationitem, parent, false));
        }

        if (viewType == BroadcastMessage) {
            return new NotificationBroadcastMessageVH(LayoutInflater.from(parent.getContext()).inflate(R.layout.broadcastmessagenotificationitem, parent, false));
        }

        if (viewType == UserBroadcastMessage) {
            return new NotificationUserBroadcastMessageVH(LayoutInflater.from(parent.getContext()).inflate(R.layout.userbroadcastmessagenotificationitem, parent, false));
        }

        if (viewType == BloodRequestMessage) {
            return new NotificationBloodRequest(LayoutInflater.from(parent.getContext()).inflate(R.layout.bloodrequestmessageitem, parent, false));
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        var item = list.get(position).getMessageType();


        if (holder.getClass() == NotificationTextMessageVH.class) {
            Log.d(TAG, "text message");
            var notificationTextMessageVH = (NotificationTextMessageVH) holder;

            if (item.equals(DataManager.ChatTextMessage)) {
                notificationTextMessageVH.Message.setText(list.get(position).getMessage());
                notificationTextMessageVH.TimeDate.setText(TimestampDayConverter.getMyPrettyDate(list.get(position).getTimestamp()));

                UserRef.document(list.get(position).getSenderUID()).addSnapshotListener((value, error) -> {
                    if (error != null) {
                        return;
                    }
                    if (value.exists()) {
                        Picasso.get().load(value.getString(DataManager.ProfileImage)).into(notificationTextMessageVH.ProfileImage);
                        notificationTextMessageVH.SenderName.setText(value.getString(DataManager.FirstName));
                        notificationTextMessageVH.TimeDate.setText(TimestampDayConverter.getMyPrettyDate(list.get(position).getTimestamp()));

                        notificationTextMessageVH.itemView.setOnClickListener(view -> {
                            OnClickChat.CLick(list.get(position).getSenderUID(), value.getString(DataManager.GivenName), value.getString(DataManager.Surname));
                        });
                    }
                });
            }
        }


        if (holder.getClass() == NotificationBroadcastMessageVH.class) {
            var notificationBroadcastMessageVH = (NotificationBroadcastMessageVH) holder;
            notificationBroadcastMessageVH.Time.setText(TimestampDayConverter.getMyPrettyDate(list.get(position).getTimestamp()));

            if (item.equals(DataManager.Broadcast)) {
                notificationBroadcastMessageVH.Message.setText(list.get(position).getMessage());
                notificationBroadcastMessageVH.Topic.setText(list.get(position).getMessageTopic());
            }

            holder.itemView.setOnClickListener(view -> {
                OnCLick.Click(list.get(position).getDocumentKey());
            });


        }

        if (holder.getClass() == NotificationUserBroadcastMessageVH.class) {
            var notificationUserBroadcastMessageVH = (NotificationUserBroadcastMessageVH) holder;
            if (item.equals(DataManager.UserBroadcastMessage)) {
                notificationUserBroadcastMessageVH.Message.setText(list.get(position).getMessage());
                notificationUserBroadcastMessageVH.Topic.setText(list.get(position).getMessageType()
                );
            }

            holder.itemView.setOnClickListener(view -> {
                OnCLick.Click(list.get(position).getDocumentKey());
            });
            notificationUserBroadcastMessageVH.Time.setText(TimestampDayConverter.getMyPrettyDate(list.get(position).getTimestamp()));
        }

        if (holder.getClass() == NotificationBloodRequest.class) {
            var notificationBloodRequest = (NotificationBloodRequest) holder;

            if (item.equals(DataManager.BloodRequestAccept)) {
                notificationBloodRequest.Message.setText("Accepted " + list.get(position).getMessage() + " Blood CC");
                notificationBloodRequest.Time.setText(TimestampDayConverter.getMyPrettyDate(list.get(position).getTimestamp()));
            }
        }
    }

    @Override
    public int getItemCount() {
        if (list == null) {
            return 0;
        }
        return list.size();
    }

    @Override
    public int getItemViewType(int position) {

        var item = list.get(position);
        if (item.getMessageType().equals(DataManager.ChatTextMessage)) {
            Log.d(TAG, "text");
            return TextChatMessage;
        }

        if (item.getMessageType().equals(DataManager.Broadcast)) {
            Log.d(TAG, "call");
            return BroadcastMessage;
        }

        if (item.getMessageType().equals(DataManager.UserBroadcastMessage)) {
            return UserBroadcastMessage;
        }

        if (item.getMessageType().equals(DataManager.BloodRequestAccept)) {
            return BloodRequestMessage;
        }

        return super.getItemViewType(position);
    }


    public interface OnClickChat {
        void CLick(String UID, String GivenName, String SureName);
    }

    public void OnChatLisner(OnClickChat OnClickChat) {
        this.OnClickChat = OnClickChat;
    }


    public interface OnCLick {
        void Click(long DocumentKey);
    }

    public void OnClickState(OnCLick OnCLick) {
        this.OnCLick = OnCLick;
    }

}
