package com.blood.bloodbook.UI.ViewHolder;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.blood.bloodbook.databinding.BlooddonationresttimeitemBinding;

public class BloodDonationRestItemViewHolder extends RecyclerView.ViewHolder {

    public BlooddonationresttimeitemBinding binding;

    public BloodDonationRestItemViewHolder(@NonNull BlooddonationresttimeitemBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }
}
