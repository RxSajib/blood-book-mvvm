package com.blood.bloodbook.UI;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.os.Bundle;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.blood.bloodbook.Application;
import com.blood.bloodbook.DI.DaggerWeenaComponent;
import com.blood.bloodbook.Network.ViewModel.ViewModel;
import com.blood.bloodbook.R;
import com.blood.bloodbook.Utils.Permission;
import com.blood.bloodbook.Widget.ProgressDialog;
import com.blood.bloodbook.databinding.EditprofileBinding;

import javax.inject.Inject;

public class EditProfile extends AppCompatActivity {

    private EditprofileBinding binding;
    private ViewModel viewModel;
    public @Inject
    ProgressDialog progressBar;
    private ActivityResultLauncher<Intent> launcher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.editprofile);
        viewModel = new ViewModelProvider(this).get(ViewModel.class);

        
        var component = Application.weenaComponent;
        component.InjectEditProfile(this);

        InitView();
        GetUser();
        UpdateProfile();
        GetImageFromGallery();
    }

    private void GetImageFromGallery(){
        launcher = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), result -> {
           if(result.getResultCode() == RESULT_OK){
               progressBar.ProgressDialog(EditProfile.this);
               if(result.getData() != null){
                    viewModel.ProfileImageUpdate(result.getData().getData()).observe(this, new Observer<Boolean>() {
                        @Override
                        public void onChanged(Boolean aBoolean) {
                            if(aBoolean){
                                progressBar.CancelProgressDialog();
                            }else {
                                progressBar.CancelProgressDialog();
                            }
                        }
                    });
               }else {
                   progressBar.CancelProgressDialog();
               }
           }
        });
    }

    private void UpdateProfile(){
        binding.UpdateBtn.setOnClickListener(view -> {
            progressBar.ProgressDialog(EditProfile.this);
            var FirstName = binding.FirstNameInput.getText().toString().trim();
            var MiddleName = binding.MiddleNameInput.getText().toString().trim();
            var SureName = binding.SureNameInput.getText().toString().trim();
            var FatherName = binding.FatherNameInput.getText().toString().trim();

            viewModel.ProfileUpdate(FirstName, MiddleName, SureName, FatherName).observe(this, aBoolean -> {
                if(aBoolean){
                    progressBar.CancelProgressDialog();
                }else {
                    progressBar.CancelProgressDialog();
                }
            });
        });
    }

    private void GetUser(){
        viewModel.ProfileGET().observe(this, profileModel -> {
            if(profileModel != null){
                binding.setProfile(profileModel);
            }
        });
    }

    private void InitView(){
        binding.Toolbar.Title.setText(getResources().getString(R.string.EditProfile));
        binding.Toolbar.BackButton.setOnClickListener(view -> {
            finish();
            Animatoo.animateSlideRight(EditProfile.this);
        });

        binding.Image.setOnClickListener(view -> {
            if(Permission.PermissionExternalStorage(EditProfile.this, 52)){
                var intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("image/*");
                launcher.launch(intent);
            }

        });
    }

    @Override
    public void onBackPressed() {
        finish();
        Animatoo.animateSlideRight(EditProfile.this);
    }
}