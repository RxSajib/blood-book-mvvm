package com.blood.bloodbook.UI.Adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.squareup.picasso.Picasso;
import com.blood.bloodbook.Data.DataManager;
import com.blood.bloodbook.DonorModel;
import com.blood.bloodbook.R;
import com.blood.bloodbook.UI.ViewHolder.DonorUserViewHolder;
import com.blood.bloodbook.databinding.DonoruseritemBinding;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import lombok.Getter;
import lombok.Setter;
@Singleton
public class DonorUserAdapter extends RecyclerView.Adapter<DonorUserViewHolder> {

    @Inject
    public DonorUserAdapter(){}

    @Setter @Getter
    private List<DonorModel> list;
    private OnClick OnClick;
    private CollectionReference UserRef;

    @NonNull
    @Override
    public DonorUserViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        UserRef = FirebaseFirestore.getInstance().collection(DataManager.User);
        var l = LayoutInflater.from(parent.getContext());
        var v = DonoruseritemBinding.inflate(l, parent, false);
        return new DonorUserViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull DonorUserViewHolder holder, int position) {

        UserRef.document(list.get(position).getSenderUID()).addSnapshotListener((value, error) -> {
            if(error != null){
                return;
            }
            if(value.exists()){
                Picasso.get().load(value.getString(DataManager.ProfileImage)).placeholder(R.drawable.profileplaceholder).into(holder.binding.ProfileImage);
            }
        });
        holder.binding.Name.setText(list.get(position).getGivenName());
        holder.binding.Location.setText(list.get(position).getProvince()+" "+list.get(position).getDistrict());
        holder.binding.BloodName.setText(list.get(position).getBloodGroup());

        holder.itemView.setOnClickListener(view -> {
            OnClick.Click(list.get(position));
        });
    }

    @Override
    public int getItemCount() {
        if(list == null) {
            return 0;
        }return list.size();
    }

    public interface OnClick{
        void Click(DonorModel model);
    }
    public void OnClickLisiner(OnClick OnClick){
        this.OnClick = OnClick;
    }
}
