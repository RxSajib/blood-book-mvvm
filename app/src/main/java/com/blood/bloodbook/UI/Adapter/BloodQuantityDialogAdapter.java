package com.blood.bloodbook.UI.Adapter;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.blood.bloodbook.BloodQuantityModel;
import com.blood.bloodbook.R;
import com.blood.bloodbook.UI.ViewHolder.BloodQuantityViewHolder;
import com.blood.bloodbook.databinding.BloodquantityitemBinding;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import lombok.Getter;
import lombok.Setter;
@Singleton
public class BloodQuantityDialogAdapter extends RecyclerView.Adapter<BloodQuantityViewHolder> {
    @Setter @Getter
    private List<BloodQuantityModel> list;
    private int row_index = -1;
    private OnCLick OnCLick;

    @Inject
    public BloodQuantityDialogAdapter(){

    }

    @NonNull
    @Override
    public BloodQuantityViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        var l = LayoutInflater.from(parent.getContext());
        var v = BloodquantityitemBinding.inflate(l, parent, false);
        return new BloodQuantityViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull BloodQuantityViewHolder holder, @SuppressLint("RecyclerView") int position) {
        holder.binding.setBloodQuantity(list.get(position));
        holder.itemView.setOnClickListener(view -> {
            OnCLick.Click(list.get(position).getQuantity());
            row_index = position;
            notifyDataSetChanged();
        });

        if (row_index == position) {
            holder.binding.Quantity.setTextColor(holder.itemView.getContext().getResources().getColor(R.color.carbon_red_400));
        } else {
            holder.binding.Quantity.setTextColor(holder.itemView.getContext().getResources().getColor(R.color.carbon_black_38));
        }
    }

    @Override
    public int getItemCount() {
        if(list == null) {
            return 0;
        }return list.size();
    }

    public interface OnCLick{
        void Click(String BloodQuantity);
    }
    public void OnClickEvent(OnCLick OnCLick){
        this.OnCLick = OnCLick;
    }
}
