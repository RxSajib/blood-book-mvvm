package com.blood.bloodbook.UI;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;
import android.widget.ProgressBar;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.blood.bloodbook.Application;
import com.blood.bloodbook.Network.ViewModel.ViewModel;
import com.blood.bloodbook.R;
import com.blood.bloodbook.Utils.HandleActivity;
import com.blood.bloodbook.Utils.Toast;
import com.blood.bloodbook.Widget.ProgressDialog;
import com.blood.bloodbook.databinding.SigninupBinding;

import javax.inject.Inject;

public class SignInUp extends AppCompatActivity {

    private SigninupBinding binding;
    private ViewModel viewModel;
    @Inject
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.signinup);
        viewModel = new ViewModelProvider(this).get(ViewModel.class);
        var component = Application.weenaComponent;
        component.InjectSignInUp(this);



        InitView();
        SignUpAccount();
    }

    private void SignUpAccount(){
        binding.SignInBtn.setOnClickListener(view -> {
            var Name = binding.NameInput.getText().toString().trim();
            var Email = binding.EmailInput.getText().toString().trim();
            var Password = binding.PasswordInput.getText().toString().trim();
            var CPassword = binding.CPasswordInput.getText().toString().trim();

            if(Name.isEmpty()){
                Toast.Message(this, getResources().getString(R.string.NameEmpty));
            }else if(Email.isEmpty()){
                Toast.Message(this, getResources().getString(R.string.EmailEmpty));
            }else if(Password.isEmpty()){
                Toast.Message(this, getResources().getString(R.string.PasswordEmpty));
            }else if(CPassword.isEmpty()){
                Toast.Message(this, getResources().getString(R.string.ConfirmPasswordEmpty));
            }else if(!CPassword.equals(Password)){
                Toast.Message(this, getResources().getString(R.string.PasswordNotMatch));
            }else if(CPassword.length() <= 7){
                Toast.Message(this, getResources().getString(R.string.PasswordLengthValidation));
            }else {
                progressDialog.ProgressDialog(SignInUp.this);
                viewModel.SignUpAccount(Email, Password).observe(this, aBoolean -> {
                    if(aBoolean){
                        viewModel.CreateProfile(Name, null, null, null, null, Email, Email).observe(this, aBoolean1 -> {
                            if(aBoolean1){
                                Toast.Message(SignInUp.this, "Account create success");
                                HandleActivity.GotoHome(SignInUp.this);
                                finish();
                                Animatoo.animateSlideLeft(SignInUp.this);
                            }else {
                                progressDialog.CancelProgressDialog();
                            }
                        });
                    }else {
                        progressDialog.CancelProgressDialog();
                    }
                });
            }
        });
    }

    private void InitView(){
        binding.Toolbar.Title.setText("Sign Up Account");
        binding.Toolbar.BackButton.setOnClickListener(view -> {
            finish();
            Animatoo.animateSlideRight(SignInUp.this);
        });
    }

    @Override
    public void onBackPressed() {
        finish();
        Animatoo.animateSlideRight(SignInUp.this);
    }
}