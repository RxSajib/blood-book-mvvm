package com.blood.bloodbook.UI;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.blood.bloodbook.Application;
import com.blood.bloodbook.DI.DaggerWeenaComponent;
import com.blood.bloodbook.Network.ViewModel.ViewModel;
import com.blood.bloodbook.R;
import com.blood.bloodbook.Utils.Toast;
import com.blood.bloodbook.Widget.ProgressDialog;
import com.blood.bloodbook.databinding.ForgotpasswordBinding;

import javax.inject.Inject;

public class ForgotPassword extends AppCompatActivity {

    private ForgotpasswordBinding binding;
    private ViewModel viewModel;
    @Inject
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.forgotpassword);
        viewModel = new ViewModelProvider(this).get(ViewModel.class);

        var component = Application.weenaComponent;
        component.InjectResetPassword(this);

        InitView();
        ResetPassword();

    }

    private void InitView(){
        binding.Toolbar.Title.setText("Reset Password");
        binding.Toolbar.BackButton.setOnClickListener(view -> {
            finish();
            Animatoo.animateSlideRight(ForgotPassword.this);
        });
    }

    private void ResetPassword(){
        binding.ContinueBtn.setOnClickListener(view -> {
            var EmailAddress = binding.EmailInput.getText().toString().trim();
            if(EmailAddress.isEmpty()){
                Toast.Message(ForgotPassword.this, "Email address empty");
            }else {
                progressDialog.ProgressDialog(ForgotPassword.this);
                viewModel.ResetPassword(EmailAddress).observe(this, aBoolean -> {
                    if(aBoolean){
                        progressDialog.CancelProgressDialog();
                        finish();
                        Animatoo.animateSlideRight(ForgotPassword.this);
                    }else {
                        progressDialog.CancelProgressDialog();
                    }
                });
            }
        });

    }

    @Override
    public void onBackPressed() {
        finish();
        Animatoo.animateSlideRight(ForgotPassword.this);
    }
}