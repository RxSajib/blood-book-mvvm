package com.blood.bloodbook.UI.ViewHolder;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.blood.bloodbook.databinding.TextmessagenotificationitemBinding;

public class MessageHistoryViewHolder extends RecyclerView.ViewHolder {

    public TextmessagenotificationitemBinding binding;

    public MessageHistoryViewHolder(@NonNull TextmessagenotificationitemBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }
}
