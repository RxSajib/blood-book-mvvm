package com.blood.bloodbook.UI.BloodRequest;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.blood.bloodbook.Application;
import com.blood.bloodbook.Network.ViewModel.ViewModel;
import com.blood.bloodbook.R;
import com.blood.bloodbook.UI.Adapter.AcceptBloodRequestAdapter;
import com.blood.bloodbook.Utils.Toast;
import com.blood.bloodbook.Widget.ProgressDialog;
import com.blood.bloodbook.databinding.AcceptedrequestBinding;

import javax.inject.Inject;


public class AcceptedRequest extends Fragment {

    private AcceptedrequestBinding binding;
    private ViewModel viewModel;
    @Inject AcceptBloodRequestAdapter acceptBloodRequestAdapter;
    @Inject
    ProgressDialog progressDialog;
    private int Limit = 25;

    public AcceptedRequest() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.acceptedrequest, container, false);
        viewModel = new ViewModelProvider(this).get(ViewModel.class);

        var component = Application.weenaComponent;
        component.InjectAcceptedRequest(this);

        InitView();

        binding.SwipeRefreshLayout.setOnRefreshListener(() -> {
            GetData(Limit);
        });
        binding.RecyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if(!recyclerView.canScrollVertically(1)){
                    Limit = Limit+25;
                    GetData(Limit);
                }
            }
        });
        GetData(Limit);

        return binding.getRoot();
    }

    private void GetData(int Limit){
        viewModel.GetMyAcceptBloodRequest(Limit).observe(getActivity(), bloodRequestModels -> {
            acceptBloodRequestAdapter.setList(bloodRequestModels);
            acceptBloodRequestAdapter.notifyDataSetChanged();
            binding.SwipeRefreshLayout.setRefreshing(false);
            Toast.Message(getActivity(), "sdsd");
            if (bloodRequestModels != null) {
                binding.Icon.setVisibility(View.GONE);
                binding.message.setVisibility(View.GONE);
            } else {
                binding.Icon.setVisibility(View.VISIBLE);
                binding.message.setVisibility(View.VISIBLE);
            }
        });
    }
    private void InitView() {
        binding.RecyclerView.setHasFixedSize(true);
        binding.RecyclerView.setAdapter(acceptBloodRequestAdapter);

        acceptBloodRequestAdapter.OnClickState(bloodRequestModel -> {
            var dialog = new AlertDialog.Builder(getActivity());
            dialog.setTitle("Can’t go to Patient?");
            dialog.setMessage("Send back request");





            dialog.setPositiveButton("Send Back request", (dialogInterface, i) -> {
                progressDialog.ProgressDialog(getActivity());
               /* viewModel.GetSingleBloodRequest(String.valueOf(bloodRequestModel.getDocumentID())).observe(getActivity(), bloodRequestModel1 -> {
                    if (bloodRequestModel1 != null) {
                        Toast.Message(getActivity(), "sasd");
                    }
                });*/



                viewModel.UpdateBloodRequestQuantity(bloodRequestModel.getNumberOFQuantityPints(), String.valueOf(bloodRequestModel.getDocumentID()), bloodRequestModel).observe(getActivity(), aBoolean -> {
                    if (aBoolean) {
                        viewModel.MyAcceptedBloodRequestDelete(String.valueOf(bloodRequestModel.getDocumentKey())).observe(getActivity(), aBoolean1 -> {
                            if (aBoolean1) {
                                progressDialog.CancelProgressDialog();
                            } else {
                                progressDialog.CancelProgressDialog();
                            }
                        });
                    } else {
                        progressDialog.CancelProgressDialog();
                    }
                });
            });
            dialog.setNegativeButton("Cancel", (dialogInterface, i) -> {
            });

            var d = dialog.create();
            d.show();

        });
    }
}