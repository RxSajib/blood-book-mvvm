package com.blood.bloodbook.UI;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Context;
import android.os.Bundle;
import android.telephony.TelephonyManager;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.blood.bloodbook.Application;
import com.blood.bloodbook.Data.DataManager;
import com.blood.bloodbook.Data.SharePref;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.blood.bloodbook.DI.DaggerWeenaComponent;
import com.blood.bloodbook.R;
import com.blood.bloodbook.Utils.HandleActivity;
import com.blood.bloodbook.Utils.Toast;
import com.blood.bloodbook.Widget.ProgressDialog;
import com.blood.bloodbook.databinding.LoginphoneBinding;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

public class LoginPhone extends AppCompatActivity {

    private LoginphoneBinding binding;
    @Inject
    ProgressDialog progressDialog;
    private SharePref sharePref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.loginphone);
        sharePref = new SharePref(LoginPhone.this);

        var component = Application.weenaComponent;
        component.InjectLoginPhone(this);

        InitView();
    }

    private void InitView() {
        var tm = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
        var countryCodeValue = tm.getNetworkCountryIso();

        binding.CodePicker.setCountryForNameCode(countryCodeValue);
        binding.Indicator.BackButton.setOnClickListener(view -> {
            finish();
            Animatoo.animateSlideRight(LoginPhone.this);
        });

        binding.Indicator.Title.setText(getResources().getString(R.string.EnterYourPhone));

        binding.SignUpButton.setOnClickListener(view -> {
            var Code = binding.CodePicker.getSelectedCountryCode();
            var Number = binding.NumberInput.getText().toString();
            if (Number.isEmpty()) {
                Toast.Message(LoginPhone.this, getResources().getString(R.string.PhoneNumberIsEmpty));
            } else {
                progressDialog.ProgressDialog(LoginPhone.this);
                PhoneAuthProvider.getInstance().verifyPhoneNumber(
                        "+" + Code + Number,
                        60,
                        TimeUnit.SECONDS,
                        LoginPhone.this,
                        new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
                            @Override
                            public void onVerificationCompleted(@NonNull PhoneAuthCredential phoneAuthCredential) {

                            }

                            @Override
                            public void onVerificationFailed(@NonNull FirebaseException e) {
                                Toast.Message(getApplicationContext(), e.getMessage());
                                progressDialog.CancelProgressDialog();
                            }

                            @Override
                            public void onCodeSent(@NonNull String s, @NonNull PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                                progressDialog.CancelProgressDialog();
                                HandleActivity.GotoOTP(LoginPhone.this, "+" + Code + Number, s);
                                Animatoo.animateSlideLeft(LoginPhone.this);
                                finish();
                            }
                        }
                );


            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
        Animatoo.animateSlideRight(LoginPhone.this);
    }
}