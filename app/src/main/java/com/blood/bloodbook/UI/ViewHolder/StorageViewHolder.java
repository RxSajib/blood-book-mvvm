package com.blood.bloodbook.UI.ViewHolder;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.blood.bloodbook.databinding.StorageitemBinding;

public class StorageViewHolder extends RecyclerView.ViewHolder {

    public StorageitemBinding binding;

    public StorageViewHolder(@NonNull StorageitemBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }
}
