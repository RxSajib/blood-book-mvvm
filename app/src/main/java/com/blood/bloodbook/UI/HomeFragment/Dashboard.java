package com.blood.bloodbook.UI.HomeFragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.blood.bloodbook.Application;
import com.blood.bloodbook.ProfileModel;
import com.blood.bloodbook.Widget.ProgressDialog;
import com.blood.bloodbook.databinding.FeelingsickdialogBinding;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.rewarded.RewardedAd;
import com.google.android.gms.ads.rewarded.RewardedAdLoadCallback;
import com.blood.bloodbook.Data.Data;
import com.blood.bloodbook.Data.NotifactionResponse;
import com.blood.bloodbook.Network.ViewModel.ViewModel;
import com.blood.bloodbook.R;
import com.blood.bloodbook.Utils.HandleActivity;
import com.blood.bloodbook.databinding.DashboardBinding;

import org.eazegraph.lib.models.BarModel;

import javax.inject.Inject;

public class Dashboard extends Fragment {

    private DashboardBinding binding;
    private ViewModel viewModel;
    private RewardedAd mRewardedAd;
    private String TAG = "DashBoard";
    @Inject
    ProgressDialog progressDialog;

    public Dashboard() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.dashboard, container, false);
        viewModel = new ViewModelProvider(this).get(ViewModel.class);

        var component = Application.weenaComponent;
        component.InjectDashboard(this);

        InitView();
        LoadBannerAd();
        LoadAds();
        InitRewardedAds();
        SetbarCode();
        GetSizeOFItem();

        return binding.getRoot();
    }

    private void GetSizeOFItem() {
        viewModel.SizeOFRegisterDonor().observe(getActivity(), integer -> {
            if (integer != null) {
                binding.setRegisterDonorCount(integer);
            }
        });

        viewModel.RegisterFriendDonorSize().observe(getActivity(), integer -> {
            if (integer != null) {
                binding.setRegisterAsFriendCount(integer);
            }
        });

        viewModel.BloodRequestSize().observe(getActivity(), integer -> {
            if (integer != null) {
                binding.setOrderDoneCount(integer);
            }
        });
    }

    private void SetbarCode() {
        binding.BarChart.addBar(new BarModel(59, Color.parseColor("#64b5f6")));
        binding.BarChart.addBar(new BarModel(42, Color.parseColor("#ffb74d")));
        binding.BarChart.addBar(new BarModel(95, Color.parseColor("#81c784")));
        binding.BarChart.addBar(new BarModel(16, Color.parseColor("#e57373")));
        binding.BarChart.addBar(new BarModel(75, Color.parseColor("#81c784")));
        binding.BarChart.addBar(new BarModel(26, Color.parseColor("#64b5f6")));
        binding.BarChart.addBar(new BarModel(95, Color.parseColor("#ffb74d")));
        binding.BarChart.addBar(new BarModel(55, Color.parseColor("#ffb74d")));
        binding.BarChart.startAnimation();
    }

    private void LoadAds() {
        viewModel.GetDashBoardAdData().observe(getActivity(), adModel -> {
            if (adModel != null) {
                binding.setAdManager(adModel);

                binding.AdsBtn.setOnClickListener(view -> {
                    var Intent = new Intent(android.content.Intent.ACTION_VIEW);
                    Intent.setData(Uri.parse(adModel.getWebUrl()));
                    startActivity(Intent);
                });
            }
        });

    }

    private void LoadBannerAd() {
        var adRequest = new AdRequest.Builder().build();
        binding.AdView.loadAd(adRequest);
    }

    private void InitView() {

        binding.TelephonyManager.setOnClickListener(view -> {
            HandleActivity.GotoTelephonyManager(getActivity());
            Animatoo.animateSlideLeft(getActivity());

            if (mRewardedAd != null) {
                Activity activityContext = getActivity();
                mRewardedAd.show(activityContext, rewardItem -> {
                    // Handle the reward.
                    int rewardAmount = rewardItem.getAmount();
                    String rewardType = rewardItem.getType();
                    mRewardedAd = null;
                    InitRewardedAds();
                });
            } else {
            }
        });

        binding.FindDonor.setOnClickListener(view -> {
            HandleActivity.GotoBloodFilter(getActivity());
            Animatoo.animateSlideLeft(getActivity());
        });

        binding.BloodRequest.setOnClickListener(view -> {
            progressDialog.ProgressDialog(getActivity());
            viewModel.GetUserSickStatus().observe(getActivity(), new Observer<ProfileModel>() {
                @Override
                public void onChanged(ProfileModel profileModel) {
                    if (profileModel.isIsRequestEnable()) {
                        HandleActivity.GotoRequest(getActivity());
                        Animatoo.animateSlideLeft(getActivity());
                        progressDialog.CancelProgressDialog();
                    } else {
                        progressDialog.CancelProgressDialog();
                        OpenSickDialog();
                    }
                }
            });
        });

        binding.BloodBank.setOnClickListener(view -> {
            HandleActivity.GotoBloodBank(getActivity());
            Animatoo.animateSlideLeft(getActivity());
        });

        binding.LostAndFoundBtn.setOnClickListener(view -> {
            HandleActivity.GotoLostAndFound(getActivity());
            Animatoo.animateSlideLeft(getActivity());
        });
    }


    private void OpenSickDialog() {
        var alertdialog = new AlertDialog.Builder(getActivity());
        FeelingsickdialogBinding fs = DataBindingUtil.inflate(getLayoutInflater(), R.layout.feelingsickdialog, null, false);
        alertdialog.setView(fs.getRoot());

        var d = alertdialog.create();
        d.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        d.show();
        fs.Continue.setOnClickListener(view -> {
            d.dismiss();
        });
    }


    private void InitRewardedAds() {
        AdRequest adRequest = new AdRequest.Builder().build();
        RewardedAd.load(getActivity(), "ca-app-pub-3940256099942544/5224354917",
                adRequest, new RewardedAdLoadCallback() {
                    @Override
                    public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                        // Handle the error.
                        mRewardedAd = null;
                    }

                    @Override
                    public void onAdLoaded(@NonNull RewardedAd rewardedAd) {
                        mRewardedAd = rewardedAd;

                        Log.d(TAG, "Ad was loaded.");
                    }
                });
    }

}