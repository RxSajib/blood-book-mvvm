package com.blood.bloodbook.UI.ViewHolder;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.blood.bloodbook.databinding.ProvinceoffghanistanitemBinding;

public class ProvinceOfAFGViewHolder extends RecyclerView.ViewHolder {

    public ProvinceoffghanistanitemBinding binding;

    public ProvinceOfAFGViewHolder(@NonNull ProvinceoffghanistanitemBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }
}
