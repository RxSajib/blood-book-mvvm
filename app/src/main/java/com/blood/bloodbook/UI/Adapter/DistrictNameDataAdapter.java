package com.blood.bloodbook.UI.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.blood.bloodbook.Data.DistrictNameModel;
import com.blood.bloodbook.UI.ViewHolder.LocationViewHolder;
import com.blood.bloodbook.databinding.LocationitemBinding;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import lombok.Getter;
import lombok.Setter;
@Singleton
public class DistrictNameDataAdapter extends RecyclerView.Adapter<LocationViewHolder> {

    @Inject
    public DistrictNameDataAdapter(){
    }


    @Setter
    @Getter
    private List<DistrictNameModel> list;
    private OnClick OnClick;

    @NonNull
    @Override
    public LocationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        var l = LayoutInflater.from(parent.getContext());
        var v = LocationitemBinding.inflate(l, parent, false);
        return new LocationViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull LocationViewHolder holder, int position) {
        holder.binding.LocationName.setText(list.get(position).getDistrict());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                OnClick.Click(list.get(position).getDistrict());
            }
        });
    }

    @Override
    public int getItemCount() {
        if (list == null) {
            return 0;
        }
        return list.size();
    }

    public interface OnClick{
        void Click(String DictrictName);
    }
    public void OnClickLisiner(OnClick OnClick){
        this.OnClick = OnClick;
    }
}
