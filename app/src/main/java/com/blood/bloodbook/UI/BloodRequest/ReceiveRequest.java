package com.blood.bloodbook.UI.BloodRequest;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.blood.bloodbook.Application;
import com.blood.bloodbook.Network.ViewModel.ViewModel;
import com.blood.bloodbook.R;
import com.blood.bloodbook.UI.Adapter.BloodRequestAdapter;
import com.blood.bloodbook.Utils.HandleActivity;
import com.blood.bloodbook.databinding.ReceiverequestBinding;

import javax.inject.Inject;


public class ReceiveRequest extends Fragment {

    private ReceiverequestBinding binding;
    private ViewModel viewModel;
    @Inject
    BloodRequestAdapter bloodRequestAdapter;
    private int Limit = 25;

    public ReceiveRequest() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.receiverequest, container, false);
        viewModel = new ViewModelProvider(this).get(ViewModel.class);
        var component = Application.weenaComponent;
        component.InjectReceiveRequest(this);

        InitView();

        GetBloodRequest(Limit);
        binding.SwipeRefreshLayout.setOnRefreshListener(() -> {
            GetBloodRequest(Limit);
        });
        binding.RecyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if(!recyclerView.canScrollVertically(1)){
                    Limit = Limit+25;
                    GetBloodRequest(Limit);
                }
            }
        });

        return binding.getRoot();
    }

    private void InitView(){
        binding.RecyclerView.setHasFixedSize(true);
        binding.RecyclerView.setAdapter(bloodRequestAdapter);
        bloodRequestAdapter.OnClickLisiner(model -> {
            HandleActivity.GotoBloodRequestDetails(getActivity(), model);
            Animatoo.animateSlideLeft(getActivity());
        });
    }

    private void GetBloodRequest(int Limit){
        viewModel.GetBloodRequest(Limit).observe(getActivity(), bloodRequestModels -> {
            bloodRequestAdapter.setList(bloodRequestModels);
            bloodRequestAdapter.notifyDataSetChanged();
            binding.SwipeRefreshLayout.setRefreshing(false);
            if(bloodRequestModels == null){
                binding.Icon.setVisibility(View.VISIBLE);
                binding.message.setVisibility(View.VISIBLE);
            }else {
                binding.Icon.setVisibility(View.GONE);
                binding.message.setVisibility(View.GONE);
            }
        });
    }
}