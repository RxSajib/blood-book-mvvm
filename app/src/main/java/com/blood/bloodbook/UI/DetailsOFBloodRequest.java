package com.blood.bloodbook.UI;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.blood.bloodbook.Application;
import com.blood.bloodbook.Data.Data;
import com.blood.bloodbook.Data.NotifactionResponse;
import com.blood.bloodbook.ProfileModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.blood.bloodbook.BloodQuantityModel;
import com.blood.bloodbook.BloodRequestModel;
import com.blood.bloodbook.DI.DaggerWeenaComponent;
import com.blood.bloodbook.Data.DataManager;
import com.blood.bloodbook.Network.ViewModel.ViewModel;
import com.blood.bloodbook.R;
import com.blood.bloodbook.UI.Adapter.BloodQuantityDialogAdapter;
import com.blood.bloodbook.Utils.Permission;
import com.blood.bloodbook.Utils.Toast;
import com.blood.bloodbook.Widget.ProgressDialog;
import com.blood.bloodbook.databinding.BloodquantityitemBinding;
import com.blood.bloodbook.databinding.BloodquantitypickerdialogBinding;
import com.blood.bloodbook.databinding.DetailsofbloodrequestBinding;
import com.blood.bloodbook.databinding.LogoutdialogBinding;
import com.google.firebase.messaging.FirebaseMessaging;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class DetailsOFBloodRequest extends AppCompatActivity {

    private DetailsofbloodrequestBinding binding;
    private BloodRequestModel model;
    private static int CallPermission = 100;
    @Inject
    ProgressDialog progressDialog;
    private ViewModel viewModel;

    @Inject
    BloodQuantityDialogAdapter bloodQuantityDialogAdapter;
    private static final String TAG = "DetailsOFBloodRequest";
    private List<BloodQuantityModel> quantityModelList = new ArrayList<>();
    private FirebaseAuth Mauth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.detailsofbloodrequest);
        model = (BloodRequestModel) getIntent().getSerializableExtra(DataManager.Data);
        viewModel = new ViewModelProvider(this).get(ViewModel.class);
        Mauth = FirebaseAuth.getInstance();


        var component = Application.weenaComponent;
        component.InjectDetailsOFBloodRequest(this);

        for (int i = 100; i <= Integer.valueOf(model.getNumberOFQuantityPints()); i = i + 4) {
            var item = new BloodQuantityModel();
            item.setQuantity(String.valueOf(i));
            quantityModelList.add(item);
        }
        bloodQuantityDialogAdapter.setList(quantityModelList);
        bloodQuantityDialogAdapter.OnClickEvent(BloodQuantity -> {

        });

        InitView();
        SetData();
        AcceptBtn();

    }

    private void AcceptBtn() {
        var FirebaseUser = Mauth.getCurrentUser();
        if (FirebaseUser.getUid().equals(model.getSenderUID())) {
            binding.AcceptButton.setEnabled(false);
            binding.AcceptButton.setBackgroundResource(R.color.carbon_green_200);
        } else {
            binding.AcceptButton.setEnabled(true);
            binding.AcceptButton.setBackgroundResource(R.color.carbon_green_500);
        }
    }

    private void SetData() {
        binding.setBloodData(model);
    }

    private void InitView() {

        binding.ShapeBtn.setOnClickListener(view -> {
            if (Permission.PermissionCall(DetailsOFBloodRequest.this, CallPermission)) {
                var callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + model.getAttendantContactNumber()));
                startActivity(callIntent);
            }
        });
        binding.Toolbar.BackButton.setOnClickListener(view -> {
            finish();
            Animatoo.animateSlideRight(DetailsOFBloodRequest.this);
        });
        binding.Toolbar.BackButton.setOnClickListener(view -> {
            finish();
            Animatoo.animateSlideRight(DetailsOFBloodRequest.this);
        });

        binding.Toolbar.Title.setText(getResources().getString(R.string.DetailsOFBloodRequest));
        binding.CancelButton.setOnClickListener(view -> {
            finish();
            Animatoo.animateSlideRight(DetailsOFBloodRequest.this);
        });
        binding.AcceptButton.setOnClickListener(view -> {
            var BloodRequestID = System.currentTimeMillis();
            var dialog = new AlertDialog.Builder(DetailsOFBloodRequest.this);
            var binding = BloodquantitypickerdialogBinding.inflate(getLayoutInflater(), null, false);
            dialog.setView(binding.getRoot());

            var a = dialog.create();
            a.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            a.show();

            binding.SubmitBtn.setOnClickListener(view1 -> {
                var QuantityPint = binding.InputBloodQuantityCC.getText().toString().trim();
                if (QuantityPint.isEmpty()) {
                    Toast.Message(DetailsOFBloodRequest.this, getResources().getString(R.string.BloodQuantityPintCC));
                } else {
                    if (Integer.valueOf(QuantityPint) > Integer.valueOf(model.getNumberOFQuantityPints())) {
                        Toast.Message(DetailsOFBloodRequest.this, "Max blood quantity is " + model.getNumberOFQuantityPints());
                    } else {

                        a.dismiss();
                        progressDialog.ProgressDialog(DetailsOFBloodRequest.this);
                        var count = Integer.valueOf(model.getNumberOFQuantityPints()) - Integer.valueOf(QuantityPint);
                        viewModel.UpdateQuantity(String.valueOf(count), String.valueOf(model.getDocumentID())).observe(this, aBoolean -> {
                            if (aBoolean) {
                                viewModel.MyBloodRequest(model, String.valueOf(QuantityPint), model.getDocumentID(), String.valueOf(BloodRequestID)).observe(this, aBoolean1 -> {
                                    if (aBoolean1) {
                                        viewModel.SendBloodRequestNotification(model.getDocumentID(), String.valueOf(QuantityPint), DataManager.BloodRequestAccept, DataManager.BloodRequestAccept, model.getSenderUID(), FirebaseAuth.getInstance().getCurrentUser().getUid()).observe(DetailsOFBloodRequest.this, aBoolean2 -> {
                                            if (aBoolean2) {
                                                if (QuantityPint.equals(model.getNumberOFQuantityPints())) {
                                                    viewModel.DeleteMyBloodRequest(model.getDocumentID()).observe(DetailsOFBloodRequest.this, ab -> {
                                                        if (ab) {
                                                            SendNotificationToUser(model.getSenderUID(), QuantityPint);
                                                            progressDialog.CancelProgressDialog();
                                                        } else {
                                                            progressDialog.CancelProgressDialog();
                                                        }
                                                    });
                                                } else {
                                                    SendNotificationToUser(model.getSenderUID(), QuantityPint);
                                                    progressDialog.CancelProgressDialog();
                                                }


                                            } else {
                                                progressDialog.CancelProgressDialog();
                                                finish();
                                                Animatoo.animateSlideRight(DetailsOFBloodRequest.this);
                                            }
                                        });

                                    } else {
                                        progressDialog.CancelProgressDialog();
                                    }
                                });

                            } else {
                                progressDialog.CancelProgressDialog();
                            }
                        });
                    }
                }
            });

        });
    }

    @Override
    public void onBackPressed() {
        finish();
        Animatoo.animateSlideRight(DetailsOFBloodRequest.this);
    }

    private void SendNotificationToUser(String UID, String Count) {

        viewModel.GetSingleUser(UID).observe(this, profileModel -> {
            if (profileModel != null) {
                var data = new Data("Accept your " + Count + " CC blood request", "Accept your blood request", "Blood", "zdc");
                var response = new NotifactionResponse(data, profileModel.getToken());
                viewModel.SendNotification(response).observe(DetailsOFBloodRequest.this, aBoolean -> {
                    if (aBoolean) {
                        finish();
                        Animatoo.animateSlideRight(DetailsOFBloodRequest.this);
                        progressDialog.CancelProgressDialog();
                    } else {
                        progressDialog.CancelProgressDialog();
                    }
                });
            } else {

            }
        });
    }
}