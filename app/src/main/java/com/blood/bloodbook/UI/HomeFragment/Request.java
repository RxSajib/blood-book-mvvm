package com.blood.bloodbook.UI.HomeFragment;

import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentPagerAdapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.ads.AdRequest;
import com.blood.bloodbook.R;
import com.blood.bloodbook.UI.Adapter.RequestViewPagerAdapter;
import com.blood.bloodbook.UI.RegisterTab.BloodDonorFriendRegister;
import com.blood.bloodbook.UI.RegisterTab.RegisterDonation;
import com.blood.bloodbook.databinding.RequestBinding;

public class Request extends Fragment {

    private RequestBinding binding;

    public Request() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.request, container, false);

        SetFragment();
        LoadBannerAd();

        return binding.getRoot();
    }

    private void LoadBannerAd(){
        var adRequest = new AdRequest.Builder().build();
        binding.AdView.loadAd(adRequest);
    }

    private void SetFragment(){
        binding.TabLayout.setupWithViewPager(binding.ViewPager);
        var fadapter = new RequestViewPagerAdapter(getChildFragmentManager(), FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        fadapter.AddFragement(new RegisterDonation(), getResources().getString(R.string.Register));
        fadapter.AddFragement(new BloodDonorFriendRegister(), getResources().getString(R.string.RegisterFriend));
        binding.ViewPager.setAdapter(fadapter);
    }
}