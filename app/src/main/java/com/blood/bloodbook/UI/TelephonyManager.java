package com.blood.bloodbook.UI;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.blood.bloodbook.Application;
import com.blood.bloodbook.DI.DaggerWeenaComponent;
import com.blood.bloodbook.Network.ViewModel.LocationViewModel;
import com.blood.bloodbook.Network.ViewModel.ViewModel;
import com.blood.bloodbook.R;
import com.blood.bloodbook.UI.Adapter.DistrictNameDataAdapter;
import com.blood.bloodbook.UI.Adapter.LocationDataAdapter;
import com.blood.bloodbook.UI.Adapter.OrganizationTypeAdapter;
import com.blood.bloodbook.UI.Adapter.ProvinceNameDataAdapter;
import com.blood.bloodbook.UI.Adapter.TelephonyManagerAdapter;
import com.blood.bloodbook.Utils.FilterOptionDialog;
import com.blood.bloodbook.Utils.Permission;
import com.blood.bloodbook.Utils.Toast;
import com.blood.bloodbook.databinding.FilterdialogBinding;
import com.blood.bloodbook.databinding.LocationdialogBinding;
import com.blood.bloodbook.databinding.TelephonymanagerBinding;

import javax.inject.Inject;

public class TelephonyManager extends AppCompatActivity {

    private TelephonymanagerBinding binding;
    private ViewModel viewModel;
    @Inject
    TelephonyManagerAdapter telephonyManagerAdapter;
    @Inject
    FilterOptionDialog filterOptionDialog;
    @Inject
    LocationDataAdapter locationDataAdapter;
    @Inject
    ProvinceNameDataAdapter provinceNameDataAdapter;
    @Inject
    DistrictNameDataAdapter districtNameDataAdapter;
    @Inject
    OrganizationTypeAdapter organizationTypeAdapter;
    private LocationViewModel locationViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.telephonymanager);
        viewModel = new ViewModelProvider(this).get(ViewModel.class);

        locationViewModel = new ViewModelProvider(this).get(LocationViewModel.class);
        var component = Application.weenaComponent;
        component.InjectTelephonyManager(this);

        InitView();
        GetCountryName();
        GetProvincesName();
        GetDistrictName();
        GetOrganizationType();
        GetTelephonyManagerData();
        SearchByOrgName();
    }

    private void SearchByOrgName() {
        binding.SearchInput.setOnEditorActionListener((textView, i, keyEvent) -> {
            if (i == EditorInfo.IME_ACTION_SEARCH) {

                if (binding.SearchInput.getText().toString().trim().isEmpty()) {
                    Toast.Message(getApplicationContext(), "Search by organization name empty");
                } else {
                    SearchData(binding.SearchInput.getText().toString().trim());
                }

                return true;
            }
            return false;
        });
    }

    private void SearchData(String OrgName) {
        viewModel.SearchOrganization(OrgName).observe(this, organizationModels -> {
            telephonyManagerAdapter.setList(organizationModels);
            telephonyManagerAdapter.notifyDataSetChanged();
            if (organizationModels != null) {
                binding.Icon.setVisibility(View.GONE);
                binding.Message.setVisibility(View.GONE);
            } else {
                binding.Message.setVisibility(View.VISIBLE);
                binding.Icon.setVisibility(View.VISIBLE);
            }
        });
    }

    private void GetTelephonyManagerData() {
        viewModel.Getorganization().observe(this, organizationModels -> {
            telephonyManagerAdapter.setList(organizationModels);
            telephonyManagerAdapter.notifyDataSetChanged();
            if (organizationModels != null) {
                binding.Icon.setVisibility(View.GONE);
                binding.Message.setVisibility(View.GONE);
            } else {
                binding.Message.setVisibility(View.VISIBLE);
                binding.Icon.setVisibility(View.VISIBLE);
            }
        });
    }

    private void InitView() {
        filterOptionDialog.OnclickLisiner(Item -> {
            if (Item == 0) {
                GetTelephonyManagerData();
            }
            if (Item == 1) {
                var alertdialog = new AlertDialog.Builder(TelephonyManager.this);
                    FilterdialogBinding v = DataBindingUtil.inflate(getLayoutInflater(), R.layout.filterdialog, null, false);
                alertdialog.setView(v.getRoot());

                var dialog = alertdialog.create();
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();

                DialogView(dialog, v);
                v.ClearBtn.setOnClickListener(view -> {
                    dialog.dismiss();
                });
            }
        });

        binding.SearchInput.setOnTouchListener((view, motionEvent) -> {
            final int DRAWABLE_LEFT = 0;
            final int DRAWABLE_TOP = 1;
            final int DRAWABLE_RIGHT = 2;
            final int DRAWABLE_BOTTOM = 3;

            if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                if (motionEvent.getRawX() >= (binding.SearchInput.getRight() - binding.SearchInput.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                    filterOptionDialog.show(getSupportFragmentManager(), "filter");
                    return true;
                }
            }
            return false;
        });

        binding.RecyclerView.setHasFixedSize(true);
        binding.RecyclerView.setAdapter(telephonyManagerAdapter);

        binding.Toolbar.BackButton.setOnClickListener(view -> {
            finish();
            Animatoo.animateSlideRight(TelephonyManager.this);
        });
        binding.Toolbar.Title.setText(getResources().getString(R.string.TelephoneDirectory));


        telephonyManagerAdapter.OnCallLisiner(organizationModel -> {
            if (Permission.PermissionCall(TelephonyManager.this, 150)) {
                var intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse("tel:" + organizationModel.getPhoneNumber()));
                startActivity(intent);
            }
        });
    }

    private void DialogView(AlertDialog dialog, FilterdialogBinding v) {

        v.CountryInput.setOnClickListener(view -> {
            locationViewModel.DeleteAllCountryNameData();
            var mydialog = new android.app.AlertDialog.Builder(TelephonyManager.this);
            LocationdialogBinding locationitemBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.locationdialog, null, false);
            mydialog.setView(locationitemBinding.getRoot());

            var alertdialog = mydialog.create();
            alertdialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            locationitemBinding.LocationRecyclerView.setHasFixedSize(true);
            locationitemBinding.LocationRecyclerView.setAdapter(locationDataAdapter);

            alertdialog.show();

            viewModel.GetCountryName().observe(TelephonyManager.this, countryNameModels -> {
                locationViewModel.InsertCounyryNameData(countryNameModels);
                GetCountryName();

            });
            locationitemBinding.SearchInput.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    var CountryName = editable.toString();
                    if (CountryName.isEmpty()) {
                        GetCountryName();
                    } else {
                        GetCountryNameSearch(CountryName);
                    }
                }
            });

            locationDataAdapter.OnCountryLisiner(CountryName -> {
                v.Country.setText(CountryName);
                v.District.setText(null);
                v.Province.setText(null);
                alertdialog.dismiss();
            });
        });

        v.SelectProvince.setOnClickListener(view -> {
            var CountryName = v.Country.getText().toString().trim();
            if (CountryName == "") {
                Toast.Message(TelephonyManager.this, "Please select your country name first");
            } else {
                locationViewModel.DeleteAllProvinceNameData();
                var provincedialog = new android.app.AlertDialog.Builder(TelephonyManager.this);
                LocationdialogBinding locationitemBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.locationdialog, null, false);
                provincedialog.setView(locationitemBinding.getRoot());

                var alertdialog = provincedialog.create();
                locationitemBinding.LocationRecyclerView.setHasFixedSize(true);
                locationitemBinding.LocationRecyclerView.setAdapter(provinceNameDataAdapter);
                alertdialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                alertdialog.show();

                viewModel.GetProvinceName(CountryName).observe(TelephonyManager.this, provinceNameModels -> {
                    locationViewModel.InsertProvincesNameData(provinceNameModels);
                });

                provinceNameDataAdapter.OnClickLisiner(ProvinceName -> {
                    alertdialog.dismiss();
                    v.Province.setText(ProvinceName);
                    v.District.setText(null);
                });
                locationitemBinding.SearchInput.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        var Data = editable.toString();
                        if (Data.isEmpty()) {
                            GetProvincesName();
                        } else {
                            SearchProvinceName(Data);
                        }
                    }
                });
            }
        });

        v.DistrictInput.setOnClickListener(view -> {
            var CountryName = v.Country.getText().toString().trim();
            var ProvincesName = v.Province.getText().toString().trim();

            if (CountryName == "") {
                Toast.Message(TelephonyManager.this, "Select Country Name");
            } else if (ProvincesName == "") {
                Toast.Message(TelephonyManager.this, "Select Province Name");
            } else {
                locationViewModel.DeleteDistrct();
                var districtdialog = new android.app.AlertDialog.Builder(TelephonyManager.this);
                LocationdialogBinding locationitemBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.locationdialog, null, false);
                districtdialog.setView(locationitemBinding.getRoot());

                var alertdialog = districtdialog.create();
                locationitemBinding.LocationRecyclerView.setHasFixedSize(true);
                locationitemBinding.LocationRecyclerView.setAdapter(districtNameDataAdapter);
                alertdialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                alertdialog.show();


                viewModel.GetDistrictName(CountryName, ProvincesName).observe(TelephonyManager.this, districtNameModelList -> {
                    locationViewModel.InsertDistrict(districtNameModelList);
                });

                districtNameDataAdapter.OnClickLisiner(DictrictName -> {
                    alertdialog.dismiss();
                    v.District.setText(DictrictName);
                });

                locationitemBinding.SearchInput.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        var Data = editable.toString();
                        if (Data.isEmpty()) {
                            GetDistrictName();
                        } else {
                            SearchDustrictName(Data);
                        }
                    }
                });
            }
        });

        v.SelectOrgTypeInput.setOnClickListener(view -> {
            var districtdialog = new android.app.AlertDialog.Builder(TelephonyManager.this);
            LocationdialogBinding locationitemBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.locationdialog, null, false);
            districtdialog.setView(locationitemBinding.getRoot());

            var alertdialog = districtdialog.create();
            locationitemBinding.LocationRecyclerView.setHasFixedSize(true);
            locationitemBinding.LocationRecyclerView.setAdapter(organizationTypeAdapter);
            alertdialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            alertdialog.show();

            viewModel.GetOrganizationType().observe(this, organizationTypeModels -> {
                locationViewModel.InsertOrganizationTypeData(organizationTypeModels);
            });

            organizationTypeAdapter.OnClickListner(Name -> {
                v.OrgType.setText(Name);
                alertdialog.dismiss();
            });

            locationitemBinding.SearchInput.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    var s = editable.toString();
                    if (s.isEmpty()) {
                        GetOrganizationType();
                    } else {
                        SearchOrganizationType(s);
                    }
                }
            });
        });


        v.ApplyBtn.setOnClickListener(view -> {
            var Country = v.Country.getText().toString().trim();
            var Province = v.Province.getText().toString().trim();
            var District = v.District.getText().toString().trim();
            var OrgType = v.OrgType.getText().toString().trim();

            if (Country == "") {
                Toast.Message(getApplicationContext(), "Country empty");
            } else if (Province == "") {
                Toast.Message(getApplicationContext(), "Province empty");
            } else if (District == "") {
                Toast.Message(getApplicationContext(), "District empty");
            } else if (OrgType == "") {
                Toast.Message(getApplicationContext(), "Organization Type empty");
            } else {
                FilterTelephonyManager(Country, Province, District, OrgType);
                dialog.dismiss();
            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
        Animatoo.animateSlideRight(TelephonyManager.this);
    }


    private void GetCountryNameSearch(String CountryName) {
        locationViewModel.SearchByCountryName(CountryName).observe(TelephonyManager.this, countryNameModels -> {
            locationDataAdapter.setList(countryNameModels);
            locationDataAdapter.notifyDataSetChanged();
        });
    }

    private void GetCountryName() {
        locationViewModel.GetCountryName().observe(TelephonyManager.this, countryNameModels -> {
            locationDataAdapter.setList(countryNameModels);
            locationDataAdapter.notifyDataSetChanged();
        });
    }

    private void GetProvincesName() {
        locationViewModel.GetProvincesNameData().observe(TelephonyManager.this, provinceNameModelList -> {
            provinceNameDataAdapter.setList(provinceNameModelList);
            provinceNameDataAdapter.notifyDataSetChanged();
        });
    }

    private void SearchProvinceName(String Name) {
        locationViewModel.SearchProvincesName(Name).observe(TelephonyManager.this, provinceNameModelList -> {
            provinceNameDataAdapter.setList(provinceNameModelList);
            provinceNameDataAdapter.notifyDataSetChanged();
        });
    }

    private void GetDistrictName() {
        locationViewModel.GetDistrictName().observe(TelephonyManager.this, districtNameModelList -> {
            districtNameDataAdapter.setList(districtNameModelList);
            districtNameDataAdapter.notifyDataSetChanged();
        });
    }

    private void SearchDustrictName(String Name) {
        locationViewModel.SearchDistrictName(Name).observe(TelephonyManager.this, districtNameModelList -> {
            districtNameDataAdapter.setList(districtNameModelList);
            districtNameDataAdapter.notifyDataSetChanged();
        });
    }

    private void GetOrganizationType() {
        locationViewModel.GetOrganizationType().observe(this, organizationTypeModels -> {
            organizationTypeAdapter.setList(organizationTypeModels);
            organizationTypeAdapter.notifyDataSetChanged();
        });
    }

    private void SearchOrganizationType(String Name) {
        locationViewModel.SearchOrganizationType(Name).observe(this, organizationTypeModels -> {
            organizationTypeAdapter.setList(organizationTypeModels);
            organizationTypeAdapter.notifyDataSetChanged();
        });
    }


    private void FilterTelephonyManager(String CountryName, String Province, String District, String OrgType) {
        viewModel.FilterOrganizationData(CountryName, Province, District, OrgType).observe(this, organizationModels -> {
            telephonyManagerAdapter.setList(organizationModels);
            telephonyManagerAdapter.notifyDataSetChanged();
            if (organizationModels != null) {
                binding.Icon.setVisibility(View.GONE);
                binding.Message.setVisibility(View.GONE);
            } else {
                binding.Message.setVisibility(View.VISIBLE);
                binding.Icon.setVisibility(View.VISIBLE);
            }
        });
    }
}