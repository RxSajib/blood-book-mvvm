package com.blood.bloodbook.UI.ChatMessageViewHolder;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.imageview.ShapeableImageView;
import com.blood.bloodbook.R;

public class ReceiverImageMessageVH extends RecyclerView.ViewHolder {

    public ShapeableImageView receiverimage;
    public TextView receiverimagetime;

    public ReceiverImageMessageVH(@NonNull View itemView) {
        super(itemView);

        receiverimage = itemView.findViewById(R.id.ReceiverImage);
        receiverimagetime = itemView.findViewById(R.id.ReceiverImageTime);
    }
}
