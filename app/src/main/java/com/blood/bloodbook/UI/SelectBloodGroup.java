package com.blood.bloodbook.UI;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.GridLayoutManager;

import android.os.Bundle;
import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.blood.bloodbook.Application;
import com.blood.bloodbook.Data.Constant.BloodData;
import com.blood.bloodbook.Data.DataManager;
import com.blood.bloodbook.R;
import com.blood.bloodbook.UI.Adapter.BloodGroupAdapter;
import com.blood.bloodbook.Utils.HandleActivity;
import com.blood.bloodbook.Utils.SpacingItemDecorator;
import com.blood.bloodbook.databinding.SelectbloodgroupBinding;

import javax.inject.Inject;

public class SelectBloodGroup extends AppCompatActivity {

    private SelectbloodgroupBinding binding;
    private String Province;
    @Inject BloodGroupAdapter bloodGroupAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.selectbloodgroup);
        Province = getIntent().getStringExtra(DataManager.Data);

        var component = Application.weenaComponent;
        component.InjectSelectBloodGroup(this);

        InitView();
    }

    private void InitView(){
        binding.RecyclerView.setHasFixedSize(true);
        binding.RecyclerView.setLayoutManager(new GridLayoutManager(SelectBloodGroup.this, 2));
        bloodGroupAdapter.setList(BloodData.GetBlood(Province));
        binding.RecyclerView.setAdapter(bloodGroupAdapter);

        binding.RecyclerView.addItemDecoration(new SpacingItemDecorator(15,15,15,15));

        binding.Toolbar.BackButton.setOnClickListener(view -> {
            finish();
            Animatoo.animateSlideRight(SelectBloodGroup.this);
        });
        binding.Toolbar.Title.setText(getResources().getString(R.string.SelectBloodGroup));


        bloodGroupAdapter.OnClickState(Data -> {
            HandleActivity.GotoAfgBloodList(SelectBloodGroup.this, Province, Data);
            Animatoo.animateSlideLeft(SelectBloodGroup.this);
        });
    }

    @Override
    public void onBackPressed() {
        finish();
        Animatoo.animateSlideRight(SelectBloodGroup.this);
    }
}