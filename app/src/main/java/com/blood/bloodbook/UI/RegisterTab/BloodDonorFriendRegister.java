package com.blood.bloodbook.UI.RegisterTab;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.RadioButton;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.blood.bloodbook.Application;
import com.nex3z.togglebuttongroup.button.CircularToggle;
import com.blood.bloodbook.Data.DataManager;
import com.blood.bloodbook.Network.ViewModel.LocationViewModel;
import com.blood.bloodbook.Network.ViewModel.ViewModel;
import com.blood.bloodbook.R;
import com.blood.bloodbook.UI.Adapter.DistrictNameDataAdapter;
import com.blood.bloodbook.UI.Adapter.LocationDataAdapter;
import com.blood.bloodbook.UI.Adapter.ProvinceNameDataAdapter;
import com.blood.bloodbook.Utils.BetweenTwoDate;
import com.blood.bloodbook.Utils.DatePickerDialogFragment;
import com.blood.bloodbook.Utils.Toast;
import com.blood.bloodbook.Widget.ProgressDialog;
import com.blood.bloodbook.Widget.TermsAndConditionDialog;
import com.blood.bloodbook.databinding.BlooddonorfriendregisterBinding;
import com.blood.bloodbook.databinding.LocationdialogBinding;
import com.blood.bloodbook.databinding.RegisterasafriendBinding;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import javax.inject.Inject;

public class BloodDonorFriendRegister extends Fragment  implements DatePickerDialog.OnDateSetListener{

    private BlooddonorfriendregisterBinding binding;
    private String[] RegisterDonationDate = {"3 Months", "4 Months", "5 Months", "6 Months", "7 Months", "8 Months", "9 Months", "10 Months", "11 Months", "12 Months"};
    private String SelectRadioItem = null;
    @Inject TermsAndConditionDialog termsAndConditionDialog;
    @Inject
    ProgressDialog progressDialog;
    private String Month;
    private ViewModel viewModel;
    private int LastDonationTotalDay;

    @Inject LocationDataAdapter locationDataAdapter;
    @Inject ProvinceNameDataAdapter provinceNameDataAdapter;
    private LocationViewModel locationViewModel;
    @Inject DistrictNameDataAdapter districtNameDataAdapter;
    private String BloodGroup = null;
    private CircularToggle circularToggle;

    public BloodDonorFriendRegister() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.blooddonorfriendregister, container, false);
        termsAndConditionDialog = new TermsAndConditionDialog();
        var component = Application.weenaComponent;
        component.InjectBloodDonorFriendRegister(this);
        viewModel = new ViewModelProvider(this).get(ViewModel.class);

        locationViewModel = new ViewModelProvider(this).get(LocationViewModel.class);
        binding.BloodGroup.setOnCheckedChangeListener((group, checkedId) -> {
            circularToggle = container.findViewById(checkedId);
            BloodGroup = circularToggle.getText().toString();
        });

        InitView();
        UploadRegisterDonor(container);
        GetCountryName();
        GetProvincesName();
        GetDistrictName();

        return binding.getRoot();
    }



    private void DisableDate() {
        binding.LastDonationDateInput.setVisibility(View.GONE);
    }

    private void EnableDate() {
        binding.LastDonationDateInput.setVisibility(View.VISIBLE);
    }

    private void InitView() {


        binding.CountryInput.setOnClickListener(view -> {
            locationViewModel.DeleteAllCountryNameData();
            var dialog = new AlertDialog.Builder(getActivity());
            LocationdialogBinding locationitemBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.locationdialog, null, false);
            dialog.setView(locationitemBinding.getRoot());

            var alertdialog = dialog.create();
            alertdialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            locationitemBinding.LocationRecyclerView.setHasFixedSize(true);
            locationitemBinding.LocationRecyclerView.setAdapter(locationDataAdapter);

            alertdialog.show();
            viewModel.GetCountryName().observe(getActivity(), countryNameModels -> {
                locationViewModel.InsertCounyryNameData(countryNameModels);

            });
            locationitemBinding.SearchInput.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    var CountryName = editable.toString();
                    if (CountryName.isEmpty()) {
                        GetCountryName();
                    } else {
                        GetCountryNameSearch(CountryName);
                    }
                }
            });

            locationDataAdapter.OnCountryLisiner(CountryName -> {
                binding.Country.setText(CountryName);
                binding.District.setText(null);
                binding.Province.setText(null);
                alertdialog.dismiss();
            });
        });


        binding.ProvinceNameInput.setOnClickListener(view -> {
            var CountryName = binding.Country.getText().toString().trim();
            if (CountryName == "") {
                Toast.Message(getActivity(), "Please select your country name first");
            } else {
                locationViewModel.DeleteAllProvinceNameData();
                var dialog = new AlertDialog.Builder(getActivity());
                LocationdialogBinding locationitemBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.locationdialog, null, false);
                dialog.setView(locationitemBinding.getRoot());

                var alertdialog = dialog.create();
                locationitemBinding.LocationRecyclerView.setHasFixedSize(true);
                locationitemBinding.LocationRecyclerView.setAdapter(provinceNameDataAdapter);
                alertdialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                alertdialog.show();

                viewModel.GetProvinceName(CountryName).observe(getActivity(), provinceNameModels -> {
                    locationViewModel.InsertProvincesNameData(provinceNameModels);
                });

                provinceNameDataAdapter.OnClickLisiner(ProvinceName -> {
                    alertdialog.dismiss();
                    binding.Province.setText(ProvinceName);
                    binding.District.setText("");
                });
                locationitemBinding.SearchInput.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        var Data = editable.toString();
                        if (Data.isEmpty()) {
                            GetProvincesName();
                        } else {
                            SearchProvinceName(Data);
                        }
                    }
                });
            }
        });


        binding.DistrictNameInput.setOnClickListener(view -> {
            var CountryName = binding.Country.getText().toString().trim();
            var ProvincesName = binding.Province.getText().toString().trim();
            if (CountryName == "") {
                Toast.Message(getActivity(), "Select Country Name");
            } else if (ProvincesName == "") {
                Toast.Message(getActivity(), "Select Province Name");
            } else {
                locationViewModel.DeleteDistrct();
                var dialog = new AlertDialog.Builder(getActivity());
                LocationdialogBinding locationitemBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.locationdialog, null, false);
                dialog.setView(locationitemBinding.getRoot());

                var alertdialog = dialog.create();
                locationitemBinding.LocationRecyclerView.setHasFixedSize(true);
                locationitemBinding.LocationRecyclerView.setAdapter(districtNameDataAdapter);
                alertdialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                alertdialog.show();

                viewModel.GetDistrictName(CountryName, ProvincesName).observe(getActivity(), districtNameModelList -> {
                    locationViewModel.InsertDistrict(districtNameModelList);

                });

                districtNameDataAdapter.OnClickLisiner(DictrictName -> {
                    alertdialog.dismiss();
                    binding.District.setText(DictrictName);
                });

                locationitemBinding.SearchInput.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        var Data = editable.toString();
                        if (Data.isEmpty()) {
                            GetDistrictName();
                        } else {
                            SearchDustrictName(Data);
                        }
                    }
                });
            }
        });




        binding.Never.setChecked(true);
        DisableDate();
        binding.Never.setOnClickListener(view -> {
            DisableDate();
        });
        binding.SelectDate.setOnClickListener(view -> {
            EnableDate();
        });

        var AdapterRegisterDonate = new ArrayAdapter<String>(getActivity(), R.layout.spinnerlayout, R.id.SpinnerTitle, RegisterDonationDate);
        binding.HowManyMonthSpinner.setAdapter(AdapterRegisterDonate);
        binding.HowManyMonthSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                var Name = adapterView.getItemAtPosition(i).toString();
                Month = Name.replaceAll("[^0-9]", "");
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        binding.LastDonationDateInput.setKeyListener(null);


        binding.LastDonationDateInput.setOnClickListener(view -> {
            var dialog = new DatePickerDialogFragment();
            dialog.show(getActivity().getSupportFragmentManager(), "sdsaasd");
        });
    }


    //todo get all location name
    private void GetCountryNameSearch(String CountryName) {
        locationViewModel.SearchByCountryName(CountryName).observe(getActivity(), countryNameModels -> {
            locationDataAdapter.setList(countryNameModels);
            locationDataAdapter.notifyDataSetChanged();
        });
    }

    private void GetCountryName() {
        locationViewModel.GetCountryName().observe(getActivity(), countryNameModels -> {
            locationDataAdapter.setList(countryNameModels);
            locationDataAdapter.notifyDataSetChanged();
        });
    }

    private void GetProvincesName() {
        locationViewModel.GetProvincesNameData().observe(getActivity(), provinceNameModelList -> {
            provinceNameDataAdapter.setList(provinceNameModelList);
            provinceNameDataAdapter.notifyDataSetChanged();
        });
    }

    private void SearchProvinceName(String Name) {
        locationViewModel.SearchProvincesName(Name).observe(getActivity(), provinceNameModelList -> {
            provinceNameDataAdapter.setList(provinceNameModelList);
            provinceNameDataAdapter.notifyDataSetChanged();
        });
    }

    private void GetDistrictName() {
        locationViewModel.GetDistrictName().observe(getActivity(), districtNameModelList -> {
            districtNameDataAdapter.setList(districtNameModelList);
            districtNameDataAdapter.notifyDataSetChanged();
        });
    }

    private void SearchDustrictName(String Name) {
        locationViewModel.SearchDistrictName(Name).observe(getActivity(), districtNameModelList -> {
            districtNameDataAdapter.setList(districtNameModelList);
            districtNameDataAdapter.notifyDataSetChanged();
        });
    }
    //todo get all location name


    private void UploadRegisterDonor(ViewGroup container) {
        binding.CheckBox.setOnClickListener(view1 -> {
            if (binding.CheckBox.isChecked()) {
                termsAndConditionDialog.Show(getActivity());
            }
        });
        termsAndConditionDialog.OnClickLisiner(() -> {

        });
        binding.UploadBtn.setOnClickListener(view -> {

            var sdf = new SimpleDateFormat(DataManager.DatePattern);

            long timestamp = System.currentTimeMillis();
            var calender = Calendar.getInstance(Locale.ENGLISH);
            calender.setTimeInMillis(timestamp);
            var currentdate = DateFormat.format(DataManager.DatePattern, calender).toString();


            RadioButton DateRadioButton;
            var RadioButtonID = binding.RadioGroup.getCheckedRadioButtonId();
            if (RadioButtonID > 0) {
                DateRadioButton = container.findViewById(RadioButtonID);
                SelectRadioItem = DateRadioButton.getText().toString();
            }


            var GivenName = binding.GivenNamesInput.getText().toString().trim();
            var SureName = binding.SurnameText.getText().toString().trim();
            var Age = binding.AgeInput.getText().toString().trim();
            var Weight = binding.WeightInput.getText().toString().trim();
            var Country = binding.Country.getText().toString().trim();
            var Province = binding.Province.getText().toString().trim();
            var District = binding.District.getText().toString().trim();
            var AreaName = binding.AreaNameInput.getText().toString().trim();


            var MainPhoneNumber = binding.MainNumberInput.getText().toString().trim();
            var RoshanPhoneNumber = binding.RoshanNumberInput.getText().toString().trim();
            var EtisalatPhoneNumber = binding.EtisalatNumberInput.getText().toString().trim();
            var AWCCPhoneNumber = binding.AWCCNumberInput.getText().toString().trim();
            var MTNPhoneNumber = binding.MTNNumberInput.getText().toString().trim();
            var WhatsappNumber = binding.WhatsappNumberInput.getText().toString().trim();
            var EmailAddress = binding.EmailAddressInput.getText().toString().trim();

            if (GivenName.isEmpty()) {
                Toast.Message(getActivity(), "Given name is empty");
            } else if (SureName.isEmpty()) {
                Toast.Message(getActivity(), "Sure name is empty");
            } else if (BloodGroup == null) {
                Toast.Message(getActivity(), "Blood group is empty");
            } else if (Age.isEmpty()) {
                Toast.Message(getActivity(), "Age is empty");
            } else if (Weight.isEmpty()) {
                Toast.Message(getActivity(), "Weight is empty");
            } else if (Country == "") {
                Toast.Message(getActivity(), "Country is empty");
            } else if (Province == "") {
                Toast.Message(getActivity(), "Province is empty");
            } else if (District == "") {
                Toast.Message(getActivity(), "District is empty");
            } else if (AreaName.isEmpty()) {
                Toast.Message(getActivity(), "Area name is empty");
            }else if(MainPhoneNumber.isEmpty()){
                Toast.Message(getActivity(), getResources().getString(R.string.MainNumberEmpty));
            } else if (Integer.valueOf(Age) < 17) {
                Toast.Message(getActivity(), "Age must be at least 18 years");
            } else if (Integer.valueOf(Weight) < 54) {
                Toast.Message(getActivity(), "Weight must be at least 55");
            } else {
                //todo ?
                if (binding.CheckBox.isChecked()) {
                    if (SelectRadioItem.equals(getResources().getString(R.string.never))) {
                        UploadData(GivenName, SureName, null, BloodGroup, Age, Weight, Country, Province, District, AreaName,
                                MainPhoneNumber, RoshanPhoneNumber, EtisalatPhoneNumber, AWCCPhoneNumber, MTNPhoneNumber, WhatsappNumber,
                                EmailAddress, Month, null, false, null);
                    }
                    if (SelectRadioItem.equals(getResources().getString(R.string.select_a_date))) {
                        var LastDonationDate = binding.LastDonationDateInput.getText().toString().trim();
                        if (LastDonationDate.isEmpty()) {
                            Toast.Message(getActivity(), "Select your last donation date");
                        } else {
                            try {
                                var fromdate = sdf.parse(currentdate);
                                var lastdate = sdf.parse(LastDonationDate);
                                if (fromdate.compareTo(lastdate) < 0) {
                                    Toast.Message(getActivity(), "error your blood donation date is wrong");
                                } else if (fromdate.compareTo(lastdate) == 0) {
                                    Toast.Message(getActivity(), "error your blood donation date and today date was same");
                                } else {
                                    LastDonationTotalDay = BetweenTwoDate.daysBetweenDates(currentdate, LastDonationDate);
                                    UploadData(GivenName, SureName, null, BloodGroup, Age, Weight, Country, Province, District, AreaName,
                                            MainPhoneNumber, RoshanPhoneNumber, EtisalatPhoneNumber, AWCCPhoneNumber, MTNPhoneNumber, WhatsappNumber,
                                            EmailAddress, Month, LastDonationDate, false, String.valueOf(LastDonationTotalDay));
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                    }
                } else {
                    Toast.Message(getActivity(), "you must agree our terms and condition first");
                }

            }


        });
    }

    private void UploadData(String GivenName, String SureName, String FatherName, String BloodGroup, String Age, String Width,
                            String Country, String Province, String District, String AreaName, String MainPhoneNumber,
                            String RoshanNumber, String EtisalatNumber, String AWCCNumber, String MTNNumber, String WhatsappNumber,
                            String EmailAddress, String DonateMonth, String LastDonationDate, boolean EnableMyNumber, String LastDonationDayCount) {
        progressDialog.ProgressDialog(getActivity());
        viewModel.RegisterDonorNumberExists(MainPhoneNumber).observe(getActivity(), new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                if (!aBoolean) {
                    viewModel.RegisterBloodDonorFriend(DataManager.RegisterAsFriend, GivenName, SureName, FatherName, BloodGroup, Age, Width, Country, Province, District, AreaName, MainPhoneNumber,
                                    RoshanNumber, EtisalatNumber, AWCCNumber, MTNNumber, WhatsappNumber, EmailAddress, DonateMonth, LastDonationDate, EnableMyNumber, LastDonationDayCount)
                            .observe(getActivity(), new Observer<Boolean>() {
                                @Override
                                public void onChanged(Boolean aBoolean) {
                                    if (aBoolean) {
                                        progressDialog.CancelProgressDialog();
                                        Animatoo.animateSlideRight(getActivity());
                                    } else {
                                        progressDialog.CancelProgressDialog();
                                    }
                                }
                            });
                } else {
                    progressDialog.CancelProgressDialog();
                    Toast.Message(getActivity(), "Number is already exists");
                }
            }
        });
    }

    @Override
    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
        var format = new DecimalFormat("00");
        var day = format.format(Double.valueOf(i2));
        var month = format.format(Double.valueOf(i1));
        binding.LastDonationDateInput.setText(i + "-" + month + "-" + day);
    }


}