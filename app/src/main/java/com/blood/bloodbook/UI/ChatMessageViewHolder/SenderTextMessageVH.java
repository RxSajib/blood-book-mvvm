package com.blood.bloodbook.UI.ChatMessageViewHolder;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.blood.bloodbook.R;

public class SenderTextMessageVH extends RecyclerView.ViewHolder {

    public TextView Message;
    public TextView TimeDate;

    public SenderTextMessageVH(@NonNull View itemView) {
        super(itemView);

        Message = itemView.findViewById(R.id.SenderMessage);
        TimeDate = itemView.findViewById(R.id.SenderMessageTime);
    }
}
