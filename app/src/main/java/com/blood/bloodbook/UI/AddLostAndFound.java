package com.blood.bloodbook.UI;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.blood.bloodbook.Application;
import com.blood.bloodbook.Network.ViewModel.LocationViewModel;
import com.blood.bloodbook.Network.ViewModel.ViewModel;
import com.blood.bloodbook.R;
import com.blood.bloodbook.UI.Adapter.DistrictNameDataAdapter;
import com.blood.bloodbook.UI.Adapter.LocationDataAdapter;
import com.blood.bloodbook.UI.Adapter.LostAndFoundTypeAdapter;
import com.blood.bloodbook.UI.Adapter.ProvinceNameDataAdapter;
import com.blood.bloodbook.Utils.Permission;
import com.blood.bloodbook.Utils.Toast;
import com.blood.bloodbook.Widget.ProgressDialog;
import com.blood.bloodbook.databinding.AddlostandfoundBinding;
import com.blood.bloodbook.databinding.LocationdialogBinding;
import com.blood.bloodbook.databinding.OrgnaizationtypedialogBinding;
import com.blood.bloodbook.databinding.OrgtypecategorydialogBinding;

import javax.inject.Inject;

public class AddLostAndFound extends AppCompatActivity {

    private AddlostandfoundBinding binding;
    @Inject ProgressDialog progressDialog;
    private ViewModel viewModel;
    private LocationViewModel locationViewModel;
    @Inject
    LocationDataAdapter locationDataAdapter;
    @Inject LostAndFoundTypeAdapter lostAndFoundTypeAdapter;
    @Inject ProvinceNameDataAdapter provinceNameDataAdapter;
    @Inject DistrictNameDataAdapter districtNameDataAdapter;
    private String ImageDownloadUri = null;
    private ActivityResultLauncher<Intent> launcher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.addlostandfound);
        viewModel = new ViewModelProvider(this).get(ViewModel.class);
        locationViewModel = new ViewModelProvider(this).get(LocationViewModel.class);
        var component = Application.weenaComponent;
        component.InjectAddLostAndFound(this);


        InitView();
        LocationDialog();
        GetCountryName();
        GetProvincesName();
        GetDistrictName();
        UploadData();
        SendImageToServer();
    }

    private void SendImageToServer(){
        launcher = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), result -> {
            progressDialog.ProgressDialog(AddLostAndFound.this);
           if(result.getResultCode() == RESULT_OK){
               binding.LogoUpload.setImageURI(result.getData().getData());
               viewModel.UploadLostAndFoundImage(result.getData().getData()).observe(this, s -> {
                   ImageDownloadUri = s;
                   progressDialog.CancelProgressDialog();
               });
           }else {
               progressDialog.CancelProgressDialog();
           }
        });
    }
    private void UploadData() {
        binding.UploadButton.setOnClickListener(view -> {
            var Title = binding.PostTitleInput.getText().toString().trim();
            var LostFound = binding.LostAndFound.getText().toString().trim();
            var TelephoneNumbers = binding.TelephoneNumbersInput.getText().toString().trim();
            var Country = binding.Country.getText().toString().trim();
            var Province = binding.Province.getText().toString().trim();
            var District = binding.District.getText().toString().trim();
            var FullDetails = binding.FullDetailsInput.getText().toString().trim();


            if (Title.isEmpty()) {
                Toast.Message(getApplicationContext(), getResources().getString(R.string.PostTitleEmpty));
            } else if (LostFound == "") {
                Toast.Message(getApplicationContext(), getResources().getString(R.string.LostFoundEmpty));
            } else if (TelephoneNumbers.isEmpty()) {
                Toast.Message(AddLostAndFound.this, getResources().getString(R.string.TelephoneNumbersEmpty));
            } else if (Country == "") {
                Toast.Message(AddLostAndFound.this, getResources().getString(R.string.CountryEmpty));
            } else if (Province == "") {
                Toast.Message(AddLostAndFound.this, getResources().getString(R.string.ProvinceEmpty));
            } else if (District == "") {
                Toast.Message(AddLostAndFound.this, getResources().getString(R.string.DistrictEmpty));
            } else if (FullDetails.isEmpty()) {
                Toast.Message(AddLostAndFound.this, getResources().getString(R.string.FullDetailsEmpty));
            } else {

                progressDialog.ProgressDialog(AddLostAndFound.this);
                viewModel.LostAndFoundDataPost(Title, LostFound, TelephoneNumbers, Country, Province, District, FullDetails, ImageDownloadUri).observe(this, aBoolean -> {
                    if (aBoolean) {
                        progressDialog.CancelProgressDialog();
                        finish();
                        Animatoo.animateSlideRight(AddLostAndFound.this);
                    } else {
                        progressDialog.CancelProgressDialog();
                    }
                });
            }
        });
    }

    private void LocationDialog() {
        binding.CountryName.setOnClickListener(view -> {
            locationViewModel.DeleteAllCountryNameData();
            var dialog = new AlertDialog.Builder(AddLostAndFound.this);
            LocationdialogBinding locationitemBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.locationdialog, null, false);
            dialog.setView(locationitemBinding.getRoot());

            var alertdialog = dialog.create();
            alertdialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            locationitemBinding.LocationRecyclerView.setHasFixedSize(true);
            locationitemBinding.LocationRecyclerView.setAdapter(locationDataAdapter);
            alertdialog.show();

            viewModel.GetCountryName().observe(this, countryNameModels -> {
                if(countryNameModels != null){
                    locationViewModel.InsertCounyryNameData(countryNameModels);
                }
            });

            locationitemBinding.SearchInput.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    var CountryName = editable.toString();
                    if (CountryName.isEmpty()) {
                        GetCountryName();
                    } else {
                        GetCountryNameSearch(CountryName);
                    }
                }
            });
            locationDataAdapter.OnCountryLisiner(CountryName -> {
                binding.Country.setText(CountryName);
                alertdialog.dismiss();
                binding.District.setText(null);
                binding.Province.setText(null);
            });

        });

        binding.ProvinceStateInput.setOnClickListener(view -> {
            var CountryName = binding.Country.getText().toString().trim();
            if (CountryName == "") {
                Toast.Message(getApplicationContext(), "Country name empty");
            } else {
                locationViewModel.DeleteAllProvinceNameData();
                var dialog = new AlertDialog.Builder(AddLostAndFound.this);
                LocationdialogBinding locationitemBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.locationdialog, null, false);
                dialog.setView(locationitemBinding.getRoot());

                var alertdialog = dialog.create();
                alertdialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                locationitemBinding.LocationRecyclerView.setHasFixedSize(true);
                locationitemBinding.LocationRecyclerView.setAdapter(provinceNameDataAdapter);
                alertdialog.show();

                viewModel.GetProvinceName(CountryName).observe(this, provinceNameModels -> {
                    if(provinceNameModels != null){
                        locationViewModel.InsertProvincesNameData(provinceNameModels);
                    }
                });
                provinceNameDataAdapter.OnClickLisiner(ProvinceName -> {
                    binding.Province.setText(ProvinceName);
                    alertdialog.dismiss();
                    binding.District.setText(null);
                });
                locationitemBinding.SearchInput.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        var Data = editable.toString();
                        if (Data.isEmpty()) {
                            GetProvincesName();
                        } else {
                            SearchProvinceName(Data);
                        }
                    }
                });
            }
        });

        binding.SelectDistrictInput.setOnClickListener(view -> {
            var CountryName = binding.Country.getText().toString().trim();
            var ProvincesName = binding.Province.getText().toString().trim();
            if (CountryName.isEmpty()) {
                Toast.Message(AddLostAndFound.this, getResources().getString(R.string.CountryEmpty));
            } else if (ProvincesName.isEmpty()) {
                Toast.Message(AddLostAndFound.this, getResources().getString(R.string.ProvinceEmpty));
            } else {
                locationViewModel.DeleteDistrct();
                var dialog = new AlertDialog.Builder(AddLostAndFound.this);
                LocationdialogBinding locationitemBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.locationdialog, null, false);
                dialog.setView(locationitemBinding.getRoot());

                var alertdialog = dialog.create();
                alertdialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                locationitemBinding.LocationRecyclerView.setHasFixedSize(true);
                locationitemBinding.LocationRecyclerView.setAdapter(districtNameDataAdapter);
                alertdialog.show();

                viewModel.GetDistrictName(CountryName, ProvincesName).observe(this, districtNameModels -> {
                    if(districtNameModels != null){
                        locationViewModel.InsertDistrict(districtNameModels);
                    }

                });
                districtNameDataAdapter.OnClickLisiner(DictrictName -> {
                    binding.District.setText(DictrictName);
                    alertdialog.dismiss();
                });
                locationitemBinding.SearchInput.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        var Data = editable.toString();
                        if (Data.isEmpty()) {
                            GetDistrictName();
                        } else {
                            SearchDustrictName(Data);
                        }
                    }
                });

            }
        });
    }

    private void InitView() {
        binding.LogoUpload.setOnClickListener(view -> {
            if(Permission.PermissionExternalStorage(AddLostAndFound.this, 12)){
                var intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                launcher.launch(intent);
            }
        });

        binding.Toolbar.Title.setText(getResources().getString(R.string.PostanAd));
        binding.Toolbar.BackButton.setOnClickListener(view -> {
            finish();
            Animatoo.animateSlideRight(AddLostAndFound.this);
        });

        binding.OrgTypeInput.setOnClickListener(view -> {
            var alertdialog = new AlertDialog.Builder(AddLostAndFound.this);
            OrgnaizationtypedialogBinding dialogbinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.orgnaizationtypedialog, null, false);
            alertdialog.setView(dialogbinding.getRoot());
            dialogbinding.RecyclerView.setHasFixedSize(true);
            dialogbinding.RecyclerView.setAdapter(lostAndFoundTypeAdapter);

            var dialog = alertdialog.create();
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.show();

            viewModel.GetLostAndFoundType().observe(this, lostAndFoundTypeModels -> {
                lostAndFoundTypeAdapter.setList(lostAndFoundTypeModels);
                lostAndFoundTypeAdapter.notifyDataSetChanged();
            });

            lostAndFoundTypeAdapter.OnClickState(Category -> {
                binding.LostAndFound.setText(Category);
                dialog.dismiss();
            });

            lostAndFoundTypeAdapter.OnDeleteState(DocumentID -> {
                progressDialog.ProgressDialog(AddLostAndFound.this);
                viewModel.DeleteLostAndFound(DocumentID).observe(this, aBoolean -> progressDialog.CancelProgressDialog());
            });
        });

        binding.AddOrgTypeBtn.setOnClickListener(view -> {
            var alertdialog = new AlertDialog.Builder(AddLostAndFound.this);
            OrgtypecategorydialogBinding v = DataBindingUtil.inflate(getLayoutInflater(), R.layout.orgtypecategorydialog, null, false);
            alertdialog.setView(v.getRoot());

            var d = alertdialog.create();
            d.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            d.show();

            v.AddBtn.setOnClickListener(view1 -> {
                var Title = v.OrgTypeInput.getText().toString().trim();
                if (Title.isEmpty()) {
                    Toast.Message(AddLostAndFound.this, getResources().getString(R.string.TitleEmpty));
                } else {
                    progressDialog.ProgressDialog(AddLostAndFound.this);
                    viewModel.UploadLostAndFoundCategory(Title).observe(this, aBoolean -> {
                        d.dismiss();
                        progressDialog.CancelProgressDialog();
                    });
                    v.OrgTypeInput.setText(null);
                }
            });
        });
    }

    @Override
    public void onBackPressed() {
        finish();
        Animatoo.animateSlideRight(AddLostAndFound.this);
    }


    private void GetCountryNameSearch(String CountryName) {
        locationViewModel.SearchByCountryName(CountryName).observe(AddLostAndFound.this, countryNameModels -> {
            locationDataAdapter.setList(countryNameModels);
            locationDataAdapter.notifyDataSetChanged();
        });
    }

    private void GetCountryName() {
        locationViewModel.GetCountryName().observe(AddLostAndFound.this, countryNameModels -> {
            locationDataAdapter.setList(countryNameModels);
            locationDataAdapter.notifyDataSetChanged();

        });
    }

    private void GetProvincesName() {
        locationViewModel.GetProvincesNameData().observe(AddLostAndFound.this, provinceNameModelList -> {
            provinceNameDataAdapter.setList(provinceNameModelList);
            provinceNameDataAdapter.notifyDataSetChanged();
        });
    }

    private void SearchProvinceName(String Name) {
        locationViewModel.SearchProvincesName(Name).observe(AddLostAndFound.this, provinceNameModelList -> {
            provinceNameDataAdapter.setList(provinceNameModelList);
            provinceNameDataAdapter.notifyDataSetChanged();
        });
    }

    private void GetDistrictName() {
        locationViewModel.GetDistrictName().observe(AddLostAndFound.this, districtNameModelList -> {
            districtNameDataAdapter.setList(districtNameModelList);
            districtNameDataAdapter.notifyDataSetChanged();
        });
    }

    private void SearchDustrictName(String Name) {
        locationViewModel.SearchDistrictName(Name).observe(AddLostAndFound.this, districtNameModelList -> {
            districtNameDataAdapter.setList(districtNameModelList);
            districtNameDataAdapter.notifyDataSetChanged();
        });
    }
}