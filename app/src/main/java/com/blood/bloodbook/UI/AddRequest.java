package com.blood.bloodbook.UI;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.RadioButton;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.blood.bloodbook.Application;
import com.blood.bloodbook.DI.DaggerWeenaComponent;
import com.blood.bloodbook.Data.DataManager;
import com.blood.bloodbook.Network.ViewModel.LocationViewModel;
import com.blood.bloodbook.Network.ViewModel.ViewModel;
import com.blood.bloodbook.R;
import com.blood.bloodbook.UI.Adapter.DistrictNameDataAdapter;
import com.blood.bloodbook.UI.Adapter.LocationDataAdapter;
import com.blood.bloodbook.UI.Adapter.ProvinceNameDataAdapter;
import com.blood.bloodbook.Utils.DatePickerDialogFragment;
import com.blood.bloodbook.Utils.Toast;
import com.blood.bloodbook.Widget.ProgressDialog;
import com.blood.bloodbook.databinding.AddrequestBinding;
import com.blood.bloodbook.databinding.LocationdialogBinding;
import com.nex3z.togglebuttongroup.button.CircularToggle;

import java.util.Calendar;
import java.util.Locale;

import javax.inject.Inject;

public class AddRequest extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {

    private AddrequestBinding binding;
    private int hoursofday, minit;
    private String[] BloodNeedForData = {"Thalassemia Patient", "Bleeding", "Surgery", "Medical Procedure", "Cancer Patient"};
    private String[] PatientsConditionSpinnerData = {"Very Much Serious", "Serious", "Not too much Serious"};
    private String[] BloodConditionSpinnerData = {"Fresh", "Reserved/Refrigerated"};
    private String BloodGroup;
    private String BloodCondition;
    private String BloodNeedFor;
    private String TransportAvailability;
    private String ExchangePossibility;
    private String PatientsCondition;
    private ViewModel viewModel;
    @Inject LocationDataAdapter locationDataAdapter;
    @Inject ProvinceNameDataAdapter provinceNameDataAdapter;
    @Inject DistrictNameDataAdapter districtNameDataAdapter;
    private AlertDialog countryalertDialog;
    private LocationViewModel locationViewModel;

    @Inject
    ProgressDialog progressDialog;
    private CircularToggle circularToggle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.addrequest);
        viewModel = new ViewModelProvider(this).get(ViewModel.class);

        locationViewModel = new ViewModelProvider(this).get(LocationViewModel.class);
        var component = Application.weenaComponent;
        component.InjectAddRequest(this);


        GetCountryName();
        GetProvincesName();
        GetDistrictName();
        InitView();
        Dialog();
        LisenBloodGroup();
    }

    private void LisenBloodGroup() {
        binding.BloodGroup.setOnCheckedChangeListener((group, checkedId) -> {
            circularToggle = findViewById(checkedId);
            BloodGroup = circularToggle.getText().toString();
        });
    }

    private void Dialog() {

        locationDataAdapter.OnCountryLisiner(CountryName -> {
            countryalertDialog.dismiss();
        });


        provinceNameDataAdapter.OnClickLisiner(ProvinceName -> {
            countryalertDialog.dismiss();
        });

    }

    private void InitView() {

        binding.CountryInput.setOnClickListener(view -> {
            locationViewModel.DeleteAllCountryNameData();
            var dialog = new android.app.AlertDialog.Builder(AddRequest.this);
            LocationdialogBinding locationitemBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.locationdialog, null, false);
            dialog.setView(locationitemBinding.getRoot());

            var alertdialog = dialog.create();
            alertdialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            locationitemBinding.LocationRecyclerView.setHasFixedSize(true);
            locationitemBinding.LocationRecyclerView.setAdapter(locationDataAdapter);

            alertdialog.show();
            viewModel.GetCountryName().observe(AddRequest.this, countryNameModels -> {
                locationViewModel.InsertCounyryNameData(countryNameModels);

            });
            locationitemBinding.SearchInput.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    var CountryName = editable.toString();
                    if (CountryName.isEmpty()) {
                        GetCountryName();
                    } else {
                        GetCountryNameSearch(CountryName);
                    }
                }
            });

            locationDataAdapter.OnCountryLisiner(CountryName -> {
                binding.Country.setText(CountryName);
                binding.District.setText(null);
                binding.Province.setText(null);
                alertdialog.dismiss();
            });
        });


        binding.ProvinceNameInput.setOnClickListener(view -> {
            var CountryName = binding.Country.getText().toString().trim();
            if (CountryName == "") {
                Toast.Message(this, getResources().getString(R.string.PleaseSelectYourCountryNameFirst));
            } else {
                locationViewModel.DeleteAllProvinceNameData();
                var dialog = new android.app.AlertDialog.Builder(this);
                LocationdialogBinding locationitemBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.locationdialog, null, false);
                dialog.setView(locationitemBinding.getRoot());

                var alertdialog = dialog.create();
                locationitemBinding.LocationRecyclerView.setHasFixedSize(true);
                locationitemBinding.LocationRecyclerView.setAdapter(provinceNameDataAdapter);
                alertdialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                alertdialog.show();

                viewModel.GetProvinceName(CountryName).observe(this, provinceNameModels -> {
                    locationViewModel.InsertProvincesNameData(provinceNameModels);
                });

                provinceNameDataAdapter.OnClickLisiner(ProvinceName -> {
                    alertdialog.dismiss();
                    binding.Province.setText(ProvinceName);
                    binding.District.setText("");
                });
                locationitemBinding.SearchInput.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        var Data = editable.toString();
                        if (Data.isEmpty()) {
                            GetProvincesName();
                        } else {
                            SearchProvinceName(Data);
                        }
                    }
                });
            }
        });


        binding.DistrictNameInput.setOnClickListener(view -> {
            var CountryName = binding.Country.getText().toString().trim();
            var ProvincesName = binding.Province.getText().toString().trim();
            if (CountryName == "") {
                Toast.Message(this, getResources().getString(R.string.SelectCountryName));
            } else if (ProvincesName == "") {
                Toast.Message(this, getResources().getString(R.string.SelectProvinceName));
            } else {
                locationViewModel.DeleteDistrct();
                var dialog = new android.app.AlertDialog.Builder(this);
                LocationdialogBinding locationitemBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.locationdialog, null, false);
                dialog.setView(locationitemBinding.getRoot());

                var alertdialog = dialog.create();
                locationitemBinding.LocationRecyclerView.setHasFixedSize(true);
                locationitemBinding.LocationRecyclerView.setAdapter(districtNameDataAdapter);
                alertdialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                alertdialog.show();


                viewModel.GetDistrictName(CountryName, ProvincesName).observe(this, districtNameModelList -> {
                    locationViewModel.InsertDistrict(districtNameModelList);
                });

                districtNameDataAdapter.OnClickLisiner(DictrictName -> {
                    alertdialog.dismiss();
                    binding.District.setText(DictrictName);
                });

                locationitemBinding.SearchInput.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        var Data = editable.toString();
                        if (Data.isEmpty()) {
                            GetDistrictName();
                        } else {
                            SearchDustrictName(Data);
                        }
                    }
                });
            }
        });


        var BloodConditionSpinner = new ArrayAdapter<String>(getApplicationContext(), R.layout.spinnerlayout, R.id.SpinnerTitle, BloodConditionSpinnerData);
        binding.BloodConditionSpinner.setAdapter(BloodConditionSpinner);

        var BloodNeedForSpinner = new ArrayAdapter<String>(getApplicationContext(), R.layout.spinnerlayout, R.id.SpinnerTitle, BloodNeedForData);
        binding.BloodNeedForTextSpinner.setAdapter(BloodNeedForSpinner);

        var PatientsConditionSpinner = new ArrayAdapter<String>(getApplicationContext(), R.layout.spinnerlayout, R.id.SpinnerTitle, PatientsConditionSpinnerData);
        binding.TypePatientConditionSpinner.setAdapter(PatientsConditionSpinner);


        binding.TypePatientConditionSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                PatientsCondition = adapterView.getItemAtPosition(i).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        binding.BloodNeedForTextSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                BloodNeedFor = adapterView.getItemAtPosition(i).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        binding.BloodConditionSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                BloodCondition = adapterView.getItemAtPosition(i).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        binding.DateLimit.setKeyListener(null);
        binding.TimeLimit.setKeyListener(null);
        // binding.CountryInput.setKeyListener(null);
        // binding.ProvinceNameInput.setKeyListener(null);
        // binding.DistrictNameInput.setKeyListener(null);

        binding.Toolbar.BackButton.setOnClickListener(view -> {
            finish();
            Animatoo.animateSlideRight(AddRequest.this);
        });
        binding.Toolbar.Title.setText("Add Blood Request");

        binding.DateLimit.setOnClickListener(view -> {
            var dialog = new DatePickerDialogFragment();
            dialog.show(getSupportFragmentManager(), "sdsaasd");
        });

        binding.TimeLimit.setOnClickListener(view -> {
            var TimePickerDialog = new TimePickerDialog(
                    AddRequest.this,
                    android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                    (timePicker, i, i1) -> {
                        hoursofday = i;
                        minit = i1;

                        var calender = Calendar.getInstance(Locale.ENGLISH);
                        binding.TimeLimit.setText(DateFormat.format(DataManager.TimetoAM_PMpattern, calender));
                    }, 12, 0, false


            );
            TimePickerDialog.updateTime(hoursofday, minit);
            TimePickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            TimePickerDialog.show();
        });


        binding.BloodGroup.setOnCheckedChangeListener((group, checkedId) -> {
            // var name = group.getCheckedId();
            // Toast.Message(AddRequest.this, group.getCheckedId().);
        });

        binding.RequestBtn.setOnClickListener(view -> {
            RadioButton ExchangepossibilityRadioBtn;
            var ExchangepossibilityRadioBtnID = binding.ExchangepossibilityRadioGroup.getCheckedRadioButtonId();
            if (ExchangepossibilityRadioBtnID > 0) {
                ExchangepossibilityRadioBtn = findViewById(ExchangepossibilityRadioBtnID);
                ExchangePossibility = ExchangepossibilityRadioBtn.getText().toString();
            }

            RadioButton TransportAvailabilityRadio;
            var TransportAvailabilityBtnID = binding.TransportAvailabilityRadioGroup.getCheckedRadioButtonId();
            if (TransportAvailabilityBtnID > 0) {
                TransportAvailabilityRadio = findViewById(TransportAvailabilityBtnID);
                TransportAvailability = TransportAvailabilityRadio.getText().toString().trim();
            }


            var PatientName = binding.PatientNameInput.getText().toString().trim();
            var Age = binding.AgeInput.getText().toString().trim();
            var Country = binding.Country.getText().toString().trim();
            var Province = binding.Province.getText().toString().trim();
            var District = binding.District.getText().toString().trim();
            var Time = binding.TimeLimit.getText().toString().trim();
            var Date = binding.DateLimit.getText().toString().trim();
            var AttendantContactNumber = binding.AttendantContactNoInput.getText().toString().trim();
            var HospitalName = binding.HospitalNameInput.getText().toString().trim();
            var TypeOfHbLevel = binding.TypeOFHBLevelInput.getText().toString().trim();
            var NumberOfQuantityPints = binding.BloodQuantityInput.getText().toString().trim();

            // Toast.Message(getApplicationContext(), BloodCondition);

            if (PatientName.isEmpty()) {
                Toast.Message(AddRequest.this, "Patient Name required");
            } else if (Age.isEmpty()) {
                Toast.Message(AddRequest.this, "Age empty");
            } else if (BloodGroup == null) {
                Toast.Message(AddRequest.this, "Blood group empty");
            } else if (Time.isEmpty()) {
                Toast.Message(AddRequest.this, "Time required");
            } else if (Country == "") {
                Toast.Message(this, "Country is empty");
            } else if (Province == "") {
                Toast.Message(this, "Province is empty");
            } else if (District == "") {
                Toast.Message(this, "District is empty");
            } else if (Date.isEmpty()) {
                Toast.Message(AddRequest.this, "Date required");
            } else if (AttendantContactNumber.isEmpty()) {
                Toast.Message(AddRequest.this, "Attendant Contact Number required");
            } else if (HospitalName.isEmpty()) {
                Toast.Message(AddRequest.this, "Hospital Name required");
            } else if (NumberOfQuantityPints.isEmpty()) {
                Toast.Message(AddRequest.this, "Number of Quantity required");
            } else {
                progressDialog.ProgressDialog(AddRequest.this);
                viewModel.BloodRequest(PatientName, Age, BloodGroup, BloodCondition, Country, Province, District, BloodNeedFor, NumberOfQuantityPints, Time, Date, AttendantContactNumber,
                        HospitalName, ExchangePossibility, TransportAvailability, TypeOfHbLevel, PatientsCondition).observe(this, aBoolean -> {
                    if (aBoolean) {
                        progressDialog.CancelProgressDialog();
                        Animatoo.animateSlideRight(AddRequest.this);
                        finish();
                    } else {
                        progressDialog.CancelProgressDialog();
                    }
                });
            }

        });
    }

    @Override
    public void onBackPressed() {
        finish();
        Animatoo.animateSlideRight(AddRequest.this);
    }


    @Override
    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
        binding.DateLimit.setText(i + "-" + i1 + "-" + i2);
    }


    private void GetCountryNameSearch(String CountryName) {
        locationViewModel.SearchByCountryName(CountryName).observe(this, countryNameModels -> {
            locationDataAdapter.setList(countryNameModels);
            locationDataAdapter.notifyDataSetChanged();
        });
    }

    private void GetCountryName() {
        locationViewModel.GetCountryName().observe(this, countryNameModels -> {
            locationDataAdapter.setList(countryNameModels);
            locationDataAdapter.notifyDataSetChanged();
        });
    }

    private void GetProvincesName() {
        locationViewModel.GetProvincesNameData().observe(this, provinceNameModelList -> {
            provinceNameDataAdapter.setList(provinceNameModelList);
            provinceNameDataAdapter.notifyDataSetChanged();
        });
    }

    private void SearchProvinceName(String Name) {
        locationViewModel.SearchProvincesName(Name).observe(this, provinceNameModelList -> {
            provinceNameDataAdapter.setList(provinceNameModelList);
            provinceNameDataAdapter.notifyDataSetChanged();
        });
    }

    private void GetDistrictName() {
        locationViewModel.GetDistrictName().observe(this, districtNameModelList -> {
            districtNameDataAdapter.setList(districtNameModelList);
            districtNameDataAdapter.notifyDataSetChanged();
        });
    }

    private void SearchDustrictName(String Name) {
        locationViewModel.SearchDistrictName(Name).observe(this, districtNameModelList -> {
            districtNameDataAdapter.setList(districtNameModelList);
            districtNameDataAdapter.notifyDataSetChanged();
        });
    }
}