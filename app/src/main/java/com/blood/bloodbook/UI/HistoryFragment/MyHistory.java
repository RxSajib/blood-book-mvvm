package com.blood.bloodbook.UI.HistoryFragment;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.blood.bloodbook.Application;
import com.blood.bloodbook.Network.ViewModel.ViewModel;
import com.blood.bloodbook.R;
import com.blood.bloodbook.UI.Adapter.HistoryAdapter;
import com.blood.bloodbook.UI.Adapter.MyHistoryAdapter;
import com.blood.bloodbook.databinding.MyhistoryBinding;

import javax.inject.Inject;

public class MyHistory extends Fragment {

    private MyhistoryBinding binding;
    private ViewModel viewModel;
    @Inject
    MyHistoryAdapter myHistoryAdapter;
    private int Limit = 25;

    public MyHistory() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.myhistory, container, false);
        viewModel = new ViewModelProvider(this).get(ViewModel.class);

        var component = Application.weenaComponent;
        component.InjectMyHistory(this);
        InitView();

        binding.SwipeRefreshLayout.setOnRefreshListener(() -> {
            GetDataFromServer(Limit);
        });

        binding.RecyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if(!recyclerView.canScrollVertically(1)){
                    Limit = Limit+25;
                    GetDataFromServer(Limit);
                }
            }
        });

        GetDataFromServer(Limit);
        return binding.getRoot();
    }

    private void InitView(){
        binding.RecyclerView.setHasFixedSize(true);
        binding.RecyclerView.setAdapter(myHistoryAdapter);
    }
    private void GetDataFromServer(int Limit){
        viewModel.MyMyRegisterDonation(Limit).observe(getActivity(), historyModels -> {
            myHistoryAdapter.setList(historyModels);
            myHistoryAdapter.notifyDataSetChanged();
            binding.SwipeRefreshLayout.setRefreshing(false);

            if(historyModels != null){
                binding.Icon.setVisibility(View.GONE);
                binding.Message.setVisibility(View.GONE);
            }else {
                binding.Icon.setVisibility(View.VISIBLE);
                binding.Message.setVisibility(View.VISIBLE);
            }
        });
    }
}