package com.blood.bloodbook.UI;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;

import com.blood.bloodbook.R;
import com.blood.bloodbook.databinding.UserprofileBinding;

public class UserProfile extends AppCompatActivity {

    private UserprofileBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.userprofile);


    }
}