package com.blood.bloodbook.UI.Adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.blood.bloodbook.Data.OnBoardingModel;
import com.blood.bloodbook.UI.ViewHolder.OnBoardingViewHolder;
import com.blood.bloodbook.databinding.OnboardingitemBinding;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import lombok.Setter;
@Singleton
public class OnBoardingAdapter extends RecyclerView.Adapter<OnBoardingViewHolder>{

    @Inject
    public OnBoardingAdapter(){
    }
    @Setter
    private List<OnBoardingModel> onBoardingModelList;

    @NonNull
    @Override
    public OnBoardingViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        var layoutInflater = LayoutInflater.from(parent.getContext());
        var binding = OnboardingitemBinding.inflate(layoutInflater, parent, false);
        return new OnBoardingViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull OnBoardingViewHolder holder, int position) {
        holder.binding.OnBoardingAnimImage.setImageResource(onBoardingModelList.get(position).getImage());
        //  holder.binding.OnBoardingAnimImage.setImageResource(onBoardingModelList.get(position).getImage());
        holder.binding.OnBoardingTitle.setText(onBoardingModelList.get(position).getTitle());
        holder.binding.OnBoardingDetails.setText(onBoardingModelList.get(position).getDetails());
    }

    @Override
    public int getItemCount() {
        if(onBoardingModelList == null){
            return 0;
        }
        return onBoardingModelList.size();
    }
}
