package com.blood.bloodbook.UI.HomeFragment;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.blood.bloodbook.DonorModel;
import com.blood.bloodbook.Utils.Toast;
import com.blood.bloodbook.databinding.DonorexistsalertBinding;
import com.blood.bloodbook.databinding.RegistrationalertdialogBinding;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.blood.bloodbook.Application;
import com.blood.bloodbook.Data.Constant.ProfileData;
import com.blood.bloodbook.Data.DataManager;
import com.blood.bloodbook.Network.ViewModel.ViewModel;
import com.blood.bloodbook.R;
import com.blood.bloodbook.UI.Adapter.ProfileItemAdapter;
import com.blood.bloodbook.Utils.HandleActivity;
import com.blood.bloodbook.Widget.ProgressDialog;
import com.blood.bloodbook.databinding.LogoutdialogBinding;
import com.blood.bloodbook.databinding.OptoutblooddonationDialogBinding;
import com.blood.bloodbook.databinding.ProfileBinding;
import com.google.firebase.auth.FirebaseAuth;

import javax.inject.Inject;

public class Profile extends Fragment {

    private ProfileBinding binding;
    private ViewModel viewModel;
    @Inject
    ProfileItemAdapter profileItemAdapter;
    @Inject
    ProgressDialog progressDialog;

    public Profile() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.profile, container, false);
        viewModel = new ViewModelProvider(getActivity()).get(ViewModel.class);

        var component = Application.weenaComponent;
        component.InjecetProfile(this);
        InitView();
        SetProfile();
        SetProfileData();
        CountMessage();


        return binding.getRoot();
    }

    private void CountMessage(){
        viewModel.MessageSize().observe(getActivity(), integer -> {
            if(integer != null){
                binding.MessageBtn.setBadgeValue(integer);
            }
        });
    }


    private void SetProfileData() {
        binding.RecyclerView.setHasFixedSize(true);
        profileItemAdapter.setList(ProfileData.GetProfileData());
        binding.RecyclerView.setAdapter(profileItemAdapter);

        profileItemAdapter.OnClickLisiner(Title -> {
            if (Title.equals(DataManager.LogOut)) {
                OpenLogOutDialog();
            }
            if (Title.equals(DataManager.Blooddonationmonthlygap)) {
                progressDialog.ProgressDialog(getActivity());
                viewModel.IsDonorUserExists().observe(getActivity(), aBoolean -> {
                    if (!aBoolean) {
                        progressDialog.CancelProgressDialog();
                        DashBoardRegisterAlertDialog();
                    } else {
                        viewModel.GetUserByUserID().observe(getActivity(), new Observer<DonorModel>() {
                            @Override
                            public void onChanged(DonorModel donorModel) {
                                if(donorModel != null){
                                    if(donorModel.getLastDonationDate() == null){
                                        progressDialog.CancelProgressDialog();
                                        Toast.Message(getActivity(), "Last donation not found");
                                    }else {
                                        progressDialog.CancelProgressDialog();
                                        HandleActivity.GotoBloodDonationRestTime(getActivity());
                                        Animatoo.animateSlideLeft(getActivity());
                                    }
                                }else {
                                    progressDialog.CancelProgressDialog();
                                }
                            }
                        });

                    }
                });
            }
            if (Title.equals(DataManager.Registerdonationdate)) {
                HandleActivity.GotoRegisterDonationDate(getActivity());
                Animatoo.animateSlideLeft(getActivity());
                progressDialog.CancelProgressDialog();
            }
            if (Title.equals(DataManager.History)) {
                HandleActivity.GotoHistory(getActivity());
                Animatoo.animateSlideLeft(getActivity());
            }
            if (Title.equals(DataManager.ProfileSetting)) {
                HandleActivity.GotoEditProfile(getActivity());
                Animatoo.animateSlideLeft(getActivity());
            }
            if (Title.equals(DataManager.RegisterAsADonor)) {
                progressDialog.ProgressDialog(getActivity());
                viewModel.IsDonorUserExists().observe(getActivity(), aBoolean -> {
                    if (aBoolean) {
                        DonorExistsDialog();
                        progressDialog.CancelProgressDialog();
                    } else {
                        HandleActivity.GotoRegisterAsDonor(getActivity());
                        Animatoo.animateSlideLeft(getActivity());
                        progressDialog.CancelProgressDialog();
                    }
                });
            }
            if (Title.equals(DataManager.AddBlooddonorfriend)) {
                HandleActivity.GotoRegisterAsFriend(getActivity());
                Animatoo.animateSlideLeft(getActivity());
            }
            if (Title.equals(DataManager.Optoutofblooddonation)) {
                progressDialog.ProgressDialog(getActivity());
                viewModel.IsDonorUserExists().observe(getActivity(), aBoolean -> {
                    if (!aBoolean) {
                        progressDialog.CancelProgressDialog();
                        DashBoardRegisterAlertDialog();
                    } else {
                        progressDialog.CancelProgressDialog();
                        OpenDialog();
                    }
                });
            }
            if (Title.equals(DataManager.AboutUS)) {
                HandleActivity.GotoAboutUS(getActivity());
                Animatoo.animateSlideLeft(getActivity());
            }
            if (Title.equals(DataManager.UpdateLocation)) {
                HandleActivity.UpdateFindDonorLocation(getActivity());
                Animatoo.animateSlideLeft(getActivity());
            }

        });

        profileItemAdapter.OnClickFeelingSickSwitchState(IsEnable -> {
            progressDialog.ProgressDialog(getActivity());
            viewModel.IsDonorUserExists().observe(getActivity(), new Observer<Boolean>() {
                @Override
                public void onChanged(Boolean aBoolean) {
                    if (aBoolean) {
                        if (IsEnable) {
                            viewModel.UpdateDonorUserShowToUser(false).observe(getActivity(), b -> {
                                if (b) {
                                    progressDialog.CancelProgressDialog();
                                } else {
                                    progressDialog.CancelProgressDialog();
                                }
                            });
                        } else {
                            viewModel.UpdateDonorUserShowToUser(true).observe(getActivity(), b -> {
                                if (b) {
                                    progressDialog.CancelProgressDialog();
                                } else {
                                    progressDialog.CancelProgressDialog();
                                }
                            });
                        }
                    } else {
                        HandleActivity.GotoRegisterAsDonor(getActivity());
                        Animatoo.animateSlideLeft(getActivity());
                        progressDialog.CancelProgressDialog();
                    }
                }
            });


        });
    }

    private void DashBoardRegisterAlertDialog() {
        var alertdialog = new android.app.AlertDialog.Builder(getActivity());
        RegistrationalertdialogBinding d = DataBindingUtil.inflate(getLayoutInflater(), R.layout.registrationalertdialog, null, false);
        alertdialog.setView(d.getRoot());

        var al = alertdialog.create();
        al.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        d.ContinueBtn.setOnClickListener(view -> {
            al.dismiss();
            HandleActivity.GotoRegisterAsDonor(getActivity());
            Animatoo.animateSlideLeft(getActivity());
        });
        al.show();
    }

    private void DonorExistsDialog() {
        var alertdialog = new android.app.AlertDialog.Builder(getActivity());
        DonorexistsalertBinding bi = DataBindingUtil.inflate(getLayoutInflater(), R.layout.donorexistsalert, null, false);
        alertdialog.setView(bi.getRoot());

        var d = alertdialog.create();
        d.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        d.show();
        bi.ContactBtn.setOnClickListener(view -> {
            d.dismiss();
            try {
                var uri = Uri.parse("smsto:" + DataManager.ContactWhatsaAppNumber);
                var sendIntent = new Intent(Intent.ACTION_SENDTO, uri);
                sendIntent.setPackage("com.whatsapp");
                startActivity(sendIntent);
            } catch (Exception e) {
                e.printStackTrace();
                Toast.Message(getActivity(), getResources().getString(R.string.WhatsappNotInstall));
            }

        });
    }

    private void OpenDialog() {
        var dialog = new BottomSheetDialog(getActivity(), R.style.BottomSheetDialog);
        var v = OptoutblooddonationDialogBinding.inflate(getLayoutInflater(), null, false);
        dialog.setContentView(v.getRoot());
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();

        v.CancelButton.setOnClickListener(view -> {
            dialog.dismiss();
        });
        v.ContinueButton.setOnClickListener(view -> {
            dialog.dismiss();
            progressDialog.ProgressDialog(getActivity());
            viewModel.DeleteRegisterDonor().observe(getActivity(), aBoolean -> {
                if (aBoolean) {
                    progressDialog.CancelProgressDialog();
                } else {
                    progressDialog.CancelProgressDialog();
                }
            });

        });
    }

    private void OpenLogOutDialog() {
        var dialog = new AlertDialog.Builder(getActivity());
        var binding = LogoutdialogBinding.inflate(getLayoutInflater(), null, false);
        dialog.setView(binding.getRoot());

        var a = dialog.create();
        a.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        a.show();

        binding.CancelButton.setOnClickListener(view -> {
            a.dismiss();
        });
        binding.LogOutBtn.setOnClickListener(view -> {
            a.dismiss();
            viewModel.LogOutAccount().observe(getActivity(), aBoolean -> {
                if (aBoolean) {
                    HandleActivity.GotoSplashScreen(getActivity());
                    Animatoo.animateSlideLeft(getActivity());
                    getActivity().finish();
                } else {

                }
            });
        });
    }

    private void SetProfile() {
        viewModel.ProfileGET().observe(getActivity(), profileModel -> {
            if (profileModel != null) {
                binding.setProfile(profileModel);
            }
        });
    }

    private void InitView() {
        binding.NotificationBtn.setOnClickListener(view -> {
            HandleActivity.GotoNotification(getActivity());
            Animatoo.animateSlideLeft(getActivity());
        });

        binding.MessageBtn.setOnClickListener(view -> {
            HandleActivity.GotoChatNotification(getActivity());
            Animatoo.animateSlideLeft(getActivity());
        });
    }
}