package com.blood.bloodbook.UI;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;

import android.os.Bundle;
import android.view.View;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.blood.bloodbook.Application;
import com.blood.bloodbook.Network.ViewModel.LocationViewModel;
import com.blood.bloodbook.Network.ViewModel.ViewModel;
import com.blood.bloodbook.R;
import com.blood.bloodbook.UI.Adapter.DistrictNameDataAdapter;
import com.blood.bloodbook.UI.Adapter.LocationDataAdapter;
import com.blood.bloodbook.UI.Adapter.ProvinceNameDataAdapter;
import com.blood.bloodbook.UI.Adapter.ProvinceOfAfgAdapter;
import com.blood.bloodbook.Utils.HandleActivity;
import com.blood.bloodbook.Utils.SpacingItemDecorator;
import com.blood.bloodbook.Utils.Toast;
import com.blood.bloodbook.databinding.BloodfilterBinding;
import com.blood.bloodbook.databinding.LocationdialogBinding;

import javax.inject.Inject;

public class BloodFilter extends AppCompatActivity {

    private BloodfilterBinding binding;
    @Inject LocationDataAdapter locationDataAdapter;
    @Inject ProvinceNameDataAdapter provinceNameDataAdapter;
    @Inject DistrictNameDataAdapter districtNameDataAdapter;
    private LocationViewModel locationViewModel;
    private ViewModel viewModel;
    @Inject ProvinceOfAfgAdapter provinceOfAfgAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.bloodfilter);

        locationViewModel = new ViewModelProvider(this).get(LocationViewModel.class);
        viewModel = new ViewModelProvider(this).get(ViewModel.class);

        var component = Application.weenaComponent;
        component.InjectBloodFilter(this);

        InitView();
        GetProvince();
        GetData();
    }

    private void GetData(){
        locationViewModel.GetProvincesNameData().observe(this, provinceNameModelList -> {
            if(provinceNameModelList != null){
                provinceOfAfgAdapter.setList(provinceNameModelList);
                provinceOfAfgAdapter.notifyDataSetChanged();

                if(provinceNameModelList.size() > 0){
                    NoItemMessage();
                }else {
                    ItemMessage();
                }
            }else {
                ItemMessage();
            }
        });

        provinceOfAfgAdapter.OnClickState(Province -> {
            HandleActivity.GotoSelectBloodGroup(BloodFilter.this, Province);
            Animatoo.animateSlideLeft(BloodFilter.this);
        });
    }


    private void GetProvince(){
        viewModel.GetProvinceName(getResources().getString(R.string.Afghanistan)).observe(this, provinceNameModelList -> {
            if(provinceNameModelList != null){
                locationViewModel.InsertProvincesNameData(provinceNameModelList);
                NoItemMessage();
            }else {
                ItemMessage();
            }
        });
    }


    private void NoItemMessage(){
        binding.Message.setVisibility(View.GONE);
        binding.MessageText.setVisibility(View.GONE);
    }
    private void ItemMessage(){
        binding.Message.setVisibility(View.VISIBLE);
        binding.MessageText.setVisibility(View.VISIBLE);
    }

    private void InitView(){
        binding.RecyclerView.setHasFixedSize(true);
        binding.RecyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        binding.RecyclerView.setAdapter(provinceOfAfgAdapter);
        binding.RecyclerView.addItemDecoration(new SpacingItemDecorator(15,15,15,15));

        binding.Toolbar.Title.setText(getResources().getString(R.string.SelectProvince));
        binding.Toolbar.BackButton.setOnClickListener(view -> {
            finish();
            Animatoo.animateSlideRight(BloodFilter.this);
        });
    }


    @Override
    public void onBackPressed() {
        finish();
        Animatoo.animateSlideRight(BloodFilter.this);
    }
}