package com.blood.bloodbook;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity(tableName = "OrganizationTypeDB")
public class OrganizationTypeModel {
    @PrimaryKey(autoGenerate = false)
    private @NonNull
    String Category;
    private long DocumentKey, Timestamp;
}
