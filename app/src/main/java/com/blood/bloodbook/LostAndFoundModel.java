package com.blood.bloodbook;

import android.widget.ImageView;
import androidx.databinding.BindingAdapter;
import lombok.Data;

@Data
public class LostAndFoundModel {
    private String Country, District, Image, LostAndFound, PhoneNumber, Province,Title, FullDetails, UID;
    private long DocumentKey, Timestamp;

    @BindingAdapter("android:loadImage")
    public static void loadImage(ImageView imageView, String Uri){
        com.squareup.picasso.Picasso.get().load(Uri).placeholder(R.drawable.profileplaceholder).into(imageView);
    }
}
