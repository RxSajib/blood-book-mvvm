package com.blood.bloodbook;

import lombok.Data;

@Data
public class RegisterDonationModel {

    private String BloodDonationDate, Country, District, HospitalName, Province, QuantityOfBlood,RegisterDonationBloodImage;
    private String SenderUID;
    private long Timestamp, DocumentKey;
}
