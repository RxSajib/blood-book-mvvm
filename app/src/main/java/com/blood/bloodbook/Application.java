package com.blood.bloodbook;

import androidx.appcompat.app.AppCompatDelegate;

import com.blood.bloodbook.DI.DaggerWeenaComponent;
import com.blood.bloodbook.DI.WeenaComponent;
import com.google.android.gms.ads.MobileAds;
import com.blood.bloodbook.OpenAd.AppOpenManager;
import javax.inject.Singleton;

@Singleton
public class Application extends android.app.Application {

    public static AppOpenManager appOpenManager;
    public static WeenaComponent weenaComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        MobileAds.initialize(this, initializationStatus -> {
        });

        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        appOpenManager = new AppOpenManager(this);

        weenaComponent = DaggerWeenaComponent.create();
    }
}
