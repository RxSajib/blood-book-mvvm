package com.blood.bloodbook;

import com.google.gson.annotations.SerializedName;

public class Priority {
    @SerializedName("priority")
    String priority;

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }
}
