package com.blood.bloodbook.ViewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.blood.bloodbook.DonorModel;
import com.blood.bloodbook.Network.SearchBloodGET;
import com.blood.bloodbook.Network.SearchProvinceBloodGroup;

import java.util.List;

public class ViewModel extends AndroidViewModel {

    private SearchBloodGET searchBloodGET;
    private SearchProvinceBloodGroup searchProvinceBloodGroup;

    public ViewModel(@NonNull Application application) {
        super(application);

        searchBloodGET = new SearchBloodGET(application);
        searchProvinceBloodGroup = new SearchProvinceBloodGroup(application);
    }

    public LiveData<List<DonorModel>> SearchProvinceBlood(String BloodName, String Country, String Province){
        return searchProvinceBloodGroup.SearchProvinceBlood(BloodName, Country, Province);
    }

    public LiveData<List<DonorModel>> SearchBloodGroup(String BloodName, String Country, String Province, String District, int Limit){
        return searchBloodGET.SearchBloodDonor(BloodName, Country, Province, District, Limit);
    }

}
