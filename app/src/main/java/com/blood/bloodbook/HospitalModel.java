package com.blood.bloodbook;

import android.widget.ImageView;
import androidx.databinding.BindingAdapter;
import java.io.Serializable;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class HospitalModel implements Serializable {
    private long DocumentKey, Timestamp;
    private String Email, HospitalLogo, HospitalName, Location,UID;
    @BindingAdapter("android:loadImage")
    public static void loadImage(ImageView imageView, String Uri){
        com.squareup.picasso.Picasso.get().load(Uri).placeholder(R.drawable.profileplaceholder).into(imageView);
    }
}
