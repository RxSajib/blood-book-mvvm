package com.blood.bloodbook;

import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class BloodModel {
    private @NonNull String BloodGroup;
    private @NonNull String Province;
    private @NonNull String BloodGroupSendName;
}
