package com.blood.bloodbook;

import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class BloodDonationRestTimeModel {

    private @NonNull String Day;
}
