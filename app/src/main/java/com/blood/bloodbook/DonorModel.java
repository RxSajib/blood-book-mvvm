package com.blood.bloodbook;

import java.io.Serializable;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class DonorModel implements Serializable {

    private String GivenName, Surname, FatherName, BloodGroup, Age, Width, Country;
    private String Province, District, AreaName, MainPhoneNumber, RoshanNumber, EtisalatNumber, AWCCNumber;
    private String MTNNumber, WhatsappNumber, EmailAddress, DonateMonth, LastDonationDate, SenderUID;
    private String EnableMyNumber;
    private String LastDonationDayCount;
    private Boolean ShowToOther;


}
