package com.blood.bloodbook;

import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class ContactModel {
    private  @NonNull String Provider;
    private  @NonNull String Number;
}
