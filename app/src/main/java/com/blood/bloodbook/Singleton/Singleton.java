package com.blood.bloodbook.Singleton;

import androidx.lifecycle.ViewModelProvider;

import com.blood.bloodbook.Network.ViewModel.ViewModel;
import com.blood.bloodbook.UI.Adapter.ProfileItemAdapter;
import com.blood.bloodbook.UI.Notification;


public class Singleton {

    private static ViewModel viewModel;
    public static ProfileItemAdapter profileItemAdapter;

    public static ProfileItemAdapter getProfileItemAdapter(){
        if(profileItemAdapter == null){
            profileItemAdapter = new ProfileItemAdapter();
        }
        return profileItemAdapter;
    }


    public static ViewModel getNotification(Notification notification){
        if(viewModel == null){
            viewModel = new ViewModelProvider(notification).get(ViewModel.class);
        }
        return viewModel;
    }


}
