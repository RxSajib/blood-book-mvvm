package com.blood.bloodbook;

import lombok.Data;

@Data
public class MessageHistoryModel {

    private String Message;
    private String ReceiverUID;
    private String SenderUID;
    private String Type;
    private long Timestamp;
}
