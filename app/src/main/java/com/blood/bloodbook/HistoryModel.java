package com.blood.bloodbook;

import android.widget.ImageView;

import androidx.databinding.BindingAdapter;

import lombok.Data;

@Data
public class HistoryModel {

    private String BloodDonationDate;
    private String QuantityOfBlood;
    private String Country;
    private String Province;
    private String District;
    private String HospitalName;
    private String RegisterDonationBloodImage;
    private String SenderUID;
    private long DocumentKey;
    private long Timestamp;

    @BindingAdapter("android:loadImage")
    public static void loadImage(ImageView imageView, String Uri){
        com.squareup.picasso.Picasso.get().load(Uri).placeholder(R.drawable.profileplaceholder).into(imageView);
    }
}
