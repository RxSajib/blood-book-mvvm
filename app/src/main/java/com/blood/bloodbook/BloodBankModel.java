package com.blood.bloodbook;

import lombok.Data;

@Data
public class BloodBankModel {
    private String BloodCategory, BloodGroup, DonationDate, DonorName, DonorPhoneNumber, ExpiryDate, HospitalLogo, HospitalName;
    private String Location, Quantity, Status, TagNumber, UID;
    private long Timestamp, DocumentID;
}
