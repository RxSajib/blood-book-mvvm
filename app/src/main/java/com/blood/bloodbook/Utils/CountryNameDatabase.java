package com.blood.bloodbook.Utils;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.blood.bloodbook.Data.CountryNameModel;
import com.blood.bloodbook.Data.DistrictNameModel;
import com.blood.bloodbook.Data.ProvinceNameModel;
import com.blood.bloodbook.LostAndFoundTypeModel;
import com.blood.bloodbook.OrganizationTypeModel;

@Database(entities = {CountryNameModel.class, ProvinceNameModel.class, DistrictNameModel.class, OrganizationTypeModel.class, LostAndFoundTypeModel.class}, version = 6)
public abstract class CountryNameDatabase extends RoomDatabase {

    public abstract LocationDao countryDao();

    public static volatile CountryNameDatabase getInstance;

    public static CountryNameDatabase GetContactDatabase(Context context) {
        if (getInstance == null) {
            synchronized (CountryNameDatabase.class) {
                if (getInstance == null) {
                    getInstance = Room.databaseBuilder(context, CountryNameDatabase.class, "contact")
                            .fallbackToDestructiveMigration()
                            .build();
                }
            }
        }
        return getInstance;
    }
}
