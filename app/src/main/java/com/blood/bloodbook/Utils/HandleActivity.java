package com.blood.bloodbook.Utils;

import android.content.Context;
import android.content.Intent;

import com.blood.bloodbook.BloodRequestModel;
import com.blood.bloodbook.Data.DataManager;
import com.blood.bloodbook.DonorModel;
import com.blood.bloodbook.Home;
import com.blood.bloodbook.HospitalModel;
import com.blood.bloodbook.UI.AboutUS;
import com.blood.bloodbook.UI.Account;
import com.blood.bloodbook.UI.AccountRestriction;
import com.blood.bloodbook.UI.AddLostAndFound;
import com.blood.bloodbook.UI.AddRequest;
import com.blood.bloodbook.UI.AfgListOFBlood;
import com.blood.bloodbook.UI.BloodBank;
import com.blood.bloodbook.UI.BloodDonationRestTime;
import com.blood.bloodbook.UI.BloodFilter;
import com.blood.bloodbook.UI.BloodSearchResult;
import com.blood.bloodbook.UI.Chat;
import com.blood.bloodbook.UI.DetailsOFBloodRequest;
import com.blood.bloodbook.UI.DonorUserDetails;
import com.blood.bloodbook.UI.EditProfile;
import com.blood.bloodbook.UI.EmailSignIn;
import com.blood.bloodbook.UI.EmailSignUp;
import com.blood.bloodbook.UI.ForgotPassword;
import com.blood.bloodbook.UI.HistoryHome;
import com.blood.bloodbook.UI.ListOFBloodBank;
import com.blood.bloodbook.UI.LoginPhone;
import com.blood.bloodbook.UI.LostAndFound;
import com.blood.bloodbook.UI.MessageHistory;
import com.blood.bloodbook.UI.Notification;
import com.blood.bloodbook.UI.OTP;
import com.blood.bloodbook.UI.OnBoardingScreen;
import com.blood.bloodbook.UI.RegisterAsADonor;
import com.blood.bloodbook.UI.RegisterAsFriend;
import com.blood.bloodbook.UI.RegisterDonationDate;
import com.blood.bloodbook.UI.RequestOFBloodHome;
import com.blood.bloodbook.UI.SelectBloodGroup;
import com.blood.bloodbook.UI.SetUpProfile;
import com.blood.bloodbook.UI.SplashScreen;
import com.blood.bloodbook.UI.TelephonyManager;
import com.blood.bloodbook.UI.TransactionPayment;
import com.blood.bloodbook.UI.UpdateFindDonorLocation;

public class HandleActivity {

    public static void GotoSignIn(Context context) {
        var intent = new Intent(context, Account.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }

    public static void GotoChatNotification(Context context){
        var intent = new Intent(context, MessageHistory.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }

    public static void GotoOnBoardingScreen(Context context){
        var intent = new Intent(context, OnBoardingScreen.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }

    public static void GotoAddLostAndFound(Context context){
        var intent = new Intent(context, AddLostAndFound.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }

    public static void GotoRequest(Context context){
        var intent = new Intent(context, RequestOFBloodHome.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }

    public static void UpdateFindDonorLocation(Context context){
        var intent = new Intent(context, UpdateFindDonorLocation.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }

     public static void GotoRestrictionAccount(Context context){
        var intent = new Intent(context, AccountRestriction.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }



    public static void GotoTelephonyManager(Context context){
        var intent = new Intent(context, TelephonyManager.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }

    public static void GotoSetUpProfile(Context context){
        var intent = new Intent(context, SetUpProfile.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }

    public static void GotoBloodRequestDetails(Context context, BloodRequestModel model) {
        var intent = new Intent(context, DetailsOFBloodRequest.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra(DataManager.Data, model);
        context.startActivity(intent);
    }

    public static void GotoSplashScreen(Context context){
        var intent = new Intent(context, SplashScreen.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }

    public static void GotoAfgBloodList(Context context, String Province, String BloodGroup){
        var intent = new Intent(context, AfgListOFBlood.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra(DataManager.Province, Province);
        intent.putExtra(DataManager.BloodGroup, BloodGroup);
        context.startActivity(intent);
    }

    public static void GotoRegisterAsFriend(Context context) {
        var intent = new Intent(context, RegisterAsFriend.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }

    public static void GotoAboutUS(Context context){
        var intent = new Intent(context, AboutUS.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }

    public static void GotoChat(Context context, String SenderUID, String GivenName, String SureName){
        var intent = new Intent(context, Chat.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra(DataManager.SenderUID, SenderUID);
        intent.putExtra(DataManager.GivenName, GivenName);
        intent.putExtra(DataManager.Surname, SureName);
        context.startActivity(intent);
    }

    public static void GotoHistory(Context context){
        var intent = new Intent(context, HistoryHome.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }

    public static void GotoNotification(Context context){
        var intent = new Intent(context, Notification.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }

    public static void GotoFindDonorsResult(Context context,String BloodGroup, String CountryName, String ProvinceName, String District) {
        var intent = new Intent(context, BloodSearchResult.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra(DataManager.BloodGroup, BloodGroup);
        intent.putExtra(DataManager.CountryName, CountryName);
        intent.putExtra(DataManager.Province, ProvinceName);
        intent.putExtra(DataManager.District, District);
        context.startActivity(intent);
    }

    public static void GotoRegisterAsDonor(Context context) {
        var intent = new Intent(context, RegisterAsADonor.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }

    public static void GotoLostAndFound(Context context){
        var intent = new Intent(context, LostAndFound.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }

    public static void GotoBloodDonationRestTime(Context context){
        var intent = new Intent(context, BloodDonationRestTime.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }

    public static void GotoRegisterDonationDate(Context context){
        var intent = new Intent(context, RegisterDonationDate.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }

    public static void GotoPhoneLogin(Context context) {
        var intent = new Intent(context, LoginPhone.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }

    public static void GotoAddRequest(Context context) {
        var intent = new Intent(context, AddRequest.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }

    public static void GotoAccount(Context context) {
        var intent = new Intent(context, Account.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }

    public static void GotoEditProfile(Context context) {
        var intent = new Intent(context, EditProfile.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }

    public static void GotoOTP(Context context, String Number, String Code) {
        var intent = new Intent(context, OTP.class);
        intent.putExtra(DataManager.PhoneNumber, Number);
        intent.putExtra(DataManager.Code, Code);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }

    public static void GotoEmailSIgnIn(Context context) {
        var intent = new Intent(context, EmailSignIn.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }

    public static void GotoEmailSIgnUp(Context context) {
        var intent = new Intent(context, EmailSignUp.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }

    public static void GotoResetPassword(Context context){
        var intent = new Intent(context, ForgotPassword.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }

    public static void GotoHome(Context context) {
        var intent = new Intent(context, Home.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }

    public static void GotoBloodResult(Context context, DonorModel donorModel){
        var intent = new Intent(context, DonorUserDetails.class);
        intent.putExtra(DataManager.Data, donorModel);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        context.startActivity(intent);
    }

    public static void GotoBloodFilter(Context context){
        var intent = new Intent(context, BloodFilter.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        context.startActivity(intent);
    }

    public static void GotoSelectBloodGroup(Context context, String Province){
        var intent = new Intent(context, SelectBloodGroup.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(DataManager.Data, Province);
        context.startActivity(intent);
    }

    public static void GotoTransactionMoney(Context context){
        var intent = new Intent(context, TransactionPayment.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        context.startActivity(intent);
    }

    public static void GotoBloodBank(Context context){
        var intent = new Intent(context, BloodBank.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        context.startActivity(intent);
    }
    public static void GotoListOFBloodBank(Context context, HospitalModel hospitalModel){
        var intent = new Intent(context, ListOFBloodBank.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra(DataManager.Data, hospitalModel);
        context.startActivity(intent);
    }
}
