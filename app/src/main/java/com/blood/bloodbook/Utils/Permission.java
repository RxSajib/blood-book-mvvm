package com.blood.bloodbook.Utils;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

public class Permission {

    public static boolean PermissionCall(Activity context, int PermissionCode){
        if(ContextCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED){
            return true;
        }else {
            ActivityCompat.requestPermissions(context, new String[]{Manifest.permission.CALL_PHONE}, PermissionCode);
            return false;
        }
    }

    public static boolean PermissionSendMessage(Activity activity, int PermissionCode){
        if(ContextCompat.checkSelfPermission(activity, Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_GRANTED){
            return true;
        }else {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.SEND_SMS}, PermissionCode) ;
            return false;
        }
    }

    public static boolean PermissionExternalStorage(Activity context, int Permission){
        if(ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED){
            return true;
        }else {
            ActivityCompat.requestPermissions(context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, Permission);
            return false;
        }
    }

    public static boolean PermissionCamera(Activity activity, int Permission){
        if(ContextCompat.checkSelfPermission(activity, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED){
            return true;
        }else {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.CAMERA}, Permission);
            return false;
        }
    }
}
