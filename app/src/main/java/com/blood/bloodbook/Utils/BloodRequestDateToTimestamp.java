package com.blood.bloodbook.Utils;

import com.blood.bloodbook.Data.DataManager;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class BloodRequestDateToTimestamp {

    public static Timestamp GetBloodRequestDateToTimestamp(String Date, String Time){
        String inputDateInString= Date+" "+Time;
        SimpleDateFormat dateFormat = new SimpleDateFormat(DataManager.DatePattern+" "+DataManager.TimetoAM_PMpattern);

        Date parsedDate = null;
        try {
            parsedDate = dateFormat.parse(inputDateInString);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        var timestamp = new java.sql.Timestamp(parsedDate.getTime());
        return timestamp;
    }
}
