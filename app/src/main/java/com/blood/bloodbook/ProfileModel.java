package com.blood.bloodbook;

import android.widget.ImageView;

import androidx.databinding.BindingAdapter;

import lombok.Data;

@Data
public class ProfileModel {

    private String Email, FatherName, FirstName, MiddleName, ProfileImage, Surname, Token, LoginWith;
    private boolean IsActive, IsRequestEnable;
    @BindingAdapter("android:loadImage")
    public static void loadImage(ImageView imageView, String Uri){
        com.squareup.picasso.Picasso.get().load(Uri).placeholder(R.drawable.profileplaceholder).into(imageView);
    }
}
