package com.blood.bloodbook.Widget;

import android.app.Dialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import com.blood.bloodbook.R;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class ChatOptionDialog extends DialogFragment {

    @Inject
    public ChatOptionDialog(){
    }

    private Onclick Onclick;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        var alertdialog = new AlertDialog.Builder(getActivity());
        alertdialog.setTitle("Select Attachment Option");

        var item= getActivity().getResources().getStringArray(R.array.ChatOption);
        alertdialog.setItems(item, (dialogInterface, i) -> {
            Onclick.Click(i);
        });
        return alertdialog.create();
    }

    public interface Onclick{
        void Click(int Item);
    }

    public void OnclickLisiner(Onclick Onclick){
        this.Onclick = Onclick;
    }


}
