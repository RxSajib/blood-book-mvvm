package com.blood.bloodbook;

import lombok.Data;

@Data
public class MessageModel {
    private String Message, SenderUID, ReceiverUID, Type, FileName, FileExtension, FileSize;
    private long DocumentKey, Timestamp;
}
