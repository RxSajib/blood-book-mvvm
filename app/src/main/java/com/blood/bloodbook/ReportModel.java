package com.blood.bloodbook;

import lombok.Data;

@Data
public class ReportModel {

    private long DocumentKey, Timestamp;
    private String Message, ReceiverUID, SenderUID;
}
